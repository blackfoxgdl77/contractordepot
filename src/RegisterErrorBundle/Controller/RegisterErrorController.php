<?php

namespace RegisterErrorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Exception\Exception;

/**
 * Class will contain all the information
 * that is going to be used for displaying the
 * errors in a more appropiated way and custom
 * the error. Inside the class we could identify
 * the error faster and fixing as soon as possible
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package SiteBundle\Libs\PrincipalController
 */
class RegisterErrorController extends Controller {

    private $fileSystem      = NULL;
    private $nameLogFile     = "error.txt";

    /**
     * Constructor
     */
    public function __construct() {
        $this->fileSystem = new Filesystem();
    }

    /**
     * Method is going to work as entry point of the
     * system to know that the file has been created and
     * also write in the file the error
     *
     * @param array  $arrayParameters
     * @return void
     */
    public function accessPoint($arrayParameters = array()) {
        $this->__createFileLog();
        $this->writeLogFile($arrayParameters);
    }

    /**
     * Method will create the file is going to be
     * used to create the error log and display
     * information to know what is the error thrown
     * by the system
     *
     * @return void
     */
    protected function __createFileLog() {
        try {
            if (!$this->fileSystem->exists($this->nameLogFile)) {
                $this->fileSystem->touch($this->nameLogFile);
                $this->__setHeaderLogFile();
            }
        } catch(\Exception $e) {
            print($e);
        }
    }

    /**
     * Set the header with the structure of the string will contain
     * the file once some actions has thrown an exception
     *
     * @return void
     */
    private function __setHeaderLogFile() {
        $headerString = "***** Date - Module - Function - Action - CustomMessage - ErrorMessage ***** \n";
        $this->fileSystem->appendToFile($this->nameLogFile, $headerString);
    }

    /**
     * Method will use the filesystem to create the
     * record inside the filesystem
     *
     * @param array $arrayData
     * @return void
     */
    protected function writeLogFile($arrayData) {
        try {
            // Date - Module - Function - Action - CustomMessage - Message
            extract($arrayData);
            $newString = $date . ' - ' . $module . ' - ' . $function . ' - ' . $action  . ' - ' . $customMessage . ' - ' . $message . "\n";
            $this->fileSystem->appendToFile($this->nameLogFile, $newString);
        } catch(\Exception $e) {
            print_r($e->getMessage());
        }
    }
}
