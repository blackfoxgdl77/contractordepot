<?php

namespace APIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use APIBundle\APILibs\APILib;
use APIBundle\APILibs\RestResponse;
use APIBundle\Entity\Cities;
use RegisterErrorBundle\Controller\RegisterErrorController;

/**
 * API Controller will be used to get information of all the cities.
 *
 * @author Fernando Molina Mejia <fernando.molina.mejia@gmail.com>
 * @package APIBundle\Controller\APICitiesController
 */
class APICitiesController extends APILib {

    /**
     * Variable to contain the name of the repo and entity
     *
     * @var string
     */
    protected $repositoryName = "";

    /**
     * Variable to contain a reference of the repo
     *
	 * @var object | NULL
     */
    protected $repository = NULL;

    /**
     * Array will contain the default values
     *
     * @var array
     */
    private $default = array(
        'id'    => 0,
        'value' => 'Select a City',
    );

    /**
     * Constructor
     */
    public function __construct() {
        $this->repositoryName = "Cities";
    }

    /**
     *
     */
    public function searchAction() {

        $data = NULL;
        $dataArr = array();

        try {

            $data = $this->loadRepo($this->bundleName, $this->repositoryName)->findAll();

            foreach ($data as $city) {
                $dataArr[] = array(
                    'id'    => $city->getId(),
                    'value' => $city->getNameCity(),
                );
            }

            array_unshift($dataArr, $this->default);

            return $this->generateResponse($dataArr, RestResponse::RESP_OK);

        } catch (\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APICitiesController',
                'function'      => 'search',
                'action'        => 'searchCities',
                'customMessage' => 'Error: can not get the cities',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'error_message'  => 'Error retrieving Cities.',
                'error'          => $e->getMessage(),
                'data'           => $data
            ), RestResponse::RESP_ERROR);
        }

    }

}
