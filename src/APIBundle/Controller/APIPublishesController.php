<?php

namespace APIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Config\Definition\Exception\Exception;
use RegisterErrorBundle\Controller\RegisterErrorController;
use APIBundle\APILibs\APILib;
use APIBundle\APILibs\RestResponse;
use APIBundle\Entity\Users;
use APIBundle\Entity\Publishes;
use APIBundle\Entity\Posts;

/**
 * Class will contain all the methods and information
 * that are related to the main publish on the posts
 * and where after that will be displayed and linked
 * a group of comments
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Controller\APIPublishesContoller
 */
class APIPublishesController extends APILib {
    /**
     * Variable will contain the publishes entity
     *
     * @var object | NULL
     */
    protected $publishes = NULL;

    /**
     * Variable will store the users entity
     *
     * @var object | NULL
     */
    protected $users = NULL;

    /**
     * Variable will store the posts entity
     *
     * @var object | NULL
     */
    protected $posts = NULL;

    /**
     * Variable will contain the name of the publishes repo
     *
     * @var string
     */
    protected $publishesRepo = "";

    /**
     * Variable will contain the name of the posts repo
     *
     * @var string
     */
    protected $postsRepo = "";

    /**
     * Variable will contain the name of the users repo
     *
     * @var string
     */
    protected $usersRepo = "";

    /**
     * Constructor and Initializer
     */
    public function __construct() {
        $this->publishes     = new Publishes();
        $this->users         = new Users();
        $this->posts         = new Posts();
        $this->publishesRepo = "Publishes";
        $this->postsRepo     = "Posts";
        $this->usersRepo     = "Users";
    }

    /**
     * Method will be used to store the comments will be
     * added to specific post once the user has some doubts
     * about what should be do in the job
     *
     * @return json $response
     */
    public function savePublishesAction() {

        $emailRegexp1 = '/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i';
        $emailRegexp2 = '/\A[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,4}\z/';
        $emailRegexp3 = '/^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/';
        $telRegExp1 = '^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$';
        $telRegExp2 = '/\\D\*\(\[2\-9\]\\d\{2\}\)\(\\D\*\)\(\[2\-9\]\\d\{2\}\)\(\\D\*\)\(\\d\{4\}\)\\D\*/';
        $telRegExp = '^[+]?[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$';

        try {
            $publishData = json_decode(file_get_contents('php://input'), true);

            $userID   = $publishData['usersId'];
            $postID   = $publishData['postsId'];
            $comments = $publishData['comments'];
            $title    = $publishData['title'];
            $source   = $publishData['source'];
            $token    = $publishData['token'];
            $nameT    = $publishData['nameT'];
            $em       = $this->getDoctrine()->getManager();

            // Check if token is valid
            $validToken = $this->isCsrfTokenValid($nameT, $token);

            if ($validToken == 0) {
                // Put the text to write in the file log
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIPublishesContoller',
                    'function'      => 'savePublishes',
                    'action'        => 'saveNewPublishesToken',
                    'customMessage' => 'Error: can not use token, its invalid',
                    'message'       => 'CSRF Token is invalid',
                );
                $this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'Application Error!, Please contact support or try again!',
                    'id'      => '0'
                ), RestResponse::RESP_ERROR);
            }

            // Get foreign information
            $this->users = $this->loadRepo($this->bundleName, $this->usersRepo)->find($userID);
            $this->posts = $this->loadRepo($this->bundleName, $this->postsRepo)->find($postID);
            $ownerObject = $this->loadRepo($this->bundleName, $this->usersRepo)->find($this->posts->getUser()->getId());

            // Remove e-mail
            $commentsArr = explode(" ", $comments);
            $commentsRes = array();

            // Validate with filter_var
            foreach ($commentsArr as $word) {
                if (!filter_var($word, FILTER_VALIDATE_EMAIL) &&
                    !preg_match($emailRegexp1, $word) &&
                    !preg_match($emailRegexp2, $word) &&
                    !preg_match($emailRegexp3, $word) &&
                    !preg_match($telRegExp1, $word) &&
                    !preg_match($telRegExp2, $word) &&
                    !preg_match($telRegExp3, $word)) {
                    array_push($commentsRes, $word);
                }
            }

            $comments = implode(" ", $commentsRes);

            // Save the records
            $this->publishes->setUserPublishesId($this->users);
            $this->publishes->setPostId($this->posts);
            $this->publishes->setOwnerPostsId($ownerObject);
            $this->publishes->setComments($comments);
            $this->publishes->setTitleComment($title);

            $em->persist($this->publishes);
            $em->flush();

            // Add email template
            $array = array(
                'userEmail'   => $ownerObject->getEmail(),
                'nameuser'    => $ownerObject->getName(),
                'username'    => $ownerObject->getUsername(),
                'userName2'   => $this->users->getUsername(),
                'project'     => $this->posts->getPostName(),
                'comments'    => $comments,
                'title'       => $title,
                'subject'     => 'Add Comments',
                'supportTeam' => $this->emailSupport,

            );

            $responseEmail = $this->__sendEmail('publish', $array);

            if ($responseEmail == 0){
                // Put the text to write in the file log
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIPublishesContoller',
                    'function'      => 'savePublishes',
                    'action'        => 'savePublishesMail',
                    'customMessage' => 'Error: send contact mail',
                    'message'       => 'Can not send the mail to users',
                );
                $this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'Error with the sending email. Please contact support using contact form for receiving the email and activate the account',
                    'id'      => 0,
                ), RestResponse::RESP_ERROR);
            }

            return $this->generateResponse(array(
                'message' => 'The comments has been published',
                'id'      => $this->publishes->getId()
            ), RestResponse::RESP_OK);
        } catch (\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIPublishesContoller',
                'function'      => 'savePublishes',
                'action'        => 'saveNewPublishes',
                'customMessage' => 'Error: can not save the new publishes',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'Application Error!',
                'id'      => 0,
                'eroor'   => $e->getMessage()
            ), RestResponse::RESP_ERROR);
        }
    }
}
