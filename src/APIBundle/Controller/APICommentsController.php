<?php

namespace APIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Config\Definition\Exception\Exception;
use RegisterErrorBundle\Controller\RegisterErrorController;
use APIBundle\APILibs\APILib;
use APIBundle\APILibs\RestResponse;
use APIBundle\Entity\Users;
use APIBundle\Entity\Publishes;
use APIBundle\Entity\Comments;
use APIBundle\Entity\Posts;

/**
 * API that is related to the comments has been done
 * once the main publish has been created. This API will handle all the
 * requests of the child comments created by the user and the owner of
 * the post
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Controller\APICommentsController
 */
class APICommentsController extends APILib {
    /**
     * Variable will store the comments entity
     *
     * @var object | NULL
     */
    protected $comments = NULL;

    /**
     * Variable will store the users entity
     *
     * @var object | NULL
     */
    protected $users = NULL;

    /**
     * Variable will store the posts entity
     *
     * @var object | NULL
     */
    protected $posts = NULL;

    /**
     * Variable will contain the publishes entity
     *
     * @var object | NULL
     */
    protected $publishes = NULL;

    /**
     * Variable will contain the repo name of the commentes
     *
     * @var string
     */
    protected $commentsRepo = "";

    /**
     * Variable will contain the repo name of the users
     *
     * @var string
     */
    protected $usersRepo = "";

    /**
     * Variable will contain the repo name of the publishes
     *
     * @var $publishesRepo;
     */
    protected $publishRepo = "";

    /**
     * Variable will contain the repo name of the posts
     *
     * @var string
     */
    protected $postsRepo = "";

    /**
     * Constructor and Initializer
     */
    public function __construct() {
        $this->comments     = new Comments();
        $this->users        = new Users();
        $this->posts        = new Posts();
        $this->publishes    = new Publishes();
        $this->commentsRepo = "Comments";
        $this->usersRepo    = "Users";
        $this->publishRepo  = "Publishes";
        $this->postsRepo    = "Posts";
    }

    /**
     * Method will be used to get all the information
     * of all the commentes related to the publishes
     * that will be added in every post
     *
     * @return response $json
     */
    public function saveCommentsAction() {

        $emailRegexp1 = '/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i';
        $emailRegexp2 = '/\A[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,4}\z/';
        $emailRegexp3 = '/^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/';
        $telRegExp1 = '^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$';
        $telRegExp2 = '/\\D\*\(\[2\-9\]\\d\{2\}\)\(\\D\*\)\(\[2\-9\]\\d\{2\}\)\(\\D\*\)\(\\d\{4\}\)\\D\*/';
        $telRegExp = '^[+]?[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$';


        try {
            $commentsData = json_decode(file_get_contents('php://input'), true);

            $usersID   = $commentsData['usersId'];
            $postsID   = $commentsData['postsId'];
            $comments  = $commentsData['comments'];
            $publishID = $commentsData['publishId'];
            $source    = $commentsData['source'];
            $token     = $commentsData['token'];
            $nameT     = $commentsData['nameT'];
            $em        = $this->getDoctrine()->getManager();

            // Check if token is valid
            $validToken = $this->isCsrfTokenValid($nameT, $token);

            if ($validToken == 0) {
                // Put the text to write in the file log
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIComments',
                    'function'      => 'saveComments',
                    'action'        => 'saveCommentsTokenAction',
                    'customMessage' => 'Error: csrf token invalid',
                    'message'       => 'CSRF Token is invalid',
                );
                $this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'Application Error!, Please contact support or try again!',
                    'id'      => '0'
                ), RestResponse::RESP_ERROR);
            }

            // Get all the relations
            $this->users     = $this->loadRepo($this->bundleName, $this->usersRepo)->find($usersID);
            $this->posts     = $this->loadRepo($this->bundleName, $this->postsRepo)->find($postsID);
            $this->publishes = $this->loadRepo($this->bundleName, $this->publishRepo)->find($publishID);
            $ownerObject     = $this->loadRepo($this->bundleName, $this->usersRepo)->find($this->posts->getUser()->getId());

            // Remove e-mail
            $commentsArr = explode(" ", $comments);
            $commentsRes = array();

            // Validate with filter_var
            foreach ($commentsArr as $word) {
                if (!filter_var($word, FILTER_VALIDATE_EMAIL) &&
                    !preg_match($emailRegexp1, $word) &&
                    !preg_match($emailRegexp2, $word) &&
                    !preg_match($emailRegexp3, $word) &&
                    !preg_match($telRegExp1, $word) &&
                    !preg_match($telRegExp2, $word) &&
                    !preg_match($telRegExp3, $word)) {
                    array_push($commentsRes, $word);
                }
            }

            $comments = implode(" ", $commentsRes);

            // Save the information
            $this->comments->setComments($comments);
            $this->comments->setPublishesId($this->publishes);
            $this->comments->setOwnerPublishId($ownerObject);
            $this->comments->setCommentedUser($this->users);
            $this->comments->setPostsCommented($this->posts);

            $em->persist($this->comments);
            $em->flush();

            // Get all the users has comment the main comment
            $dataInfo  = $this->loadRepo($this->bundleName, $this->commentsRepo)->getEmailsFromUsers($publishID, $usersID);

            foreach ($dataInfo as $key => $value) {
                $array = array(
                    'subject'      => 'Reply Comment',
                    'userEmail'    => $value['email'],
                    'nameuser'     => $value['name'],
                    'comments'     => $comments,
                    'userPosted'   => $this->users->getUsername(),
                    'project'      => $this->posts->getPostName(),
                    'titleComment' => $value['title'],
                    'supportTeam'  => $this->emailSupport,
                );

                $responseEmail = $this->__sendEmail('comments', $array);

                if ($responseEmail == 0) {
                    // Put the text to write in the file log
                    $logMessage = array(
                        'date'          => Date('Y-m-d H:i:s'),
                        'module'        => 'APIComments',
                        'function'      => 'saveComments',
                        'action'        => 'saveCommentsEmailAction',
                        'customMessage' => 'Error: send contact mail',
                        'message'       => 'Was not able to send email to users',
                    );
                    $this->logFileData->accessPoint($logMessage);

                    return $this->generateResponse(array(
                        'message' => 'Error with the sending email. Please contact support using contact form for receiving the email and activate the account',
                        'id'      => 0,
                    ), RestResponse::RESP_ERROR);
                }
            }

            return $this->generateResponse(array(
                'message' => 'The comments has been published successfully.',
                'id'      => $this->comments->getId()
            ), RestResponse::RESP_OK);
        }  catch (\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIComments',
                'function'      => 'saveComments',
                'action'        => 'saveCommentsError',
                'customMessage' => 'Error: can not save comments done by users',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => "Application Error! There was an error at the moment to publish a comment",
                'id'      => 0,
                'error'   => $e->getMessage(),
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Method will return all the information of the comments
     * done in the post, and also will return all the subcomments
     * done for reply different questions
     *
     * @param integer $id
     * @return response $json
     */
    public function getCommentSubcommentAction($id) {
        try {
            if ($id == 0 || is_null($id) || $id == '') {
                // Put the text to write in the file log
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APICommentsController',
                    'function'      => 'getCommentSubcomment',
                    'action'        => 'getAllCommentSubcommentInformation',
                    'customMessage' => 'Error: can not get inner comments information',
                    'message'       => 'Can not get inner comments from every comment created in the project',
                );
                $this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'Application Error! Please try again later or contact support team',
                    'id'      => 0,
                ), RestResponse::RESP_ERROR);
            }

            $arrayPublish = array();
            $comments = $this->loadRepo($this->bundleName, $this->publishRepo)->findBy(array('postId' => $id));

            foreach ($comments as $key => $value) {
                $arrayComment  = array();
                $innerComments = $this->loadRepo($this->bundleName, $this->commentsRepo)->findBy(array('publishId' => $value->getId()));

                foreach ($innerComments as $innerKey => $innerValue) {
                    $arrayComment[] = array(
                        'id_comment'       => $innerValue->getId(),
                        'username_comment' => $innerValue->getCommentedUser()->getUsername(),
                        'comments'         => $innerValue->getComments(),
                        'date_created'     => $this->__formatDate($innerValue->getDateCreated()->format('Y-m-d')),
                    );
                }

                $arrayPublish[] = array(
                    'id_publish'       => $value->getId(),
                    'publish_username' => $value->getUserPublishesId()->getUsername(),
                    'comments'         => $value->getComments(),
                    'title'            => $value->getTitleComment(),
                    'date_created'     => $this->__formatDate($value->getDateCreated()->format('Y-m-d')),
                    'innerComments'    => $arrayComment,
                );
            }

            return $this->generateResponse(array(
                'message'  => 'The comments has been recovery successfully.',
                'comments' => $arrayPublish,
            ), RestResponse::RESP_OK);
        } catch (\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APICommentsController',
                'function'      => 'getCommentSubcomment',
                'action'        => 'getCommentSubcommentInformation',
                'customMessage' => 'Error: can not get the inner comments of every main comment',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'Application Error! Please contact support team.',
                'id'      => 0,
                'error'   => $e->getMessage(),
            ), RestResponse::RESP_ERROR);
        }
    }
}
