<?php

namespace APIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use APIBundle\APILibs\APILib;
use APIBundle\APILibs\RestResponse;
use APIBundle\Entity\DynamicData;
use RegisterErrorBundle\Controller\RegisterErrorController;

/**
 * Class will contain all the information enabled
 * for the section auto administrable by the admin
 * of the platform
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Controller\APIAdminController
 */
class APIAdminController extends APILib {

    /**
     * Variable that contain the name of the repo
     *
     * @var string
     */
    protected $repoAdmin;

    /**
     * Variable will contain the entity object
     *
     * @var object | NULL
     */
    protected $admin;

    /**
     * Constructor
     */
    public function __construct() {
        $this->admin     = new DynamicData();
        $this->repoAdmin = "DynamicData";
    }

    /**
     * Method will be used to get all the information
     * is going to be displayed on the lists of the admin panel
     *
     * @return response $json
     */
    public function getAllDataAction() {
        try {
            $records[0] = $this->getDynamicData(1);
            $records[1] = $this->getDynamicData(2);
            $records[2] = $this->getDynamicData(3);
            $records[3] = $this->getDynamicData(4);

            return $this->generateResponse(array(
                'message' => 'The information auto administrable has been recovery successfully.',
                'data' => $records
            ), RestResponse::RESP_OK);
        } catch (\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIAdminController',
                'function'      => 'getAllData',
                'action'        => 'getAllDataAFromAdmin',
                'customMessage' => 'Error: can not get all data from admin section',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'Error, please contact support',
                'e' => $e->getMessage()
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Method will use for deleting the information
     * of the section selected. This delete will be just
     * login deletion, this means, the information will remain
     * in the database but the user won't see the data
     *
     * @param integer $recordId
     * @return response $json
     */
    public function deleteAdminRecordSectionAction($recordId) {
        try {
            if (isset($recordId) && is_numeric($recordId)) {
                $this->admin = $this->loadRepo($this->bundleName, $this->repoAdmin)->find($recordId);
                $em          = $this->getDoctrine()->getManager();

                $this->admin->setStatus(3);
                $em->persist($this->admin);
                $em->flush();

                return $this->generateResponse(array(
                    'message' => 'The data has been eliminated.',
                    'id'      => $recordId
                ), RestResponse::RESP_OK);
            }

            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIAdminController',
                'function'      => 'deleteAdminRecordSection',
                'action'        => 'deleteAdminRecordsSelected',
                'customMessage' => 'Error: can not delete records from admin section',
                'message'       => 'Can not delete the records created in admin module',
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'There was an issue at the moment to delete the data. Please try again or contact support.',
                'id'      => 'undefined'
            ), RestResponse::RESP_ERROR);
        } catch(\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIAdminController',
                'function'      => 'deleteAdminRecordSection',
                'action'        => 'deleteAdminRecordsSelected',
                'customMessage' => 'Error: can not delete admin record in admin section',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'Application Error. Please contact support!',
                'id'      => 'undefined',
                'error'   => $e->getMessage(),
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Method will be used to get specific data depending on
     * the section is going to be filled in
     *
     * @param integer $id
     * @return response $json
     */
    public function getDataSpecificSectionAction($id) {
        try {
            $records = $this->getDynamicData($id);

            return $this->generateResponse(array(
                'message' => 'The information has been recovery successfully',
                'data'    => $records
            ), RestResponse::RESP_OK);
        } catch (\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIAdminController',
                'function'      => 'getDataSpecificSection',
                'action'        => 'getAllDataOfSpecificSection',
                'customMessage' => 'Error: can not get all data of specific section',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'Error, please contact support.',
                'id'      => 0,
                'error'   => $e->getMessage(),
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Method will be used to update the information has been
     * selected by the user for any reason
     *
     * @param integer $id
     * @return response $json
     */
    public function updateDynamicDataAction($id) {
        try {
            $request = Request::createFromGlobals();

            $fileName = "";
            if ($request->files->get('file')) {
                $fileName = $request->files->get('file');
            }

            // get all the params
            $section     = $request->request->get('section');
            $description = $request->request->get('description');
            $status      = $request->request->get('status');
            $url         = $request->request->get('url_video');
            $token       = $request->request->get('token');
            $nameT       = $request->request->get('nameT');
            $em          = $this->getDoctrine()->getManager();
            $oldImage    = "";


            // Check if token is valid
            $validToken = $this->isCsrfTokenValid($nameT, $token);

            if ($validToken == 0) {
                // Put the text to write in the file log
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIAdminController',
                    'function'      => 'updateDynamicData',
                    'action'        => 'updateDynamicDataFromAdminSection',
                    'customMessage' => 'Error: can not update dynamic data from admin section',
                    'message'       => 'CSRF Token is not valid',
                );
                $this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'Application Error!, Please contact support or try again!',
                    'id'      => '0'
                ), RestResponse::RESP_ERROR);
            }

            // Update information
            $this->admin = $this->loadRepo($this->bundleName, $this->repoAdmin)->find($id);
            $oldImage    = $this->admin->getFile();
            $oldImageR   = $this->admin->getFileResize();

            $this->admin->setSection($section);
            $this->admin->setDescription($description);
            $this->admin->setStatus($status);

            if ($fileName) {
                $finalFilename = $this->__getFinalUniqueName() . "." . $fileName->guessExtension();
                $filenamePath  = $this->__setPathToUploadFile($this->__getRelativePath($section, 1));
                $extension     = $fileName->guessExtension();
                $fileName->move($filenamePath, $finalFilename);

                // resize IMG
                if ($this->__isImageFile($extension) == 1) {
                    $nameFunction       = $this->getTypeFunctionImg($extension);
                    $filenamePathResize = $this->pathFile . $this->__getRelativePath($section, 0) . DIRECTORY_SEPARATOR . $finalFilename;
                    $this->customImages->$nameFunction($fileName, 1100, 520, $filenamePathResize, $filenamePath . DIRECTORY_SEPARATOR . $finalFilename);

                    $this->admin->setFileResize($filenamePathResize);
                }

                $this->admin->setFile($this->pathFile . $this->__getRelativePath($section, 1) . DIRECTORY_SEPARATOR . $finalFilename);
                $this->admin->setOriginalName($fileName->getClientOriginalName());
                $this->admin->setTypeFile(0);
                $this->admin->setExtension($extension);

                // Check if the image should be deleted because one new was uploaded
                $this->customImages->deleteImages($this->__getCompletePath(), $oldImage);

                if ($this->__isImageFile($extension) == 1) {
                    $this->customImages->deleteImages($this->__getCompletePath(), $oldImageR);
                }
            }

            if ($url != "") {
                $this->admin->setUrl($url);
                $this->admin->setTypeFile(1);
            }

            $em->persist($this->admin);
            $em->flush();

            return $this->generateResponse(array(
                'message' => 'The information has been updated successfully!',
                'id'      => $id
            ), RestResponse::RESP_OK);
        } catch (\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIAdminController',
                'function'      => 'updateDynamicData',
                'action'        => 'updateDynamicDataFromAdminSection',
                'customMessage' => 'Error: can not update dynamic data from admin section',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'The record has not been updated!',
                'id'      => $id,
                'errot'   => $e->getMessage()
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Method will use for saving the information that
     * will be used in the platform and will be selected from
     * the admin panel list that will be accessible just for
     * admin users
     *
     * @return response $json
     */
    public function saveDynamicDataAction() {
        try {
            $request = Request::createFromGlobals();

            $section     = $request->request->get('section');
            $description = $request->request->get('description');
            $status      = $request->request->get('status');
            $url         = ($request->request->get('url_video') != "undefined") ? $request->request->get('url_video') : "";
            $fileName    = $request->files->get('file');
            $token       = $request->request->get('token');
            $nameT       = $request->request->get('nameToken');
            $em          = $this->getDoctrine()->getManager();

            // Check if token is valid
            //$validToken = $this->isCsrfTokenValid($nameT, $token);

            //if ($validToken == 0) {
            // Put the text to write in the file log
                /*$logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIAdminController',
                    'function'      => 'saveDynamicData',
                    'action'        => 'saveNewDataFromAdmin',
                    'customMessage' => 'Error: can not save dynamic data from admin section',
                    'message'       => 'Can not save dynamic data from admin section',
                );
                $this->logFileData->accessPoint($logMessage);*/
            //    return $this->generateResponse(array(
            //        'message' => 'Application Error!, Please contact support or try again!',
            //        'id'      => '0'
            //    ), RestResponse::RESP_ERROR);
            //}

            // Update the records in case the status is active
            $bool = $this->__updateStatus($section, $status);
            if ($bool == 0 || $bool == '0') {
                // Put the text to write in the file log
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIAdminController',
                    'function'      => 'saveDynamicData',
                    'action'        => 'saveDynamicDataFromAdmin',
                    'customMessage' => 'Error: can not save dynamic data from admin section',
                    'message'       => 'Can not save dynamic data from admin section',
                );
                $this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'There was some issues at the moment to update records',
                    'id'      => 0
                ), RestResponse::RESP_ERROR);
            }

            // Set data sent in the form
            $this->admin->setSection($section);
            $this->admin->setDescription($description);
            $this->admin->setStatus($status);

            if ($url != "") {
                $this->admin->setUrl($url);
                $this->admin->setTypeFile(1);
            }

            if ($fileName) {
                $finalFilename = $this->__getFinalUniqueName() . "." . $fileName->guessExtension();
                $filenamePath  = $this->__setPathToUploadFile($this->__getRelativePath($section, 1));
                $extension     = $fileName->guessExtension();

                $fileName->move($filenamePath, $finalFilename);

                if ($this->__isImageFile($extension)) {
                    $nameFunction       = $this->getTypeFunctionImg($extension);
                    $filenamePathResize = $this->pathFile . $this->__getRelativePath($section, 0) . DIRECTORY_SEPARATOR . $finalFilename;
                    $this->customImages->$nameFunction($fileName, 1100, 520, $filenamePathResize, $filenamePath . DIRECTORY_SEPARATOR . $finalFilename);

                    $this->admin->setFileResize($filenamePathResize);
                }

                $this->admin->setFile($this->pathFile . $this->__getRelativePath($section, 1) . DIRECTORY_SEPARATOR . $finalFilename);
                $this->admin->setOriginalName($fileName->getClientOriginalName());
                $this->admin->setTypeFile(0);
                $this->admin->setExtension($extension);
            }

            $em->persist($this->admin);
            $em->flush();

            return $this->generateResponse(array(
                'message' => 'The data has been saved successfully',
                'id'      => $this->admin->getId()
            ), RestResponse::RESP_OK);

        } catch (\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIAdminController',
                'function'      => 'saveDynamicData',
                'action'        => 'saveDynamicDataFromAdmin',
                'customMessage' => 'Error: can not save dynamic data from admin section',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'The records has not been saved!',
                'id'      => '0',
                'error'   => $e->getMessage()
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Method will be used to get all the information
     * from the dynamic data will be displayed on the home page
     *
     * @param integer $id
     * @return array
     */
    private function getDynamicData($id) {
        $array = array();
        $inner = array();
        $data  = $this->loadRepo($this->bundleName, $this->repoAdmin)->getAllDynamicData($id);

        $sectionData    = $this->__keyToStringData($id);

        foreach ($data as $key => $value) {
            $flagOption    = "";
            $urlVideoEmbed = "";
            $type          = "";

            if ($value['type_file'] != "" || $value['type_file'] != null) {
                $flagOption = ($value['type_file'] == 0 || $value['type_file'] == "0") ? 1 : 2;
            }

            if ($value['type_file'] == 1) {
                $count1 = explode("/", $value['url']);

                if (in_array('www.youtube.com', $count1) || in_array('youtu.be', $count1)) {
                    //explode("=", $value['url']);
                    $idVideo       = (strpos($value['url'], "=") !== false) ? explode('=', $value['url']) : explode("/", $value['url']);
                    //$idVideo[1];
                    $urlVideoEmbed = "https://www.youtube.com/embed/" . $idVideo[count($idVideo) -1];
                    $type          = "youtube";
                }
            }

            $inner[$key] = array(
                'id'          => $value['id'],
                'section'     => $this->__keyToStringData($value['section']),
                'sectionId'   => $value['section'],
                'description' => $value['description'],
                'status'      => ($value['status'] == 1) ? 'Active' : 'Inactive',
                'statusId'    => $value['status'],
                'file'        => ($value['file'] != null || $value['file'] != '') ? $value['file'] : "",
                'file_resize' => ($value['file_resize'] != null || $value['file_resize'] != '') ? $value['file_resize'] : "",
                'type_file'   => ($value['type_file'] != null || $value['type_file'] != '') ? $value['type_file'] : "",
                'extension'   => ($value['file_extension'] != null || $value['file_extension'] != '') ? $value['file_extension'] : "",
                'origName'    => ($value['original_name'] != null || $value['original_name'] != '') ? $value['original_name'] : "",
                'url'         => ($value['url'] != null || $value['url'] != '') ? $value['url'] : "",
                'flagOption'  => $flagOption,
                'videoURL'    => $urlVideoEmbed,
                'type'        => $type,
                'created_at'  => $value['date_created'],
                'updated_at'  => $value['date_updated'],
            );
        }

        $array[$sectionData] = array(
            'count' => count($data),
            'inner' => $inner
        );

        return $array;
    }

    /**
     * Method will return the information of the section as
     * string to set in the array
     *
     * @param integer $key
     * @return string
     */
    private function __keyToStringData($key) {
        $text = "";
        if ($key == 1) {
            $text = "Banners";
        } else if ($key == 2) {
            $text = "HowItWorks";
        } else if ($key == 3) {
            $text = "AboutUs";
        } else if ($key == 4) {
            $text = "TermsConditions";
        }

        return $text;
    }

    /**
     * Method will update the records that has status
     * different to 1 because those status just to have
     * one definition at the moment
     *
     * @param integer $section
     * @param integer $status
     * @return boolean
     */
    private function __updateStatus($section, $status) {
        try {
            if ($status == 1) {
                if ($section != 1) {
                    $records = $this->loadRepo($this->bundleName, $this->repoAdmin)->getDynamicBySection($section, $status);

                    if (count($records) > 0) {
                        $em = $this->getDoctrine()->getManager();
                        foreach ($records as $key => $value) {
                            $this->admin = $this->loadRepo($this->bundleName, $this->repoAdmin)->find($value->getId());

                            $this->admin->setStatus(0);
                            $em->persist($this->admin);
                            $em->flush();
                        }
                        $this->admin = new DynamicData();

                        return TRUE;
                    } else {
                        return TRUE;
                    }
                } else {
                    return TRUE;
                }
            } else {
                return TRUE;
            }
        } catch(\Exception $e) {
            // Save in custom log file
            return FALSE;
        }
    }

    /**
     * Method will return all the information of the relative
     * path is going to be used for upload the file to the server
     *
     * @param integer $section
     * @param integer $type
     * @return string
     */
    private function __getRelativePath($section, $type) {
        if (intval($section) == 1) {
            if ($type == 1) {
                return "imageSites/banners/originalImg";
            } else {
                return "imageSites/banners/resizeImg";
            }
        }
    }
}
