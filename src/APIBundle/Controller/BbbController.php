<?php

namespace APIBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Config\Definition\Exception\Exception;
use APIBundle\APILibs\APILib;
use APIBundle\APILibs\RestResponse;

/**
 * Class will contain all user's information that will be
 * set for making the request to bbb.org site and get the
 * main information of the users
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Controller\BbbController
 */
class BbbController extends APILib {

}
