<?php

namespace APIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use APIBundle\APILibs\APILib;
use APIBundle\APILibs\RestResponse;
use APIBundle\Entity\Users;
use APIBundle\Entity\Posts;
use APIBundle\Entity\Attachments;
use RegisterErrorBundle\Controller\RegisterErrorController;

/**
 * API Controller will be used to get information of all the jobs
 * has been created by the user logged in or another users registered
 * in the platform
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>, Fernando Molina Mejia <fernando.molina.mejia@gmail.com>
 * @package APIBundle\Controller\APIPostsController
 */
class APIPostsController extends APILib {

    /**
     * Variable to contain the name of the repo and entity
     *
     * @var string
     */
    protected $repositoryName = "";

    /**
     * Variable that contains the name of the categories repo
     *
     * @var string
     */
    protected $repositoryCategory = "";

    /**
     * Variable will contain the names of the users repo
     *
     * @var string
     */
    protected $repositoryUsers = "";

    /**
     * Variable will contain the name of the city repo
     *
     * @var string
     */
    protected $repositoryCity = "";

    /**
     * Variable to contain the name of the repository
     *
     * @var string
     */
    protected $repositoryImages = "";

    /**
     * Variable will contain the information from bids repository
     *
     * @var string
     */
    protected $repositoryBids = "";

    /**
     * Variable to contain a reference of the repo
     *
	 * @var object | NULL
     */
    protected $repository = NULL;

    /**
     * Variable to store the post entity
     *
     * @var object | NULL
     */
    protected $posts = NULL;

    /**
     * Variable to store the attachments entity
     *
     * @var object | NULL
     */
    protected $attachments = NULL;

    /**
     * Constructor
     */
    public function __construct() {
        $this->repositoryName     = "Posts";
        $this->repositoryImages   = "Attachments";
        $this->repositoryBids     = "Offers";
        $this->repositoryCity     = "Cities";
        $this->repositoryUsers    = "Users";
        $this->repositoryCategory = "JobsCategories";

        $this->posts       = new Posts();
    }

    /**
     * Method will be used to get all the information of the jobs
     * has been posted by the users in the platform and are enabled
     * currently to be checked by other users of the platforms
     */
    public function searchAction() {

        $data = NULL;
        $dataArr = array();
        $filters = array();
        $queryString = 'SELECT p FROM APIBundle:Posts p WHERE 1=1 ';

        $request = Request::createFromGlobals();

        // Retrieve information from request
        $postname   = $request->query->get('postname');
        $category   = $request->query->get('category');
        $city       = $request->query->get('city');


        // Setup filters
        if (isset($postname)) {
            $queryString = $queryString . ' AND p.postName LIKE :postname ';
            $filters['postName'] = $postname;
        }
        if (isset($category)) {
            if (isset($postname)) {
                $queryString = $queryString . ' AND ';
            }
            $queryString = $queryString . ' p.category = :category ';
            $filters['category'] = $category;
        }
        if (isset($city)) {
            if (isset($category)) {
                $queryString = $queryString . ' AND ';
            }
            $queryString = $queryString . ' p.city = :city ';
            $filters['city'] = $city;
        }

        $queryString.= " AND p.postStatus = 1
                         ORDER BY p.id DESC ";

        try {

            // If filters are setup
            if (count($filters) > 0) {

                $query = $this->getDoctrine()->getManager()->createQuery($queryString);
                if (isset($postname)) {
                    $query->setParameter('postname', '%' . $filters['postName'] . '%');
                }
                if (isset($category)) {
                    $query->setParameter('category', $filters['category']);
                }
                if (isset($city)) {
                    $query->setParameter('city', $filters['city']);
                }
                $data = $query->getResult();

                //$data = $this->loadRepo($this->bundleName, $this->repositoryName)->findBy($filters);
            } else {
                $data = $this->loadRepo($this->bundleName, $this->repositoryName)->findBy(array('postStatus' => 1),
                                                                                          array('id' => 'DESC'));
            }

            foreach ($data as $post) {
                array_push(
                    $dataArr,
                    $post->toBasicInformation($this->pathFile . "posts" . DIRECTORY_SEPARATOR . "no_image_medium.png")
                );
            }

            return $this->generateResponse($dataArr, RestResponse::RESP_OK);

        } catch (\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIPostsController',
                'function'      => 'search',
                'action'        => 'searchProjectsByFilters',
                'customMessage' => 'Error: can not search projects by filters',
                'message'       => $e->getMessage(),
            );
            //$this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'error_message'  => 'Application Error! Please contact support team.',
                'error'          => $e->getMessage(),
                'data'           => $data,
            ), RestResponse::RESP_ERROR);
        }
    }

	/**
	 * Method used to get all the information of the jobs posted
	 * by a specific user in the platform.
	 */
    public function dashboardAction($id) {


        $data = NULL;
        $dataArr = array();
        $filters = array();
        $queryString = 'SELECT p FROM APIBundle:Posts p WHERE p.user = :user ';

        $request = Request::createFromGlobals();

        // Retrieve information from request
        $postname   = $request->query->get('postname');
        $category   = $request->query->get('category');
        $city       = $request->query->get('city');

        // Setup filters
        if (isset($postname)) {
            $queryString = $queryString . ' AND p.postName LIKE :postname ';
            $filters['postName'] = $postname;
        }
        if (isset($category)) {
            $queryString = $queryString . ' AND p.category = :category ';
            $filters['category'] = $category;
        }
        if (isset($city)) {
            $queryString = $queryString . 'AND  p.city = :city ';
            $filters['city'] = $city;
        }
        $queryString .= " order by p.id desc ";

        try {

            // If filters are setup
            if (count($filters) > 0) {

                $query = $this->getDoctrine()->getManager()->createQuery($queryString);
                $query->setParameter('user', $id);
                if (isset($postname)) {
                    $query->setParameter('postname', '%' . $filters['postName'] . '%');
                }
                if (isset($category)) {
                    $query->setParameter('category', $filters['category']);
                }
                if (isset($city)) {
                    $query->setParameter('city', $filters['city']);
                }

                $data = $query->getResult();

                //$data = $this->loadRepo($this->bundleName, $this->repositoryName)->findBy($filters);
            } else {
                $data = $this->loadRepo($this->bundleName, $this->repositoryName)->findBy(array('user' => $id),
                                                                                          array('id'   => 'DESC'));
            }

            foreach ($data as $post) {
                array_push(
                    $dataArr,
                    $post->toBasicInformation($this->pathFile . "posts" . DIRECTORY_SEPARATOR . "no_image_medium.png")
                );
            }

            return $this->generateResponse($dataArr, RestResponse::RESP_OK);

        } catch (\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIPostsController',
                'function'      => 'dashboard',
                'action'        => 'dashboardData',
                'customMessage' => 'Error: can not display data in dashboard',
                'message'       => $e->getMessage(),
            );
            //$this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'error_message'  => 'Application Error! Please contact support team.',
                'error'          => $e->getMessage(),
                'data'           => $data,
            ), RestResponse::RESP_ERROR);
        }

    }

    /**
     * Method will be used to save data when the user
     * wants to create the new project will be published
     *
     * @return response $json
     */
    public function saveAction() {
        try {
            $request = Request::createFromGlobals();

            $userID      = $request->request->get('userId');
            $postName    = $request->request->get('postname');
            $description = $request->request->get('description');
            $city        = $request->request->get('city');
            $category    = $request->request->get('category');
            $userId      = $request->request->get('userId');
            $token       = $request->request->get('token');
            $source      = $request->request->get('source');
            $name        = $request->request->get('name');
            $payments    = ($request->request->get('payments') != 'undefined') ? $request->request->get('payments') : 0;
            $files       = $request->files->keys();

            // Check if token is valid
            $validToken = $this->isCsrfTokenValid($name, $token);

            if ($validToken == 0) {
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIPostsController',
                    'function'      => 'save',
                    'action'        => 'saveProjectToken',
                    'customMessage' => 'Error: can not use token. It is an invalid token',
                    'message'       => 'CSRF Token has been detected as invalid token',
                );
                //$this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'Application Error!, Please contact support or try again!',
                    'id'      => '0'
                ), RestResponse::RESP_ERROR);
            }

            // Get all city and the category
            $jobCategory = $this->loadRepo($this->bundleName, $this->repositoryCategory)->find($category);
            $cities      = $this->loadRepo($this->bundleName, $this->repositoryCity)->find($city);
            $users       = $this->loadRepo($this->bundleName, $this->repositoryUsers)->find($userID);
            $em          = $this->getDoctrine()->getManager();

            $this->posts->setPostName($postName);
            $this->posts->setDescription($description);
            $this->posts->setPostPayment($payments);
            $this->posts->setCategory($jobCategory);
            $this->posts->setCity($cities);
            $this->posts->setUser($users);
            $this->posts->setPostStatus(1);

            $em->persist($this->posts);
            $em->flush();

            // Save attachments
            $fileNamePath = $this->__setPathToUploadFile('posts');
            foreach ($files as $fileName) {
                // Upload attachments
                $this->attachments = new Attachments();
                $imagesName        = $request->files->get($fileName);
                $extension         = $imagesName->guessExtension();

                $finalFileName = $this->__getFinalUniqueName() . "." . $imagesName->guessExtension();
                $imagesName->move($fileNamePath, $finalFileName);

                // Create the set of variables
                $this->attachments->setPost($this->posts);
                $this->attachments->setName($imagesName->getClientOriginalName());
                $this->attachments->setPath($this->pathFile . "posts" . DIRECTORY_SEPARATOR . $finalFileName);
                $this->attachments->setFileExtension($extension);
                $this->attachments->setIsImage($this->__isImageFile($extension));

                $em->persist($this->attachments);
                $em->flush();
            }

            return $this->generateResponse(array(
                'message' => 'The projects has been created successfully',
                'id'      => $this->posts->getId(),
            ), RestResponse::RESP_OK);
        } catch (\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIPostsController',
                'function'      => 'save',
                'action'        => 'saveNewProject',
                'customMessage' => 'Error: can not save and create new project',
                'message'       => $e->getMessage(),
            );
            //$this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'The projects has not been created',
                'id'      => 0,
                'error'   => $e->getMessage(),
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Method will be used to save data when the user
     * wants to create the new project will be published
     *
     * @return response $json
     */
    public function editAction($id) {
        try {
            $request = Request::createFromGlobals();

            $userID      = $request->request->get('userId');
            $postName    = $request->request->get('postname');
            $description = $request->request->get('description');
            $city        = $request->request->get('city');
            $category    = $request->request->get('category');
            $userId      = $request->request->get('userId');
            $token       = $request->request->get('token');
            $source      = $request->request->get('source');
            $name        = $request->request->get('name');
            $payments    = ($request->request->get('payments') != 'undefined') ? $request->request->get('payments') : 0;
            $files       = $request->files->keys();

            // Check if token is valid
            $validToken = $this->isCsrfTokenValid($name, $token);

            if ($validToken == 0) {
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIPostsController',
                    'function'      => 'save',
                    'action'        => 'saveProjectToken',
                    'customMessage' => 'Error: can not use token. It is an invalid token',
                    'message'       => 'CSRF Token has been detected as invalid token',
                );
                //$this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'Application Error!, Please contact support or try again!',
                    'id'      => '0'
                ), RestResponse::RESP_ERROR);
            }

            // Get all city and the category
            $jobCategory = $this->loadRepo($this->bundleName, $this->repositoryCategory)->find($category);
            $cities      = $this->loadRepo($this->bundleName, $this->repositoryCity)->find($city);
            $users       = $this->loadRepo($this->bundleName, $this->repositoryUsers)->find($userID);
            $em          = $this->getDoctrine()->getManager();

            // Get post information
            $this->posts = $this->loadRepo("APIBundle", $repositoryName)->find(intval($id));
            $this->posts->setDateUpdated(new \DateTime('now'));

            $this->posts->setPostName($postName);
            $this->posts->setDescription($description);
            $this->posts->setPostPayment($payments);
            $this->posts->setCategory($jobCategory);
            $this->posts->setCity($cities);
            $this->posts->setUser($users);
            $this->posts->setPostStatus(1);

            $em->persist($this->posts);
            $em->flush();

            // Save attachments
            $fileNamePath = $this->__setPathToUploadFile('posts');
            foreach ($files as $fileName) {
                // Upload attachments
                $this->attachments = new Attachments();
                $imagesName        = $request->files->get($fileName);
                $extension         = $imagesName->guessExtension();

                $finalFileName = $this->__getFinalUniqueName() . "." . $imagesName->guessExtension();
                $imagesName->move($fileNamePath, $finalFileName);

                // Create the set of variables
                $this->attachments->setPost($this->posts);
                $this->attachments->setName($imagesName->getClientOriginalName());
                $this->attachments->setPath($this->pathFile . "posts" . DIRECTORY_SEPARATOR . $finalFileName);
                $this->attachments->setFileExtension($extension);
                $this->attachments->setIsImage($this->__isImageFile($extension));

                $em->persist($this->attachments);
                $em->flush();
            }

            return $this->generateResponse(array(
                'message' => 'The project has been saved successfully',
                'id'      => $this->posts->getId(),
            ), RestResponse::RESP_OK);
        } catch (\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIPostsController',
                'function'      => 'save',
                'action'        => 'editProject',
                'customMessage' => 'Error: can not save and edit project',
                'message'       => $e->getMessage(),
            );
            //$this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'The projects has not been created',
                'id'      => 0,
                'error'   => $e->getMessage(),
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Deprecated
     */
    public function saveDAction($id) {

        try {


            $request = Request::createFromGlobals();

            $fileNames   = $request->files->keys();
            $postname    = $request->request->get('postName');
            $description = $request->request->get('description');
            $cityId      = $request->request->get('city');
            $userId      = $request->request->get('user');
            $source      = $request->request->get('source');
            $token       = $request->request->get('token');
            $nameT       = $request->request->get('nameT');
            $jobs        = $request->request->get('jobCategory');
            $postPayment = ($request->request->get('postPayment') == '') ? $request->request->get('postPayment') : '';

            // Check if token is valid
            /*$validToken = $this->isCsrfTokenValid($nameT, $token);

            if ($validToken == 0) {
                return $this->generateResponse(array(
                    'message' => 'Application Error!, Please contact support or try again!',
                    'id'      => '0'
                ), RestResponse::RESP_ERROR);
            }*/

            // Retrieve objects from DB
        	$jobCategory = $this->loadRepo($this->bundleName, "JobsCategories")->find($jobs);//$request->request->get('jobCategory'));
        	$city        = $this->loadRepo($this->bundleName, "Cities")->find($cityId);
            $user        = $this->loadRepo($this->bundleName, "Users")->find($userId);

        	$post = NULL;
            $attachment = NULL;

        	/*if ($id != 0 || $id != '0') {
        		// Save edited post
        		$post = $this->loadRepo("APIBundle", $repositoryName)->find(intval($id));
        		$post->setDateUpdated(new \DateTime('now'));
        	} else {*/
        		// Save new post
        		$post = new Posts();
        	//}

        	$post->setPostName($postname);
        	$post->setDescription($description);
        	$post->setCategory($jobCategory);
        	$post->setCity($city);
            $post->setUser($user);
            $post->setPostPayment($postPayment);
            $post->setPostStatus(1);

            // Saving post
    		$em = $this->getDoctrine()->getManager();
    		$em->persist($post);
    		$em->flush();

            // Validate and save images
            if (isset($fileNames) && $fileNames != null && count($fileNames) > 0) {

                $filenamePath  = $this->__setPathToUploadFile("posts");

                foreach ($fileNames as $filename) {
                    // Create attachment to save image
                    $attachment = new Attachments();
                    //$attachment->setIsImage(true);
                    $attachment->setPost($post);

                    $image = $request->files->get($filename);
                    $extension = $image->guessExtension();

                    // Save phisical file
                    $finalFilename = $this->__getFinalUniqueName() . "." . $image->guessExtension();
                    $image->move($filenamePath, $finalFilename);
                    //move_uploaded_file($finalFilename, $filenamePath);

                    // Fill image information
                    $attachment->setName($image->getClientOriginalName());
                    $attachment->setPath($this->pathFile . "posts" . DIRECTORY_SEPARATOR . $finalFilename);
                    $attachment->setFileExtension($extension);
                    $attachment->setIsImage($this->__isImageFile($extension));

                    //var_dump($attachment);
                    // Saving image
                    $em->persist($attachment);
                    $em->flush();
                }

            } else {

                // Predefined image when principal is not loaded
                $attachment->setName("no_image_medium.png");
                $attachment->setPath($this->pathFile . "posts" . DIRECTORY_SEPARATOR . "no_image_medium.png");

                // Saving image
                $em->persist($attachment);
                $em->flush();
            }

    		return $this->generateResponse(array(
                'message' => "The post has been saved successfully.",
                'id'      => $post->getId()
            ), RestResponse::RESP_OK);

        } catch (\Exception $e) {
            // Put the text to write in the file log

            return $this->generateResponse(array(
                'message' => "There was an error to save the post ".$e->getMessage(),
                'id'      => 0,
                'e'       => $e->getMessage(),
            ), RestResponse::RESP_ERROR);

        }

    }

    /**
     * Method will return all the information of the project
     * depends on the selection of the user
     *
     * @param integer $id
     * @return json $response
     */
    public function showAction($id) {

        $data = NULL;

    	try {

            $arrayImages = array();
            $arrayBids   = array();
            $data        = $this->loadRepo($this->bundleName, $this->repositoryName)->find($id);
            $typeVideo   = "";
            $idVideo     = "";

            // Get all the images from the project
            $images = $this->loadRepo($this->bundleName, $this->repositoryImages)->findBy(array('post' => $id));
            foreach ($images as $image) {
                if ($image->getIsImage() == 0){
                    if (strpos($image->getPath(), "youtube") !== false) {
                        $typeVideo = "youtube";
                        $idValue   = (strpos($image->getPath(), "=") !== false) ? explode('=', $image->getPath()) : explode("/", $image->getPath());
                        $idVideo   = $idValue[count($idValue) -1];
                    }
                }

                $arrayImages[] = array(
                    'id'        => $image->getId(),
                    'name'      => $image->getName(),
                    'isImage'   => ($image->getIsImage() == true) ? 1 : 0,
                    'extension' => $image->getFileExtension(),
                    'path'      => $image->getPath(),
                    'videoType' => ($typeVideo != '') ? $typeVideo : '',
                    'idVideo'   => ($idVideo != '') ? $idVideo : '',
                );
            }

            // Get winner bids
            if ($data->getPostStatus() == 0) {
                $bidsData = $this->loadRepo($this->bundleName, $this->repositoryBids)->findBy(array('post'            => $id,
                                                                                                    'isActiveAuction' => array(2, 3)));

                if (count($bidsData) != 0) {
                    // Set array
                    $arrayBids = array(
                        'id'        => $bidsData[0]->getId(),
                        'userName'  => $bidsData[0]->getUser()->getUsername(),
                        'bid'       => money_format('%i', $bidsData[0]->getMoneyBidder()),
                        'statusBid' => $bidsData[0]->getIsActiveAuction(),
                    );
                }
            }

            return $this->generateResponse(array(
                'owner'         => $data->getUser()->getId(),
                'postname'      => $data->getPostname(),
                'description'   => $data->getDescription(),
                'category_id'   => $data->getCategory()->getId(),
                'category'      => $data->getCategory()->getNameJobsCategories(),
                'city_id'       => $data->getCity()->getId(),
                'city'          => $data->getCity()->getNameCity(),
                'publishedBy'   => $data->getUser()->getUsername(),
                'status'        => $data->getPostStatus(),
                'image'         => $arrayImages,
                'bids'          => $arrayBids,
            ), RestResponse::RESP_OK);

        } catch (\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIPostsController',
                'function'      => 'show',
                'action'        => 'showProjects',
                'customMessage' => 'Error: can not show the projects have been created',
                'message'       => $e->getMessage(),
            );
            //$this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'error_message'  => 'Error retrieving Post.',
                'error'          => $e->getMessage(),
                'data'           => $data,
            ), RestResponse::RESP_ERROR);
        }

    }

    /**
     * Method will return information about the
     * posts by category that exists in the platform
     * and that are active
     *
     * @param integer $id
     * @return response $json
     */
    public function displayPostByCategoryAction ($id) {
        try {
            if ($id == '' || is_null($id)) {
                return new RedirectResponse($this->generateUrl('indexpage'));
            }

            $news = array();

            $data = null;

            if ($id != 0) {
                $data = $this->loadRepo($this->bundleName, $this->repositoryName)->findBy(array('postStatus' => 1,
                                                                                            'category'   => $id),
                                                                                      array('id' => 'DESC'));
            } else {
                $data = $this->loadRepo($this->bundleName, $this->repositoryName)->findBy(
                    array('postStatus' => 1),
                    array('id' => 'DESC'));
            }
            

            foreach ($data as $key => $value) {
                array_push(
                    $news,
                    $value->toBasicInformation($this->pathFile . "posts" . DIRECTORY_SEPARATOR . "no_image_medium.png")
                );
            }

            return $this->generateResponse(array(
                'message' => 'The data has been recovery successfully.',
                'data'    => $news,
                'total'   => count($data),
            ), RestResponse::RESP_OK);
        } catch (\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIPostsController',
                'function'      => 'displayPostByCategory',
                'action'        => 'displayAllPostByCategory',
                'customMessage' => 'Error: can not display post by category',
                'message'       => $e->getMessage(),
            );
            //$this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'Application Error! Please contact support team!',
                'id'      => 0,
                'error'   => $e->getMessage(),
            ), RestResponse::RESP_ERROR);
        }
    }
}
