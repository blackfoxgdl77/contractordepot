<?php

namespace APIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use APIBundle\APILibs\APILib;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Config\Definition\Exception\Exception;
use RegisterErrorBundle\Controller\RegisterErrorController;
use APIBundle\APILibs\RestResponse;
use APIBundle\Entity\Offers;

/**
 * Controller will have all the methods will be
 * used once the user has accepted the bid and
 * the platform redirects to the payment services
 * for finishing the transactions
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Controller\APIPaymentsHistoryController
 */

class APIPaymentsHistoryController extends APILib {

    /**
     * Variable will contain the name of the repository
     *
     * @var string
     */
    protected $repoOffers = '';

    /**
     * Variable will contain all the information about the entity
     *
     * @var object
     */
    protected $offers;
    protected $bController;

    /**
     * Constructor
     */
    public function __construct() {
        $this->offers     = new Offers();
        $this->repoOffers = "Offers";
    }

    /**
     * Method will be used to get all the information
     * of the projects has been paid by the owner to have a
     * single list of projects created
     *
     * @return json $response
     */
    public function getPaymentProjectsDoneAction () {
        try {
            $arrayDone  = array();
            $offersDone = $this->loadRepo($this->bundleName, $this->repoOffers)->getProjectsDonePaid();

            foreach ($offersDone as $key => $value) {
                $post     = $this->loadRepo($this->bundleName, 'Posts')->find($value['post_id']);
                $user     = $this->loadRepo($this->bundleName, 'Users')->find($value['bidder_id']);
                $city     = $this->loadRepo($this->bundleName, "Cities")->findBy(array('id' => $post->getCity()->getId()));
                $category = $this->loadRepo($this->bundleName, "JobsCategories")->findBy(array('id' => $post->getCategory()->getId()));

                $arrayDone[] = array(
                    'idPayment' => $value['id_payment'],
                    'idPost'    => $post->getId(),
                    'namePost'  => $post->getPostName(),
                    'postDesc'  => $post->getDescription(),
                    'statusP'   => $post->getPostStatus(),
                    'owner'     => $post->getUser()->getName(),
                    'emailO'    => $post->getUser()->getEmail(),
                    'username'  => $post->getUser()->getUsername(),
                    'userWin'   => $user->getUsername(),
                    'userNameW' => $user->getName(),
                    'userEmail' => $user->getEmail(),
                    'amount'    => $value['money_bidder'],
                    'statusT'   => $value['status'],
                    'cityName'  => $city[0]->getNameCity(),
                    'capital'   => $city[0]->getCapitalCity(),
                    'category'  => $category[0]->getNameJobsCategories(),
                    'idTrans'   => $value['id_transaction'],
                    'datePaid'  => $value['date_payment'],
                    'dateCrt'   => $post->getDateCreated()->format('d-Y-m H:i:s'),
                    'dateUpt'   => $post->getDateUpdated()->format('d-Y-m H:i:s'),
                );
            }

            // Generate response
            return $this->generateResponse(array(
                'message' => 'The data has been recovery successfully!',
                'data'    => $arrayDone,
            ), RestResponse::RESP_OK);
        } catch (\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIPaymentsHistoryController',
                'function'      => 'getPaymentProjectsDone',
                'action'        => 'getAllPaymentsProjectsDone',
                'customMessage' => 'Error: can not recovery all payment projects done',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'Application Error! Please contact support team via contact form.',
                'id'      => 0,
                'error'   => $e->getMessage(),
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Method will be used to get all the informartion
     * about the projects has been stayed as pending
     * because the owner has not make the payment
     *
     * @return json $response
     */
    public function getPaymentsProjectsPendingAction(){
        try {
            $arrayPending  = array();
            $offersPending = $this->loadRepo($this->bundleName, $this->repoOffers)->getProjectsPendingPaid();
            $totalPendings = count($offersPending);

            foreach ($offersPending as $key => $value) {
                $post     = $this->loadRepo($this->bundleName, 'Posts')->find($value['bidder_post']);
                $user     = $this->loadRepo($this->bundleName, 'Users')->find($value['bidder_id']);
                $city     = $this->loadRepo($this->bundleName, "Cities")->findBy(array('id' => $post->getCity()->getId()));
                $category = $this->loadRepo($this->bundleName, "JobsCategories")->findBy(array('id' => $post->getCategory()->getId()));

                $arrayPending[] = array(
                    'idPayment' => intval($value['id_offer']),
                    'idPost'    => $post->getId(),
                    'namePost'  => $post->getPostName(),
                    'postDesc'  => $post->getDescription(),
                    'statusP'   => $post->getPostStatus(),
                    'owner'     => $post->getUser()->getName(),
                    'emailO'    => $post->getUser()->getEmail(),
                    'username'  => $post->getUser()->getUsername(),
                    'userWin'   => $user->getUsername(),
                    'userNameW' => $user->getName(),
                    'userEmail' => $user->getEmail(),
                    'amount'    => $value['money_bidder'],
                    'cityName'  => $city[0]->getNameCity(),
                    'capital'   => $city[0]->getCapitalCity(),
                    'category'  => $category[0]->getNameJobsCategories(),
                    'dateCrt'   => $post->getDateCreated()->format('d-Y-m H:i:s'),
                    'dateUpt'   => $post->getDateUpdated()->format('d-Y-m H:i:s'),
                    'total'     => $totalPendings,
                );
            }

            // Generate response
            return $this->generateResponse(array(
                'message' => 'The data has been recovery successfully!',
                'data'    => $arrayPending,
            ), RestResponse::RESP_OK);
        } catch (\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIPaymentsHistoryController',
                'function'      => 'getPaymentsProjectsPending',
                'action'        => 'getAllPaymentsProjectsPending',
                'customMessage' => 'Error: can not recovery all payment pending projects',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'Application Error! Please contact support team via contact form.',
                'id'      => 0,
                'error'   => $e->getMessage(),
            ), RestResponse::RESP_ERROR);
        }
    }
}
