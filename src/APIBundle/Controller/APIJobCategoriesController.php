<?php

namespace APIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use APIBundle\APILibs\APILib;
use APIBundle\APILibs\RestResponse;
use APIBundle\Entity\JobsCategories;
use RegisterErrorBundle\Controller\RegisterErrorController;

/**
 * API Controller will be used to get information of all the job categories.
 *
 * @author Fernando Molina Mejia <fernando.molina.mejia@gmail.com>
 * @package APIBundle\Controller\APIJobCategoriesController
 */
class APIJobCategoriesController extends APILib {

    /**
     * Variable to contain the name of the repo and entity
     *
     * @var string
     */
    private $repositoryName = "";

    /**
     * Variable to contain a reference of the repo
     *
	 * @var object | NULL
     */
    private $repository = NULL;

    /**
     * Adding default value in the dropdown for job categories
     *
     * @var array
     */
    private $default = array(
        'id'    => 0,
        'value' => 'Select a Job Category',
    );

    /**
     * Constructor
     */
    public function __construct() {
        $this->repositoryName = "JobsCategories";
    }

    /**
     *
     */
    public function searchAction() {

        $data = NULL;
        $dataArr = array();

        try {
            $data = $this->loadRepo($this->bundleName, $this->repositoryName)->findAll();

            foreach ($data as $jobCategory) {
                $dataArr[] = array(
                    'id'    => $jobCategory->getId(),
                    'value' => $jobCategory->getNameJobsCategories(),
                );
            }

            array_unshift($dataArr, $this->default);

            return $this->generateResponse($dataArr, RestResponse::RESP_OK);


        } catch (\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIJobCategories',
                'function'      => 'search',
                'action'        => 'searchJobsCategories',
                'customMessage' => 'Error: can not get the jobs categories',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'error_message'  => 'Error retrieving Job Categories.',
                'error'          => $e->getMessage(),
                'data'           => $data
            ), RestResponse::RESP_ERROR);
        }

    }

    /**
     */
    public function getJobsCategoriesAction() {
        try {
            $jobs = array();
            $data = $this->loadRepo($this->bundleName, $this->repositoryName)->getPostByCategories();

            foreach ($data as $key => $value) {
                $jobs[] = array(
                    'id_job'     => $value['id_jobs_categories'],
                    'name_job'   => $value['name_jobs_categories'],
                    'total_jobs' => $value['TOTAL'],
                );
            }

            return $this->generateResponse(array(
                'message' => 'The jobs has been recovery successfully.',
                'jobs'    => $jobs,
            ), RestResponse::RESP_OK);
        } catch(\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIJobCategories',
                'function'      => 'getJobsCategories',
                'action'        => 'getAllJobsCategories',
                'customMessage' => 'Error: can not get all jobs categories',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'Application Error! Please contact support team.',
                'id'      => 0,
            ), RestResponse::RESP_ERROR);
        }
    }
}
