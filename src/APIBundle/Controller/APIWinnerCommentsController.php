<?php

namespace APIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Config\Definition\Exception\Exception;
use RegisterErrorBundle\Controller\RegisterErrorController;
use APIBundle\APILibs\APILib;
use APIBundle\APILibs\RestResponse;
use Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle;
use APIBundle\Entity\WinnerComments;
use APIBundle\Entity\Users;
use APIBundle\Entity\Posts;
use APIBundle\Entity\Offers;

/**
 * Controller will be used to get all the methods related
 * to the comments done by the project's owner or by the
 * project's winner where we can get all the information
 * doing a crud api
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Controller\APIWinnerComments
 */
class APIWinnerCommentsController extends APILib {

    /**
     * Variable will store the winner comments entity
     *
     * @var object | NULL
     */
    protected $winnerComments = NULL;

    /**
     * Variable will store the post entity
     *
     * @var object | NULL
     */
    protected $postComments = NULL;

    /**
     * Variable will store the users entity
     *
     * @var object | NULL
     */
    protected $userComments = NULL;

    /**
     * Variable will store the name of the comments repository
     *
     * @var string
     */
    protected $winnerCommentsRepo = "";

    /**
     * Variable will store the name of the users repository
     *
     * @var string
     */
    protected $usersRepo = "";

    /**
     * Variable will store the name of the posts repository
     *
     * @var string
     */
    protected $postsRepo = "";

    /**
     * Variable will store the name of the offers repository
     *
     * @var string
     */
    protected $offersRepo = "";

    /**
     * Constructor and Initializers
     */
    public function __construct() {
        $this->winnerComments     = new WinnerComments();
        $this->userComments       = new Users();
        $this->postComments       = new Posts();
        $this->winnerCommentsRepo = "WinnerComments";
        $this->usersRepo          = "Users";
        $this->postsRepo          = "Posts";
        $this->offersRepo         = "Offers";
    }

    /**
     * Method will be used to save the information
     * of the comments between the user and the
     * owner of the project
     *
     * @return response $json
     */
    public function saveWinnerCommentAction() {
        try {
            $jsonData = json_decode(file_get_contents('php://input'), true);

            //load information
            $comment = $jsonData['comment'];
            $postId  = $jsonData['postsId'];
            $userId  = $jsonData['usersId'];
            $source  = $jsonData['source'];
            $token   = $jsonData['token'];
            $nameT   = $jsonData['nameT'];
            $em      = $this->getDoctrine()->getManager();

            // Check if token is valid
            $validToken = $this->isCsrfTokenValid($nameT, $token);

            if ($validToken == 0) {
                // Put the text to write in the file log
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIWinnerComments',
                    'function'      => 'saveWinnerComment',
                    'action'        => 'saveWinnerCommentsToken',
                    'customMessage' => 'Error: can not use a invalid token',
                    'message'       => 'CSRF Token can not be used. It is a invalid token',
                );
                //$this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'Application Error!, Please contact support or try again!',
                    'id'      => '0'
                ), RestResponse::RESP_ERROR);
            }

            // Get data to insert in the table winner comments
            $post  = $this->loadRepo($this->bundleName, $this->postsRepo)->find($postId);
            $user  = $this->loadRepo($this->bundleName, $this->usersRepo)->find($userId);
            $owner = $this->loadRepo($this->bundleName, $this->usersRepo)->find($post->getUser()->getId());

            $this->winnerComments->setComments($comment);
            $this->winnerComments->setProjectOwner($owner);
            $this->winnerComments->setProjectId($post);
            $this->winnerComments->setProjectWinner($user);

            $em->persist($this->winnerComments);
            $em->flush();

            // Check the user must receive the email
            $winnerData = $this->loadRepo($this->bundleName, $this->offersRepo)->findBy(array('isActiveAuction' => 2,
                                                                                              'post' => $post->getId()));

            //Validate if the user is the same to the winner or not
            $userWinnerId    = $winnerData[0]->getUser()->getId();
            $emailSent       = '';
            $emailUsername   = '';
            $usernameComment = '';

            if ($userWinnerId == $user->getId()) {
                $emailSent       = $owner->getEmail();
                $emailUsername   = $owner->getUsername();
                $usernameComment = $winnerData[0]->getUser()->getUsername();
            } else {
                $emailSent       = $winnerData[0]->getUser()->getEmail();
                $emailUsername   = $winnerData[0]->getUser()->getUsername();
                $usernameComment = $owner->getUsername();
            }

            // Add email data
            $emailArray = array(
                'userEmail'   => $emailSent,
                'nameuser'    => $emailUsername,
                'subject'     => 'New Comment',
                'userComment' => $emailComment,
                'commentDone' => $comment,
                'projectname' => $post->getPostName(),
                'supportTeam' => $this->emailSupport,
            );

            $responseEmail = $this->__sendEmail('winnerComment', $emailArray);

            if ($responseEmail == 0) {
                // Put the text to write in the file log
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIWinnerComments',
                    'function'      => 'saveWinnerComment',
                    'action'        => 'saveWinnerCommentsEmail',
                    'customMessage' => 'Error: can not send email to users',
                    'message'       => 'The email was not sent to the users',
                );
                //$this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'Error with the sending email. Please contact support using contact form for receiving the email and activate the account',
                    'id'      => 0,
                ), RestResponse::RESP_ERROR);
            }

            // Generate response
            return $this->generateResponse(array(
                'message' => 'The comment has been saved successfully!',
                'id'      => $this->winnerComments->getId(),
            ), RestResponse::RESP_OK);
        } catch (\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIWinnerComments',
                'function'      => 'saveWinnerComment',
                'action'        => 'saveWinnerCommentsUsers',
                'customMessage' => 'Error: can not save winner comments',
                'message'       => $e->getMessage(),
            );
            //$this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'Application Error! Please contact support team!',
                'id'      => 0,
                'error'   => $e->getMessage(),
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Method will be used to get all the information of the comments
     * has been posted by the user once the projects has been tagged as
     * closed because has selected a winner
     *
     * @param integer $id ProjectId
     * @return response $json
     */
    public function getWinnerCommentsAction($id) {
        try {
            $arrayWinner      = array();
            $arrayWinnerValue = array();
            $this->winnerComments = $this->loadRepo($this->bundleName, $this->winnerCommentsRepo)->findBy(array('projectId' => 2));
            $offers = $this->loadRepo($this->bundleName, $this->offersRepo)->getOwnerAndWinnerId($id);

            $arrayWinnerValue = array(
                'ownerId'  => $offers[0]['user_id'],
                'winnerId' => $offers[0]['bidder_id']
            );

            foreach ($this->winnerComments as $key => $value) {
                $arrayWinner[] = array(
                    'id_winner'     => $value->getId(),
                    'comments'      => $value->getComments(),
                    'projectOwner'  => $value->getProjectOwner()->getUsername(),
                    'projectPost'   => $value->getProjectId()->getId(),
                    'projectWinner' => $value->getProjectWinner()->getUsername(),
                    'dateCreates'   => $this->__formatDate($value->getDateCreated()->format('Y-m-d')),
                );
            }

            return $this->generateResponse(array(
                'message' => 'The data has been recovery successfully!',
                'data'    => $arrayWinner,
                'users'   => $arrayWinnerValue,
            ), RestResponse::RESP_OK);
        } catch (\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIWinnerComments',
                'function'      => 'getWinnerComments',
                'action'        => 'getAllWinnerComments',
                'customMessage' => 'Error: can not get winner comments',
                'message'       => $e->getMessage(),
            );
            //$this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'Application Error! Please contact support team',
                'id'      => 0,
                'error'   => $e->getMessage()
            ), RestResponse::RESP_ERROR);
        }
    }
}
