<?php

namespace APIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use APIBundle\APILibs\APILib;
use APIBundle\APILibs\RestResponse;
use APIBundle\Entity\DynamicData;
use APIBundle\Entity\ReportFakeProjects;
use APIBundle\Entity\Posts;
use APIBundle\Entity\Users;
use APIBundle\Entity\JobsCategories;
use APIBundle\Entity\Cities;
use RegisterErrorBundle\Controller\RegisterErrorController;

/**
 * Class that contains all the methods related to the
 * report of the users done for fake projects, place
 * where the users will report fake projects
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Controller\APIReportFakeProjects
 */
class APIReportFakeProjectsController extends APILib {

    /**
     * Variable will contain the name of the repo
     *
     * @var object | NULL
     */
    protected $repoFake;

    /**
     * Variable will contain the name of the repo for posts
     *
     * @var object | NULL
     */
    protected $repoPosts;

    /**
     * Variable will contain the name of the repo for users
     *
     * @var object | NULL
     */
    protected $repoUsers;

    /**
     * Variable will contain the name of the repo for cities
     *
     * @var object | NULL
     */
    protected $repoCity;

    /**
     * Variable will contain the name of the repo for jobs categories
     *
     * @var object | NULL
     */
    protected $repoCategory;

    /**
     * Variable will contain the entity name object
     *
     * @var object | NULL
     */
    protected $fake;

    /**
     * Variable will contain the entity name for posts
     *
     * @var object | NULL
     */
    protected $posts;

    /**
     * Variable will contain the entity name for users
     *
     * @var object | NULL
     */
    protected $users;

    /**
     * Variable will contain tghe entity name of the city
     *
     * @var object | NULL
     */
    protected $cities;

    /**
     * Variable will contain tghe entity name of the jobs categories
     *
     * @var object | NULL
     */
    protected $categories;

    /**
     * Constructor ....
     */
    public function __construct() {
        $this->fake         = new ReportFakeProjects();
        $this->posts        = new Posts();
        $this->users        = new Users();
        $this->cities       = new Cities();
        $this->categories   = new JobsCategories();
        $this->repoFake     = "ReportFakeProjects";
        $this->repoUsers    = "Users";
        $this->repoPosts    = "Posts";
        $this->repoCity     = "Cities";
        $this->repoCategory = "JobsCategories";
    }

    /**
     * Method will be used to report fake projects that the
     * user could report if they think the job is fake
     *
     * @return response $json
     */
    public function createReportFakeJobAction() {
        try {
            $fakeData = json_decode(file_get_contents('php://input'), true);
            $em       = $this->getDoctrine()->getManager();

            $source = $fakeData['source'];
            $token  = $fakeData['token'];
            $nameT  = $fakeData['nameT'];

            // Check if token is valid
            $validToken = $this->isCsrfTokenValid($nameT, $token);

            if ($validToken == 0) {
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIReportFakeProjects',
                    'function'      => 'createReportFakeJob',
                    'action'        => 'createReportFakeProject',
                    'customMessage' => 'Error: csrf token is not valid',
                    'message'       => 'Error at the moment to check csrf valid token',
                );
                $this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'Application Error!, Please contact support or try again!',
                    'id'      => '0'
                ), RestResponse::RESP_ERROR);
            }

            $this->posts = $this->loadRepo($this->bundleName, $this->repoPosts)->find($fakeData['projectId']);
            $this->users = $this->loadRepo($this->bundleName, $this->repoUsers)->find($fakeData['userId']);

            // Save new record
            $this->fake->setReportedUserId($this->users);
            $this->fake->setReportedFakeProject($this->posts);
            $em->persist($this->fake);
            $em->flush();

            // Send email
            $emailAdmin = array(
                'userEmail'   => 'ruben.alonso21@gmail.com',
                'subject'     => 'Fake Project Report',
                'users'       => $this->users,
                'posts'       => $this->posts,
                'supportTeam' => $this->emailSupport,
            );

            $email = array(
                'userEmail'   => $this->posts->getUser()->getEmail(),
                'nameuser'    => $this->posts->getUser()->getName(),
                'username'    => $this->posts->getUser()->getUsername(),
                'subject'     => 'Fake Project Report',
                'supportTeam' => $this->emailSupport,
                'posts'       => $this->posts,
                'users'       => $this->users,
            );

            $responseEmail1 = $this->__sendEmail('fakeProject', $emailAdmin);
            $responseEmail2 = $this->__sendEmail('fakeProjectsUser', $email);

            if ($responseEmail1 == 0 || $responseEmail2 == 0) {
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIReportFakeProjects',
                    'function'      => 'createReportFakeJob',
                    'action'        => 'createReportFakeProjects',
                    'customMessage' => 'Error: messages have not been sent',
                    'message'       => 'The messages have not been sent to users',
                );
                $this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'Error with the sending email. Please contact support using contact form for receiving the email and activate the account',
                    'id'      => 0,
                ), RestResponse::RESP_ERROR);
            }

            return $this->generateResponse(array(
                'message' => 'The project has been reported as fake',
                'id'      => $this->fake->getId()
            ), RestResponse::RESP_OK);
        } catch(\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIReportFakeProjects',
                'function'      => 'createReportFakeJob',
                'action'        => 'createReportFakeProjects',
                'customMessage' => 'Error: can not create fake projects',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);


            return $this->generateResponse(array(
                'message' => 'Application Error. Please contact support!',
                'id'      => 0,
                'e'       => $e->getMessage()
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Get the data will be recovery from fake projects table and
     * should be displayed on the admin panel
     *
     * @return reponse $json
     */
    public function getReportsAction() {
        try {
            $results    = $this->loadRepo($this->bundleName, $this->repoFake)->getAllFakeProjects();
            $arrayRD    = array();
            $usersInner = array();
            $postsInner = array();

            foreach ($results as $key => $value) {
                $projects       = $this->loadRepo($this->bundleName, $this->repoPosts)
                                       ->getPostsById($value->getReportedFakeProject()->getId());
                $usersInProject = $this->loadRepo($this->bundleName, $this->repoFake)
                                       ->getUsersPerProject($value->getReportedFakeProject()->getId());
                $cities         = $this->loadRepo($this->bundleName, $this->repoCity)->find($projects[0]['city_id']);
                $category       = $this->loadRepo($this->bundleName, $this->repoCategory)->find($projects[0]['category_job_id']);
                $userBy         = $this->loadRepo($this->bundleName, $this->repoUsers)->find($projects[0]['user_id']);

                foreach ($usersInProject as $key1 => $value1) {
                    $innerUser = $this->loadRepo($this->bundleName, $this->repoUsers)->find($value1['REPORTED_USER_ID']);

                    $usersInner[$key1] = array(
                        'userId'   => $innerUser->getId(),
                        'name'     => $innerUser->getName(),
                        'email'    => $innerUser->getEmail(),
                        'username' => $innerUser->getUsername(),
                        'typeUser' => ($innerUser->getTypeUser() == true) ? "Owner" : "Contractor"
                    );
                }

                $postsInner[$key] = array(
                    'fakeProject'   => $value->getReportedFakeProject()->getId(),
                    'id'            => $projects[0]['id_post'],
                    'name'          => $projects[0]['post_name'],
                    'desc'          => $projects[0]['description'],
                    'city_id'       => $projects[0]['city_id'],
                    'city_name'     => $cities->getNameCity(),
                    'category_id'   => $projects[0]['category_job_id'],
                    'category_name' => $category->getNameJobsCategories(),
                    'user_id'       => $projects[0]['user_id'],
                    'userName'      => $userBy->getUsername(),
                    'nameOfUse'     => $userBy->getName(),
                    'email'         => $userBy->getEmail(),
                    'users'         => $usersInner,
                    'created'       => $projects[0]['date_created'],
                    'updated'       => $projects[0]['date_updated']
                );
            }

            $arrayRD['projects'] = array(
                'posts' => $postsInner
            );

            return $this->generateResponse(array(
                'message' => 'The data has been recovery',
                'data'    => $arrayRD
            ), RestResponse::RESP_OK);
        } catch(\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIReportFakeProjects',
                'function'      => 'getReports',
                'action'        => 'getProjectsReported',
                'customMessage' => 'Error: can not report fake project',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'Application Error. Please contact support!',
                'id'      => 0,
                'error'   => $e->getMessage(),
            ), RestResponse::RESP_ERROR);
        }
    }
}
