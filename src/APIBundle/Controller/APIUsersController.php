<?php

namespace APIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Config\Definition\Exception\Exception;
use RegisterErrorBundle\Controller\RegisterErrorController;
use APIBundle\APILibs\APILib;
use APIBundle\APILibs\RestResponse;
use APIBundle\Entity\Users;
use APIBundle\Entity\UsersInformation;
use APIBundle\Entity\UsersResetPassword;
use Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle;

/**
 * API Controller will be used to get all the information
 * related with the users, this will contains all the
 * data we'll be used to display or validate different data
 * in the platform
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Controller\APIUsersController
 */
class APIUsersController extends APILib {
    // test
    protected $logFileData = null;

    /**
     * Variable to store the users entity
     *
     * @var object | NULL
     */
    protected $users = NULL;

    /**
     * Variable to store the users information entity
     *
     * @var object | NULL
     */
    protected $usersInformation = NULL;

    /**
     * Variable to store the users reset password entity
     *
     * @var object | NULL
     */
    protected $usersResetPass = NULL;

    /**
     * Variable to contain the name of the repo and entity
     *
     * @var string
     */
    protected $repositoryName = "";

    /**
     * Variable to contain the name of the entity users information
     *
     * @var string
     */
    protected $repositoryName2 = "";

    /**
     * Variables to contain the name of the entity users reset password
     *
     * @var string
     */
    protected $repositoryName3 = "";

    /**
     * Variable will contain the main path for saving the profile pic
     *
     * @var string
     */
    protected $pathProfilePic = "images/imageSites/profiles";

    /**
     * Constructor
     */
    public function __construct() {
        $this->users            = new Users();
        $this->usersInformation = new UsersInformation();
        $this->UsersResetPassw  = new UsersResetPassword();
        $this->repositoryName   = "Users";
        $this->repositoryName2  = "UsersInformation";
        $this->repositoryName3  = "UsersResetPassword";
        $this->logFileData      = new RegisterErrorController();
    }

    /**
     * Method will be used to activate
     * the account that has not been activated
     * because the account still has not been confirmed
     *
     * @param string $token
     */
    public function confirmAccountAction($token) {
        try {
            $tokenValid = $this->loadRepo($this->bundleName, $this->repositoryName)->findTokenValid($token);
            //var_dump($tokenValid);
            $isConfirm = $tokenValid[0]->getIsConfirmToken();

            if ($isConfirm == 0 || $isConfirm == '0') {
                // get the is of the users and activate the account
                $this->users = $this->loadRepo($this->bundleName, $this->repositoryName)->find($tokenValid[0]->getId());
                $em          = $this->getDoctrine()->getManager();

                $this->users->setIsConfirmToken(1);
                $em->persist($this->users);
                $em->flush();

                return $this->redirect($this->generateUrl('confirm_account_user', array('validMessage' => 'success')));
                /*return $this->generateResponse(array(
                    'message' => 'The account has been activated successfully.',
                    'token'   => $this->users->getId()
                ), RestResponse::RESP_OK);*/
            } else {
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIUsersController',
                    'function'      => 'confirmAccount',
                    'action'        => 'ConfirmCreateAccount',
                    'customMessage' => 'Error: token is not valid',
                    'message'       => 'The token sent is not valid',
                );
                $this->logFileData->accessPoint($logMessage);

                return $this->redirect($this->generateUrl('confirm_account_user', array('validMessage' => 'error')));
                /*return $this->generateResponse(array(
                    'message' => 'The token is being used is not valid. It could already have been used to activate your account.',
                    'token'   => 'undefined'
                ), RestResponse::RESP_ERROR);*/
            }
        } catch(\Exception $e) {
            // write the error in the log custom file
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIUsersController',
                'function'      => 'confirmAccount',
                'action'        => 'ConfirmCreateAccount',
                'customMessage' => 'Error: confirm account when register new user',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->redirect($this->generateUrl('confirm_account_user', array('validMessage' => 'error')));
        }
    }

    /**
     * Method will be used to get all the information
     * will be used to register the new users that
     * wants to create an account
     *
     * @return response $json
     */
    public function saveAction() {
        try {
            $usersData = json_decode(file_get_contents('php://input'), true);

            $name        = $usersData['name'];
            $phone       = $usersData['phone'];
            $email       = $usersData['email'];
            $password    = $usersData['password'];
            $token       = $usersData['token'];
            $nameT       = $usersData['nameT'];
            $source      = $usersData['source'];
            $userID      = $usersData['user'];
            $typeUser    = $usersData['type_user'];
            $bbbAccredit = (isset($usersData['bbb_accredited'])) ? $usersData['bbb_accredited'] : "";
            $raiting     = (isset($usersData['raiting'])) ? $usersData['raiting'] : "";
            $wcbNumber   = (isset($usersData['wcb_number']) && !empty($usersData['wcb_number'])) ? $usersData['wcb_number'] : "";

            // Check token is valid
            $validToken = $this->isCsrfTokenValid($nameT, $token);

            if ($validToken == 0) {
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIUsersController',
                    'function'      => 'save',
                    'action'        => 'saveUsers',
                    'customMessage' => 'Error: can not check the csrf valid token',
                    'message'       => 'Can not check the csrf valid token',
                );
                $this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'Application Error!, Please contact support or try again!',
                    'id'      => '0'
                ), RestResponse::RESP_ERROR);
            }

            // Validate Users
            if ($this->__checkEmailValidation($email) > 0) {
                return $this->generateResponse(array(
                    'message' => 'The email you are using to create the account already exists!',
                    'id'      => 0
                ), RestResponse::RESP_ERROR);
            }

            // Encode users password
            $encoder   = $this->container->get('security.password_encoder');
            $em        = $this->getDoctrine()->getManager();

            // Get all the information to save in the database
            $encoded     = $encoder->encodePassword($this->users, $password);
            $activeToken = $this->__generateActivationToken($name, $email, $encoded);

            // Set empty value if the flag = 0
            if ($bbbAccredit == 0 || $bbbAccredit == '0') {
                $raiting = '';
            }

            $this->users->setName($name);
            $this->users->setEmail($email);
            $this->users->setUsername("TBD");
            $this->users->setPassword($encoded);
            $this->users->setIsActive(2);
            $this->users->setTypeUser($typeUser);
            $this->users->setIsAdmin(0);
            $this->users->setActivationToken($activeToken);
            $this->users->setIsConfirmToken(0);
            $this->users->setEmailBase($email);

            $em->persist($this->users);
            $em->flush();

            // Insert data in Users's Information table
            $this->usersInformation->setPhone($phone);
            if ($bbbAccredit != "")
                $this->usersInformation->setBBBAccredited($bbbAccredit);
                $this->usersInformation->setRaiting($raiting);
            if ($wcbNumber != "")
                $this->usersInformation->setWCBNumber($wcbNumber);
            $this->usersInformation->setUserId($this->users);

            $em->persist($this->usersInformation);
            $em->flush();

            // Update UserName
            $usersId = $this->users->getId();
            $this->users = $this->loadRepo($this->bundleName, $this->repositoryName)->find($usersId);

            $username = $this->generateFinalUsername($usersId);
            $this->users->setUsername(strtoupper($username));
            $this->users->setDateUpdated(new \DateTime('now'));

            $em->persist($this->users);
            $em->flush();


            // Send Email
            $arrayEmail = array(
                'userEmail'   => $email,
                'nameuser'    => $name,
                'subject'     => 'Create New Account',
                'userphone'   => $phone,
                'username'    => $username,
                'tokenUI'     => $activeToken,
                'url'         => $this->baseUrl . $this->generateUrl('api_confirm_users_account', array('token' => $activeToken)),
                'supportTeam' => $this->emailSupport
            );

            $responseEmail = $this->__sendEmail('new_account', $arrayEmail);

            if ($responseEmail == 0) {
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIUsersController',
                    'function'      => 'save',
                    'action'        => 'saveUsers',
                    'customMessage' => 'Error: can not send the email to the users',
                    'message'       => 'Was not send the email to the users',
                );
                $this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'Error with the sending email. Please contact support using contact form for receiving the email and activate the account',
                    'id'      => 0,
                ), RestResponse::RESP_ERROR);
            }

            return $this->generateResponse(array(
                'message' => "The user's account has been created successfully.",
                'id'      => $this->users->getId()
            ), RestResponse::RESP_OK);
        } catch (\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIUsersController',
                'function'      => 'save',
                'action'        => 'saveUsers',
                'customMessage' => 'Error: save new register user',
                'message'       => $e->getMessage(),
            );

            return $this->generateResponse(array(
                'message' => "There was an error to create the user's account",
                'id'      => 0,
                'error'   => $e->getMessage(),
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Method will be used to update the user's information
     * is not filled in the register of the new account, when the
     * user is logged in for the first time, the information must
     * be filled in
     *
     * @return response $json
     */
    public function updateUsersAction($id) {
        try {
            $fromPost = json_decode(file_get_contents('php://input'), true);
            $em       = $this->getDoctrine()->getManager();

            // Set data in post variables
            $name          = $fromPost['name'];
            $email         = $fromPost['email'];
            $paymentMethod = $fromPost['paymentMethod'];
            $bbbAccount    = $fromPost['bbbAccount'];
            $wcbNumber     = $fromPost['wcb_number'];
            $bbbAccredited = $fromPost['bbb_accredited'];
            $raiting       = $fromPost['raiting'];
            $address       = $fromPost['address'];
            $phone         = $fromPost['phone'];
            $profilePic    = $fromPost['profile_pic'];
            $token         = $fromPost['token'];
            $nameT         = $fromPost['nameT'];

            // Check token is valid
            $validToken = $this->isCsrfTokenValid($nameT, $token);

            if ($validToken == 0) {
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIUsersController',
                    'function'      => 'updateUsers',
                    'action'        => 'updateUserInformation',
                    'customMessage' => 'Error: can not check the csrf valid token',
                    'message'       => 'Can not check the csrf valid token',
                );
                $this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'Application Error!, Please contact support or try again!',
                    'id'      => '0'
                ), RestResponse::RESP_ERROR);
            }

            // Update information from users
            $this->users = $this->loadRepo($this->bundleName, $this->repositoryName)->find($id);

            // Check if the flag is 0 or 1
            if ($bbbAccredited == 0 || $bbbAccredited == '0') {
                $raiting = '';
            }

            $this->users->setName($name);
            $this->users->setEmail($email);

            $em->persist($this->users);
            $em->flush();

            // Update information from users information
            $personalInformation    = $this->loadRepo($this->bundleName, $this->repositoryName2)->findBy(array('user' => $id));
            $this->usersInformation = $this->loadRepo($this->bundleName, $this->repositoryName2)->find($personalInformation[0]->getId());

            $this->usersInformation->setPyamentMethod($paymentMethod);
            $this->usersInformation->setBBBAccount($bbbAccount);
            $this->usersInformation->setPhone($phone);
            $this->usersInformation->setAddress($address);
            $this->usersInformation->setDateUpdated(new \DateTime('now'));

            // set the information in case the data has been updated on personal information section

            $em->persist($this->usersInformation);
            $em->flush();

            return $this->generateResponse(array(
                'message' => "The User's Information has been updated successfully.",
                'id'      => $this->usersInformation->getId()
            ), RestResponse::RESP_OK);
        } catch(\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIUsersController',
                'function'      => 'updateUsers',
                'action'        => 'updateUserInformation',
                'customMessage' => 'Error: update users information',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => "There was an error at the moment to update user's information",
                'id'      => $this->users->getId(),
                'error'   => $e->getMessage(),
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Method will be used to validate all the information
     * will be send to the user once fill in the contact
     * form. Also the same data will be send to the
     * support team
     *
     * @param response $json
     */
    public function sendContactMailAction() {
        try {
            $jsonData = json_decode(file_get_contents('php://input'), true);

            $subject  = $jsonData['subject'];
            $email    = $jsonData['email'];
            $comments = $jsonData['comments'];
            $phone    = $jsonData['phone'];
            $token    = $jsonData['token'];
            $nameT    = $jsonData['nameT'];

            // Check if token is valid
            $validToken = $this->isCsrfTokenValid($nameT, $token);

            if ($validToken == 0) {
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIUsersController',
                    'function'      => 'sendContactMail',
                    'action'        => 'sendMail',
                    'customMessage' => 'Error: problems at the moment to chelc the csrf token',
                    'message'       => 'Error at the moment to check csrf valid token',
                );
                $this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'Application Error!, Please contact support or try again!',
                    'id'      => '0'
                ), RestResponse::RESP_ERROR);
            }

            $arrayDataContact = array(
                'subject'     => 'Contact Form - ' . $subject,
                'subjects'    => $subject,
                'emailU'      => $email,
                'userEmail'   => 'ruben.alonso21@gmail.com',
                'comments'    => $comments,
                'phone'       => $phone,
                'supportTeam' => $this->emailSupport
            );

            $arrayDataContacts2 = array(
                'subject'     => "Contact Form - " .$subject,
                'subjects'    => $subject,
                'userEmail'   => $email,
                'comments'    => $comments,
                'phone'       => $phone,
                'supportTeam' => $this->emailSupport,
            );

            $responseEmail2 = $this->__sendEmail('success_contact', $arrayDataContacts2);
            $responseEmail1 = $this->__sendEmail('contacts', $arrayDataContact);

            if ($responseEmail1 == 0 && $responseEmail2 == 0) {
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIUsersController',
                    'function'      => 'sendContactMail',
                    'action'        => 'sendMail',
                    'customMessage' => 'Error: messages have not been sent',
                    'message'       => 'The messages have not been sent to users',
                );
                $this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'Application error! The message has not been sent. Please contact support team.',
                    'id'      => 0
                ), RestResponse::RESP_ERROR);
            }

            return $this->generateResponse(array(
                'message' => 'Your comments has been sent successfully.',
                'id'      => 1
            ), RestResponse::RESP_OK);
        } catch (\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIUsersController',
                'function'      => 'sendContactMail',
                'action'        => 'sendMail',
                'customMessage' => 'Error: send contact mail',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'Application Error! Email has not been sent. Please contact support!',
                'id'      => 0,
                'error'   => $e->getMessage()
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Method will be used to get the users information that
     * is going to be displayed and give the user the opportunity
     * to update it
     *
     * @param integer $id
     * @return response json
     */
    public function getUsersAction($id) {
        try {
            $data = $this->loadRepo($this->bundleName, $this->repositoryName2)->getUserInformationById($id);

            return $this->generateResponse(array(
                'id'             => $data['id'],
                'name'           => $data['name'],
                'email'          => $data['email'],
                'username'       => $data['username'],
                'is_active'      => $data['is_active'],
                'type_user'      => $data['type_user'],
                'payment_method' => $data['payment_method'],
                'bbb_account'    => $data['bbb_account'],
                'bbb_accredited' => $data['bbb_accredited'],
                'raiting'        => $data['raiting'],
                'wcb_number'     => $data['wcb_number'],
                'address'        => $data['address'],
                'phone'          => $data['phone'],
                'date_created'   => $data['date_created'],
                'date_updated'   => $data['date_updated']
            ), RestResponse::RESP_OK);

        } catch (\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIUsersController',
                'function'      => 'getUsers',
                'action'        => 'getUsersData',
                'customMessage' => 'Error: can not get users',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'error'          => "Error at the moment of get User details"
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Update the password if the user doesn't remember
     * the password then the user will be set the new password
     * once confirm both passwords
     *
     * @param string $validToken
     * @return response $json
     */
    public function updatePasswordAction($validToken) {
        try {
            $valid = json_decode(file_get_contents('php://input'), true);

            $source    = $valid['source'];
            $token     = $valid['token'];
            $nameT     = $valid['nameToken'];
            $password  = $valid['password'];
            $cPassword = $valid['c_password'];
            $total     = $this->loadRepo($this->bundleName, $this->repositoryName3)->checkTokenIsValid($validToken);

            // Check if token is valid
            $validToken = $this->isCsrfTokenValid($nameT, $token);

            if ($validToken == 0) {
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIUsersController',
                    'function'      => 'updatePassword',
                    'action'        => 'updatePasswordUser',
                    'customMessage' => 'Error: can not check the csrf valid token',
                    'message'       => 'Can not check the csrf valid token',
                );
                $this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'Application Error!, Please contact support or try again!',
                    'id'      => '0'
                ), RestResponse::RESP_ERROR);
            }

            if ($total == 1) {
                $tokenData   = $this->loadRepo($this->bundleName, $this->repositoryName3)->getResetPasswordData($validToken);
                $tokenUpt    = $this->loadRepo($this->bundleName, $this->repositoryName3)->find($tokenData[0]->getId());
                $usersData   = $this->loadRepo($this->bundleName, $this->repositoryName)->findById($tokenData[0]->getUser()->getId());
                $em          = $this->getDoctrine()->getManager();
                $encoder     = $this->container->get('security.password_encoder');
                $newPassword = $encoder->encodePassword($this->users, $password);

                // Update status in Reset Password table
                $tokenUpt->setIsActive(0);
                $em->flush();

                // Update Password in Users Table
                $usersData[0]->setPassword($newPassword);
                $em->flush();

                // Send Email
                $arrayEmail = array(
                    'userEmail'   => $usersData[0]->getEmail(),
                    'nameuser'    => $usersData[0]->getName(),
                    'username'    => $usersData[0]->getUsername(),
                    'subject'     => 'Password Reset Successfully',
                    'url'         => $this->baseUrl . $this->generateUrl('login'),
                    'supportTeam' => $this->emailSupport
                );

                $responseEmail = $this->__sendEmail('password_updated', $arrayEmail);

                if ($responseEmail == 0) {
                    $logMessage = array(
                        'date'          => Date('Y-m-d H:i:s'),
                        'module'        => 'APIUsersController',
                        'function'      => 'updatePassword',
                        'action'        => 'updatePasswordUser',
                        'customMessage' => 'Error: can not send the email to the users',
                        'message'       => 'Was not send the email to the users',
                    );
                    $this->logFileData->accessPoint($logMessage);

                    return $this->generateResponse(array(
                        'message' => 'Error with the sending email. Please contact support using contact form for receiving the email and activate the account',
                        'id'      => 0,
                    ), RestResponse::RESP_ERROR);
                }

                return $this->generateResponse(array(
                    'message' => 'Password changed successfully',
                    'id'      => $tokenUpt->getId()
                ), RestResponse::RESP_OK);
            } else {
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIUsersController',
                    'function'      => 'updatePassword',
                    'action'        => 'updatePasswordUser',
                    'customMessage' => 'Error: token is not valid',
                    'message'       => 'The token sent is not valid',
                );
                $this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'The token is not valid!',
                    'id'      => 0
                ), RestResponse::RESP_ERROR);
            }
        } catch (\Exception $e) {
            // Type the error in the custom log file
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIUsersController',
                'function'      => 'updatePassword',
                'action'        => 'updatePasswordUser',
                'customMessage' => 'Error: update users password',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'Application Error, please contact support',
                'id'      => 0,
                'error'   => $e->getMessage(),
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Send the email once the user wants to
     * reset the password but first he needs to check
     * if the email exists and then the user will receive
     * an email with the url needs for reset the password
     *
     * @return response $json
     */
    public function sendEmailResetAction() {
        try {
            $email        = json_decode(file_get_contents('php://input'), true);
            $isValidEmail = $this->loadRepo($this->bundleName, $this->repositoryName)->findByEmail($email['email']);
            $token        = $email['token'];
            $nameT        = $email['nameToken'];

            // Check if token is valid
            $validToken = $this->isCsrfTokenValid($nameT, $token);

            if ($validToken == 0) {
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIUsersController',
                    'function'      => 'sendEmailReset',
                    'action'        => 'sendEmailResetUser',
                    'customMessage' => 'Error: can not check the csrf valid token',
                    'message'       => 'Can not check the csrf valid token',
                );
                $this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'Application Error!, Please contact support or try again!',
                    'id'      => '0'
                ), RestResponse::RESP_ERROR);
            }

            if (count($isValidEmail) == 1) {
                $em    = $this->getDoctrine()->getManager();
                $token = $this->__generateTokenChangePassword($email['email'],
                                                              $isValidEmail[0]->getName(),
                                                              $isValidEmail[0]->getDateCreated()->format('YmdHis'));

                $user    = $this->loadRepo($this->bundleName, $this->repositoryName)->find($isValidEmail[0]->getId());
                $userPwd = $this->loadRepo($this->bundleName, $this->repositoryName3)->checkEnableStatus($user->getId());

                if (count($userPwd) > 0) {
                    foreach ($userPwd as $key => $value) {
                        $value->setIsActive(0);
                    }
                    $em->flush();
                }

                // Update Email Base field
                if ($email['email'] != $user->getEmail()) {
                    $user->setEmailBase($email['email']);

                    $em->persist($users);
                    $em->flush();
                }

                $this->UsersResetPassw->setTokenReset($token);
                $this->UsersResetPassw->setIsActive(1);
                $this->UsersResetPassw->setUser($user);
                $this->UsersResetPassw->setUsersEmail($email['email']);

                $em->persist($this->UsersResetPassw);
                $em->flush();

                // Send Email
                $arrayEmail = array(
                    'userEmail'   => $email['email'],
                    'subject'     => 'Reset Password',
                    'tokenUI'     => $token,
                    'url'         => $this->baseUrl . $this->generateUrl('reset_pwd', array('validToken' => $token)),
                    'supportTeam' => $this->emailSupport
                );

                $responseEmail = $this->__sendEmail('reset_password', $arrayEmail);

                if ($responseEmail == 0) {
                    $logMessage = array(
                        'date'          => Date('Y-m-d H:i:s'),
                        'module'        => 'APIUsersController',
                        'function'      => 'sendEmailReset',
                        'action'        => 'sendResetEmailUsers',
                        'customMessage' => 'Error: can not send the email to the users',
                        'message'       => 'Was not send the email to the users',
                    );
                    $this->logFileData->accessPoint($logMessage);

                    return $this->generateResponse(array(
                        'message' => 'Error with the sending email. Please contact support using contacto form for receiving the email and activate the account',
                        'id'      => 0,
                    ), RestResponse::RESP_ERROR);
                }

                return $this->generateResponse(array(
                    'message' => 'You will receive an email to reset you password.',
                    'email'   => $email['email']
                ), RestResponse::RESP_OK);
            } else {
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIUsersController',
                    'function'      => 'sendEmailReset',
                    'action'        => 'sendResetEmailUsers',
                    'customMessage' => 'Error: email does not exists',
                    'message'       => 'Email does not exists on the database',
                );
                $this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'The email doesn\'t exists!',
                    'email'   => $email['email']
                ), RestResponse::RESP_ERROR);
            }
        } catch (\Exception $e) {
            // Put the text to write in custom log file
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIUsersController',
                'function'      => 'sendEmailReset',
                'action'        => 'sendResetEmailUsers',
                'customMessage' => 'Error: can not send email to reset it',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'There was an error, please contact support.',
                'email'   => '',
                'error'   => $e->getMessage()
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Verify if the username and the emails exists
     * in the database. If case exists, the username
     * and the emails has been registered
     *
     * @param string $name
     * @param string $email
     *
     * @return boolean
     */
    public function checkEmailAction() {
        try {
            $request = Request::createFromGlobals();

            $email = $request->request->get('email');
            $result = $this->loadRepo("APIBundle", $this->repositoryName)->findByEmail($email);

            if (count($result) > 0) {
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIUsersController',
                    'function'      => 'checkEmail',
                    'action'        => 'emailExists',
                    'customMessage' => 'Error: email already exists',
                    'message'       => 'Error at the moment to check email, email already exists.',
                );
                $this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'The Email already exists.',
                    'flag'    => 0
                ), RestResponse::RESP_OK);
            } else {
                $name = $request->request->get('first_name') . " " . $request->request->get('last_name');
                $totalInside = $this->loadRepo("APIBundle", $this->repositoryName)->findBy(array('name' => $name));

                if (count($totalInside) > 0) {
                    $logMessage = array(
                        'date'          => Date('Y-m-d H:i:s'),
                        'module'        => 'APIUsersController',
                        'function'      => 'checkEmail',
                        'action'        => 'checkEmailUsers',
                        'customMessage' => 'Error: name already exists',
                        'message'       => 'Error at the moment to check the email, name already exists',
                    );
                    $this->logFileData->accessPoint($logMessage);

                    return $this->generateResponse(array(
                        'message' => 'The name already exists',
                        'flag'    => 0
                    ), RestResponse::RESP_OK);
                }

                return $this->generateResponse(array(
                    'message' => 'OK',
                    'flag'    => 1
                ), RestResponse::RESP_OK);
            }
        } catch(\Exception $e) {
            // Put the text to write in the file log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIUsersController',
                'function'      => 'checkEmail',
                'action'        => 'checkEmailUsers',
                'customMessage' => 'Error: can not check the email',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => "There was an error in the platform.",
                'error'   => $e->getMessage(),
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Methdo is going to be used to get the
     * username generated automatically in the
     * platform with the data typed by the user
     *
     * @return string
     */
    private function generateFinalUsername($number) {
        $finalNumber = 0;

        if (strlen($number) < 5) {
            $finalNumber = "cb_" . str_pad($number, 5, '0', STR_PAD_LEFT);
        } else {
            $finalNumber = "cb_" . $number;
        }

        return $finalNumber;
    }

    /**
     * Generate token will be used at the moment the user
     * wants to change the password due to the user doesn't
     * remember the password
     *
     * @param string $email
     * @param string $name
     * @param string $dateCreated
     *
     * @return string
     */
    private function __generateTokenChangePassword($email, $name, $dateCreated) {
        try {
            $token = md5(date('dmYHis') . '_' . $email . '_' . $name . '_' . $dateCreated);

            return $token;
        } catch(\Exception $e) {
            // Write the error iun the custom error log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIUsersController',
                'function'      => '__generateTokenChangePassword',
                'action'        => 'generateTokenChangePasswordUser',
                'customMessage' => 'Error: can not generate token for change users password',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);
        }
    }

    /**
     * Validate if the email exists in the platform
     * to know if could continue the process. This method
     * will return the total of records that exists with the
     * same email
     *
     * @param string $email
     * @return integer
     */
    private function __checkEmailValidation($email) {
        $existsEmail = $this->loadRepo($this->bundleName, $this->repositoryName)->findByEmail($email);

        return count($existsEmail);
    }

    /**
     * Method will be used to generate the
     * activation token will be used for send the
     * email and then confirm the account
     *
     * @param string $name
     * @param string $email
     * @param string $password
     *
     * @return string
     */
    private function __generateActivationToken($name, $email, $password) {
        $curentDate   = new \DateTime('now');
        $randomNumber = rand();
        $stringToken  = $name . "_" . $randomNumber . "_activationToken_" . $email
                              . "_" . $curentDate->format('dmYHis') . "_" . $password;

        $md5Token = md5($stringToken);

        return $md5Token;
    }

}
