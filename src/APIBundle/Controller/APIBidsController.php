<?php

namespace APIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use APIBundle\APILibs\APILib;
use APIBundle\APILibs\RestResponse;
use APIBundle\Entity\Offers;
use APIBundle\Entity\Users;
use APIBundle\Entity\Posts;
use APIBundle\Entity\PaymentHistory;
use Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle;
use Symfony\Component\HttpFoundation\RedirectResponse;
use RegisterErrorBundle\Controller\RegisterErrorController;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\CreditCard;
use PayPal\Exception\PayPalConnectionException;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\CreditCardToken;
use PayPal\Api\Transaction;
use PayPal\Api\Payment;
use PayPal\Api\Payer;
use PayPal\Api\FundingInstrument;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;

/**
 * Controller will contain all the information about
 * the API related to the bids done by the user, it
 * doesn't matter if the data is related to end bids
 * or my bids, I mean current active bids
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Controller\APIBidsController
 */
class APIBidsController extends APILib {
    /**
     * Variable will contain the name of the repo for offers
     *
     * @var object | NULL
     */
    protected $repoOffers;

    /**
     * Variable will contain the name of the users repo
     *
     * @var object | NULL
     */
    protected $repoUsers;

    /**
     * Variable will contain the post repo
     *
     * @var object | NULL
     */
    protected $repoPost;

    /**
     * Variable will store the name of the repo for categories
     *
     * @var string
     */
    protected $categoryRepo;

    /**
     * Variable will store the name of the repo for cities
     *
     * @var string
     */
    protected $cityRepo;

    /**
     * Variable will contain the entity name for offers
     *
     * @var string
     */
    protected $offers;

    /**
     * Variable will contain the entity name for users
     *
     * @var string
     */
    protected $users;

    /**
     * Variable will contain the entity name for posts
     *
     * @var string
     */
    protected $posts;

    private $apiContext = null;

    /**
     * Constructor
     */
    public function __construct() {
        $this->offers       = new Offers();
        $this->users        = new Users();
        $this->posts        = new Posts();
        $this->repoOffers   = "Offers";
        $this->repoUsers    = "Users";
        $this->repoPost     = "Posts";
        $this->categoryRepo = "JobsCategories";
        $this->cityRepo     = "Cities";

        $this->apiContext = new ApiContext(
			new OAuthTokenCredential(
				'ATpQbrHs2rtQuJvfjfDQPjncHIusx0zJ3RtcDPruWKmdr-OaK_Ph8fxIkDAUHN4nB92en9kEjwHf8Rti',
				'EMYNMJg8KeEloTMCTC1LFw5w_gy8GV0s59wFBEmhYspfVSR07VHMhBcT0xmJlZbVUMYc1o3ipy7X8fvr'
			)
		);

		$this->apiContext->setConfig(
			array(
				'mode'           => 'sandbox',
                'log.LogEnabled' => true,
                'log.FileName'   => 'PayPal.log',
                'log.LogLevel'   => 'FINE'
			)
		);
	}

    /**
     * Method will be used to create the offers
     * to specific project
     *
     * @return response $json
     */
    public function createAction() {
        try {
            $bids   = json_decode(file_get_contents('php://input'), true);
            $source = $bids['source'];
            $token  = $bids['token'];
            $nameT  = $bids['nameT'];
            $em     = $this->getDoctrine()->getManager();

            // Check if token is valid
            $validToken = $this->isCsrfTokenValid($nameT, $token);

            if ($validToken == 0) {
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIBidsController',
                    'function'      => 'create',
                    'action'        => 'createBidPerUser',
                    'customMessage' => 'Error: problems at the moment to check the csrf token',
                    'message'       => 'Error at the moment to check csrf valid token',
                );
                $this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'Application Error!, Please contact support or try again!',
                    'id'      => '0'
                ), RestResponse::RESP_ERROR);
            }

            // get the objects for save in the table
            $user  = $this->loadRepo($this->bundleName, $this->repoUsers)->find($bids['userId']);
            $post  = $this->loadRepo($this->bundleName, $this->repoPost)->find($bids['postId']);
            $owner = $this->loadRepo($this->bundleName, $this->repoUsers)->find($post->getUser()->getId());

            // Save the offer
            $this->offers->setUser($user);
            $this->offers->setPost($post);
            $this->offers->setAuctionerId($owner);
            $this->offers->setMoneyBidder($bids['amount']);
            $em->persist($this->offers);
            $em->flush();

            // Send Email
            $arrayUser  = array(
                'subject'     => 'Bid done',
                'userEmail'   => $user->getEmail(),
                'user'        => $user->getUsername(),
                'projectName' => $post->getPostName(),
                'username'    => $owner->getUsername(),
                'category'    => $post->getCategory()->getNameJobsCategories(),
                'city'        => $post->getCity()->getNameCity(),
                'bids'        => $bids['amount'],
                'supportTeam' => $this->emailSupport,
            );
            $arrayOwner = array(
                'subject'     => 'Bid done!',
                'userEmail'   => $post->getUser()->getEmail(),
                'user'        => $post->getUser()->getUsername(),
                'projectname' => $post->getPostName(),
                'username'    => $user->getUsername(),
                'bids'        => $bids['amount'],
                'supportTeam' => $this->emailSupport,
            );

            $responseEmail1 = $this->__sendEmail('bids_user', $arrayUser);
            $responseEmail2 = $this->__sendEmail('bids', $arrayOwner);

            if ($responseEmail1 == 0 && $responseEmail2 == 0) {
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIBidsController',
                    'function'      => 'create',
                    'action'        => 'createBidPerUser',
                    'customMessage' => 'Error: messages have not been sent',
                    'message'       => 'The messages have not been sent to users',
                );
                $this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'Error with the sending email. Please contact support using contact form to fix the issue with emails!',
                    'id'      => 0,
                ), RestResponse::RESP_ERROR);
            }

            return $this->generateResponse(array(
                'message' => 'The offers has been done successfully.',
                'id'      => $this->offers->getId(),
            ), RestResponse::RESP_OK);
        } catch (\Exception $e) {
            // Add error un custom log file
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIBidsController',
                'function'      => 'create',
                'action'        => 'createBidPerUser',
                'customMessage' => 'Error: create bid in project per user',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'Application Error! Please contact support team!',
                'id'      => 0,
                'error'   => $e->getMessage()
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Method will be used to get all the information about the
     * bids active per user. This method will return the information
     * is currently enabled
     *
     * @param integer $id
     * @return response $json
     */
    public function getMyActiveBidsAction($id) {
        try {
            $bidsArray = array();
            $this->offers = $this->loadRepo($this->bundleName, $this->repoOffers)->getBiddersByUser($id);

            foreach ($this->offers as $key => $value) {
                $bidsArray = array();
            }

            return $this->generateResponse(array(
                'message' => "",
                'id'      => $id,
                'bids'    => $bidsArray
            ), RestResponse::RESP_OK);
        } catch (\Exception $e) {
            // write in custom log file
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIBidsController',
                'function'      => 'getMyActiveBids',
                'action'        => 'getAllMyActiveBids',
                'customMessage' => 'Error: can not get my active bids',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'Application Error!, please contact support',
                'data'    => 'undefined',
                'error'   => $e->getMessage(),
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Method will be enabled to get all the information about the
     * bids have finished in the platform and the user logged in
     * has participated on it
     *
     * @param integer $id
     * @return response $json
     */
    public function getMyEndBidsAction($id) {
        try {
            $bidsEnd = array();

            $this->offers = $this->loadRepo($this->bundleName, $this->repoOffers)->getFinishedBidsByUser($id);

            foreach ($this->offers as $key => $value) {
                $bidsEnd = array();
            }

            return $this->generateResponse(array(
                'message' => 'The bids finished has been returned',
                'id'      => $id,
                'bids'    => $bidsEnd
            ), RestResponse::RESP_OK);
        } catch (\Exception $e) {
            // write in custom log file
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIBidsController',
                'function'      => 'getMyEndBids',
                'action'        => 'getAllMyEndBids',
                'customMessage' => 'Error: can not get my end bids',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'Application Error!, please contact support',
                'data'    => 'undefined',
                'error'   => $e->getMessage(),
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Get all the offers for every user that has
     * accepted and has been done by other users
     *
     * @param integer $id
     * @return integer
     */
    public function getBidsEndedByUserAction($id) {
        try {
            $arrayOffers = array();
            $arrayUsers  = array();
            $userWinner  = 0;
            $offers      = $this->loadRepo($this->bundleName, $this->repoOffers)->getPostByUserOfferd($id);
            $count       = count($offers);

            foreach ($offers as $key => $value) {
                $posts    = $this->loadRepo($this->bundleName, $this->repoPost)->find($value['bidder_post']);
                $users    = $this->loadRepo($this->bundleName, $this->repoOffers)->getUsersOffersById($posts->getId(), array(0, 2));
                $category = $this->loadRepo($this->bundleName, $this->categoryRepo)->find($posts->getCategory()->getId());
                $city     = $this->loadRepo($this->bundleName, $this->cityRepo)->find($posts->getCity()->getId());

                foreach ($users as $keyI => $valueI) {
                    if ($valueI['is_active_auction'] == 2 && $valueI['id'] == $id) {
                        $userWinner = 1;
                    }

                    $arrayUsers[] = array(
                        'id_user'     => $valueI['id'],
                        'username'    => $valueI['username'],
                        'moneyBidder' => money_format("%i", $valueI['money_bidder']),
                        'status'      => (intval($valueI['is_active_auction']) == 2) ? 'Accepted' : 'Not Accepted',
                        'date'        => $this->__formatDate($valueI['date_created']),
                    );
                }

                $arrayOffers[] = array(
                    'id_offer'    => $value['id_offer'],
                    'id_post'     => $posts->getId(),
                    'postName'    => $posts->getPostName(),
                    'description' => $posts->getDescription(),
                    'category'    => $category->getNameJobsCategories(),
                    'city'        => $city->getNameCity(),
                    'is_winner'   => $userWinner,
                    'status'      => 'Finished',
                    'users'       => $arrayUsers,
                );
            }


            return $this->generateResponse(array(
                'message' => 'The information has been recovery successfully!',
                'counter' => $count,
                'data'    => $arrayOffers,
            ), RestResponse::RESP_OK);
        } catch (\Exception $e) {
            // write error in custom error log
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIBidsController',
                'function'      => 'getBidsEndedByUser',
                'action'        => 'getAllMyBidsEndedByUser',
                'customMessage' => 'Error: can not get my bids ended by user',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'Application Error!, Please contact support team!',
                'id'      => 0,
                'error'   => $e->getMessage(),
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Method will be used to get all the projects with the
     * bids done until the moment and still the bid is open
     * to do more offers to win the job
     *
     * @param integer $id
     * @return integer
     */
    public function getBidsOpenByUserAction($id) {
        try {
            $arrayOffers = array();
            $arrayUser   = array();
            $offers      = $this->loadRepo($this->bundleName, $this->repoOffers)->getPostsByUserOpenOffers($id);
            $count       = count($offers);

            foreach ($offers as $key => $value) {
                $post     = $this->loadRepo($this->bundleName, $this->repoPost)->find($value['bidder_post']);
                $user     = $this->loadRepo($this->bundleName, $this->repoOffers)->getUsersOffersById($post->getId(), array(1));
                $category = $this->loadRepo($this->bundleName, $this->categoryRepo)->find($post->getCategory()->getId());
                $city     = $this->loadRepo($this->bundleName, $this->cityRepo)->find($post->getCity()->getId());

                foreach($user as $keyU => $valueU) {
                    $arrayUser[] = array(
                        'id_user'     => $valueU['id'],
                        'username'    => $valueU['username'],
                        'moneyBidder' => money_format("%i", $valueU['money_bidder']),
                        'status'      => 'Open',
                        'date'        => $this->__formatDate($valueU['date_created']),
                    );
                }

                $arrayOffers[] = array(
                    'id_offer'    => $value['id_offer'],
                    'id_post'     => $post->getId(),
                    'postName'    => $post->getPostName(),
                    'description' => $post->getDescription(),
                    'city'        => $city->getNameCity(),
                    'category'    => $category->getNameJobsCategories(),
                    'status'      => 'Open',
                    'users'       => $arrayUser,
                );
            }

            return $this->generateResponse(array(
                'message' => 'The data has been recovery successfully!',
                'counter' => $count,
                'data'    => $arrayOffers,
            ), RestResponse::RESP_OK);
        } catch (\Exception $e) {
            // Write the issues in the custom log file
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIBidsController',
                'function'      => 'getBidsOpenByUser',
                'action'        => 'getAllMyBidsOpenByUser',
                'customMessage' => 'Error: can not get my bids open by user',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'Application Error! Please contact support team!',
                'id'      => 0,
                'error'   => $e->getMessage(),
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Method will be used to set the information
     * of the offer or bid has been approved and accepted
     * by the owner of the project
     *
     * @param response $json
     */
    public function acceptBidAction() {
        try {
            $response = json_decode(file_get_contents('php://input'), true);

            $post_id  = $response['postId'];
            $bid_id   = $response['offerId'];
            $source   = $response['source'];
            $token    = $response['token'];
            $nameT    = $response['nameT'];
            $em       = $this->getDoctrine()->getManager();
            $template = '';

            // Check if token is valid
            $validToken = $this->isCsrfTokenValid($nameT, $token);

            if ($validToken == 0) {
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIBidsController',
                    'function'      => 'acceptBid',
                    'action'        => 'acceptBidSelected',
                    'customMessage' => 'Error: problems at the moment to check the csrf token',
                    'message'       => 'Error at the moment to check csrf valid token',
                );
                $this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'Application Error!, Please contact support or try again!',
                    'id'      => '0'
                ), RestResponse::RESP_ERROR);
            }

            $this->offers = $this->loadRepo($this->bundleName, $this->repoOffers)->findBy(array('post' => $post_id));

            foreach ($this->offers as $key => $value) {
                $users = $this->loadRepo($this->bundleName, $this->repoUsers)->find($value->getUser()->getId());
                $posts = $this->loadRepo($this->bundleName, $this->repoPost)->find($value->getPost()->getId());

                if ($value->getId() != $bid_id) {
                    $value->setIsActiveAuction(0);

                    $em->persist($value);
                    $em->flush();

                    $template = "declineBids";
                    $array    = array('userEmail'   => $users->getEmail(),
                                      'nameuser'    => $users->getName(),
                                      'subject'     => 'Bid Declined',
                                      'username'    => $users->getUsername(),
                                      'bid'         => money_format("%i", $value->getMoneyBidder()),
                                      'projectname' => $posts->getPostName(),
                                      'supportTeam' => $this->emailSupport);
                } else {
                    $value->setIsActiveAuction(2);

                    $em->persist($value);
                    $em->flush();

                    $template = "acceptBids";
                    $array    = array('userEmail'   => $users->getEmail(),
                                      'nameuser'    => $users->getName(),
                                      'subject'     => 'Bid Accepted',
                                      'username'    => $users->getUsername(),
                                      'bid'         => money_format("%i", $value->getMoneyBidder()),
                                      'projectname' => $posts->getPostName(),
                                      'supportTeam' => $this->emailSupport);
                }

                $responseEmail = $this->__sendEmail($template, $array);
                if ($responseEmail == 0) {
                    $logMessage = array(
                        'date'          => Date('Y-m-d H:i:s'),
                        'module'        => 'APIBidsController',
                        'function'      => 'acceptBid',
                        'action'        => 'acceptBidSelected',
                        'customMessage' => 'Error: messages have not been sent',
                        'message'       => 'The messages have not been sent to users',
                    );
                    $this->logFileData->accessPoint($logMessage);

                    return $this->generateResponse(array(
                        'message' => 'Error at the moment to send email. Please contact support using contact form to fix this issue.',
                        'id'      => 0,
                    ), RestResponse::RESP_OK);
                }
            }

            $linkAccepted = $this->paypalLink($bid_id);
            $this->posts = $this->loadRepo($this->bundleName, $this->repoPost)->find($post_id);
            $this->posts->setPostStatus(0);

            $em->persist($this->posts);
            $em->flush();

            return $this->generateResponse(array(
                'message'    => 'You have accepted the offer',
                'id'         => $bid_id,
                'linkButton' => $linkAccepted,
            ), RestResponse::RESP_OK);
        } catch (\Exception $e) {
            // Write the issue in the log custom file
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIBidsController',
                'function'      => 'acceptBid',
                'action'        => 'acceptBidSelected',
                'customMessage' => 'Error: can not accept the bid selected',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'Application Error! Please contact support team',
                'id'      => 0,
                'error'   => $e->getMessage(),
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Method will be used to decline the bids
     * done by specific user, this action is going to
     * check what will decline the user owner of the project
     *
     * @param response $json
     */
    public function declineBidAction() {
        try {
            $response = json_decode(file_get_contents('php://input'), true);

            $offerId = $response['offerId'];
            $source  = $response['source'];
            $token   = $response['token'];
            $nameT   = $response['nameT'];
            $em      = $this->getDoctrine()->getManager();

            // Check if token is valid
            $validToken = $this->isCsrfTokenValid($nameT, $token);

            if ($validToken == 0) {
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIBidsController',
                    'function'      => 'declineBid',
                    'action'        => 'tokenInvalid',
                    'customMessage' => 'Error: problems at the moment to check the csrf token',
                    'message'       => 'Error at the moment to check csrf valid token',
                );
                $this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'Application Error!, Please contact support or try again!',
                    'id'      => '0'
                ), RestResponse::RESP_ERROR);
            }

            $this->offers = $this->loadRepo($this->bundleName, $this->repoOffers)->find($offerId);
            $this->offers->setIsActiveAuction(0);

            // Users data
            $users = $this->loadRepo($this->bundleName, $this->repoUsers)->find($this->offers->getUser()->getId());
            $posts = $this->loadRepo($this->bundleName, $this->repoPost)->find($this->offers->getPost()->getId());

            $em->persist($this->offers);
            $em->flush();

            // Send Email
            $array = array('userEmail'   => $users->getEmail(),
                           'nameuser'    => $users->getName(),
                           'subject'     => 'Bid Declined',
                           'username'    => $users->getUsername(),
                           'bid'         => money_format("%i", $this->offers->getMoneyBidder()),
                           'projectname' => $posts->getPostName());

            $responseEmail = $this->__sendEmail('declineBids', $array);

            if ($responseEmail == 0) {
                $logMessage = array(
                    'date'          => Date('Y-m-d H:i:s'),
                    'module'        => 'APIBidsController',
                    'function'      => 'declineBid',
                    'action'        => 'emailDeclineBid',
                    'customMessage' => 'Error: messages have not been sent',
                    'message'       => 'The messages have not been sent to users',
                );
                $this->logFileData->accessPoint($logMessage);

                return $this->generateResponse(array(
                    'message' => 'Error at the momento to send email. Please contact support using contact form to fix this issue.',
                    'id'      => 0,
                ), RestResponse::RESP_OK);
            }

            return $this->generateResponse(array(
                'message' => 'You have declined the offer',
                'id'      => $offerId,
            ), RestResponse::RESP_OK);
        } catch (\Exception $e) {
            // Write the issue in the log custom file
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIBidsController',
                'function'      => 'declineBid',
                'action'        => 'declineBidSelected',
                'customMessage' => 'Error: can not decline bid selected',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'Application Error! Please contact support team',
                'id'      => 0,
                'e'       => $e->getMessage(),
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Method will return the bids done in the platform
     * if the owner of the project still have not decided
     * what is the bid going to choose
     *
     * @param integer $postId
     * @return response $json
     */
    public function getBidsPerProjectAction($id) {
        try {
            $posts = array();
            $bids  = $this->loadRepo($this->bundleName, $this->repoOffers)->findBy(array('post' => $id));

            foreach ($bids as $key => $value) {
                $posts[] = array(
                    'id_post'  => $value->getId(),
                    'username' => $value->getUser()->getUsername(),
                    'email'    => $value->getUser()->getEmail(),
                    'isActive' => $value->getIsActiveAuction(),
                    'moneyBid' => money_format('%i', $value->getMoneyBidder()),
                    'link'     => $this->getLinkPaymentAction($value->getId(), 1),
                );
            }

            return $this->generateResponse(array(
                'message' => 'The post has been recovery successfully.',
                'posts'   => $posts,
                'id'      => $id,
            ), RestResponse::RESP_OK);
        } catch (\Exception $e) {
            // Write in custom log file
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIBidsController',
                'function'      => 'getBidsPerProject',
                'action'        => 'getAllBidsPerProject',
                'customMessage' => 'Error: Can not get bids per project',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return $this->generateResponse(array(
                'message' => 'Application Error! Please contact support for fixing this issue!',
                'id'      => 0,
            ), RestResponse::RESP_ERROR);
        }
    }

    /**
     * Get paypal link to do payments when the project
     * is pending to payment because the owner cancel the
     * payment process
     *
     * @param integer $idOffer
     * @param boolean $flag = 0
     *
     * @return string|json_response
     */
    public function getLinkPaymentAction($idOffer, $flag = 0) {
        try {
            $link = $this->paypalLink($idOffer);

            if ($flag == 1) {
                return $link;
            } else {
                return $this->generateResponse(array(
                    'message' => 'The data has been recovery successfully!',
                    'id'      => $idOffer,
                    'link'    => $link,
                ), RestResponse::RESP_OK);
            }
        } catch (\Exception $e) {
            return $this->generateResponse(array(
                'message' => 'Application Error! Please contact support team.',
                'id'      => 0,
            ), RestResponse::RESP_ERROR);
        }
    }


    /**
     * Test Paypal
     */
    private function paypalLink($id) {
        // get dynamic data
        $bidAccepted = $this->loadRepo($this->bundleName, $this->repoOffers)->find($id);
        $postName    = $this->loadRepo($this->bundleName, $this->repoPost)->find($bidAccepted->getPost());

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item = new Item();
        $item->setName($postName->getPostName())
             ->setCurrency("CAD")
             ->setQuantity(1)
             ->setSku($postName->getId())
             ->setPrice($bidAccepted->getMoneyBidder());

        $itemList = new ItemList();
        $itemList->setItems(array($item));

        $details = new Details();
        $details->setShipping(0)
                ->setTax(0)
                ->setSubtotal($bidAccepted->getMoneyBidder());

        $amount = new Amount();
        $amount->setCurrency("CAD")
               ->setTotal($bidAccepted->getMoneyBidder())
               ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
                    ->setItemList($itemList)
                    ->setDescription($postName->getPostName())
                    ->setInvoiceNumber(uniqid());

        $redirectUrl = new RedirectUrls();
        $redirectUrl->setReturnUrl("http://127.0.0.1/contractordepot/web/api/makePayment/" . $id)
                    ->setCancelUrl("http://127.0.0.1/contractordepot/web/api/cancelPayment/" . $id);

        $payments = new Payment();
        $payments->setIntent("sale")
                 ->setPayer($payer)
                 ->setRedirectUrls($redirectUrl)
                 ->setTransactions(array($transaction));

        try {
            $payments->create($this->apiContext);
            return $payments->getApprovalLink();
        } catch (PayPalConnectionException $e){
        }
    }

    /**
     * Method will be used once the user cancels the payments
     * of project for any reason, will be redirect to this
     * view where the user will displaye the message related to
     * cancel payments
     *
     * @return json $response
     */
    public function cancelPaymentsAction() {
        try {
            $projectId = explode("/", $_SERVER['REQUEST_URI']);
            $idProject = $projectId[count($projectId) - 2];
            $em        = $this->getDoctrine()->getManager();

            // Update Information
            $offer = $this->loadRepo($this->bundleName, $this->repoOffers)->find($idProject);
            $offer->setIsActiveAuction(3);
            $em->persist($offer);
            $em->flush();

            return new RedirectResponse($this->generateUrl('cancel_payment_project', array('idProject' => $idProject)));
        } catch (\Exception $e) {
            // Redirect to the error page
            $logMessage = array(
                'date'          => Date('Y-m-d H:i:s'),
                'module'        => 'APIBidsController',
                'function'      => 'cancelPayments',
                'action'        => 'cancelPaymentSelected',
                'customMessage' => 'Error: cancel payment selected',
                'message'       => $e->getMessage(),
            );
            $this->logFileData->accessPoint($logMessage);

            return new RedirectResponse($this->generateUrl('error_page_payments'));
        }
    }

    /**
     * Method will be used to set the values will be
     * sent to the payment platform for the user makes
     * the payment related to the bid has accepted
     */
    public function makePaymentAction() { //}$param) {
        if ($_GET == true) {
            // send success response
            $paymentId = $_GET["paymentId"];
            $token     = $_GET["token"];
            $payerId   = $_GET["PayerID"];
            $projectId = explode("/", $_SERVER['REQUEST_URI']);
            $idProject = $projectId[count($projectId) - 2];

            //print_r($_GET);
            //$id = $this->successPaymentAction($paymentId, $payerId, $idProject);
            $payments  = Payment::get($paymentId, $this->apiContext);
            $execution = new PaymentExecution();
            $execution->setPayerId($payerId);

            try {
                $em = $this->getDoctrine()->getManager();
                $paymentsHistory = new PaymentHistory();

                $payments->execute($execution, $this->apiContext);

                // Get data from users and posts
                $bids = $this->loadRepo($this->bundleName, $this->repoOffers)->find($idProject);
                $post = $this->loadRepo($this->bundleName, $this->repoPost)->find($bids->getPost()->getId());
                $user = $this->loadRepo($this->bundleName, $this->repoUsers)->find($bids->getAuctionerId()->getId());

                // Save data in payment history
                $paymentsHistory->setUser($user);
                $paymentsHistory->setPost($post);
                $paymentsHistory->setAmount($bids->getMoneyBidder());
                $paymentsHistory->setTransactionId($payments->getId());
                $paymentsHistory->setDatePayment($payments->getCreateTime());
                $paymentsHistory->setCart($payments->getCart());
                $paymentsHistory->setStatus($payments->getState());

                $em->persist($paymentsHistory);
                $em->flush();

                // Add email to the user
                $arrayEmail = array(
                    'userEmail'     => $user->getEmail(),
                    'nameuser'      => $user->getName(),
                    'subject'       => 'Payment Done - Project Won for you!',
                    'username'      => $user->getUsername(),
                    'usernameOwner' => $post->getUser()->getUsername(),
                    'nameuserOwner' => $post->getUser()->getName(),
                    'bid'           => money_format("%i", $bids->getMoneyBidder()),
                    'projectname'   => $post->getPostName(),
                    'supportTeam'   => $this->emailSupport,
                );

                $responseEmail = $this->__sendEmail("confirm_payments", $arrayEmail);

                // redirect to success payment
                return new RedirectResponse($this->generateUrl('success_payment_project', array('idProject' => $bids->getId())));
            } catch (PayPalConnectionException $ex) {
                return $this->generateResponse(array(
                    'message' => 'Error',
                    'error'   => $ex->getMessage(),
                ), RestResponse::RESP_ERROR);
            }
        } else {
            // send response error
            return new RedirectResponse($this->generateUrl('error_page_payments'));
        }
    }

    /**
     * Method will be used to get all the values returned by the
     * platform once the payment has been done successfully by the
     * user and is the return data of the payment platform
     */
/*    public function successPaymentAction($payment, $payer) {
        echo "holas";
        $payments = Payment::get($payment, $this->apiContext);

        $execution = new PaymentExecution();
        $execution->setPayerId($payer);

        try {
            $payments->execute($execution, $this->apiContext);
            //echo "payments: " . $payments->getPayer()['payer_info']['email'];
            //echo "<br /> transaction: " . $payments->getTransactions();
            echo "holas";
            var_dump($payments);
            die();
            //return $payments;
        } catch (PayPalConnectionException $e){
            return $this->generateResponse(array(
                'message' => $e->getData(),
                'id'      => 0,
            ), RestResponse::RESP_ERROR);
        }
    }*/
}
