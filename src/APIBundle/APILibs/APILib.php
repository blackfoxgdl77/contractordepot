<?php

namespace APIBundle\APILibs;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use APIBundle\APILibs\RestResponse;
use RegisterErrorBundle\Controller\RegisterErrorController;
use APIBundle\APILibs\CustomImages;

/**
 * Class will contain the main methods used
 * for all the API's that will be enabled
 * in common. These methods will be used to
 * avoid repeat code in every controller
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\APILibs\APILib
 */
class APILib extends Controller {

    const MINIMAL  = 'MINIMAL';
    const WARNING  = 'WARNING';
    const CRITICAL = 'CRITICAL';

    // Demo Variables
    //public $secretKey  = "EMYNMJg8KeEloTMCTC1LFw5w_gy8GV0s59wFBEmhYspfVSR07VHMhBcT0xmJlZbVUMYc1o3ipy7X8fvr";
    //public $clientID   = "ATpQbrHs2rtQuJvfjfDQPjncHIusx0zJ3RtcDPruWKmdr-OaK_Ph8fxIkDAUHN4nB92en9kEjwHf8Rti";
    //public $ApiContext = '';

    /**
     * Variable will be used to get base URL
     *
     * @var string
     */
    protected $baseURL = '';

    /**
     * Variable will contain the main path for upload files
     *
     * @var string | empty
     */
    protected $pathFile = "/bundles/site/images/";

    /**
     * Variable will contain the response
     *
     * @var object | NULL
     */
    public $response = NULL;

    /**
     * Variable will be used to generate base path
     *
     * @var string
     */
    public $baseUrl = 'www.ruvicdev.com';

    /**
     * Variable will contain the mail used for support issues
     *
     * @var string
     */
    public $emailSupport = 'support@contractorsbids.com';

    /**
     * Variable will contain error issues
     *
     * @var object | NULL
     */
    protected $registerError = NULL;

    /**
     * Variable will contain the object to manage all the image methods
     *
     * @var object | NULL
     */
    protected $customImages = NULL;

    /**
     * Name of the bundle to call the repository
     *
     * @var string
     */
    public $bundleName = "APIBundle";

    /**
     * Contructor....
     */
    public function __construct() {
        /*$this->ApiContext = new ApiContext(
            new OAuthTokenCredentials(
                $this->clientID,
                $this->secretKey
            )
        );

        $this->ApiContext->setConfig(
            array(
                'mode'           => 'sandbox',
                'log.LogEnabled' => true,
                'log.FileName'   => 'PayPal.log',
                'log.LogLevel'   => 'FINE'
            )
        );*/

        $this->__initAllMainClasses();
        $this->restartData();
    }

    /**
     * Method will be used to initialize all the
     * classes needed to use in all the classes
     * inherit from this parent class
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     */
    public function beforeAction() {
        $this->customImages = new CustomImages();
    }

    /**
     * Private method will be used to initialize all the
     * main functionality is going to be used in all the
     * controllers will inheritance from this class
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     */
    final public function __initAllMainClasses() {
        $this->response      = new RestResponse();
        $this->registerError = new RegisterErrorController();
    }

    /**
     * Method will be used to generate the response must be
     * used to return the data will be JSON format
     *
     * @param array $data
     * @param string $type
     * @param integer $code
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     */
    public function generateResponse($data, $type) {
        $this->response = new RestResponse();

        $inArray = array(
            RestResponse::RESP_OK,
            RestResponse::RESP_ERROR,
            RestResponse::RESP_UNKNOWN
        );

        if (!empty($data) && in_array($type, $inArray)) {
            $this->response->setContent(
                array(
                    'status' => $type,
                    'data'   => $data
                )
            );
        }

        return $this->response;
    }

    /**
     * Method will be used to load all the information
     * of the entity manager and just return the object
     * will be used to do some of CRUD operation
     *
     * @param string $bundleName
     * @param string $tableName
     *
     * @return object | NULL
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     */
    public function loadRepo($bundleName, $tableName) {
        $em = $this->getDoctrine(); //->getManager(); //getEntityManager();

        if(is_string($bundleName) && is_string($tableName)) {
            $repoName = $bundleName . ':' . $tableName;
        }

        return isset($repoName) ? $em->getRepository($repoName) : false;
    }

    /**
     * Method will be used to check what is the response depending
     * if the token is valid or not. This issue will be defined to know
     * if the response will be an error or keep doing normal proccess
     * depending on the action defined by the user
     *
     * @param string $source
     * @param string $token
     *
     * @return boolean
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     */
    final protected function __validateToken($source, $token) {
        if ($source == "web") {
            if ($this->isCsrfTokenValid('authenticate', $token)) {
                echo "holas";
            } else {
                echo "adios";
            }
        }
    }

    /**
     * Method will be used to generate an unique file name
     * that will be used for saving the information in the
     * database
     *
     * @return string
     */
    final public function __getFinalUniqueName() {
        return md5(uniqid());
    }

    /**
     * Method will be used for checking what is the path is going
     * to use for uploading the file that the user selects from
     * the form
     *
     * @param string $relativePath
     * @return string $fullPath
     */
    final protected function __setPathToUploadFile($relativePath) {
        if (!empty($relativePath)) {
            $fullPath = $this->get('kernel')->getRootDir() . "/../web" . $this->pathFile . $relativePath;
            return $fullPath;
        }

        return $this->get('kernel')->getRootDir() . "/../web" . $this->pathFile . "noFolderPath";
    }

    /**
     * Method will return the complete path to delete the images
     * are going to be deleted due to the new image is going to be
     * uploaded
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @return string $fullPath
     */
    final protected function __getCompletePath() {
        return $this->get('kernel')->getRootDir() . "/../web";
    }

    /**
     * Method will be used to get the response in json in every
     * request done by the user once he has started session
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     */
    final protected function getParamsJSON() {
    }

    /**
     * Method will be used to get the type of function should be called
     * once the user has uploaded the image to resize every image to
     * specific sizes
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     *
     * @param string $typeImg
     * @return string $nameFunction
     */
    final protected function getTypeFunctionImg($typeImg) {
        $nameFunction = "";

        // Check if the image has PNG extension
        if ($typeImg == 'png') {
            $nameFunction = "resizePNGImg";
        }

        // Check if the image has JPEG or JPG extension
        if ($typeImg == 'jpg' || $typeImg == 'jpeg') {
            $nameFunction = "resizeJPGImg";
        }

        // Check if the image has GIF extension
        if ($typeImg == 'gif') {
            $nameFunction = "resizeGIFImg";
        }

        // Check if the image has BMP extension
        if ($typeImg == "bmp") {
            $nameFunction = "resizeBMPImg";
        }

        return $nameFunction;
    }

    /**
     * Check if the file is image or not and with this result
     * and know if needs to be stored the file in the
     * resize folder
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     *
     * @param string $extension
     * @return boolean
     */
    final protected function __isImageFile($extension) {
        $arrayImages = array('png', 'jpg', 'jpeg', 'bmp', 'gif');
        $data = in_array($extension, $arrayImages);

        return ($data == 1) ? 1 : 0;
    }

    /**
     * Method is going to give a specific format of the
     * date once this method has been called. The format returned
     * will be Month Day, Year
     *
     * @param string date
     * @return string
     */
    final protected function __formatDate($date) {
        $month = date('F', strtotime($date));
        $day   = date('d', strtotime($date));
        $year  = date('Y', strtotime($date));

        $finalDate = $month . " " . $day. ", " . $year;

        return $finalDate;
    }

    /**
     * Method will be used to manage all the information
     * related with the mails in the platform and will be
     * sent to users wants to register
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     *
     * @param string $nameMethod
     * @param array $dataArray
     * @param string $ccArray
     *
     * @return integer $intVal
     */
    final protected function __sendEmail($nameMethod, $dataArray, $ccArray = null) {
        $from = array('testemail@ruvicdev.com' => 'noReply@ruvicdev.com');

        if (isset($dataArray['nameuser']) && $dataArray['nameuser'] != '') {
            $to   = array($dataArray['userEmail'] => $dataArray['nameuser']);
        } else {
            $to   = array($dataArray['userEmail'] => $dataArray['userEmail']);
        }

        $transport = \Swift_SmtpTransport::newInstance('mail.ruvicdev.com', 25)
                        ->setUsername('testemail@ruvicdev.com')
                        ->setPassword('TestEmail2018');
        $mail      = \Swift_Mailer::newInstance($transport);
        $message   = new \Swift_Message($dataArray['subject']);
        $message->setFrom($from);
        $message->setTo($to);
        $message->setBody($this->renderView('@API/Emails/' . $nameMethod . '.html.twig',
                                            $dataArray),
                                            'text/html');

        $values = $mail->send($message, $failures);
        return $values;
    }

    /**
     * Method will be used for get the base URL
     * of the site and could use it in whole bundle
     *
     * @return string $baseURL
     */
    final protected function __baseUrl() {
        $this->baseURL = "http://" . $_SERVER['HTTP_HOST'];

        return $this->baseURL;
    }
}
