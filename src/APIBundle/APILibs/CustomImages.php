<?php

namespace APIBundle\APILibs;

/**
 * Class will be used to manage all the information and all
 * the operations will use for resize the image and another more
 * things could be added
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\APILibs\CustomImages
 */
class CustomImages {

    /**
     * Constructor ....
     */
    public function __construct() {
    }

    /**
     * Method will resize the image that has PNG extension
     *
     * @param object | array $filename
     * @param integer $width
     * @param integer $height
     * @param string $pathFile
     * @param string $originalImg
     */
    public function resizePNGImg($filename, $width, $height, $pathFile, $originalImg) {
        $originalFilename     = imagecreatefrompng($originalImg);
        list($anchoI, $altoI) = getimagesize($originalImg);

        $widthRatio  = $width / $anchoI;
        $heightRatio = $height / $altoI;

        if (($anchoI <= $width) && ($altoI <= $height)) {
            $finalWidth  = $anchoI;
            $finalHeight = $altoI;
        }
        else if (($widthRatio * $altoI) < $height){
            $finalHeight = ceil($widthRatio * $altoI);
            $finalWidth  = $width;
        }
        else {
            $finalWidth  = ceil($heightRatio * $anchoI);
            $finalHeight = $height;
        }

        $canvasImg = imagecreatetruecolor($finalWidth, $finalHeight);
        imagecopyresampled($canvasImg, $originalFilename, 0, 0, 0, 0, $finalWidth, $finalHeight, $anchoI, $altoI);
        imagedestroy($originalFilename);
        imagepng($canvasImg, "." . $pathFile, 9);
    }

    /**
     * Method will resize the image that has JPG / JPEG extension
     *
     * @param object | array $filename
     * @param integer $width
     * @param integer $height
     * @param string $pathFile
     * @param string $originalImg
     */
    public function resizeJPGImg($filename, $width, $height, $pathFile, $originalImg) {
        $originalFilename     = imagecreatefromjpeg($originalImg);
        list($anchoI, $altoI) = getimagesize($originalImg);

        $widthRatio  = $width / $anchoI;
        $heightRatio = $height / $altoI;

        if (($anchoI <= $width) && ($altoI <= $height)) {
            $finalWidth  = $anchoI;
            $finalHeight = $altoI;
        }
        else if (($widthRatio * $altoI) < $height){
            $finalHeight = ceil($widthRatio * $altoI);
            $finalWidth  = $width;
        }
        else {
            $finalWidth  = ceil($heightRatio * $anchoI);
            $finalHeight = $height;
        }

        $canvasImg = imagecreatetruecolor($finalWidth, $finalHeight);
        imagecopyresampled($canvasImg, $originalFilename, 0, 0, 0, 0, $finalWidth, $finalHeight, $anchoI, $altoI);
        imagedestroy($originalFilename);
        imagejpeg($canvasImg, "." . $pathFile, 95);
    }

    /**
     * Method will resize the image that has GIF extension
     *
     * @param object | array $filename
     * @param integer $width
     * @param integer $height
     * @param string $pathFile
     * @param string $originalImg
     */
    public function resizeGIFImg($filename, $width, $height, $pathFile, $originalImg) {
        $originalFilename     = imagecreatefromgif($originalImg);
        list($anchoI, $altoI) = getimagesize($originalImg);

        $widthRatio  = $width / $anchoI;
        $heightRatio = $height / $altoI;

        if (($anchoI <= $width) && ($altoI <= $height)) {
            $finalWidth  = $anchoI;
            $finalHeight = $altoI;
        }
        else if (($widthRatio * $altoI) < $height){
            $finalHeight = ceil($widthRatio * $altoI);
            $finalWidth  = $width;
        }
        else {
            $finalWidth  = ceil($heightRatio * $anchoI);
            $finalHeight = $height;
        }

        $canvasImg = imagecreatetruecolor($finalWidth, $finalHeight);
        imagecopyresampled($canvasImg, $originalFilename, 0, 0, 0, 0, $finalWidth, $finalHeight, $anchoI, $altoI);
        imagedestroy($originalFilename);
        imagegif($canvasImg, "." . $pathFile);
    }

    /**
     * Method will resize the image that has BMP extension
     *
     * @param object | array $filename
     * @param integer $width
     * @param integer $height
     * @param string $pathFile
     * @param string $originalImg
     */
    public function resizeBMPImg($filename, $width, $height, $pathFile, $originalImg) {
        $originalFilename     = imagecreatefrombmp($originalImg);
        list($anchoI, $altoI) = getimagesize($originalImg);

        $widthRatio  = $width / $anchoI;
        $heightRatio = $height / $altoI;

        if (($anchoI <= $width) && ($altoI <= $height)) {
            $finalWidth  = $anchoI;
            $finalHeight = $altoI;
        }
        else if (($widthRatio * $altoI) < $height){
            $finalHeight = ceil($widthRatio * $altoI);
            $finalWidth  = $width;
        }
        else {
            $finalWidth  = ceil($heightRatio * $anchoI);
            $finalHeight = $height;
        }

        $canvasImg = imagecreatetruecolor($finalWidth, $finalHeight);
        imagecopyresampled($canvasImg, $originalFilename, 0, 0, 0, 0, $finalWidth, $finalHeight, $anchoI, $altoI);
        imagedestroy($originalFilename);
        imagebmp($canvasImg, "." . $pathFile);
    }

    /**
     * Method will be used to get the path of the image and
     * could be deleted in case the image exists in the platform
     * and a new image has been uploaded
     *
     * @param string $completePath
     * @param string $imagePath
     * @return void
     */
    public function deleteImages($completePath, $imagePath) {
        if (!is_null($imagePath) || !empty($imagePath)) {
            unlink($completePath . $imagePath);
        }
    }
}
