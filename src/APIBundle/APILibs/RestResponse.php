<?php

namespace APIBundle\APILibs;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class that will contains the base
 * to get the response json and the
 * issue error returned in json as well
 * The data will be enabled just in the
 * web site for display data
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\APILibs\RestResponse
 */
class RestResponse extends Response {

    // Declare const
    const RESP_OK      = 'OK';
    const RESP_ERROR   = 'ERROR';
    const RESP_UNKNOWN = 'UNKNOWN';

    /**
     * Constructor...
     */
    public function __construct() {
        parent::__construct();
        $this->setStatusCode(200);
        $this->headers->set('Content-Type', 'application/json');
    }

    /**
     * Set json data content in the response once the
     * system returns the data expected
     *
     * @param array $data
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     */
    public function setContent($data) {
        parent::setContent(json_encode($data));
    }
}
