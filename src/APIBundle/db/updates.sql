/** UPDATES TABLE USERS INFORMATION 18/02/2018 **/
ALTER TABLE `users_information` ADD `telefono` VARCHAR(50) NULL AFTER `id`;
ALTER TABLE `users_information` CHANGE `telefono` `phone` VARCHAR(50) CHARACTER SET utf16 COLLATE utf16_bin NULL DEFAULT NULL;

/** UPDATES TABLE USERS 06/03/2018 **/
ALTER TABLE `users` ADD `is_admin` TINYINT(1) NOT NULL COMMENT '1-Admin 0-User' AFTER `is_active`;
ALTER TABLE `users` CHANGE `is_admin` `is_admin` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '1-Admin 0-User';

/** UPDATES TABLE CITIES 15/03/2018 **/
ALTER TABLE `cities` CHANGE `code_city` `capital_city` VARCHAR(70) CHARACTER SET utf16 COLLATE utf16_bin NOT NULL COMMENT 'code of the city';

/** UPDATES TABLE USERS INFORMATION 18/03/2018 **/
ALTER TABLE `users_information` ADD `bbb_accredited` VARCHAR(200) NULL AFTER `bbb_account`,
                                ADD `wcb_number` VARCHAR(100) NULL AFTER `bbb_accredited`;

/** UPDATES TABLE DYNAMIC DATA 23/03/2018 **/
ALTER TABLE `dynamic_data` ADD `file` VARCHAR(200) NULL AFTER `description`;

/** UPDATES TABLE USERS INFORMATION 24/03/2018 **/
ALTER TABLE `users_information` ADD `profile_pic` VARCHAR(250) NULL AFTER `user_id`;

/** UPDATES TABLE DYNAMIC DATA 25/03/2018 **/
ALTER TABLE `dynamic_data` CHANGE `status` `status` TINYINT(1) NOT NULL COMMENT '1-Activo, 2-Inactivo';

/** UPDATES TABLE DYNAMIC DATA 26/03/2018 **/
ALTER TABLE `dynamic_data` ADD `original_name` VARCHAR(300) NULL AFTER `file`;
ALTER TABLE `dynamic_data` CHANGE `file` `file` VARCHAR(500) CHARACTER SET utf16 COLLATE utf16_bin NULL DEFAULT NULL,
                           CHANGE `original_name` `original_name` VARCHAR(500) CHARACTER SET utf16 COLLATE utf16_bin NULL DEFAULT NULL;

/** UPDATES TABLE USERS 01/04/2018 **/
ALTER TABLE `users` CHANGE `is_active` `is_active` BIGINT(1) NOT NULL COMMENT '0- Inactive, 1- Active, 2- Verify?';
ALTER TABLE `users` ADD `activation_token` VARCHAR(200) NOT NULL AFTER `is_admin`;
ALTER TABLE `users` ADD `is_confirm_token` TINYINT(1) NOT NULL COMMENT '0 - Not Confirmed, 1- Confirmed' AFTER `activation_token`;

/** UPDATES TABLE DYNAMIC DATA 02/04/2018 **/
ALTER TABLE `dynamic_data` CHANGE `section` `section` INT(11) NULL DEFAULT NULL COMMENT '1-banner,2-works,3-about,4-terms';
ALTER TABLE `dynamic_data` CHANGE `status` `status` TINYINT(1) NOT NULL COMMENT '1-Activo, 2-Inactivo, 3-soft_delete';
ALTER TABLE `dynamic_data` CHANGE `status` `status` TINYINT(3) NOT NULL COMMENT '1-Activo, 2-Inactivo, 3-soft_delete';

/** UPDATES TABLE DYNAMIC DATA 08/04/2018 **/
ALTER TABLE `dynamic_data` ADD `url` VARCHAR(500) NULL AFTER `original_name`;
ALTER TABLE `dynamic_data` ADD `type_file` INT NULL AFTER `url`;
ALTER TABLE `dynamic_data` CHANGE `type_file` `type_file` INT(11) NULL DEFAULT NULL COMMENT '0-Image, 1-video';

/** UPDATES TABLE OFFERS 11/04/2018 **/
ALTER TABLE `offers` ADD `is_winner_bid` TINYINT NOT NULL COMMENT '0-Lost, 1-Win' AFTER `is_active_auction`;

/*******************************************************************/
/*******************************************************************/
/*******************************************************************/
/***************************** NEW UPDATES *************************/
/*******************************************************************/
/*******************************************************************/
/*******************************************************************/

/** UPDATES USERS INFORMATION TABLE 20/04/2018 **/
- Delete profile_pic field from users_information table

/** ADD NEW FIELD RAITING IN USERS INFORMATION TABLE 22/04/2018 **/
ALTER TABLE `users_information` ADD `raiting` VARCHAR(10) NULL AFTER `bbb_accredited`;

/** Insert Cities **/
INSERT INTO `cities`( `name_city`, `capital_city`, `date_created`) VALUES ('Canada','Ottawa','2018-04-25');
INSERT INTO `cities`( `name_city`, `capital_city`, `date_created`) VALUES ('Alberta','Edmonton','2018-04-25')
INSERT INTO `cities`( `name_city`, `capital_city`, `date_created`) VALUES ('British Columbia','Victoria','2018-04-25');
INSERT INTO `cities`( `name_city`, `capital_city`, `date_created`) VALUES ('Manitoba','Winnipeg','2018-04-25');
INSERT INTO `cities`( `name_city`, `capital_city`, `date_created`) VALUES ('New Brunswick','Fredericton','2018-04-25');
INSERT INTO `cities`( `name_city`, `capital_city`, `date_created`) VALUES ('Newfoundland and Labrador','St. John''s','2018-04-25');
INSERT INTO `cities`( `name_city`, `capital_city`, `date_created`) VALUES ('Nova Scotia','Toronto','2018-04-25');
INSERT INTO `cities`( `name_city`, `capital_city`, `date_created`) VALUES ('Ontario','Ottawa','2018-04-25');
INSERT INTO `cities`( `name_city`, `capital_city`, `date_created`) VALUES ('Prince Edward Island','Charlottetown','2018-04-25');
INSERT INTO `cities`( `name_city`, `capital_city`, `date_created`) VALUES ('Quebec','Quebec City','2018-04-25');
INSERT INTO `cities`( `name_city`, `capital_city`, `date_created`) VALUES ('Saskatchewan','Regina','2018-04-25');
INSERT INTO `cities`( `name_city`, `capital_city`, `date_created`) VALUES ('Northwest Territories','Yellowknife','2018-04-25');
INSERT INTO `cities`( `name_city`, `capital_city`, `date_created`) VALUES ('Nunavut','Iqaluit','2018-04-25');
INSERT INTO `cities`( `name_city`, `capital_city`, `date_created`) VALUES ('Yukon','Whitehorse','2018-04-25');

/** Insert Job Categories */
INSERT INTO `jobs_categories`( `name_jobs_categories`, `date_created`) VALUES ('Administrative','2018-04-25');
INSERT INTO `jobs_categories`( `name_jobs_categories`, `date_created`) VALUES ('Education','2018-04-25');
INSERT INTO `jobs_categories`( `name_jobs_categories`, `date_created`) VALUES ('Laws','2018-04-25');
INSERT INTO `jobs_categories`( `name_jobs_categories`, `date_created`) VALUES ('Engineering','2018-04-25');
INSERT INTO `jobs_categories`( `name_jobs_categories`, `date_created`) VALUES ('Human Resources','2018-04-25');

/** ADD NEW FIELD IN DYNAMIC DATA TABLE 26/04/2017 **/
ALTER TABLE `dynamic_data` ADD `file_extension` VARCHAR(10) NULL AFTER `type_file`;

/** ADD NEW TABLE COMMENTS AND PUBLISHES 03/05/2018 **/
- SEE FILE COMMENTS.SQL
- SEE FILE PUBLISHES.sql

/** NEW FIELD WILL STORE THE IMAGE HAS BEEN RESIZED IN TABLE DYNAMIC DATA 06/05/2018 **/
ALTER TABLE `dynamic_data` ADD `file_resize` VARCHAR(400) NULL AFTER `file`;
ALTER TABLE `dynamic_data` CHANGE `file_resize` `file_resize` VARCHAR(500) CHARACTER SET utf16 COLLATE utf16_bin NULL DEFAULT NULL;

/** NEW FIELD WILL STORE THE STATUS OF THE PROJECT IN TABLE POSTS 13/05/2018 **/
ALTER TABLE `posts` ADD `post_status` INT NOT NULL DEFAULT '1' COMMENT '0.- Won , 1.- Activo, 2.- Eliminado' AFTER `user_id`;
ALTER TABLE `posts` ADD `post_payment` DECIMAL(10,5) NULL COMMENT 'base payment' AFTER `description`;

/** NEW CHANGES IN OFFERS TABLE 17/05/2018 **/
ALTER TABLE `offers` ADD `bidder_post` INT NOT NULL AFTER `id_offer`;
ALTER TABLE `offers` CHANGE `bidder_post` `bidder_post` INT(11) NOT NULL COMMENT 'id of the post where the bid has been done';

/** CHANGE CAPITAL LETTERS ALL THE USERNAME OF THE USERS TABLE 20/05/2018 **/
UPDATE `users` SET `username`= UPPER(username);

/** CHANGE THE STATUS TO TWO FOR GET THE BID WON OF THE OFFERS TABLE 20/05/2018 **/
ALTER TABLE `offers` CHANGE `is_active_auction` `is_active_auction` TINYINT(1) NOT NULL COMMENT '0-deactive, 1-active, 2-won';
ALTER TABLE `offers` CHANGE `is_active_auction` `is_active_auction` INT(1) NOT NULL COMMENT '0-deactive, 1-active, 2-won';

/** CHANGE FIELD VALUE TO INT = 11 OFFERS TABLE 21/05/2018 **/
ALTER TABLE `offers` CHANGE `is_active_auction` `is_active_auction` INT NOT NULL COMMENT '0-deactive, 1-active, 2-won';

/** ADD FIELD file_extension IN ATTACHMENTS TABLE 04/05/2018 **/
ALTER TABLE `attachments` ADD `file_extension` VARCHAR(30) NULL AFTER `is_image`;

/** ADD FIELD amount IN PAYMENTS_HISTORY TABLE 21/06/2018 **/
ALTER TABLE `payment_history` ADD `amount` DECIMAL(10,5) NOT NULL AFTER `post_id`;

/** ADD FIELD email_base IN USERS TABLE 21/06/2018 **/
ALTER TABLE `users` ADD `email_base` VARCHAR(400) NOT NULL AFTER `date_updated`;

/** UPDATE email_base IN USERS TABLE 22/06/2018 **/
UPDATE `users` SET `email_base` = `email` WHERE 1;


/** UPDATE is_active_auction IN OFFERS TABLE 03/07/2018 **/
ALTER TABLE `offers` CHANGE `is_active_auction` `is_active_auction` INT(11) NOT NULL COMMENT '0-deactive, 1-active, 2-won, 3-pending';
