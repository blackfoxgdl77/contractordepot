-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 02-08-2018 a las 05:27:05
-- Versión del servidor: 10.2.11-MariaDB
-- Versión de PHP: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `contractordepots`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `attachments`
--

CREATE TABLE `attachments` (
  `id_attachment` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf16_bin NOT NULL COMMENT 'name of the attachment',
  `path` varchar(100) COLLATE utf16_bin NOT NULL COMMENT 'path where the attachment will be stored',
  `is_image` tinyint(1) NOT NULL COMMENT '0-images, 1-videos',
  `file_extension` varchar(30) COLLATE utf16_bin DEFAULT NULL,
  `post_id` int(11) NOT NULL COMMENT 'foreign key from posts table',
  `date_created` datetime NOT NULL COMMENT 'date when the data was created',
  `date_updated` datetime NOT NULL COMMENT 'Date when the records were updated'
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table will contain information about the post''s attachments';

--
-- Volcado de datos para la tabla `attachments`
--

INSERT INTO `attachments` (`id_attachment`, `name`, `path`, `is_image`, `file_extension`, `post_id`, `date_created`, `date_updated`) VALUES
(1, 'Captura de pantalla 2018-02-04 a la(s) 21.36.32.png', '/bundles/site/images/posts/2d6fca99c3c57c52fe8776e7ec78ccf1.png', 1, 'png', 1, '2018-06-29 04:32:25', '2018-06-29 04:32:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cities`
--

CREATE TABLE `cities` (
  `id_city` int(11) NOT NULL COMMENT 'primary key',
  `name_city` varchar(60) COLLATE utf16_bin NOT NULL COMMENT 'name of the city',
  `capital_city` varchar(70) COLLATE utf16_bin NOT NULL COMMENT 'code of the city',
  `date_created` datetime NOT NULL COMMENT 'date when was created the record'
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table will contain the data of the cities used on the system';

--
-- Volcado de datos para la tabla `cities`
--

INSERT INTO `cities` (`id_city`, `name_city`, `capital_city`, `date_created`) VALUES
(1, 'Canada', 'Ottawa', '2018-04-25 00:00:00'),
(2, 'Alberta', 'Edmonton', '2018-04-25 00:00:00'),
(3, 'British Columbia', 'Victoria', '2018-04-25 00:00:00'),
(4, 'Manitoba', 'Winnipeg', '2018-04-25 00:00:00'),
(5, 'New Brunswick', 'Fredericton', '2018-04-25 00:00:00'),
(6, 'Newfoundland and Labrador', 'St. John\'s', '2018-04-25 00:00:00'),
(7, 'Nova Scotia', 'Toronto', '2018-04-25 00:00:00'),
(8, 'Ontario', 'Ottawa', '2018-04-25 00:00:00'),
(9, 'Prince Edward Island', 'Charlottetown', '2018-04-25 00:00:00'),
(10, 'Quebec', 'Quebec City', '2018-04-25 00:00:00'),
(11, 'Saskatchewan', 'Regina', '2018-04-25 00:00:00'),
(12, 'Northwest Territories', 'Yellowknife', '2018-04-25 00:00:00'),
(13, 'Nunavut', 'Iqaluit', '2018-04-25 00:00:00'),
(14, 'Yukon', 'Whitehorse', '2018-04-25 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `comments` varchar(400) NOT NULL,
  `publish_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dynamic_data`
--

CREATE TABLE `dynamic_data` (
  `id` int(11) NOT NULL,
  `section` int(11) DEFAULT NULL COMMENT '1-banner,2-works,3-about,4-terms',
  `description` text COLLATE utf16_bin DEFAULT NULL,
  `file` varchar(500) COLLATE utf16_bin DEFAULT NULL,
  `file_resize` varchar(500) COLLATE utf16_bin DEFAULT NULL,
  `original_name` varchar(500) COLLATE utf16_bin DEFAULT NULL,
  `url` varchar(500) COLLATE utf16_bin DEFAULT NULL,
  `type_file` int(11) DEFAULT NULL COMMENT '0-Image, 1-video',
  `file_extension` varchar(10) COLLATE utf16_bin DEFAULT NULL,
  `status` tinyint(3) NOT NULL COMMENT '1-Activo, 2-Inactivo, 3-soft_delete',
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

--
-- Volcado de datos para la tabla `dynamic_data`
--

INSERT INTO `dynamic_data` (`id`, `section`, `description`, `file`, `file_resize`, `original_name`, `url`, `type_file`, `file_extension`, `status`, `date_created`, `date_updated`) VALUES
(1, 2, 'test ', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-07-13 05:02:32', '2018-07-13 05:02:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jobs_categories`
--

CREATE TABLE `jobs_categories` (
  `id_jobs_categories` int(11) NOT NULL COMMENT 'primary key',
  `name_jobs_categories` varchar(100) COLLATE utf16_bin NOT NULL COMMENT 'name of the jobs categories',
  `date_created` datetime NOT NULL COMMENT 'date when the data was created'
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='table will contain the categories of the jobs';

--
-- Volcado de datos para la tabla `jobs_categories`
--

INSERT INTO `jobs_categories` (`id_jobs_categories`, `name_jobs_categories`, `date_created`) VALUES
(1, 'Administrative', '2018-04-25 00:00:00'),
(2, 'Education', '2018-04-25 00:00:00'),
(3, 'Laws', '2018-04-25 00:00:00'),
(4, 'Engineering', '2018-04-25 00:00:00'),
(5, 'Human Resources', '2018-04-25 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `offers`
--

CREATE TABLE `offers` (
  `id_offer` int(11) NOT NULL COMMENT 'primary key',
  `bidder_post` int(11) NOT NULL COMMENT 'id of the post where the bid has been done',
  `bidder_id` int(11) NOT NULL COMMENT 'id of the user made and offer for the job',
  `auctioneer_id` int(11) NOT NULL COMMENT 'id of the user is posting the job',
  `money_bidder` decimal(10,5) NOT NULL COMMENT 'field where the offer will be stored',
  `is_active_auction` int(11) NOT NULL COMMENT '0-deactive, 1-active, 2-won, 3-pending',
  `date_created` datetime NOT NULL COMMENT 'date when the data was created'
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='table will contain all the information of the auctions';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment_history`
--

CREATE TABLE `payment_history` (
  `id_payment` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `amount` decimal(10,5) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `status` varchar(20) COLLATE utf16_bin NOT NULL,
  `cart` varchar(100) COLLATE utf16_bin NOT NULL,
  `id_transaction` varchar(100) COLLATE utf16_bin NOT NULL,
  `date_payment` varchar(100) COLLATE utf16_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts`
--

CREATE TABLE `posts` (
  `id_post` int(11) NOT NULL,
  `post_name` varchar(150) COLLATE utf16_bin NOT NULL COMMENT 'name of the post or new job',
  `description` text COLLATE utf16_bin NOT NULL COMMENT 'description of the job',
  `post_payment` decimal(10,5) DEFAULT NULL COMMENT 'base payment',
  `category_job_id` int(11) NOT NULL COMMENT 'foreign key from categories jobs',
  `city_id` int(11) NOT NULL COMMENT 'foreign key from city',
  `user_id` int(11) NOT NULL COMMENT 'foreign key from user',
  `post_status` int(11) NOT NULL DEFAULT 1 COMMENT '0.- Won , 1.- Activo, 2.- Eliminado',
  `date_created` datetime NOT NULL COMMENT 'date when the data was created',
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='table where will be stored the posts';

--
-- Volcado de datos para la tabla `posts`
--

INSERT INTO `posts` (`id_post`, `post_name`, `description`, `post_payment`, `category_job_id`, `city_id`, `user_id`, `post_status`, `date_created`, `date_updated`) VALUES
(1, 'Title postname Test', 'Test postmame paypal platform', NULL, 4, 2, 1, 1, '2018-06-29 04:32:25', '2018-06-29 04:32:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publishes`
--

CREATE TABLE `publishes` (
  `id` int(11) NOT NULL,
  `user_publish_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `owner_post_id` int(11) NOT NULL,
  `comments` varchar(400) NOT NULL,
  `title` varchar(200) NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='tabla para comentarios base';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `report_fake_projects`
--

CREATE TABLE `report_fake_projects` (
  `id` int(11) NOT NULL,
  `fake_project_id` int(11) NOT NULL COMMENT 'Foreign key from posts',
  `reported_user_id` int(11) NOT NULL COMMENT 'Foreign key from users',
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

--
-- Volcado de datos para la tabla `report_fake_projects`
--

INSERT INTO `report_fake_projects` (`id`, `fake_project_id`, `reported_user_id`, `date_created`, `date_updated`) VALUES
(1, 1, 1, '2018-07-19 00:00:00', '2018-07-19 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL COMMENT 'primary key',
  `name` varchar(150) COLLATE utf16_bin NOT NULL COMMENT 'name of the company / user',
  `email` varchar(200) COLLATE utf16_bin NOT NULL COMMENT 'email of the company / user',
  `username` varchar(200) COLLATE utf16_bin NOT NULL COMMENT 'User name will be autogenerated',
  `password` varchar(200) COLLATE utf16_bin NOT NULL COMMENT 'password of the company / user',
  `is_company` bigint(1) DEFAULT NULL COMMENT '1 - Company, 0 - User',
  `type_user` bigint(1) DEFAULT NULL COMMENT '0-Contractor, 1-Owner',
  `is_active` bigint(1) NOT NULL COMMENT '0- Inactive, 1- Active, 2- Verify?',
  `is_admin` tinyint(1) NOT NULL DEFAULT 0 COMMENT '1-Admin 0-User',
  `activation_token` varchar(200) COLLATE utf16_bin NOT NULL,
  `is_confirm_token` tinyint(1) NOT NULL COMMENT '0 - Not Confirmed, 1- Confirmed',
  `date_created` datetime NOT NULL COMMENT 'Date when the account was created',
  `date_updated` datetime NOT NULL,
  `email_base` varchar(400) COLLATE utf16_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='table will contain all the information of the users register';

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `password`, `is_company`, `type_user`, `is_active`, `is_admin`, `activation_token`, `is_confirm_token`, `date_created`, `date_updated`, `email_base`) VALUES
(1, 'Ruben Cortes', 'ruben.alonso21@gmail.com', 'CB_00001', '$2y$04$U02cHHtjdfTQ4PdHr3MA.unBZnwQVRNa1bVfbBM05Zvd7KBtkWUfW', NULL, 0, 1, 1, 'add1dfaba8008b0ffffe8258b56ab2f8', 1, '2018-06-29 04:02:02', '2018-06-29 04:02:02', 'ruben.alonso21@gmail.com'),
(2, 'Alonso Mendoza', 'ruben.cortes21@gmail.com', 'CB_00002', '$2y$04$vhnSsh2Izfi4p5oNmfwWtOtGgfEfQ85PUtb7UcMK7VwqmAtcKInKC', NULL, 1, 1, 0, '122006604f4fec7f9dc6a9f4df50cf52', 1, '2018-06-29 04:37:22', '2018-06-29 04:37:23', 'ruben.cortes21@gmail.com'),
(3, 'test', 'test@gmail.com', 'CB_00003', '$2y$04$IHI3dBBikXvIDl3ZFCm.x.h8DPwdOsX.19qZdj9LlRB0gnsyXxjtW', NULL, 0, 1, 0, 'daa7a717747276e4a8026b586c9cf792', 0, '2018-08-02 04:53:32', '2018-08-02 04:53:32', 'test@gmail.com'),
(4, 'ggggg', 'ggg@gmail.com', 'CB_00004', '$2y$04$f7SrKddhjnnUgsYcoyuh3etbE6d.lwjXbj11CqzvHChs5yp5BUgSW', NULL, 1, 1, 0, '8f30c736da69948041b264422f78d3b6', 0, '2018-08-02 04:55:38', '2018-08-02 04:55:38', 'ggg@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_information`
--

CREATE TABLE `users_information` (
  `id` int(11) NOT NULL,
  `phone` varchar(50) COLLATE utf16_bin DEFAULT NULL,
  `payment_method` varchar(100) COLLATE utf16_bin DEFAULT NULL,
  `bbb_account` varchar(200) COLLATE utf16_bin DEFAULT NULL,
  `bbb_accredited` varchar(200) COLLATE utf16_bin DEFAULT NULL,
  `raiting` varchar(10) COLLATE utf16_bin DEFAULT NULL,
  `wcb_number` varchar(100) COLLATE utf16_bin DEFAULT NULL,
  `address` varchar(250) COLLATE utf16_bin DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

--
-- Volcado de datos para la tabla `users_information`
--

INSERT INTO `users_information` (`id`, `phone`, `payment_method`, `bbb_account`, `bbb_accredited`, `raiting`, `wcb_number`, `address`, `user_id`, `date_created`, `date_updated`) VALUES
(1, '3311331133', NULL, NULL, '0', '', NULL, NULL, 1, '2018-06-29 04:02:02', '2018-06-29 04:02:02'),
(2, '4455667788', NULL, NULL, NULL, '', NULL, NULL, 2, '2018-06-29 04:37:22', '2018-06-29 04:37:22'),
(3, '123123123', NULL, NULL, '0', '', NULL, NULL, 3, '2018-08-02 04:53:32', '2018-08-02 04:53:32'),
(4, '55555555', NULL, NULL, NULL, '', NULL, NULL, 4, '2018-08-02 04:55:38', '2018-08-02 04:55:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_reset_password`
--

CREATE TABLE `users_reset_password` (
  `id` int(11) NOT NULL,
  `token_reset` varchar(255) COLLATE utf16_bin NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `users_id` int(11) NOT NULL,
  `users_email` varchar(200) COLLATE utf16_bin NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

--
-- Volcado de datos para la tabla `users_reset_password`
--

INSERT INTO `users_reset_password` (`id`, `token_reset`, `is_active`, `users_id`, `users_email`, `date_created`, `date_updated`) VALUES
(1, '000f270d4f9495633f27f2296a7eac53', 1, 4, 'ggg@gmail.com', '2018-08-02 05:18:38', '2018-08-02 05:18:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `winner_comments`
--

CREATE TABLE `winner_comments` (
  `id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `project_owner` int(11) NOT NULL COMMENT 'id of owner / creator of the project',
  `project_winner` int(11) NOT NULL COMMENT 'id of the winner to do the project',
  `project_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `attachments`
--
ALTER TABLE `attachments`
  ADD PRIMARY KEY (`id_attachment`);

--
-- Indices de la tabla `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id_city`);

--
-- Indices de la tabla `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `dynamic_data`
--
ALTER TABLE `dynamic_data`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `jobs_categories`
--
ALTER TABLE `jobs_categories`
  ADD PRIMARY KEY (`id_jobs_categories`);

--
-- Indices de la tabla `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id_offer`);

--
-- Indices de la tabla `payment_history`
--
ALTER TABLE `payment_history`
  ADD PRIMARY KEY (`id_payment`);

--
-- Indices de la tabla `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id_post`);

--
-- Indices de la tabla `publishes`
--
ALTER TABLE `publishes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `report_fake_projects`
--
ALTER TABLE `report_fake_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users_information`
--
ALTER TABLE `users_information`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users_reset_password`
--
ALTER TABLE `users_reset_password`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `winner_comments`
--
ALTER TABLE `winner_comments`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `attachments`
--
ALTER TABLE `attachments`
  MODIFY `id_attachment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cities`
--
ALTER TABLE `cities`
  MODIFY `id_city` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key', AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dynamic_data`
--
ALTER TABLE `dynamic_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `jobs_categories`
--
ALTER TABLE `jobs_categories`
  MODIFY `id_jobs_categories` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key', AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `offers`
--
ALTER TABLE `offers`
  MODIFY `id_offer` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key';

--
-- AUTO_INCREMENT de la tabla `payment_history`
--
ALTER TABLE `payment_history`
  MODIFY `id_payment` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `posts`
--
ALTER TABLE `posts`
  MODIFY `id_post` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `publishes`
--
ALTER TABLE `publishes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `report_fake_projects`
--
ALTER TABLE `report_fake_projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `users_information`
--
ALTER TABLE `users_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `users_reset_password`
--
ALTER TABLE `users_reset_password`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `winner_comments`
--
ALTER TABLE `winner_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
