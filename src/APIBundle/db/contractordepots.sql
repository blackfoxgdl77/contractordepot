-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 19-04-2018 a las 02:12:02
-- Versión del servidor: 10.2.11-MariaDB
-- Versión de PHP: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `contractordepots`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `attachments`
--

CREATE TABLE `attachments` (
  `id_attachment` int(11) NOT NULL COMMENT 'primary key',
  `name` varchar(200) COLLATE utf16_bin NOT NULL COMMENT 'name of the attachment',
  `path` varchar(100) COLLATE utf16_bin NOT NULL COMMENT 'path where the attachment will be stored',
  `is_image` tinyint(1) NOT NULL COMMENT '0-images, 1-videos',
  `post_id` int(11) NOT NULL COMMENT 'foreign key from posts table',
  `date_created` datetime NOT NULL COMMENT 'date when the data was created',
  `date_updated` datetime NOT NULL COMMENT 'Date when the records were updated'
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table will contain information about the post''s attachments';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cities`
--

CREATE TABLE `cities` (
  `id_city` int(11) NOT NULL COMMENT 'primary key',
  `name_city` varchar(60) COLLATE utf16_bin NOT NULL COMMENT 'name of the city',
  `capital_city` varchar(70) COLLATE utf16_bin NOT NULL COMMENT 'code of the city',
  `date_created` datetime NOT NULL COMMENT 'date when was created the record'
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table will contain the data of the cities used on the system';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dynamic_data`
--

CREATE TABLE `dynamic_data` (
  `id` int(11) NOT NULL,
  `section` int(11) DEFAULT NULL COMMENT '1-banner,2-works,3-about,4-terms',
  `description` text COLLATE utf16_bin DEFAULT NULL,
  `file` varchar(500) COLLATE utf16_bin DEFAULT NULL,
  `original_name` varchar(500) COLLATE utf16_bin DEFAULT NULL,
  `url` varchar(500) COLLATE utf16_bin DEFAULT NULL,
  `type_file` int(11) DEFAULT NULL COMMENT '0-Image, 1-video',
  `status` tinyint(3) NOT NULL COMMENT '1-Activo, 2-Inactivo, 3-soft_delete',
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jobs_categories`
--

CREATE TABLE `jobs_categories` (
  `id_jobs_categories` int(11) NOT NULL COMMENT 'primary key',
  `name_jobs_categories` varchar(100) COLLATE utf16_bin NOT NULL COMMENT 'name of the jobs categories',
  `date_created` datetime NOT NULL COMMENT 'date when the data was created'
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='table will contain the categories of the jobs';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `offers`
--

CREATE TABLE `offers` (
  `id_offer` int(11) NOT NULL COMMENT 'primary key',
  `bidder_id` int(11) NOT NULL COMMENT 'id of the user made and offer for the job',
  `auctioneer_id` int(11) NOT NULL COMMENT 'id of the user is posting the job',
  `money_bidder` decimal(10,5) NOT NULL COMMENT 'field where the offer will be stored',
  `is_active_auction` tinyint(1) NOT NULL COMMENT '0-deactive, 1-active',
  `date_created` datetime NOT NULL COMMENT 'date when the data was created'
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='table will contain all the information of the auctions';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment_history`
--

CREATE TABLE `payment_history` (
  `id_payment` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts`
--

CREATE TABLE `posts` (
  `id_post` int(11) NOT NULL COMMENT 'primary key',
  `post_name` varchar(150) COLLATE utf16_bin NOT NULL COMMENT 'name of the post or new job',
  `description` text COLLATE utf16_bin NOT NULL COMMENT 'description of the job',
  `category_job_id` int(11) NOT NULL COMMENT 'foreign key from categories jobs',
  `city_id` int(11) NOT NULL COMMENT 'foreign key from city',
  `user_id` int(11) NOT NULL COMMENT 'foreign key from user',
  `date_created` datetime NOT NULL COMMENT 'date when the data was created',
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='table where will be stored the posts';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `report_fake_projects`
--

CREATE TABLE `report_fake_projects` (
  `id` int(11) NOT NULL,
  `fake_project_id` int(11) NOT NULL COMMENT 'Foreign key from posts',
  `reported_user_id` int(11) NOT NULL COMMENT 'Foreign key from users',
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL COMMENT 'primary key',
  `name` varchar(150) COLLATE utf16_bin NOT NULL COMMENT 'name of the company / user',
  `email` varchar(200) COLLATE utf16_bin NOT NULL COMMENT 'email of the company / user',
  `username` varchar(200) COLLATE utf16_bin NOT NULL COMMENT 'User name will be autogenerated',
  `password` varchar(200) COLLATE utf16_bin NOT NULL COMMENT 'password of the company / user',
  `is_company` bigint(1) DEFAULT NULL COMMENT '1 - Company, 0 - User',
  `type_user` bigint(1) DEFAULT NULL COMMENT '0-Contractor, 1-Owner',
  `is_active` bigint(1) NOT NULL COMMENT '0- Inactive, 1- Active, 2- Verify?',
  `is_admin` tinyint(1) NOT NULL DEFAULT 0 COMMENT '1-Admin 0-User',
  `activation_token` varchar(200) COLLATE utf16_bin NOT NULL,
  `is_confirm_token` tinyint(1) NOT NULL COMMENT '0 - Not Confirmed, 1- Confirmed',
  `date_created` datetime NOT NULL COMMENT 'Date when the account was created',
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='table will contain all the information of the users register';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_information`
--

CREATE TABLE `users_information` (
  `id` int(11) NOT NULL,
  `phone` varchar(50) COLLATE utf16_bin DEFAULT NULL,
  `payment_method` varchar(100) COLLATE utf16_bin DEFAULT NULL,
  `bbb_account` varchar(200) COLLATE utf16_bin DEFAULT NULL,
  `bbb_accredited` varchar(200) COLLATE utf16_bin DEFAULT NULL,
  `wcb_number` varchar(100) COLLATE utf16_bin DEFAULT NULL,
  `address` varchar(250) COLLATE utf16_bin DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `profile_pic` varchar(250) COLLATE utf16_bin DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_reset_password`
--

CREATE TABLE `users_reset_password` (
  `id` int(11) NOT NULL,
  `token_reset` varchar(255) COLLATE utf16_bin NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `users_id` int(11) NOT NULL,
  `users_email` varchar(200) COLLATE utf16_bin NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `attachments`
--
ALTER TABLE `attachments`
  ADD PRIMARY KEY (`id_attachment`);

--
-- Indices de la tabla `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id_city`);

--
-- Indices de la tabla `dynamic_data`
--
ALTER TABLE `dynamic_data`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `jobs_categories`
--
ALTER TABLE `jobs_categories`
  ADD PRIMARY KEY (`id_jobs_categories`);

--
-- Indices de la tabla `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id_offer`);

--
-- Indices de la tabla `payment_history`
--
ALTER TABLE `payment_history`
  ADD PRIMARY KEY (`id_payment`);

--
-- Indices de la tabla `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id_post`);

--
-- Indices de la tabla `report_fake_projects`
--
ALTER TABLE `report_fake_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users_information`
--
ALTER TABLE `users_information`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users_reset_password`
--
ALTER TABLE `users_reset_password`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `attachments`
--
ALTER TABLE `attachments`
  MODIFY `id_attachment` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key';

--
-- AUTO_INCREMENT de la tabla `cities`
--
ALTER TABLE `cities`
  MODIFY `id_city` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key';

--
-- AUTO_INCREMENT de la tabla `dynamic_data`
--
ALTER TABLE `dynamic_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `jobs_categories`
--
ALTER TABLE `jobs_categories`
  MODIFY `id_jobs_categories` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key';

--
-- AUTO_INCREMENT de la tabla `offers`
--
ALTER TABLE `offers`
  MODIFY `id_offer` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key';

--
-- AUTO_INCREMENT de la tabla `payment_history`
--
ALTER TABLE `payment_history`
  MODIFY `id_payment` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `posts`
--
ALTER TABLE `posts`
  MODIFY `id_post` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key';

--
-- AUTO_INCREMENT de la tabla `report_fake_projects`
--
ALTER TABLE `report_fake_projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key';

--
-- AUTO_INCREMENT de la tabla `users_information`
--
ALTER TABLE `users_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users_reset_password`
--
ALTER TABLE `users_reset_password`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
