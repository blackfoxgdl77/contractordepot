<?php

namespace APIBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constrains as Assert;

/**
 * Class will contains all the information about
 * cities table
 *
 * APIBundle\Entity\Cities
 * ruvicdev
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 *
 * @ORM\Entity(repositoryClass="APIBundle\Repository\CitiesRepository")
 * @ORM\Table(name="cities")
 */
 class Cities {

     /**
      * ID for the unique key in the database for cities table
      *
      * @var integer
      *
      * @ORM\Id
      * @ORM\Column(name="id_city", type="integer")
      * @ORM\GeneratedValue(strategy="AUTO")
      */
     private $id;

     /**
      * Name of the city
      *
      * @var string
      *
      * @ORM\Column(name="name_city", type="string", length=60)
      */
     private $nameCity;

     /**
      * Code of the city
      *
      * @var string
      *
      * @ORM\Column(name="capital_city", type="string", length=70)
      */
     private $capitalCity;

     /**
      * Date of thd creation
      *
      * @var datetime
      *
      * @ORM\Column(name="date_created", type="datetime")
      */
     private $dateCreated;

     /**
      * Constructor and initializers
      */
     public function __construct() {
         $this->dateCreated = new \DateTime('now');
     }

    /**
     * Get ID
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get nameCity
     *
     * @return string
     */
    public function getNameCity() {
        return $this->nameCity;
    }

    /**
     * Set nameCity
     *
     * @param string $nameCity
     */
    public function setNameCity($nameCity) {
        $this->nameCity = $nameCity;
    }

    /**
     * Get codeCity
     *
     * @return string
     */
    public function getCapitalCity() {
        return $this->capitalCity;
    }

    /**
     * Set codeCity
     *
     * @param string $codeCity
     */
    public function setCapitalCity($capitalCity) {
        $this->capitalCity = $capitalCity;
    }

    public function __toString() {

        return json_encode(array(
            'id'            => $this->id,
            'value'      => $this->nameCity
        ));
    }

 }
