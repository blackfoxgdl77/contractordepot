<?php

namespace APIBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constrains as Assert;

/**
 * Class will contain all the information about
 * the comments done by the use has won the
 * bid of the project
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Entity\WinnerComments
 *
 * @ORM\Entity(repositoryClass="APIBundle\Repository\WinnerCommentsRepository")
 * @ORM\Table(name="winner_comments")
 */
class WinnerComments {
    /**
     * ID used as primary key
     *
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Comment done by the users in the project
     *
     * @var string
     *
     * @ORM\Column(name="comment", type="string")
     */
    protected $comment;

    /**
     * Id of the owner has created the project
     *
     * @var object
     *
     * @ORM\ManyToOne(targetEntity="APIBundle\Entity\Users", inversedBy="winner_comments")
     * @ORM\JoinColumn(name="project_owner", referencedColumnName="id")
     */
    protected $projectOwner;

    /**
     * Id of the user has win the bid
     *
     * @var object
     *
     * @ORM\ManyToOne(targetEntity="APIBundle\Entity\Users", inversedBy="winner_comments")
     * @ORM\JoinColumn(name="project_winner", referencedColumnName="id")
     */
    protected $projectWinner;

    /**
     * Id of the project has finished because has been winner
     *
     * @var object
     *
     * @ORM\ManyToOne(targetEntity="APIBundle\Entity\Posts", inversedBy="winner_comments")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id_post")
     */
    protected $projectId;

    /**
     * Date when the comment was created
     *
     * @var datetime
     *
     * @ORM\Column(name="date_created", type="datetime")
     */
    protected $dateCreated;

    /**
     * Constructors and initializers
     */
    public function __construct() {
        $this->dateCreated = new \DateTime('now');
    }

    /**
     * Get the Id of the comments
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get the comments
     *
     * @return string
     */
    public function getComments() {
        return $this->comment;
    }

    /**
     * Set the comments
     *
     * @param string $comment
     */
    public function setComments($comment) {
        $this->comment = $comment;
    }

    /**
     * Get the project owner object data
     *
     * @return object
     */
    public function getProjectOwner() {
        return $this->projectOwner;
    }

    /**
     * Set the project owner id
     *
     * @param object $projectOwner
     */
    public function setProjectOwner($projectOwner) {
        $this->projectOwner = $projectOwner;
    }

    /**
     * Get the project winner object data
     *
     * @return object
     */
    public function getProjectWinner() {
        return $this->projectWinner;
    }

    /**
     * Set the id of the project winner
     *
     * @param object $projectWinner
     */
    public function setProjectWinner($projectWinner) {
        $this->projectWinner = $projectWinner;
    }

    /**
     * Get the project post id
     *
     * @return object
     */
    public function getProjectId() {
        return $this->projectId;
    }

    /**
     * Set the project id
     *
     * @param object $projectId
     */
    public function setProjectId($projectId) {
        $this->projectId = $projectId;
    }

    /**
     * Get date when the comment was created
     *
     * @return datetime
     */
    public function getDateCreated() {
        return $this->dateCreated;
    }
}
