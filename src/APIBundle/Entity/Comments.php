<?php

namespace APIBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constrains as Assert;

/**
 * Class that contains all the information has been
 * used to get the data should be displayed on the
 * page depending the main comment is displayed to the user
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Entity\Comments
 *
 * @ORM\Entity(repositoryClass="APIBundle\Repository\CommentsRepository")
 * @ORM\Table(name="comments")
 */
class Comments {
    /**
     * ID key for identify unique key
     *
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * ID of the user has posted the comment
     *
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="APIBundle\Entity\Users", inversedBy="comments")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $userId;

    /**
     * ID of the post has this comment
     *
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="APIBundle\Entity\Posts", inversedBy="comments")
     * @ORM\JoinColumn(name="post_id", referencedColumnName="id_post")
     */
    protected $postId;

    /**
     * Id of the owner has created the post
     *
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="APIBundle\Entity\Users", inversedBy="comments")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    protected $ownerId;

    /**
     * Comments has been added to the main publish
     *
     * @var string
     *
     * @ORM\Column(name="comments", type="string", length=400, nullable=false)
     */
    protected $comments;

    /**
     * Id of the publish will be added the comment
     *
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="APIBundle\Entity\Publishes", inversedBy="comments")
     * @ORM\JoinColumn(name="publish_id", referencedColumnName="id")
     */
    protected $publishId;

    /**
     * Date when the comment has been created
     *
     * @var datetime
     *
     * @ORM\Column(name="date_created", type="datetime")
     */
    protected $dateCreated;

    /**
     * Constructor and Initializers
     */
    public function __construct() {
        $this->dateCreated = new \DateTime('now');
    }

    /**
     * Get the Id of the comments
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get comments of the user
     *
     * @return string
     */
    public function getComments() {
        return $this->comments;
    }

    /**
     * Set the comments of the users
     *
     * @param string $comments
     */
    public function setComments($comments) {
        $this->comments = $comments;
    }

    /**
     * Get the date when the post was created
     *
     * @return datetime
     */
    public function getDateCreated() {
        return $this->dateCreated;
    }

    /**
     * Get publish Id
     *
     * @return object APIBundle\Entity\Publishes
     */
    public function getPublishesId() {
        return $this->publishId;
    }

    /**
     * Set publish Id
     *
     * @param object APIBundle\Entity\Publishes $publishId
     */
    public function setPublishesId($publishId) {
        $this->publishId = $publishId;
    }

    /**
     * Get the Owner ID
     *
     * @return object APIBundle\Entity\Users
     */
    public function getOwnerPublishId() {
        return $this->ownerId;
    }

    /**
     * Set the Owner Id
     *
     * @param object APIBundle\Entity\Users $ownerId
     */
    public function setOwnerPublishId($ownerId) {
        $this->ownerId = $ownerId;
    }

    /**
     * Get the user has commented the post
     *
     * @return object APIBundle\Entity\Users
     */
    public function getCommentedUser() {
        return $this->userId;
    }

    /**
     * Set the user Id has commented the post
     *
     * @param object APIBundle\Entity\Users $userId
     */
    public function setCommentedUser($userId) {
        $this->userId = $userId;
    }

    /**
     * Get the post related to the comments
     *
     * @return object APIBundle\Entity\Posts
     */
    public function getPostsCommented() {
        return $this->postId;
    }

    /**
     * Set the post related to the comments
     *
     * @param object APIBundle\Entity\Posts $postId
     */
    public function setPostsCommented($postId) {
        $this->postId = $postId;
    }
}
