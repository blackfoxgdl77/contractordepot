<?php

namespace APIBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constrains as Assert;

/**
 * Class will contains all the definition of the users information
 * table
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Entity\UsersInformation
 *
 * @ORM\Entity(repositoryClass="APIBundle\Repository\UsersInformationRepository")
 * @ORM\Table(name="users_information")
 */
class UsersInformation {
    /**
     * ID key to identify unique value
     *
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Payment method will use the user
     *
     * @var string
     *
     * @ORM\Column(name="payment_method", type="string", length=100, nullable=true)
     */
    private $paymentMethod;

    /**
     * Phone of the user
     *
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=50, nullable=true)
     */
    private $phone;

    /**
     * BBB account to review the historial
     *
     * @var string
     *
     * @ORM\Column(name="bbb_account", type="string", length=200, nullable=true)
     */
    private $bbbAccount;

    /**
     * BBB Accredited from bbb platform
     *
     * @var string
     *
     * @ORM\Column(name="bbb_accredited", type="string", length=200, nullable=true)
     */
    private $bbbAccredited;

    /**
     * Raiting to see the grade of the user
     *
     * @var string
     *
     * @ORM\Column(name="raiting", type="string", length=10, nullable=true)
     */
    private $raiting;

    /**
     * WCB Number from bbb platform
     *
     * @var string
     *
     * @ORM\Column(name="wcb_number", type="string", length=100, nullable=true)
     */
    private $wcbNumber;

    /**
     * Address of the user register
     *
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=250, nullable=true)
     */
    private $address;

    /**
     * Id of the user, this is a foreign Key
     *
     * @var integer
     *
     * @ORM\OneToOne(targetEntity="APIBundle\Entity\Users")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Date when the record was created
     *
     * @var DateTime
     *
     * @ORM\Column(name="date_created", type="datetime")
     */
    private $dateCreated;

    /**
     * Date when the record was updated
     *
     * @var DateTime
     *
     * @ORM\Column(name="date_updated", type="datetime")
     */
    private $dateUpdated;

    /**
     * Constructor and Initializers
     */
    public function __construct() {
        $this->dateCreated = new \DateTime('now');
        $this->dateUpdated = new \DateTime('now');
    }

    /**
     * Get the ID
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get the Phone
     *
     * @return string
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     * Set the phone
     *
     * @param string $phone
     */
    public function setPhone($phone) {
        $this->phone = $phone;
    }

    /**
     * Get the payment method
     *
     * @return string
     */
    public function getPaymentMethod() {
        return $this->paymentMethod;
    }

    /**
     * Set the payment method
     *
     * @param string $paymentMethod
     */
    public function setPyamentMethod($paymentMethod) {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * Get BBB Account
     *
     * @return string
     */
    public function getBBBAccount() {
        return $this->bbbAccount;
    }

    /**
     * Set the BBB Account
     *
     * @param string $bbbAccount
     */
    public function setBBBAccount($bbbAccount) {
        $this->bbbAccount = $bbbAccount;
    }

    /**
     * Get the information from BBB Accredited
     *
     * @return string
     */
    public function getBBBAccredited() {
        return $this->bbbAccredited;
    }

    /**
     * Set the bbb Accredited value
     *
     * @param string $bbbAccredited
     */
    public function setBBBAccredited($bbbAccredited) {
        $this->bbbAccredited = $bbbAccredited;
    }

    /**
     * Get the raiting of the user
     *
     * @return string
     */
    public function getRaiting() {
        $this->raiting = $raiting;
    }

    /**
     * Set the raiting of the user
     *
     * @var string $raiting
     */
    public function setRaiting($raiting) {
        $this->raiting = $raiting;
    }

    /**
     * Get the WCB Number
     *
     * @return string
     */
    public function getWCBNumber() {
        return $this->wcbNumber;
    }

    /**
     * Set the WCB Number
     *
     * @param string $wcbNumber
     */
    public function setWCBNumber($wcbNumber) {
        $this->wcbNumber = $wcbNumber;
    }

    /**
     * Get the address
     *
     * @return string
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     * Set the address
     *
     * @param string $address
     */
    public function setAddress($address) {
        $this->address = $address;
    }

    /**
     * Get the user ID
     *
     * @return APIBundle\Entity\Users $userId
     */
    public function getUserId() {
        return $this->user;
    }

    /**
     * Set tge user id
     *
     * @param object APIBundle\Entity\Users $userId
     */
    public function setUserId($user) {
        $this->user = $user;
    }

    /**
     * Set the date updated
     *
     * @param datetime $dateUpdated
     */
    public function setDateUpdated($dateUpdated) {
        $this->dateUpdated = $dateUpdated;
    }
}
