<?php

namespace APIBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constrains as Assert;

/**
 * Class will contains all the information about the
 * JobsCategories table
 *
 * APIBundle\Entity\JobsCategories
 * ruvicdev
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 *
 * @ORM\Entity(repositoryClass="APIBundle\Repository\JobsCategoriesRepository")
 * @ORM\Table(name="jobs_categories")
 */
class JobsCategories {
    /**
     * ID for unique key in the database
     *
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id_jobs_categories", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Name of the job's categories
     *
     * @var string
     *
     * @ORM\Column(name="name_jobs_categories", type="string", length=100)
     */
    private $nameJobsCategories;

    /**
     * Date when the job category was created
     *
     * @var datetime
     *
     * @ORM\Column(name="date_created", type="datetime")
     */
    private $dateCreated;

    /**
     * Constructor and Initializers
     */
    public function __construct() {
        $this->dateCreated = new \DateTime('now');
    }

    /**
     * Get ID
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get nameJobsCategories
     *
     * @return string
     */
    public function getNameJobsCategories() {
        return $this->nameJobsCategories;
    }

    /**
     * Set nameJobsCategories
     *
     * @param string $nameJobsCategories
     */
    public function setNameJobsCategories($nameJobsCategories) {
        $this->nameJobsCategories = $nameJobsCategories;
    }

    public function __toString() {

        return json_encode(array(
            'id'                => $this->id,
            'value'   => $this->nameJobsCategories
        ));
    }


}
