<?php

namespace APIBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constrains as Assert;

/**
 * Class will contain all the information about
 * posts table
 *
 * APIBundle\Entity\Posts
 * ruvicdev
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 *
 * @ORM\Entity(repositoryClass="APIBundle\Repository\PostsRepository")
 * @ORM\Table(name="posts")
 */
class Posts {
    /**
     * ID for the unique key in the database
     *
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id_post", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Name of the post will be published
     *
     * @var string
     *
     * @ORM\Column(name="post_name", type="string", length=150)
     */
    private $postName;

    /**
     * Description of the post published
     *
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * Base payment of the post in case the user set the data
     *
     * @var decimal
     *
     * @ORM\Column(name="post_payment", type="decimal", nullable=true)
     */
    protected $postPayment;

    /**
     * Category set in the job
     *
     * @var object
     *
     * @ORM\OneToOne(targetEntity="APIBundle\Entity\JobsCategories")
     * @ORM\JoinColumn(name="category_job_id", referencedColumnName="id_jobs_categories")
     */
    private $category;

    /**
     * City is going to be used for doing the job
     *
     * @var object
     *
     * @ORM\OneToOne(targetEntity="APIBundle\Entity\Cities")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id_city")
     */
    private $city;

    /**
     * Many posts belong to one user
     *
     * @var object
     *
     * @ORM\ManyToOne(targetEntity="APIBundle\Entity\Users", inversedBy="posts")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * Status of the Post
     *
     * @var string
     *
     * @ORM\Column(name="post_status", type="integer")
     */
    protected $postStatus;

    /**
     * One post have one payment history
     *
     * @var integer
     *
     * @ORM\OneToOne(targetEntity="APIBundle\Entity\PaymentHistory", mappedBy="post", cascade={"persist", "remove"})
     *
     */
    private $paymentHistory;

    // /**
    //  * Many posts are watched for many users

    //  * @ORM\ManyToMany(targetEntity="APIBundle\Entity\Users", cascade={"persist", "remove"})
    //  * @ORM\JoinTable(name="posts_users",
    //  *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
    //  *      joinColumns={@ORM\JoinColumn(name="post_id", referencedColumnName="id_post")}
    //  *      )
    //  */
    // protected $watchers;

    /**
     * One posts have many attachments

     * @ORM\OneToMany(targetEntity="APIBundle\Entity\Attachments", mappedBy="post", cascade={"persist", "remove"})
     *
     */
    protected $attachments;

    /**
     * One posts have many attachments

     * @ORM\OneToMany(targetEntity="APIBundle\Entity\Offers", mappedBy="post", cascade={"persist", "remove"})
     *
     */
    protected $offers;

    /**
     * Date when the job was created
     *
     * @var datetime
     *
     * @ORM\Column(name="date_created", type="datetime")
     */
    private $dateCreated;

    /**
     * Date when the job was updated
     *
     * @var datetime
     *
     * @ORM\Column(name="date_updated", type="datetime")
     */
    private $dateUpdated;

    /**
     * Constructor and initializers
     */
    public function __construct() {
        $this->dateCreated  = new \DateTime('now');
        $this->dateUpdated = new \DateTime('now');
        $this->watchers = new ArrayCollection();
        $this->attachments = new ArrayCollection();
        $this->offers = new ArrayCollection();
    }

    /**
     * Get ID
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get postName
     *
     * @return string
     */
    public function getPostName() {
        return $this->postName;
    }

    /**
     * Set postName
     *
     * @param string $postName
     */
    public function setPostName($postName) {
        $this->postName = $postName;
    }

    /**
     * Get description
     *
     * @return description
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * Get the post base payment
     *
     * @return decimal
     */
    public function getPostPayment() {
        return $this->postPayment;
    }

    /**
     * Set the post base payment
     *
     * @param decimal $postPayment
     */
    public function setPostPayment($postPayment) {
        $this->postPayment = $postPayment;
    }

    /**
     * Get category
     *
     * @return object
     */
    public function getCategory() {
        return $this->category;
    }

    /**
     * Set category
     *
     * @param object $category
     */
    public function setCategory($category) {
        $this->category = $category;
    }


    /**
     * Get city
     *
     * @return object
     */
    public function getCity() {
        return $this->city;
    }

    /**
     * Set city
     *
     * @param object $city
     */
    public function setCity($city) {
        $this->city = $city;
    }

    /**
     * Get dateCreated
     *
     * @return Date
     */
    public function getDateCreated() {
        return $this->dateCreated;
    }

    /**
     * Get dateUpdated
     *
     * @return Date
     */
    public function getDateUpdated() {
        return $this->dateUpdated;
    }

    /**
     * Set dateUpdated
     *
     * @param Date $dateUpdated
     */
    public function setDateUpdated($dateUpdated) {
        $this->dateUpdated = $dateUpdated;
    }

    /**
     * Get user
     *
     * @return object
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param object $user
     */
    public function setUser($user) {
        $this->user = $user;
    }

    /**
     * Get the post status
     *
     * @return integer
     */
    public function getPostStatus() {
        return $this->postStatus;
    }

    /**
     * Set the post status
     *
     * @param integer $postStatus
     */
    public function setPostStatus($postStatus) {
        $this->postStatus = $postStatus;
    }

    // /**
    //  * Get watchers
    //  *
    //  * @return collection
    //  */
    // public function getWatchers() {
    //     return $this->watchers;
    // }

    // *
    //  * Set watchers
    //  *
    //  * @param collection $watchers

    // public function setWatchers($watchers) {
    //     $this->watchers = $watchers;
    // }

    /**
     * Get attachments
     *
     * @return collection
     */
    public function getAttachments() {
        return $this->attachments;
    }

    /**
     * Set attachments
     *
     * @param object $attachments
     */
    public function setAttachments($attachments) {
        $this->attachments = $attachments;
    }

    /**
     * Get offers
     *
     * @return collection
     */
    public function getOffers() {
        return $this->offers;
    }

    /**
     * Set offers
     *
     * @param object $offers
     */
    public function setOffers($offers) {
        $this->offers = $offers;
    }

    /**
     * Method will return the basic information related to
     * the card will be displayed on the site
     *
     * @param string $noImagePath
     */
    public function toBasicInformation($noImagePath) {
        return array(
            'id'            => $this->id,
            'postname'      => $this->postName,
            'description'   => $this->description,
            'category'      => $this->category->getNameJobsCategories(),
            'city'          => $this->city->getNameCity(),
            'publishedBy'   => $this->user->getUsername(),
            'image'         => !empty($this->attachments) && count($this->attachments) > 0? $this->attachments[0]->getPath():$noImagePath
        );
    }

}
