<?php

namespace APIBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constrains as Assert;

/**
 * Class will contain all definitions for the table
 * Report Fake Projects
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Entity\ReportFakeProject
 *
 * @ORM\Entity(repositoryClass="APIBundle\Repository\ReportFakeProjectsRepository")
 * @ORM\Table(name="report_fake_projects")
 */
class ReportFakeProjects {
    /**
     * ID primary key used in the table
     *
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Id of the post will be marked as fake project
     *
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="APIBundle\Entity\Posts", inversedBy="report_fake_projects")
     * @ORM\JoinColumn(name="fake_project_id", referencedColumnName="id_post")
     */
    protected $fakeProjectId;

    /**
     * Id of the user has been reported the fake project
     *
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="APIBundle\Entity\Users", inversedBy="report_fake_projects")
     * @ORM\JoinColumn(name="reported_user_id", referencedColumnName="id")
     */
    protected $reportedUserId;

    /**
     * Date when the recors was created
     *
     * @var datetime
     *
     * @ORM\Column(name="date_created", type="datetime")
     */
    protected $dateCreated;

    /**
     * Date when the record was updated
     *
     * @var datetime
     *
     * @ORM\Column(name="date_updated", type="datetime")
     */
    protected $dateUpdated;

    /**
     * Constructor and Initializers
     */
    public function __construct() {
        $this->dateCreated = new \DateTime('now');
        $this->dateUpdated = new \DateTime('now');
    }

    /**
     * Get the id of the record
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get the Id of the project reported as fake
     *
     * @return object
     */
    public function getReportedFakeProject() {
        return $this->fakeProjectId;
    }

    /**
     * Set the Id of the project reported as fake
     *
     * @var object
     */
    public function setReportedFakeProject($fakeProjectId) {
        $this->fakeProjectId = $fakeProjectId;
    }

    /**
     * Get the user has reported the project as fake
     *
     * @return object
     */
    public function getReportedUserId() {
        return $this->reportedUserId;
    }

    /**
     * Set the user has reported the project as fake
     *
     * @var object
     */
    public function setReportedUserId($reportedUserId) {
        $this->reportedUserId = $reportedUserId;
    }

    /**
     * Get the date created
     *
     * @return datetime
     */
    public function getDateCreated() {
        return $this->dateCreated;
    }

    /**
     * Get the date updated
     *
     * @return datetime
     */
    public function getDateUpdated() {
        return $this->dateUpdated;
    }

    /**
     * Set the date updated
     *
     * @var datetime
     */
    public function setDateUpdated($dateUpdated) {
        $this->dateUpdated = $dateUpdated;
    }
}
