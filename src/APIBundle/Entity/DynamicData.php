<?php

namespace APIBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constrains as Assert;

/**
 * Class will contain all the information
 * is going to be used to fill the information
 * dynamically
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Entity\DynamicData
 *
 * @ORM\Entity(repositoryClass="APIBundle\Repository\DynamicDataRepository")
 * @ORM\Table(name="dynamic_data")
 */
class DynamicData {
    /**
     * ID Primary Key for dynamic data
     *
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Section will be displayed dynamic data
     *
     * @var string
     *
     * @ORM\Column(name="section", type="integer", nullable=true)
     */
    protected $section;

    /**
     * Text will be displayed or paths to video / images
     *
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    protected $description;

    /**
     * Image will be displayed in case the data is required
     *
     * @var string
     *
     * @ORM\Column(name="file", type="string", length=500, nullable=true)
     */
    protected $file;

    /**
     * Image will be resized in case exceeds the dimensions
     *
     * @var string
     *
     * @ORM\Column(name="file_resize", type="string", length=500, nullable=true)
     */
    protected $fileResize;

    /**
     * Original name of the image
     *
     * @var string
     *
     * @ORM\Column(name="original_name", type="string", length=500, nullable=true)
     */
    protected $originalName;

    /**
     * URL in case the user wants to add a video in the slider
     *
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=500, nullable=true)
     */
    protected $url;

    /**
     * Value that will contain if it is a file or url
     *
     * @var integer
     *
     * @ORM\Column(name="type_file", type="integer")
     */
    protected $typeFile;

    /**
     * Get the type file will be stored in the file column
     *
     * @var string
     *
     * @ORM\Column(name="file_extension", type="string", length=10, nullable=true)
     */
    protected $extension;

    /**
     * Status of the record to know if the data is active or not
     *
     * @var boolean
     *
     * @ORM\Column(name="status", type="integer")
     */
    protected $status;

    /**
     * Date the record was created
     *
     * @var datetime
     *
     * @ORM\Column(name="date_created", type="datetime")
     */
    protected $dateCreated;

    /**
     * Date the record was updated
     *
     * @var datetime
     *
     * @ORM\Column(name="date_updated", type="datetime")
     */
    protected $dateUpdated;

    /**
     * Constructors and initializers
     */
    public function __construct() {
        $this->dateCreated = new \DateTime('now');
        $this->dateUpdated = new \DateTime('now');
    }

    /**
     * Get the ID
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get section
     *
     * @return string
     */
    public function getSection() {
        return $this->section;
    }

    /**
     * Set section
     *
     * @var string $section
     */
    public function setSection($section) {
        $this->section = $section;
    }

    /**
     * Get the description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set the description
     *
     * @var string $description
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * Get the file
     *
     * @return string
     */
    public function getFile() {
        return $this->file;
    }

    /**
     * Set the file
     *
     * @param string $file
     */
    public function setFile($file) {
        $this->file = $file;
    }

    /**
     * Get the file resized
     *
     * @return string
     */
    public function getFileResize() {
        return $this->fileResize;
    }

    /**
     * Set the file resized
     *
     * @param string $fileResize
     */
    public function setFileResize($fileResize) {
        $this->fileResize = $fileResize;
    }

    /**
     * Get the original name of the image
     *
     * @return string
     */
    public function getOriginalName() {
        return $this->originalName;
    }

    /**
     * Set the original name of the image
     *
     * @param string $originalName
     */
    public function setOriginalName($originalName) {
        $this->originalName = $originalName;
    }

    /**
     * Get the url of the file
     *
     * @return string
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * Set the url of the file
     *
     * @param string $url
     */
    public function setUrl($url) {
        $this->url = $url;
    }

    /**
     * Get the type of file
     *
     * @return integer
     */
    public function getTypeFile() {
        return $this->typeFile;
    }

    /**
     * Set the type of file
     *
     * @param integer $typeFile
     */
    public function setTypeFile($typeFile) {
        $this->typeFile = $typeFile;
    }

    /**
     * Get the value of the file extension stores in file column
     *
     * @return string
     */
    public function getExtension() {
        return $this->extension;
    }

    /**
     * Set the extension of the file will store in the file column
     *
     * @var string $extension
     */
    public function setExtension($extension) {
        $this->extension = $extension;
    }

    /**
     * Get the status
     *
     * @return boolean
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set the status
     *
     * @var boolean $status
     */
    public function setStatus($status) {
        $this->status = $status;
    }

    /**
     * Get the date created
     *
     * @return datetime
     */
    public function getDateCreated() {
        return $this->dateCreated;
    }

    /**
     * Get date updated
     *
     * @return datetime
     */
    public function getDateUpdated() {
        return $this->dateUpdated;
    }

    /**
     * Set date updated
     *
     * @var datetime $dateUpdated
     */
    public function setDateUpdated($dateUpdated) {
        $this->dateUpdated = $dateUpdated;
    }
}
