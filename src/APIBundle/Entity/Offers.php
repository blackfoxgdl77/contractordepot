<?php

namespace APIBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constrains as Assert;

/**
 * Class will contain all the information of the
 * Offers table
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Entity\Offers
 *
 * @ORM\Entity(repositoryClass="APIBundle\Repository\OffersRepository")
 * @ORM\Table(name="offers")
 */
 class Offers {
     /**
      * Id for unique key in the database
      *
      * @var integer
      *
      * @ORM\Id
      * @ORM\Column(name="id_offer", type="integer")
      * @ORM\GeneratedValue(strategy="AUTO")
      */
     protected $id;

     /**
      * Many offers have one user
      *
      * @var object
      *
      * @ORM\ManyToOne(targetEntity="APIBundle\Entity\Users", inversedBy="offers")
      * @ORM\JoinColumn(name="bidder_id", referencedColumnName="id", nullable = true)
      */
     protected $user;

     /**
      * Many offers have the owner of the project
      *
      * @var object
      *
      * @ORM\ManyToOne(targetEntity="APIBundle\Entity\Users", inversedBy="offers")
      * @ORM\JoinColumn(name="auctioneer_id", referencedColumnName="id", nullable = true)
      */
     protected $auctionerId;

     /**
      * Many offers have one post
      *
      * @var object
      *
      * @ORM\ManyToOne(targetEntity="APIBundle\Entity\Posts", inversedBy="offers")
      * @ORM\JoinColumn(name="bidder_post", referencedColumnName="id_post")
      *
      */
     protected $post;

     /**
      * Amount of the money bidder
      *
      * @var decimal
      *
      * @ORM\Column(name="money_bidder", type="decimal")
      */
     protected $moneyBidder;

     /**
      * Is active auction flag
      *
      * @var integer
      *
      * @ORM\Column(name="is_active_auction", type="integer")
      */
     protected $isActiveAuction;

     /**
      * Date when the account was created
      *
      * @var datetime
      *
      * @ORM\Column(name="date_created", type="datetime")
      */
     protected $dateCreated;

     /**
      * Constructor and Initializers
      */
     public function __construct() {
        $this->moneyBidder     = 0.0;
        $this->isActiveAuction = true;
        $this->dateCreated     = new \DateTime('now');
     }

    /**
     * Get ID
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get user
     *
     * @return object
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param object $user
     */
    public function setUser($user) {
        $this->user = $user;
    }

    /**
     * Get auctioneer_id
     *
     * @return object
     */
    public function getAuctionerId() {
        return $this->auctionerId;
    }

    /**
     * Set the auctioneer_id
     *
     * @param object $auctionerId
     */
    public function setAuctionerId($auctionerId) {
        $this->auctionerId = $auctionerId;
    }

    /**
     * Get post
     *
     * @return object
     */
    public function getPost() {
        return $this->post;
    }

    /**
     * Set post
     *
     * @param object $post
     */
    public function setPost($post) {
        $this->post = $post;
    }

    /**
     * Get moneyBidder
     *
     * @return float
     */
    public function getMoneyBidder() {
        return $this->moneyBidder;
    }

    /**
     * Set moneyBidder
     *
     * @param float $moneyBidder
     */
    public function setMoneyBidder($moneyBidder) {
        $this->moneyBidder = $moneyBidder;
    }

    /**
     * Get isActiveAuction
     *
     * @return integer
     */
    public function getIsActiveAuction() {
        return $this->isActiveAuction;
    }

    /**
     * Set isActiveAuction
     *
     * @param integer $isActiveAuction
     */
    public function setIsActiveAuction($isActiveAuction) {
        $this->isActiveAuction = $isActiveAuction;
    }

    /**
     * Get dateCreated
     *
     * @return datetime
     */
    public function getDateCreated() {
        return $this->dateCreated;
    }
 }
