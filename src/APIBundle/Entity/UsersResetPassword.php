<?php

namespace APIBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constrains as Assert;

/**
 * Class will contain all the fields will be used to
 * update the users password when they forget it
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Entity\UsersResetPassword
 *
 * @ORM\Entity(repositoryClass="APIBundle\Repository\UsersResetPasswordRepository")
 * @ORM\Table(name="users_reset_password")
 */
 class UsersResetPassword {
     /**
      * ID primary key for this table
      *
      * @var string
      *
      * @ORM\Id
      * @ORM\Column(name="id", type="integer")
      * @ORM\GeneratedValue(strategy="AUTO")
      */
     private $id;

     /**
      * Token will be used for the reset
      *
      * @var string
      *
      * @ORM\Column(name="token_reset", type="string", length=255)
      */
     private $tokenReset;

     /**
      * Flag to know if is active any record to reset password
      *
      * @var boolean
      *
      * @ORM\Column(name="is_active", type="boolean")
      */
     private $isActive;

     /**
      * Id of the users table
      *
      * @var integer
      *
      * @ORM\ManyToOne(targetEntity="APIBundle\Entity\Users", inversedBy="resetPasswords")
      * @ORM\JoinColumn(name="users_id", referencedColumnName="id")
      */
     private $user;

     /**
      * Email of the user tries to reset password
      *
      * @var string
      *
      * @ORM\Column(name="users_email", type="string", length=200)
      */
     private $usersEmail;

     /**
      * Date when the record has been created
      *
      * @var datetime
      *
      * @ORM\Column(name="date_created", type="datetime")
      */
     private $dateCreated;

     /**
      * Date when the record has been updated
      *
      * @var datetime
      *
      * @ORM\Column(name="date_updated", type="datetime")
      */
     private $dateUpdated;

     /**
      * Constructor and Initializers
      */
     public function __construct() {
        //$this->user = new Users();
        $this->dateCreated = new \DateTime('now');
        $this->dateUpdated = new \DateTime('now');
     }

     /**
      * Get Id of the table
      *
      * @return integer
      */
     public function getId() {
         return $this->id;
     }

     /**
      * Get token used for reset password
      *
      * @return string
      */
     public function getTokenReset() {
         return $this->tokenReset;
     }

     /**
      * Set token used for reset password
      *
      * @var string $tokenReset
      */
     public function setTokenReset($tokenReset) {
         $this->tokenReset = $tokenReset;
     }

     /**
      * Get the flag to know if the status is active
      *
      * @return boolean
      */
     public function getIsActive() {
         return $this->isActive;
     }

     /**
      * Set the flag to know if the status is active
      *
      * @var boolean $isActive
      */
     public function setIsActive($isActive) {
         $this->isActive = $isActive;
     }

     /**
      * Get the user
      *
      * @return object
      */
     public function getUser() {
         return $this->user;
     }

     /**
      * Set the user
      *
      * @var object
      */
     public function setUser($user) {
         $this->user = $user;
     }

     /**
      * Get the email of user wants to reset password
      *
      * @return string
      */
     public function getUsersEmail() {
         return $this->usersEmail;
     }

     /**
      * Set the user email wants to reset the password
      *
      * @var string $usersEmail
      */
     public function setUsersEmail($usersEmail) {
         $this->usersEmail = $usersEmail;
     }

     /**
      * Get the date when was created the record
      *
      * @return $dateCreated
      */
     public function getDateCreated() {
         return $this->dateCreated;
     }

     /**
      * Get the date when the record is updated
      *
      * @return datetime
      */
     public function getDateUpdated() {
         return $this->dateUpdated;
     }

     /**
      * Set the date when the records is updated
      *
      * @var datetime $dateUpdated
      */
     public function setDateUpdated($dateUpdated) {
         $this->dateUpdated = $dateUpdated;
     }
 }
