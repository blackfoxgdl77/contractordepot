<?php

namespace APIBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentHistory
 *
 * @ORM\Table(name="payment_history")
 * @ORM\Entity(repositoryClass="APIBundle\Repository\PaymentHistoryRepository")
 */
class PaymentHistory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_payment", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Many payments history belong to one user
     *
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="APIBundle\Entity\Users", inversedBy="paymentsHistory")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable = true)
     */
    protected $user;

    /**
     * Many payments history belong to one post
     *
     * @var integer
     *
     * @ORM\OneToOne(targetEntity="APIBundle\Entity\Posts", inversedBy="paymentHistory")
     * @ORM\JoinColumn(name="post_id", referencedColumnName="id_post")
     */
    protected $post;

    /**
     * Amount will be paid by the user once accept the bids
     *
     * @var decimal
     *
     * @ORM\Column(name="amount", type="decimal")
     */
    protected $amount;

    /**
     * Date when the account was created
     *
     * @var datetime
     *
     * @ORM\Column(name="date_created", type="datetime")
     */
    protected $dateCreated;

    /**
     * Date when the data of this table has been updated
     *
     * @var datetime
     *
     * @ORM\Column(name="date_updated", type="datetime")
     */
    protected $dateUpdated;

    /****** TEST ******/
    /**
     * @ORM\Column(name="status", type="string", length=20)
     */
    protected $status;
    /**
     * @ORM\Column(name="cart", type="string", length=100)
     */
    protected $cart;
    /**
     * @ORM\Column(name="id_transaction", type="string", length=100)
     */
    protected $idTransaction;
    /**
     * @ORM\Column(name="date_payment", type="string", length=100)
     */
    protected $datePayment;

    public function getStatus() {
        return $this->status;
    }
    public function setStatus($status) {
        $this->status = $status;
    }
    public function getCart() {
        return $this->cart;
    }
    public function setCart($cart) {
        $this->cart = $cart;
    }
    public function getTransactionId() {
        return $this->idTransaction;
    }
    public function setTransactionId($idTransaction) {
        $this->idTransaction = $idTransaction;
    }
    public function getDatePayment() {
        return $this->datePayment;
    }
    public function setDatePayment($datePayment) {
        $this->datePayment = $datePayment;
    }

    /**
     * Constructor and initializers
     */
    public function __construct() {
        $this->user = new Users();
        $this->post = new Posts();
        $this->dateCreated = new \DateTime('now');
        $this->dateUpdated = new \DateTime('now');
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param object $user
     */
    public function setUser($user) {
        $this->user = $user;
    }

    /**
     * Get user
     *
     * @return object
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set post
     *
     * @param object $post
     */
    public function setPost($post) {
        $this->post = $post;
    }

    /**
     * Get post
     *
     * @return object
     */
    public function getPost() {
        return $this->post;
    }

    /**
     * Get the amount will be paid by the owner of the post
     *
     * @return decimal
     */
    public function getAmount() {
        return $this->amount;
    }

    /**
     * Set the amount will be paid by the owner of the post
     *
     * @var decimal $amount
     */
    public function setAmount($amount) {
        $this->amount = $amount;
    }

    /**
     * Get dateCreated
     *
     * @return datetime
     */
    public function getDateCreated() {
        return $this->dateCreated;
    }

    /**
     * Get dateUpdated
     *
     * @return datetime
     */
    public function getDateUpdated() {
        return $this->dateUpdated;
    }

    /**
     * Set dateUpdated
     *
     * @param datetime
     */
    public function setDateUpdated($dateUpdated) {
        $this->dateUpdated = $dateUpdated;
    }

}
