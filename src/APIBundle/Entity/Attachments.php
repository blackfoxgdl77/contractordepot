<?php

namespace APIBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constrains as Assert;

/**
 * Class contains all the information of the
 * database and the table attachment
 *
 * APIBundle\Entity\Attachments
 * ruvicdev
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 *
 * @ORM\Entity(repositoryClass="APIBundle\Repository\AttachmentsRepository")
 * @ORM\Table(name="attachments")
 */
class Attachments {
    /**
     * ID for the unique key of the attachment table
     *
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id_attachment", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Name of the attachment
     *
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200)
     */
    private $name;

    /**
     * Path of the attachment
     *
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=100)
     */
    private $path;

    /**
     * Field to know if is image or video
     *
     * @var boolean
     *
     * @ORM\Column(name="is_image", type="boolean")
     */
    private $isImage;

    /**
     * Field is used to save the file extension
     *
     * @var string
     *
     * @ORM\Column(name="file_extension", type="string", length=30)
     */
    private $fileExtension;

    /**
     * Many attachments have one post
     *
     * @var object
     *
     * @ORM\ManyToOne(targetEntity="APIBundle\Entity\Posts", inversedBy="attachments")
     * @ORM\JoinColumn(name="post_id", referencedColumnName="id_post")
     *
     */
    private $post;

    /**
     * Date created the image
     *
     * @var datetime
     *
     * @ORM\Column(name="date_created", type="datetime")
     */
    private $dateCreated;

    /**
     * Date updated the image
     *
     * @var datetime
     *
     * @ORM\Column(name="date_updated", type="datetime")
     */
    private $dateUpdated;

    /**
     * Constructor and initializers
     */
    public function __construct() {
        $this->dateCreated = new \DateTime('now');
        $this->dateUpdated = new \DateTime('now');
        $this->posts = new Posts();
    }


    /**
     * Get ID
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath() {
        return $this->path;
    }

    /**
     * Set path
     *
     * @param string $path
     */
    public function setPath($path) {
        $this->path = $path;
    }

    /**
     * Get isImage
     *
     * @return boolean
     */
    public function getIsImage() {
        return $this->isImage;
    }

    /**
     * Set isImage
     *
     * @param string $isImage
     */
    public function setIsImage($isImage) {
        $this->isImage = $isImage;
    }

    /**
     * Get file extension
     *
     * @return string
     */
    public function getFileExtension(){
        return $this->fileExtension;
    }

    /**
     * Set file extension
     *
     * @param string $fieldExtension
     */
    public function setFileExtension($fileExtension) {
        $this->fileExtension = $fileExtension;
    }

    /**
     * Get post
     *
     * @return object
     */
    public function getPost() {
        return $this->post;
    }

    /**
     * Set post
     *
     * @param object $post
     */
    public function setPost($post) {
        $this->post = $post;
    }

    /**
     * Get dateCreated
     *
     * @return Date
     */
    public function getDateCreated() {
        return $this->dateCreated;
    }

    /**
     * Get dateUpdated
     *
     * @return Date
     */
    public function getDateUpdated() {
        return $this->dateUpdated;
    }

    /**
     * Set dateUpdated
     *
     * @param Date $dateUpdated
     */
    public function setDateUpdated($dateUpdated) {
        $this->dateUpdated = $dateUpdated;
    }

}
