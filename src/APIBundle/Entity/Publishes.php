<?php

namespace APIBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constrains as Assert;

/**
 * Class will contains all the information about
 * all the data will be  used for posting information
 * base of conversation in the platform
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Entity\Publishes
 *
 * @ORM\Entity(repositoryClass="APIBundle\Repository\PublishesRepository")
 * @ORM\Table(name="publishes")
 */
class Publishes {
    /**
     * ID key to identify primary key
     *
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * ID of the user has created the entry
     *
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="APIBundle\Entity\Users", inversedBy="publishes")
     * @ORM\JoinColumn(name="user_publish_id", referencedColumnName="id")
     */
    protected $userPublishId;

    /**
     * ID of the post has been commented
     *
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="APIBundle\Entity\Posts", inversedBy="publishes")
     * @ORM\JoinColumn(name="post_id", referencedColumnName="id_post")
     */
    protected $postId;

    /**
     * ID of the user has created the post
     *
     * @ORM\ManyToOne(targetEntity="APIBundle\Entity\Users", inversedBy="publishes")
     * @ORM\JoinColumn(name="owner_post_id", referencedColumnName="id")
     */
    protected $ownerPostId;

    /**
     * Comments will be added to the project
     *
     * @var string
     *
     * @ORM\Column(name="comments", type="string", length=400, nullable=false)
     */
    protected $comments;

    /**
     * Title will be added to the comment
     *
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=200)
     */
    protected $title;

    /**
     * Date when the comment has been created
     *
     * @var datetime
     *
     * @ORM\Column(name="date_created", type="datetime")
     */
    protected $dateCreated;

    /**
     * Constructor and Initializers
     */
    public function __construct() {
        $this->dateCreated = new \DateTime('now');
    }

    /**
     * Get the Id of the comment
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get the Id of the user has created the post
     *
     * @return APIBundle\Entity\Users
     */
    public function getUserPublishesId() {
        return $this->userPublishId;
    }

    /**
     * Set the id of the user has created the post
     *
     * @param object APIBundle\Entity\Users $userPublishId
     */
    public function setUserPublishesId($userPublishId) {
        $this->userPublishId = $userPublishId;
    }

    /**
     * Get the post Id
     *
     * @return APIBundle\Entity\Posts
     */
    public function getPostsId() {
        return $this->postId;
    }

    /**
     * Set the post Id
     *
     * @param object APIBundle\Entity\Posts $postId
     */
    public function setPostId($postId) {
        $this->postId = $postId;
    }

    /**
     * Get the ID of owner of the post
     *
     * @return APIBundle\Entity\Users
     */
    public function getOwnerPostsId() {
        return $this->ownerPostId;
    }

    /**
     * Set the owner of the post
     *
     * @param object APIBundle\Entity\Users $ownerPostId
     */
    public function setOwnerPostsId($ownerPostId) {
        $this->ownerPostId = $ownerPostId;
    }

    /**
     * Get the comment of the user
     *
     * @return string
     */
    public function getComments() {
        return $this->comments;
    }

    /**
     * Set the comments of the user
     *
     * @param string $comments
     */
    public function setComments($comments) {
        $this->comments = $comments;
    }

    /**
     * Get the title of the comment
     *
     * @return string
     */
    public function getTitleComment() {
        return $this->title;
    }

    /**
     * Set the title of the comment
     *
     * @param string $title
     */
    public function setTitleComment($title) {
        $this->title = $title;
    }

    /**
     * Get the date when the post was created
     *
     * @return datetime
     */
    public function getDateCreated() {
        return $this->dateCreated;
    }
}
