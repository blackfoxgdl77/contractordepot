<?php

namespace APIBundle\Repository;

use Doctrine\ORM\EntityRepository;
use APIBundle\Entity\Comments;

/**
 * Class will contain methods are going to
 * be used to get all the information of the
 * comments has been related to the main
 * published
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Repository\CommentsRepository
 */
class CommentsRepository extends EntityRepository {

    /**
     * Get all the email users for sending the emails
     * to all the users involved in the comments replied
     *
     * @param integer $publishId
     * @param integer $userPostedId
     *
     * @return array
     */
    public function getEmailsFromUsers($publishId, $userPostedId) {
        $query = "SELECT DISTINCT C.id, U.email, U.name, P.title
                  FROM comments AS C
                  INNER JOIN publishes AS P ON P.id = C.publish_id
                  INNER JOIN users AS U on U.id = C.user_id
                  WHERE C.publish_id = " . $publishId . "
                  AND   C.user_id != " . $userPostedId . "
                  GROUP BY U.email";

        $smtm = $this->getEntityManager()->getConnection()->prepare($query);
        $smtm->execute();
        $results = $smtm->fetchAll();

        return $results;
    }
}
