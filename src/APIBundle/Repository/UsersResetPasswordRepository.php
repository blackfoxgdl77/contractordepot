<?php

namespace APIBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Repository will contain all the information about queries
 * enabled to validate information about reset password
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Repository\UsersResetPassword
 */
class UsersResetPasswordRepository extends EntityRepository {

    /**
     * Method will be used to validate if the
     * token is valid regarding the Information
     * set by the user once requested the change
     * of the password
     *
     * @param string $token
     * @return integer $totalNumber
     */
    public function checkTokenIsValid($token) {
        $query = $this->createQueryBuilder('u')
                      ->select('COUNT(u.id)')
                      ->where('u.tokenReset = :token')
                      ->andWhere('u.isActive = 1')
                      ->setParameter('token', $token)
                      ->getQuery()
                      ->getSingleScalarResult();

        return $query;
    }

    /**
     * Get the information of the users in reset
     * password once the user wants to reset it
     * because he forgot the password, if exists any
     * request, the system must change the status of old
     * enabled request before create another one
     *
     * @param int $userID
     * @return integer
     */
    public function checkEnableStatus($id) {
        $query = $this->createQueryBuilder('u')
                      ->where('u.user = :id')
                      ->andWhere('u.isActive = 1')
                      ->setParameter('id', $id)
                      ->getQuery();

        return $query->getResult();
    }

    /**
     * Method will return the information that
     * is nedded to update the status of the reset
     * password just to get 1 record updated
     *
     * @param string $token
     * @return array
     */
    public function getResetPasswordData($token) {
        $query = $this->createQueryBuilder('u')
                      ->where('u.tokenReset = :token')
                      ->andWhere('u.isActive = 1')
                      ->setParameter('token', $token)
                      ->getQuery();

        return $query->getResult();
    }
}
