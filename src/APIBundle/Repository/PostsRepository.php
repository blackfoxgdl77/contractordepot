<?php

namespace APIBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Repository of posts that will be used to
 * store main posts
 *
 * @author
 * @package APIBundle\Repository\PostsRepository
 */
class PostsRepository extends EntityRepository {

    /**
     * Method will be used to get all the information
     * of the post has been reported as fake project
     *
     * @param integer $id
     * @return array $query
     */
    public function getPostsById($id) {
        $query = "SELECT *
                  FROM Posts
                  WHERE id_post = " . $id;

        $smtm = $this->getEntityManager()->getConnection()->prepare($query);
        $smtm->execute();
        $results = $smtm->fetchAll();

        return $results;
    }

    /**
     * Method will be used to get the information of the first 5
     * post has been created most recently. If there are fewer than
     * 5, then will be displayed those posts
     *
     * @param integer $totalRecords
     * @return array $query
     */
    public function getPostRecently($totalRecords) {
        $query = "SELECT P.id_post, P.post_name, P.description,
                         A.id_attachment, A.name, A.path, A.is_image,
                         A.file_extension, A.post_id,
                         C.id_city, C.name_city, C.capital_city,
                         JC.id_jobs_categories, JC.name_jobs_categories
                  FROM posts AS P
                  INNER JOIN attachments AS A ON A.post_id = P.id_post
                  INNER JOIN cities AS C ON C.id_city = P.city_id
                  INNER JOIN jobs_categories AS JC ON JC.id_jobs_categories = P.category_job_id
                  WHERE P.post_status = 1
                  GROUP BY P.id_post
                  ORDER BY P.date_created DESC,
                           P.date_updated DESC
                  LIMIT 0, " . $totalRecords;

        $smtm = $this->getEntityManager()->getConnection()->prepare($query);
        $smtm->execute();
        $results = $smtm->fetchAll();

        return $results;
    }
}
