<?php

namespace APIBundle\Repository;

use Doctrine\ORM\EntityRepository;
use APIBundle\Entity\UsersInformation;

/**
 * Repository of the users information will contains
 * custom methods to get information from the
 * the table users Information
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso@gmail.com>
 * @package APIBundle\Repository\UsersInformationRepository
 */
class UsersInformationRepository extends EntityRepository {

    /**
     * Method will be used to get the information related to the users
     * personal information
     *
     * @param integer $id
     * @return array
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     */
    public function getUserInformationById($id) {
        $query = "SELECT U.id, U.name, U.email, U.username, U.is_active, U.date_created,  U.date_updated,
                         UI.payment_method, UI.bbb_account, UI.address, UI.date_created, UI.date_updated,
                         UI.phone, UI.bbb_accredited, UI.wcb_number, U.type_user, UI.raiting
                  FROM users_information as UI
                  INNER JOIN users as U ON U.ID = UI.USER_ID
                  WHERE UI.USER_ID = " . $id .
                  " AND U.IS_ACTIVE = 1 ";

        $smtm = $this->getEntityManager()->getConnection()->prepare($query);
        $smtm->execute();
        $results = $smtm->fetchAll();

        return $results[0];
    }
}
