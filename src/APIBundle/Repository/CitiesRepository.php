<?php

namespace APIBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Repository of cities that will be used to store
 * cities information
 *
 * @author 
 * @package APIBundle\Repository\CitiesRepository
 */
class CitiesRepository extends EntityRepository {
}