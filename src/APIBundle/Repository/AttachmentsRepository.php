<?php

namespace APIBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Repository will contain queries related
 * to attachments in order to get the data
 * and could display the information on the site
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Repository\AttachmentsRepository
 */
class AttachmentsRepository extends  EntityRepository {
}
