<?php

namespace APIBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Repository will contain methods related to the
 * dynamic data
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Repository\DynamicDataRepository
 */
class DynamicDataRepository extends EntityRepository {

    /**
     * Method will get all the records with specific status
     * and section to know if there are more records enabled
     * that should be updated to inactive
     *
     * @param integer $section
     * @param integer $status
     * @return object
     */
    public function getDynamicBySection($section, $status) {
        $query = $this->createQueryBuilder('u')
                      ->where('u.section = :section')
                      ->andWhere('u.status = :status')
                      ->setParameters(array('section' => $section,
                                            'status'  => $status))
                      ->getQuery();

        return $query->getResult();
    }

    /**
     * Method will return all the information depending the status
     * enabled in the records. This method just returns all the records
     * active or inactive
     *
     * @param integer $section
     * @return object
     */
     public function getAllDynamicData($section) {
         $query = "SELECT DD.*
                   FROM dynamic_data AS DD
                   WHERE DD.STATUS != 3
                   AND   DD.SECTION = " . $section . "
                   ORDER BY DD.SECTION ASC,
                            DD.DATE_CREATED DESC,
                            DD.DATE_UPDATED DESC";

         $results = $this->getEntityManager()->getConnection()->prepare($query);
         $results->execute();
         $finalData = $results->fetchAll();

         return $finalData;
     }

     /**
      * Method will return the information that is required to display
      * in the slider in the index page with all the filters
      * required for the different sections
      *
      * @return object
      */
     public function imagesForBanners() {
         $returnedResults = array();
         $typeVideo       = "";
         $idVideo         = "";

         $query = "SELECT *
                   FROM dynamic_data
                   WHERE section = 1
                   AND   status = 1";

         $results = $this->getEntityManager()->getConnection()->prepare($query);
         $results->execute();
         $finalData = $results->fetchAll();

         foreach ($finalData as $data) {
             if ($data['type_file'] == 1){
                 if (strpos($data['url'], "youtube") !== false || strpos($data['url'], "youtu.be") !== false) {
                     $typeVideo = "youtube";
                     $idValue   = (strpos($data['url'], "=") !== false) ? explode('=', $data['url']) : explode("/", $data['url']);
                     $idVideo   = $idValue[count($idValue) -1];
                 }
             }

             $returnedResults[] = array(
                 'id'           => $data['id'],
                 'description'  => $data['description'],
                 'file'         => $data['file'],
                 'file_resize'  => $data['file_resize'],
                 'originalName' => $data['original_name'],
                 'url'          => $data['url'],
                 'type_file'    => $data['type_file'],
                 'typeVideo'    => $typeVideo,
                 'idVideo'      => $idVideo,
             );
         }

         return $returnedResults;
     }
}
