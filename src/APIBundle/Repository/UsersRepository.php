<?php

namespace APIBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use APIBundle\Entity\Users;

/**
 * Repository of users that will be used to get
 * different information about the users section and
 * get custom query information
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Repository\UserRepository
 */
class UsersRepository extends EntityRepository implements UserProviderInterface {
    /**
     * Method for checking the email typed by the user
     *
     * @param string $data
     *
     * @return object
     */
    public function loadUserByUsername($data) {
        $query = $this->createQueryBuilder('u')
                      ->where('u.email = :email')
                      ->setParameter('email', $data)
                      ->getQuery()
                      ->getOneOrNullResult();

        if (null == $query) {
            $message = sprintf(
                'Unable to find an active admin identified by "%s".', $data
            );
        }

        return $query;
    }

    /**
     * Refresh the users login
     *
     * @param UserInterface user
     *
     * @return object row found
     */
    public function refreshUser(UserInterface $user) {
        $class = get_class($user);

        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', $class)
            );
        }

        return $this->find($user->getId());
    }

    /**
     * Check if the class is supported
     *
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class) {
        return $this->getEntityName() === $class || is_subclass_of($class, $this->getEntityName());
    }

    /**
     * Method is going to be used to check the data from
     * the email will be displayed on the site, but for this
     * query I'm going to get the information of the user
     * via unique field email
     *
     * @param string $email
     * @return object
     */
    public function findByEmail($email) {
        $query = $this->createQueryBuilder('u')
                      ->where('u.email = :email')
                      ->setParameter('email', $email)
                      ->getQuery();

        return $query->getResult();
    }

    /**
     * Method will be used to return the data
     * of the user via ID where we can get for
     * update any data. This method will return
     * and array with all the information of the user
     *
     * @param integer $id
     * @return object
     */
    public function findById($id) {
        $query = $this->createQueryBuilder('u')
                      ->where('u.id = :id')
                      ->setParameter('id', $id)
                      ->getQuery();

        return $query->getResult();
    }

    /**
     * Method will be used to get all the information
     * of the token to know if it has been used or hasn't be
     *
     * @param string $token
     * @return $response
     */
    public function findTokenValid($token) {
        $query = $this->createQueryBuilder('u')
                      ->where('u.activationToken = :token')
                      ->setParameter('token', $token)
                      ->getQuery()
                      ->getResult();

        return $query;
    }
}
