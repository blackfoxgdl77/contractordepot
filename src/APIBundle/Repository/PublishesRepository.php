<?php

namespace APIBundle\Repository;

use Doctrine\ORM\EntityRepository;
use APIBundle\Entity\Publishes;

/**
 * Reporsitory of the publishes that has been
 * posted on the post. This class will return
 * the information needed that will be displayed on
 * the platform
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Repository\PublishesRepository
 */
class PublishesRepository extends EntityRepository {
}
