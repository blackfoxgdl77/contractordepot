<?php

namespace APIBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Repository of job categories that will be used to
 * store main job categories
 *
 * @author
 * @package APIBundle\Repository\JobsCategoriesRepository
 */
class JobsCategoriesRepository extends EntityRepository {

    /**
     * Method will return all the jobs categories will be
     * displayed as filters on the left side of the posts
     * for giving the users another option to see jobs
     * by category
     *
     * @return array
     */
    public function getPostByCategories() {
        $query = "SELECT C.id_jobs_categories, C.name_jobs_categories, COUNT(P.id_post) as TOTAL
                  FROM posts as P
                  RIGHT JOIN jobs_categories AS C ON C.id_jobs_categories = P.category_job_id
                  GROUP BY P.category_job_id
                  HAVING TOTAL > 0";

        $smtm = $this->getEntityManager()->getConnection()->prepare($query);
        $smtm->execute();
        $results = $smtm->fetchAll();

        return $results;
    }
}
