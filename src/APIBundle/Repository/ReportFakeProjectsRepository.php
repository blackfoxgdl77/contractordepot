<?php

namespace APIBundle\Repository;

use Doctrine\ORM\EntityRepository;
use APIBundle\Entity\ReportFakeProjects;

/**
 * Repository of the fake projects that will
 * contain custom methods used for get different
 * information from report fake projects table
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Repository\ReportFakeProjectsRepository
 */
class ReportFakeProjectsRepository extends EntityRepository {

    /**
     * Method will be used to return the information is going
     * to show all the data about the report of fake projects
     *
     * @return objects $query
     */
    public function getAllFakeProjects() {
        $query = $this->createQueryBuilder('u')
                      ->groupBy('u.fakeProjectId')
                      ->getQuery();

        return $query->getResult();
    }

    /**
     * Method will return all the users are enabled per project
     * to get the information of them and displaying on the
     * UI when the user will want check the user's information
     *
     * @param integer $id
     * @return array $query
     */
    public function getUsersPerProject($id) {
        $query = "SELECT RFP.REPORTED_USER_ID
                  FROM REPORT_FAKE_PROJECTS AS RFP
                  WHERE RFP.FAKE_PROJECT_ID = " . $id;

        $smtm = $this->getEntityManager()->getConnection()->prepare($query);
        $smtm->execute();
        $results = $smtm->fetchAll();

        return $results;
    }
}
