<?php

namespace APIBundle\Repository;

use Doctrine\ORM\EntityRepository;
use APIBundle\Entity\Offers;
use APIBundle\Entity\Posts;

/**
 * Class will contain all the methods
 * are going to be used on the repo
 * at the moment to get sepcific information
 * of the bids done by specific user
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Repository\OffersRepository
 */
class OffersRepository extends EntityRepository {

    /**
     * Method will return all the bids done by specific
     * user and in descendent order
     *
     * @param integer $id
     * @return object
     */
    public function getBiddersByUser($id) {
        $sql = "SELECT *
                FROM offers
                WHERE bidder_id = " . $id . "
                ORDER BY date_created DESC";

        $smtm = $this->getEntityManager()->getConnection()->prepare($sql);
        $smtm->execute();
        $result = $smtm->fetchAll();

        return $result;
    }

    /**
     * Method will return all the information of the bids
     * finished by the user where he makes an offer to
     * do the job or project
     *
     * @param integer $id
     * @return object
     */
    public function getFinishedBidsByUser($id) {
        $sql = "SELECT *
                FROM offers
                WHERE is_active_auction = 0
                AND bidder_id = " . $id . "
                ORDER BY date_created DESC";

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $smtm->execute();
        $result = $smtm->fetchAll();

        return $result;
    }

    /**
     * Get the post depending the status and the
     * user has offered on the project created by
     * the user and has finished
     *
     * @param integer $id
     * @return array
     */
    public function getPostByUserOfferd($id) {
        $sql = "SELECT *
                FROM offers as O
                LEFT JOIN posts as P on P.id_post = O.id_offer
                WHERE O.bidder_id = " . $id . "
                AND (O.is_active_auction = 0 OR O.is_active_auction = 2)
                GROUP BY O.bidder_post";

        $smtm = $this->getEntityManager()->getConnection()->prepare($sql);
        $smtm->execute();
        $result = $smtm->fetchAll();

        return $result;
    }

    /**
     * Method will be used to get all the information of the users with the
     * currents bids open in the platform and the user has make a bid
     * to try to win the project
     *
     * @param integer $id
     * @return array
     */
    public function getPostsByUserOpenOffers($id) {
        $sql = "SELECT *
                FROM offers AS O
                LEFT JOIN posts AS P ON P.id_post = O.id_offer
                WHERE O.bidder_id = " . $id . "
                AND O.is_active_auction = 1
                GROUP BY O.bidder_post";

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
    }

    /**
     * Method will be used to get all the information has been added
     * to the actions of users that has offers on the project
     *
     * @param integer $idBidder
     * @param array $status
     */
    public function getUsersOffersById($idBidder, $status) {
        $sql = "SELECT U.id, U.username, O.money_bidder, O.is_active_auction, O.date_created
                FROM users AS U
                INNER JOIN offers AS O ON O.bidder_id = U.id
                WHERE O.bidder_post = " . $idBidder . "
                AND O.is_active_auction IN (" . implode(",", $status) . ")";

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
    }

    /**
     * Method wll return the information is going to be
     * used at the moment to get the data for winning
     * users, I mean to get the user win the bid and
     * the owner of the project
     *
     * @param integer $id
     * @return array
     */
    public function getOwnerAndWinnerId($id) {
        $sql = "SELECT O.bidder_id, P.user_id
                FROM offers AS O
                INNER JOIN posts AS P ON P.id_post = O.bidder_post
                WHERE O.is_active_auction = 2
                AND O.bidder_post = " . $id;

        $smtm = $this->getEntityManager()->getConnection()->prepare($sql);
        $smtm->execute();
        $result = $smtm->fetchAll();

        return $result;
    }

    /**
     * Method will return all the information
     * related with the projects has been paid
     * by the owner of the project
     *
     * @return response $queryData
     */
    public function getProjectsDonePaid() {
        $query = "SELECT P.*, O.*
                  FROM offers AS O
                  INNER JOIN payment_history AS P ON P.user_id = O.auctioneer_id
                                                 AND P.post_id = O.bidder_post
                 WHERE O.is_active_auction = 2";

        $results = $this->getEntityManager()->getConnection()->prepare($query);
        $results->execute();
        $queryData = $results->fetchAll();

        return $queryData;
    }

    /**
     * Method will be used to get all the information
     * about the pending projects payment for display the
     * users all the data due to the owner has cancelled the
     * payment
     *
     * @return response $queryData
     */
    public function getProjectsPendingPaid() {
        $query = "SELECT O.*
                  FROM offers AS O
                  WHERE O.is_active_auction = 3";

        $results = $this->getEntityManager()->getConnection()->prepare($query);
        $results->execute();
        $queryData = $results->fetchAll();

        return $queryData;
    }
}
