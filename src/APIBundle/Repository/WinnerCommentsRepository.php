<?php

namespace APIBundle\Repository;

use Doctrine\ORM\EntityRepository;
use APIBundle\Entity\WinnerComments;

/**
 * Repository will have all the methods that
 * will relate with the comments has been done
 * by the project's owner and the project's winner
 * once the owner has accepted the bid
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package APIBundle\Repository\WinnerComments
 */
class WinnerCommentsRepository extends EntityRepository {
    
}
