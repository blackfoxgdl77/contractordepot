<?php

namespace SiteBundle\Libs;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SiteBundle\Libs\RestResponse;
use RegisterErrorBundle\Controller\RegisterErrorController;

/**
 * Class contains all the base functions
 * for the different controllers that contains
 * methods will share between them.
 * Contains functions for parsing JSON data,
 * templates and more.
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package SiteBundle\Libs
 */
class PrincipalController extends Controller {

    /**
     * Name of the bundle to call the repository
     *
     * @var string
     */
    public $bundleName = "APIBundle";

    /**
     * Variable will contain an array
     *
     * @var array | Empty
     */
    protected $arrayData = array();

     /**
      * Variable will contain the registrer error object
      *
      * @var object | NULL
      */
     protected $registerError = NULL;

     /**
      * Variable that contains the request object
      *
      * @var object | NULL
      */
     protected $request = NULL;

     /**
      * Variable will contain the username of the user logged in
      *
      * @var object | NULL
      */
     public $userSession = NULL;

     /**
      * Variable will contain the breadcrumbs
      *
      * @var string
      */
     public $breadcrumbs = array();

     /**
      * Constructor ....
      */
     public function __construct() {
         $this->request = Request::createFromGlobals();
         $this->initMainClases();
     }

     /**
      * Initialize before actions requested
      *
      * @return void
      */
     public function beforeAction() {
         $this->initGlobals();
     }

     /**
      * Init all the clases where going to use in
      * this module once the first load of the site
      * has been done
      *
      * @return void
      *
      * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
      */
     final protected function initMainClases() {
         $this->registerError = new RegisterErrorController();
     }

     /**
      * Set session variables when the user logged in
      * in the platform
      *
      * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
      */
     final protected function __setSessionVariables() {
         if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
             $this->userSession = $this->get('security.token_storage')->getToken()->getUser();
         }
     }

     /**
      * Verify if the username is logged in or not
      *
      * @return boolean
      *
      * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
      */
     final private function __checkUserStatus() {
         if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
             return false;
         }

         return true;
     }

     /**
      * Clear all the information of the session object, thats because the
      * user logout
      *
      * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
      */
     final protected function __clearSessionVariables() {
         if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
             $this->userSession = NULL;
         }
     }

     /**
      * Method used to check if the user is logged in or not
      * and check if the session variable should be filled in
      *
      * @return boolean
      *
      * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
      */
     public function __usersStatus() {
         $flag = $this->__checkUserStatus();
         if (true == $flag) {
             $this->__setSessionVariables();
             return true;
         }

         return false;
     }

     /**
      * Method will be used to serialize an array that
      * will be used to send data to the error controller
      * to register the error in the file for easing read of
      * a possible exception
      *
      * @param datetime $date
      * @param string $module
      * @param string $controller
      * @param string $function
      * @param string $error
      * @param
      *
      * @return array $arrayData
      */
     protected function serializeDataInArray($date, $module, $controller, $function, $error) {
         $this->arrayData = array(
             'date'       => $date,
             'module'     => $module,
             'controller' => $controller,
             'function'   => $function,
             'error'      => $error);

         return $this->arrayData;
     }

     /**
      * Method will be used to load all the information
      * of the entity manager and just return the object
      * will be used to do some of CRUD operation
      *
      * @param string $bundleName
      * @param string $tableName
      *
      * @return object | NULL
      *
      * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
      */
     public function loadRepo($bundleName, $tableName) {
         $em = $this->getDoctrine();

         if(is_string($bundleName) && is_string($tableName)) {
             $repoName = $bundleName . ':' . $tableName;
         }

         return isset($repoName) ? $em->getRepository($repoName) : false;
     }

     /**
      * Method will be used for add the path is going to be
      * displayed on the page for let the user knows where
      * is stored all the information
      *
      * @param string $label
      * @param string $url
      */
     protected function addBreadcrumb($label, $url = null) {
         if (!$url) {
             $this->breadcrumbs[] = array(
                 'label' => $label
             );
         } else {
             $this->breadcrumbs[] = array(
                 'label' => $label,
                 'url'   => $url
             );
         }
     }

     /**
      * Add variables to the layout
      *
      * @return void
      */
     final protected function initGlobals() {
         //defined the webroot of the site
         $webroot = Request::createFromGlobals()->getBasePath();

         if (!empty($webroot)) {
             $this->addGlobal('WEBROOT', Request::createFromGlobals()->getBasePath());
         }
     }

     /**
      * Set global variables for use on layout
      *
      * @param string $name
      * @param string $value
      *
      * @return void
      */
     final protected function addGlobal($name, $value) {
         if (is_string($name) && !empty($value)) {
             $this->container->get('twig')->addGlobal($name, $value);
         }
     }

     /**
      * Generate token for validate the data that is going to be sent
      * once the user fill the form and click in the button
      *
      * @param string $name
      * @return string
      */
     final protected function __generateTokenCsrf($name) {
         $tokenProvider = $this->container->get('security.csrf.token_manager');
         $token         = $tokenProvider->refreshToken($name);

         return $token;
     }

     /**
      * Method to generate the name is going to be implemented in all
      * the forms once the name of the token has been generated will
      * use to protect the forms against csrf atacks
      *
      * @param string $initialName
      * @return string
      */
     final protected function __generateDynamicName($initialName) {
         $finalName = crypt(rand() . $initialName . date('YmdHiss'), date('YmdHiss'));

         return $finalName;
     }
}
