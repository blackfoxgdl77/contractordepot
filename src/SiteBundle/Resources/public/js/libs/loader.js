(function () {
    var paths   = document.getElementById("scripts").getAttribute('data-path'),
        scripts = document.getElementById("scripts").getAttribute('data-script');

    if ((scripts === "undefined") || (scripts === null) || !scripts.length) {
        scripts = [];
    } else {
        scripts = scripts.split(",");
    }

    var deeps = [
        "/libs/semantic.min.js",
        "/custom/customLibs/formsNoLogin.js",
    ].concat(scripts);

    window.onload = function () {
        var scriptName = "";

        while (scriptName = deeps.shift()) {
            var script   = document.createElement("script");
            script.type  = "text/javascript";
            script.async = false;
            script.src   = paths + scriptName;

            document.getElementsByTagName("head")[0].appendChild(script);
        }
    };
})();
