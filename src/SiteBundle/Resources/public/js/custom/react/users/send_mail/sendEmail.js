/**
 * Class will contain all the functionality of the interface
 * to get the email with the information to reset password
 * of the user
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */
class SendEmail extends React.Component {
    constructor(props) {
        super(props);

        this.handleInput       = this.handleInput.bind(this);
        this.submitForm        = this.submitForm.bind(this);
        this.clearFields       = this.clearFields.bind(this);
        this.showError         = this.showError.bind(this);
        this.validateError     = this.validateError.bind(this);
        this.closeClick        = this.closeClick.bind(this);
        this.closeClickSuccess = this.closeClickSuccess.bind(this);
        this.changeFlagState   = this.changeFlagState.bind(this);

        this.state = {
            validating   : 1,
            'token'      : props.token,
            'source'     : props.source,
            'success'    : false,
            errors       : [],
            'counter'    : 0,
            clearValue   : 0,
            successMsg   : '',
            loader       : 0,
            nameToken    : props.nameT,
            windowPath   : (window.$PATH !== undefined) ? window.$PATH : "",
        };
    }

    changeFlagState = (flag) => {
        if (this.state.clearValue == 1) {
            this.setState({
                clearValue : 0,
            });
        }

        if (flag == 1) {
            setTimeout(function () {
                $("#overlay_loader").css("display", "none");
            }, 3000);

            this.setState({
                loader : 0,
            });
        }
    }

    componentDidUpdate = () => {
        this.changeFlagState(0);

        if (this.state.loader == 1) {
            this.changeFlagState(1);
        }
    }

    componentDidMount = () => {
        // hide loader
        $("#overlay_loader").css("display", "none");
    }

    submitForm = (evt) => {
        evt.preventDefault();

        $("#overlay_loader").css("display", "inline");
        let counter = this.validateError();

        if (counter == 0) {
            let url = this.state.windowPath + "/api/get/users/email/token";

            axios.post(url, JSON.stringify(this.state))
                 .then((response) => {
                     if (response.data.status == 'OK' && response.status == 200) {
                         //this.clearFields();
                         this.setState({
                             'success'  : true,
                             clearValue : 1,
                             validating : 0,
                             loader     : 1,
                             email      : '',
                             c_email    : '',
                         });

                         $("#email").val('');
                         $("#c_email").val('');
                     }

                     if (response.data.status == 'ERROR' && response.status == 200) {
                        this.setState({
                            validating : 0,
                            loader     : 1,
                         });
                     }
                 })
                 .catch((error) => {
                    this.setState({
                        validating : 0,
                        loader     : 1,
                    });
                 });
        }
    }

    clearFields() {
        document.getElementById('email').value   = '';
        document.getElementById('c_email').value = '';
    }

    showError() {

    }

    handleInput = (data) => {
        if (data.label != '') {
            this.setState({
                [data.label] : data.value
            });
        }
    }

    validateError = () => {
        let counter = 0;
        let errors  = [];

        this.setState({
            validating : 1
        });

        if (this.email.state.value === "" || this.email.state.value === undefined) {
            errors['email'] = 'Please, type an email';
            $("#email").parent().parent().addClass('error');
            counter++;
        } else {
            errors['email'] = '';
            $("#email").parent().parent().removeClass('error');
        }

        if (this.c_email.state.value === "" || this.c_email.state.value === undefined) {
            errors['email_confirm'] = 'Please, type the confirm email';
            $("#c_email").parent().parent().addClass('error');
            counter++;
        } else {
            errors['email_confirm'] = '';
            $("#c_email").parent().parent().removeClass('error');
        }

        if (this.email.state.value !== "" || this.c_email.state.value !== "") {
            if (this.email.state.value !== this.c_email.state.value) {
                errors['match'] = 'The emails do not match';

                $("#email").parent().parent().addClass('error');
                $("#c_email").parent().parent().addClass('error');

                counter++;
            } else {
                errors['match'] = '';

                $("#email").parent().parent().removeClass('error');
                $("#c_email").parent().parent().removeClass('error');
            }
        }

        this.setState({
            errors  : errors,
            counter : counter,
        });

        return counter;
    }

    closeClick = () => {
        this.setState({
            counter : 0
        });
    }

    closeClickSuccess = () => {
        this.setState({
            success : false
        });
    }

    render() {
        let errorTemplate = (this.state.counter != 0) ? <Errors headerError="Error Reseting Password"
                                                                bodyError="All the fields with '*' must be filled in."
                                                                variables={this.state.errors}
                                                                onClick={this.closeClick} /> : "";
        let contentFalse =  <div>
                                <div className="ui info message">
                                    <i className="exclamation circle icon"></i>
                                    Please provide the e-mail you used to register in <strong>contractorsbids.com</strong>
                                </div>

                                <form className="ui large form thin_font">
                                    <div className="ui stacked segment">

                                        {errorTemplate}

                                        <div className="field">
                                            <InputEmail name="email" labelEmail="Email" iconTags="envelope icon" inputClass=""
                                                        defaultText="Email" handleFormFields={this.handleInput} disable="false"
                                                        require="true" classData="ui left icon input" isEmpty={this.state.clearValue}
                                                        ref={(email) => { this.email = email; }} />
                                        </div>
                                        <div className="field">
                                            <InputEmail name="c_email" labelEmail="Confirm Email" iconTags="envelope icon" inputClass=""
                                                        defaultText="Confirm Email" handleFormFields={this.handleInput} disable="false"
                                                        require="true" classData="ui left icon input" isEmpty={this.state.clearValue}
                                                        ref={(c_email) => { this.c_email = c_email; }} />
                                        </div>
                                        <div className="field">
                                            <button className="ui fluid teal button right" type="submit" onClick={this.submitForm}>
                                                Reset Password
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <div>&nbsp;</div>
                            </div>;

        let contentTrue = <SuccessTemplate headerSuccess="Reset Password Successfully"
                                            bodySuccess="The request to change password has been done successfully. You will receive an email with the instructions to change the password."
                                            onClick={this.closeClickSuccess} />;

        /*if (this.state.validating == 1) {
            return (
                <div className="loader"></div>
            );
        }*/

        return (this.state.success == true) ? contentTrue : contentFalse;
    }
}
