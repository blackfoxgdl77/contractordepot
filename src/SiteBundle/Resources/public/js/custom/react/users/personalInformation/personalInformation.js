/**
 * Class will contain all the information and processes for
 * users want to update the personal information.
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */
class PersonalInformation extends React.Component {
    constructor(props) {
        super(props);

        this.handleInput       = this.handleInput.bind(this);
        this.submitForm        = this.submitForm.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.validateError     = this.validateError.bind(this);
        this.closeClick        = this.closeClick.bind(this);
        this.closeClickSuccess = this.closeClickSuccess.bind(this);
        this.changeFlagState   = this.changeFlagState.bind(this);

        this.state = {
            validating   : 1,
            loader       : 0,
            'loading'    : false,
            'success'    : 0,
            'id_user'    : props.num,
            'source'     : props.source,
            'reference'  : props.reference,
            'successM'   : '',
            counter      : 0,
            errors       : [],
            token        : props.token,
            nameToken    : props.nameT,
            windowPath   : (window.$PATH !== undefined) ? window.$PATH : "",
        };
    }

    changeFlagState = () => {
        setTimeout(function () {
            $("#overlay_loader").css("display", "none");
        }, 3000);

        this.setState({
            loader : 0,
        });
    }

    closeClick = () => {
        this.setState({
            counter : 0
        });
    }

    closeClickSuccess = () => {
        this.setState({
            success : 0
        });
    }

    handleInput(data) {
        if (data.label != '') {
            this.setState({
                [data.label] : data.value
            });
        }
    }

    validateError = () => {
        let counter = 0;
        let errors  = {};

        this.setState({
            validating  : 1
        });

        if (this.state.name === "" || this.state.name === undefined) {
            errors['name'] = "Please, enter a name";
            $("#name").parent().parent().addClass('error');
            counter++;
        } else {
            errors['name'] = "";
            $("#name").parent().parent().removeClass('error');
        }

        if (this.state.email === "" || this.state.email === undefined) {
            errors['email'] = "Please, enter the email.";
            $("#email").parent().parent().addClass('error');
            counter++;
        } else {
            errors['email'] = "";
            $("#email").parent().parent().removeClass('error');
        }

        if (this.state.phone === "" || this.state.phone === undefined) {
            errors['phone'] = "Please, enter the phone number";
            $("#phone").parent().parent().addClass('error');
            counter++;
        } else {
            errors['phone'] = "";
            $("#phone").parent().parent().removeClass('error');
        }

        this.setState({
            errors  : errors,
            counter : counter,
        });

        return counter;
    }

    submitForm(event) {
        event.preventDefault();

        $("#overlay_loader").css("display", "inline");
        let counter = this.validateError();

        if (counter == 0) {
            let url  = this.state.windowPath + "/api/update/users/information/" + this.state.id_user;
            let data = {
                name           : this.state.name,
                email          : this.state.email,
                paymentMethod  : this.state.payment_method,
                bbbAccount     : (this.state.typeUser == 0) ? this.state.bbb_account : "",
                wcb_number     : (this.state.typeUser == 0) ? this.state.wcb_number : "",
                bbb_accredited : (this.state.typeUser == 0) ? this.state.bbb_accredited : "",
                raiting        : (this.state.typeUser == 0) ? this.state.raiting : "",
                address        : this.state.address,
                phone          : this.state.phone,
                token          : this.state.token,
                nameT          : this.state.nameToken,
            };
            axios.post(url, JSON.stringify(data)).then((response) => {
                if (response.status == 200 && response.data.status == 'OK') {
                    this.setState({
                        success     : 1,
                        successM    : response.data.data.message,
                        validating  : 0,
                        loader      : 1,
                    });
                }
            }).catch((error) => {
                this.setState({
                    validating  : 0,
                    loader      : 1,
                });
            });
        }
    }

    componentDidUpdate = () => {
        if (this.state.loader == 1) {
            this.changeFlagState();
        }
    }

    componentDidMount() {
        let url = this.state.windowPath + '/api/get/user/' + this.state.id_user;

        axios.get(url)
             .then((response) => {
                 let paymentMethod  = (response.data.data.payment_method !== null) ? response.data.data.payment_method : "";
                 let bbbAccount     = (response.data.data.payment_method !== null) ? response.data.data.payment_method : "";
                 let address        = (response.data.data.address !== null) ? response.data.data.address : "";
                 let phone          = (response.data.data.phone !== null) ? response.data.data.phone : "";
                 let wcb_number     = (response.data.data.wcb_number !== null) ? response.data.data.wcb_number : "";
                 let bbb_accredited = (response.data.data.bbb_accredited !== null) ? response.data.data.bbb_accredited : "";
                 let raiting        = (response.data.data.raiting !== null) ? response.data.data.raiting : "";

                 this.setState({
                     id             : response.data.data.id,
                     name           : response.data.data.name,
                     email          : response.data.data.email,
                     username       : response.data.data.username,
                     isActive       : response.data.data.is_active,
                     typeUser       : response.data.data.type_user,
                     payment_method : paymentMethod,
                     bbb_account    : bbbAccount,
                     wcb_number     : wcb_number,
                     bbb_accredited : bbb_accredited,
                     raiting        : raiting,
                     address        : address,
                     phone          : phone,
                     date_created   : response.data.data.date_created,
                     date_updated   : response.data.data.date_updated,
                     flag_value     : 1,
                     validating     : 0
                 });

                 this.setState({loading: false});

                 // hide loader
                 $("#overlay_loader").css("display", "none");
             }).catch((error) => {
                 console.log(error);
                 this.setState({
                    validating  : 0
                });

                // hide loader
                $("#overlay_loader").css("display", "none");
             });
    }

    render() {

        let nameFlag, emailFlag, phoneFlag, paymentFlag, addressFlag,
            accreditedFlag, wcb_numberFlag, typeUser, profile_picFlag, raitingFlag;
        let optionAccredited = [
            { 'id': 1, 'value': 'Yes' },
            { 'id': 0, 'value': 'No' }
        ];

        if (this.state.loading === false) {
            // Validate if the value has data or not
            nameFlag        = (this.state.name === "")           ? "false" : "true";
            emailFlag       = (this.state.email === "")          ? "false" : "true";
            phoneFlag       = (this.state.phone === "")          ? "false" : "true";
            paymentFlag     = (this.state.payment_method === "") ? "false" : "true";
            addressFlag     = (this.state.address === "")        ? "false" : "true";
            accreditedFlag  = (this.state.bbb_accredited === "") ? "false" : "true";
            raitingFlag     = (this.state.raiting === "")        ? "false" : "true";
            wcb_numberFlag  = (this.state.wcb_number === "")     ? "false" : "true";
        }

        let hideFields = (this.state.typeUser == 1) ? "hide" : "";

        let errorTemplate   = (this.state.counter != 0) ? <Errors headerError="Error Updating Personal Information"
                                                                  bodyError="All the fields with '*' must be filled in"
                                                                  variables={this.state.errors}
                                                                  onClick={this.closeClick} /> : "";
        let successTemplate = (this.state.success == 1) ? <SuccessTemplate headerSuccess="Personal Information Updated Successfully."
                                                                           bodySuccess={this.state.successM}
                                                                           onClick={this.closeClickSuccess} /> : "";

        let loading  = <Loading />;
        let formData =  <div>

                            <div className="ui info message">
                                <i className="edit icon"></i>
                                Edit Personal Information
                            </div>

                            <div className="twelve wide column">
                                <form className="ui large form thin_font">
                                    <div className="ui stacked segment">
                                        <div className="ui segment">
                                            <h4 className="ui header">Account Information</h4>
                                            <div className="field">
                                                <Input name="username" disable="true" labelName="Username" iconTags="user icon"
                                                       classData="ui left icon input" defaultText="Username" require="false"
                                                       handleFormFields={this.handleInput} inputClass=""
                                                       items={this.state.username} />
                                            </div>
                                            <div className="field">
                                                <Input name="name" disable="false" labelName="Name" iconTags="user circle icon"
                                                       classData="ui left icon input" defaultText="Name" require="true"
                                                       handleFormFields={this.handleInput} inputClass=""
                                                       items={this.state.name} />
                                            </div>
                                            <div className="field">
                                                <InputEmail name="email" disable="false" labelEmail="Email" iconTags="envelope icon"
                                                            classData="ui left icon input" defaultText="Email (john.doe@example.com)"
                                                            handleFormFields={this.handleInput} inputClass="" require="true"
                                                            items={this.state.email} />
                                            </div>
                                            <div className="field">
                                                <InputTel name="phone" labelName="Phone #" iconTags="phone icon" disable="false"
                                                          classData="ui left icon input" defaultText="Phone #" require="true"
                                                          handleFormFields={this.handleInput}
                                                          items={this.state.phone} />
                                            </div>
                                        </div>
                                        <div className="ui segment">
                                            <h4 className="ui header">Personal Information</h4>
                                            <div className="field">
                                                <Input name="payment_method" labelName="Payment Method" iconTags="credit card icon"
                                                       disable="false" inputClass="" classData="ui left icon input" require="false"
                                                       defaultText="Payment Method" handleFormFields={this.handleInput}
                                                       items={this.state.payment_method} />
                                            </div>
                                            <div className="field">
                                                <InputEmail name="bbb_account" labelEmail="BBB Account (john.doe@example.com)" disable="false"
                                                            iconTags="envelope icon" classData="ui left icon input" require="false"
                                                            handleFormFields={this.handleInput} defaultText="BBB Account (john.doe@example.com)"
                                                            items={this.state.bbb_account} disable="false" />
                                            </div>
                                            <div className="field">
                                                <RadioButton name="bbb_accredited" disable="false" labelRadio="Are you Accredited?" iconTags=""
                                                        handleFormFields={this.handleInput} defaultText="false" require="false"
                                                        classData="" optionsValues={optionAccredited}
                                                        setVal={this.state.bbb_accredited} />
                                            </div>
                                            <div className="field">
                                                <Input name="raiting" labelName="Raiting" iconTags="star icon" disable="false"
                                                       classData="ui left icon input" defaultText="Raiting" require="false"
                                                       handleFormFields={this.handleInput}
                                                       items={this.state.raiting} />
                                            </div>
                                            <div className="field">
                                                <Input name="wcb_number" labelName="WCB Number" iconTags="hashtag icon"
                                                        classData="ui left icon input" defaultText="WCB Number" disable="false"
                                                        handleFormFields={this.handleInput} inputClass="" require="false"
                                                        items={this.state.wcb_number} />
                                            </div>
                                            <div className="field">
                                                <Input name="address" labelName="Address" iconTags="address card icon"
                                                        classData="ui left icon input" defaultText="Address" disable="false"
                                                        handleFormFields={this.handleInput} inputClass="" require="false"
                                                        items={this.state.address} />
                                            </div>
                                        </div>
                                        <div className="field">
                                            <button className="ui fluid teal button right" type="submit" onClick={this.submitForm}>
                                                Save Information
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>;

        if (this.state.validating == 1) {
            return (
                <div className="loader"></div>
            );
        }

        return (this.state.loading == true) ? loading : formData;
    }
}
