class PersonalInformation extends React.Component {
    constructor(props) {
        super(props);

        this.handleInput = this.handleInput.bind(this);
        this.submitForm  = this.submitForm.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);

        this.state = {};
    }

    handleInput(data) {
        if (data.label != '') {
            this.setState({
                [data.label]: data.value
            });
        }
    }

    submitForm(event) {
        event.preventDefault();

        //axios.post();
    }

    componentDidMount() {
        axios.get('/api/get/user/1')
             .then((response) => {
                 this.setState({
                     id             : response.data.data.id,
                     name           : response.data.data.name,
                     email          : response.data.data.email,
                     username       : response.data.data.username,
                     isActive       : response.data.data.is_active,
                     payment_method : response.data.data.payment_method,
                     bbb_account    : response.data.data.bbb_account,
                     addess         : response.data.data.address,
                     date_created   : response.data.data.date_created,
                     date_updated   : response.data.data.date_updated,
                     flag_value     : 1
                 });
             }).catch((error) => {
             });
    }

    componentWillMount() {
    }

    render() {
        return (
            <form onSubmit={this.submitForm}>
                <Input name="name" disabledValue="disabled" labelName="Name" iconTags="account_box"
                       handleFormFields={this.handleInput} items={this.state.name} />
                <InputEmail name="email" disabledValue="disabled" labelEmail="Email" iconTags="email"
                            handleFormFields={this.handleInput} item={this.state.name} />
                <Input name="username" disabledValue="disabled" labelName="Username" iconTags="account_box"
                       handleFormFields={this.handleInput} item={this.state.username} />
                <Input name="payment_method" disabledValue="" labelName="Payment Method" iconTags="account_box"
                       handleFormFields={this.handleInput} item={this.state.payment_method} />
                <InputEmail name="bbb_account" disabledValue="" labelEmail="BBB Account" iconTags="account_box"
                       handleFormFields={this.handleInput} item={this.state.bbb_account} />
                <Input name="address" disabledValue="" labelName="Address" iconTags="account_box"
                       handleFormFields={this.handleInput} item={this.state.address} />
                <div className="row right">
                    <div className="col s12">
                        <button className="btn waves-effect waves-light" type="submit">
                            Save
                            <i className="material-icons right">send</i>
                        </button>
                    </div>
                </div>
            </form>
        );
    }
}
