/**
 * Class will contain all the functionality for reset the
 * password that if the user wants to recovery it
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */
class ResetPassword extends React.Component {
    constructor(props) {
        super(props);

        this.handleInput       = this.handleInput.bind(this);
        this.submitForm        = this.submitForm.bind(this);
        this.clearFields       = this.clearFields.bind(this);
        this.showError         = this.showError.bind(this);
        this.validateError     = this.validateError.bind(this);
        this.closeClick        = this.closeClick.bind(this);
        this.closeClickSuccess = this.closeClickSuccess.bind(this);
        this.changeFlagState   = this.changeFlagState.bind(this);

        this.state = {
            validating   : 1,
            'source'     : props.source,
            'token'      : props.token,
            'validToken' : props.valid,
            errors       : [],
            'counter'    : 0,
            clearValue   : 0,
            nameToken    : props.nameT,
            successMsg   : '',
            loader       : 0,
            windowPath   : (window.$PATH !== undefined) ? window.$PATH : "",
        };
    }

    changeFlagState = (flag) => {
        if (this.state.clearValue == 1) {
            this.setState({
                clearValue : 0,
            });
        }

        if (flag == 1) {
            setTimeout(function () {
                $("#overlay_loader").css("display", "none");
            }, 3000);

            this.setState({
                loader : 0,
            });
        }
    }

    componentDidUpdate = () => {
        this.changeFlagState(0);

        if (this.state.loader == 1) {
            this.changeFlagState(1);
        }
    }

    componentDidMount = () => {
        // hide loader
        $("#overlay_loader").css("display", "none");
    }

    submitForm(evt) {
        evt.preventDefault();

        $("#overlay_loader").css("display", "inline");
        let counter = this.validateError();

        if (counter === 0) {
            let url = this.state.windowPath + "/api/update/users/password/" + this.state.validToken;

            axios.post(url, JSON.stringify(this.state))
                 .then((response) => {
                     if (response.data.status == 'OK') {
                         //this.clearFields();
                         this.setState({
                             'success'  : true,
                             successMsg : response.data.data.message,
                             clearValue : 1,
                             validating : 0,
                             loader     : 1,
                             password   : '',
                             c_password : '',
                         });

                         $("#password").val('');
                         $("#c_password").val('');
                     } else {
                         this.showError();
                         this.setState({
                             validating : 0,
                             loader     : 1,
                         });
                     }
                 })
                 .catch((error) => {
                    this.setState({
                      validating : 0,
                      loader     : 1,
                    });
                 });
        }
    }

    clearFields() {
        document.getElementById('password').value   = '';
        document.getElementById('c_password').value = '';
    }

    showError() {

    }

    validateError = () => {
        let counter = 0;
        let errors  = {};

        this.setState({
          validating : 1
        });

        if (this.password.state.value === "" || this.password.state.value === undefined) {
            errors['email'] = 'Please, type an email';
            $("#password").parent().parent().addClass('error');
            counter++;
        } else {
            errors['email'] = '';
            $("#password").parent().parent().removeClass('error');
        }

        if (this.c_password.state.value === "" || this.c_password.state.value === undefined) {
            errors['email_confirm'] = 'Please, type the confirm email';
            $("#c_password").parent().parent().addClass('error');
            counter++;
        } else {
            errors['email_confirm'] = '';
            $("#c_password").parent().parent().removeClass('error');
        }

        if (this.password.state.value !== "" || this.c_password.state.value !== "") {
            if (this.c_password.state.value !== this.password.state.value) {
                errors['match'] = "Passwords don't match. PLease verify the password.";

                $("#password").parent().parent().addClass('error');
                $("#c_password").parent().parent().addClass('error');

                counter++;
            } else {
                $("#password").parent().parent().removeClass('error');
                $("#c_password").parent().parent().removeClass('error');

                errors['match'] = "";
            }
        }

        this.setState({
            errors  : errors,
            counter : counter,
        });

        return counter;
    }

    handleInput = (data) => {
        if (data.label != '') {
            this.setState({
                [data.label] : data.value
            });
        }
    }

    closeClick = () => {
        this.setState({
            counter : 0
        });
    }

    closeClickSuccess = () => {
        this.setState({
            success : false
        });
    }

    render() {
        let errorTemplate = (this.state.counter != 0) ? <Errors headerError="Error Reset Password"
                                                                bodyError="All the fields with '*' must be filled in."
                                                                variables={this.state.errors}
                                                                onClick={this.closeClick} /> : "";
        let content = <div>
                        <div className="ui info message">
                            <i className="exclamation circle icon"></i>
                            Please type your new password to loggin in <strong>contractorsbids.com</strong>
                        </div>

                        <form className="ui large form thin_font">
                            <div className="ui stacked segment">

                                {errorTemplate}

                                <div className="field">
                                    <Password name="password" disable="false" labelName="Password" iconTags="lock icon"
                                           handleFormFields={this.handleInput} defaultText="Password" require="true"
                                           classData="ui left icon input" isEmpty={this.state.clearValue} inputClass=""
                                           ref={(password) => { this.password = password; }} />
                                </div>
                                <div className="field">
                                    <Password name="c_password" disable="false" labelName="Confirm Password" iconTags="lock icon"
                                           handleFormFields={this.handleInput} defaultText="Confirm Password" require="true"
                                           classData="ui left icon input" isEmpty={this.state.clearValue} inputClass=""
                                           ref={(c_password) => { this.c_password = c_password; }} />
                                </div>
                                <div className="field">
                                    <button className="ui fluid teal button right" type="submit" onClick={this.submitForm}>
                                        Reset Password
                                    </button>
                                </div>
                            </div>
                        </form>
                        <div>&nbsp;</div>
                      </div>;

        let success = <SuccessTemplate headerSuccess="Password Reset Successfully."
                                       bodySuccess={this.state.successMsg}
                                       onClick={this.closeClickSuccess} />;


        return (this.state.success == true) ? success : content;
    }
}
