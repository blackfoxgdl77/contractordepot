/**
 * File will be used to get all the information of the new accounts
 * will be created from the UI by every user is going to use
 * the platform to create jobs
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */
class NewAccount extends React.Component {
    constructor(props) {
        super(props);

        this.submitFormContractor = this.submitFormContractor.bind(this);
        this.submitFormHome       = this.submitFormHome.bind(this);
        this.handleInput          = this.handleInput.bind(this);
        this.validateInput        = this.validateInput.bind(this);
        this.validateInputHome    = this.validateInputHome.bind(this);
        this.handleClick          = this.handleClick.bind(this);
        this.contractorShow       = this.contractorShow.bind(this);
        this.homeOwnerShow        = this.homeOwnerShow.bind(this);
        this.clearContractor      = this.clearContractor.bind(this);
        this.clearHomeOwner       = this.clearHomeOwner.bind(this);
        this.closeClick           = this.closeClick.bind(this);
        this.closeClickSuccess    = this.closeClickSuccess.bind(this);
        this.closeClickE          = this.closeClickE.bind(this);
        this.changeFlagState      = this.changeFlagState.bind(this);
        this.clearErrorsMessages  = this.clearErrorsMessages.bind(this);

        this.state = {
            validating       : 1,
            'token'          : props.token,
            'source'         : props.source,
            'userID'         : 0,
            errors           : [],
            counter          : 0,
            counterH         : 0,
            'successMessage' : false,
            'dataMessage'    : {},
            nameToken        : props.nameT,
            errorMessage     : 0,
            respMessage      : '',
            clearValue       : 0,
            bbb_accredited   : "0",
            loader           : 0,
            windowPath       : (window.$PATH !== undefined) ? window.$PATH : "",
        };
    }

    clearErrorsMessages = (evt, flagData) => {
        evt.preventDefault();

        if (flagData == 'contractor') {
            $("#contractor .field").removeClass('error');
        }

        if (flagData == 'home') {
            $("#home_owner .field").removeClass('error');
        }

        this.setState({
            counter      : 0,
            counterH     : 0,
            errorMessage : 0,
            respMessage  : '',
        });
    }

    changeFlagState = (flag) => {
        if (flag == 0) {
            if (this.state.clearValue == 1) {
                this.setState({
                    clearValue : 0,
                });
            }
        }

        if (flag == 1) {
            setTimeout(function () {
                $("#overlay_loader").css("display", "none");
            }, 3000);

            this.setState({
                loader : 0,
            });
        }
    }

    componentDidUpdate = () => {
        this.changeFlagState(0);

        // Init Elements
        $('.menu .item').tab();

        if (this.state.loader == 1) {
            this.changeFlagState(1);
        }
    }

    componentWillMount() {
    }

    componentDidMount() {
        let url = this.state.windowPath  + "/api/get/admin/data/4";

        axios.get(url)
             .then((response) => {
                 if (response.status == 200 && response.data.status == 'OK') {
                     //$("#messageTitle").text(response.data.data.data.TermsConditions.inner[0].section);
                     //$("#messageText").text(response.data.data.data.TermsConditions.inner[0].description);

                     this.setState({
                         dataMessage : (response.data.data.data.TermsConditions.inner[0] != undefined) ? response.data.data.data.TermsConditions.inner[0] : '',
                         validating  : 0,
                     });
                 }

                 if (response.data.status == 'ERROR' && response.status == 200) {
                     this.setState({
                         errorMessage : 1,
                         respMessage  : response.data.data.message,
                         validating   : 0,
                     });
                 }

                 // hide loader
                 $("#overlay_loader").css("display", "none");
                 $(".modal").css("display", "none");
             })
             .catch((error) => {
                 this.setState({
                     errorMessage : 1,
                     respMessage  : error.data.data.message,
                     validating   : 0,
                 });

                 // hide loader
                 $("#overlay_loader").css("display", "none");
                 $(".modal").css("display", "none");
             });
    }

    submitFormContractor = (evt) => {
        evt.preventDefault();

        $("#overlay_loader").css("display", "inline");
        let counter = 0;
        counter     = this.validateInput(0, "");

        if (counter === 0) {
            var url = this.state.windowPath + "/api/register/users";

            axios.post(url, JSON.stringify({
                c_password     : this.state.c_password,
                email          : this.state.email,
                name           : this.state.nameContractor,
                bbb_accredited : this.state.bbb_accredited,
                raiting        : this.state.raiting,
                wcb_number     : (this.state.wcb_number != '') ? this.state.wcb_number : '',
                password       : this.state.password,
                phone          : this.state.phone,
                source         : this.state.source,
                token          : this.state.token,
                nameT          : this.state.nameToken,
                user           : this.state.userID,
                type_user      : 0,
            })).then((response) => {
                if (response.data.status == 'OK' && response.status == 200) {
                    this.setState({
                        'successMessage' : true,
                        clearValue       : 1,
                        validating       : 0,
                        loader           : 1,
                        c_password       : '',
                        email            : '',
                        name             : '',
                        bbb_accredited   : '',
                        raiting          : '',
                        wcb_number       : '',
                        password         : '',
                        phone            : '',
                        user             : '',
                        type_user        : 0,
                    });

                    $("#nameContractor").val('');
                    $("#phone").val('');
                    $("#email").val('');
                    $("#wcb_number").val('');
                    $("#password").val('');
                    $("#c_password").val('');
                    $("input[name=bbb_accredited][value=0]").prop('checked', true);
                    $("#raiting").val('');
                    $("input[name=termsConditions]").prop('checked', false);
                }

                if (response.data.status == 'ERROR' && response.status == 200) {
                    this.setState({
                        respMessage  : response.data.data.message,
                        errorMessage : 1,
                        validating   : 0,
                        loader       : 1,
                    });
                }
            }).catch((error) => {
                this.setState({
                    respMessage  : error.data.data.message,
                    errorMessage : 1,
                    validating   : 0,
                    loader       : 1,
                });
            });
        }
    }

    submitFormHome = (evt) => {
        evt.preventDefault();

        $("#overlay_loader").css("display", "inline");
        let counter = 0;
        counter     = this.validateInputHome(1, "H");

        if (counter === 0) {
            var url = this.state.windowPath + "/api/register/users";

            axios.post(url, JSON.stringify({
                c_password : this.cpasswordFieldH.state.value,
                email      : this.emailFieldH.state.value,
                name       : this.nameFieldH.state.value,
                password   : this.passwordFieldH.state.value,
                phone      : this.phoneFieldH.state.value,
                source     : this.state.source,
                token      : this.state.token,
                nameT      : this.state.nameToken,
                user       : this.state.userID,
                type_user  : 1,
            })).then((response) => {
                if (response.data.status == 'OK' && response.status == 200) {
                    this.setState({
                        successMessage : true,
                        clearValue     : 1,
                        validating     : 0,
                        loader         : 1,
                    });

                    $("#nameHO").val('');
                    $("#phoneHO").val('');
                    $("#emailHO").val('');
                    $("#passwordHO").val('');
                    $("#c_passwordHO").val('');
                    $("input[name=termsConditionsHO]").prop('checked', false);
                }

                if (response.data.status == 'ERROR' && response.status == 200) {
                    this.setState({
                        respMessage  : response.data.data.message,
                        errorMessage : 1,
                        validating   : 0,
                        loader       : 1,
                    });
                }
            }).catch((error) => {
                this.setState({
                    respMessage  : error.data.data.message,
                    errorMessage : 1,
                    validating   : 0,
                    loader       : 1,
                });
            });
        } else {
            this.setState({
                errorMessage : 1,
                validating   : 0,
                loader       : 1,
            });
        }
    }

    validateInputHome = (accountType, formEndLetter) => {
        let counterH = 0;
        let errors   = {};

        this.setState({
            validating : 1,
        });

        if (this.nameFieldH.state.value === "" || this.nameFieldH.state.value === undefined) {
            errors['name'] = 'Please, enter a name';
            $("#nameHO").parent().parent().addClass('error');
            counterH++;
        } else {
            errors['name'] = '';
            $("#nameHO").parent().parent().removeClass('error');
        }

        if (this.phoneFieldH.state.value === "" || this.phoneFieldH.state.value === undefined || isNaN(parseInt(this.phoneFieldH.state.value))) {
            errors['phone'] = 'Please, enter the phone number';
            $("#phoneHO").parent().parent().addClass('error');
            counterH++;
        } else {
            errors['phone'] = '';
            $("#phoneHO").parent().parent().removeClass('error');
        }

        if (this.emailFieldH.state.value === "" || this.emailFieldH.state.value === undefined) {
            errors['email'] = 'Please, enter the email';
            $("#emailHO").parent().parent().addClass('error');
            counterH++;
        } else {
            errors['email'] = '';
            $("#emailHO").parent().parent().removeClass('error');
        }

        if (this.passwordFieldH.state.value === "" || this.passwordFieldH.state.value === undefined) {
            errors['password'] = 'Please, enter the password';
            $("#passwordHO").parent().parent().addClass('error');
            counterH++;
        } else {
            errors['password'] = '';
            $("#passwordHO").parent().parent().removeClass('error');
        }

        if (this.cpasswordFieldH.state.value === "" || this.cpasswordFieldH.state.value === undefined) {
            errors['confirm_password'] = 'Please, enter the confirm password';
            $("#c_passwordHO").parent().parent().addClass('error');
            counterH++;
        } else {
            errors['confirm_password'] = '';
            $("#c_passwordHO").parent().parent().removeClass('error')
        }

        if (!$("#termsConditionsHO").is(":checked")) {
            errors['terms'] = 'Please, check the Terms and Conditions';
            $("#termsConditionsHO").parent().parent().addClass('error');
            counterH++;
        } else {
            errors['terms'] = '';
            $("#termsConditionsHO").parent().parent().removeClass('error');
        }

        if (this.passwordFieldH.state.value !== "" || this.cpasswordFieldH.state.value !== "") {
            if (this.cpasswordFieldH.state.value !== this.passwordFieldH.state.value) {
                errors['match'] = 'Passwords do not match';

                $("#passwordHO").parent().parent().addClass('error');
                $("#c_passwordHO").parent().parent().addClass('error');
                counterH++;
            } else {
                errors['match'] = '';
                $("#c_passwordHO").parent().parent().removeClass('error');
                $("#passwordHO").parent().parent().removeClass('error');
            }
        }

        this.setState({
            errors   : errors,
            counterH : counterH,
        });


        return counterH;
    }

    validateInput = (accountType, formEndLetter) => {
        let counter = 0;
        let errors = {};

        this.setState({
            validating : 1,
        });

        if (this.state.nameContractor === "" || this.state.nameContractor === undefined) {
            errors['name'] = 'Please, enter a name';
            $("#nameContractor").parent().parent().addClass('error');
            counter++;
        } else {
            errors['name'] = '';
            $("#nameContractor").parent().parent().removeClass('error');
        }

        if (this.state.phone === "" || this.state.phone === undefined || isNaN(parseInt(this.state.phone))) {
            errors['phone'] = 'Please, enter the phone number';
            $("#phone").parent().parent().addClass('error');
            counter++;
        } else {
            errors['phone'] = '';
            $("#phone").parent().parent().removeClass('error');
        }

        if (this.state.email === "" || this.state.email === undefined) {
            errors['email'] = 'Please, enter the email';
            $("#email").parent().parent().parent().addClass('error');
            counter++;
        } else {
            errors['email'] = '';
            $("#email").parent().parent().parent().removeClass('error');
        }

        if (this.state.password === "" || this.state.password === undefined) {
            errors['password'] = 'Please, enter the password';
            $("#password").parent().parent().parent().addClass('error');
            counter++;
        } else {
            errors['password'] = '';
            $("#password").parent().parent().parent().removeClass('error');
        }

        if (this.state.c_password === "" || this.state.c_password === undefined) {
            errors['cPassword'] = 'Please, enter the confirm password';
            $("#c_password").parent().parent().parent().addClass('error');
            counter++;
        } else {
            errors['cPassword'] = '';
            $("#c_password").parent().parent().parent().removeClass('error');
        }

        if (!$("#termsConditions").is(':checked')) {
            errors['terms'] = 'Please, check the Terms and Conditions';
            $("#termsConditions").parent().parent().addClass('error');
            counter++;
        } else {
            errors['terms'] = '';
            $("#termsConditions").parent().parent().removeClass('error');
        }

        if (this.state.password !== "" || this.state.c_password !== "") {
            if (this.state.password !== this.state.c_password) {
                errors['match'] = 'Passwords do not match';

                $("#password").parent().parent().addClass('error');
                $("#c_password").parent().parent().addClass('error');
                counter++;
            } else {
                errors['match'] = '';
                $("#password").parent().parent().removeClass('error');
                $("#c_password").parent().parent().removeClass('error');
            }
        }

        if (accountType === 0) {
            if (this.state.bbb_accredited == 1) {
                if ($("#raiting").val() == "") {
                    errors['bbb_accredit'] = 'Please, enter the raiting';
                    $("#raiting").parent().parent().addClass('error');
                    counter++;
                } else {
                    errors['bbb_accredit'] = '';
                    $("#raiting").parent().parent().removeClass('error');
                }
            }
        }

        if (this.state.bbb_accredited == 0) {
            this.setState({
                raiting: ''
            });
        }

        this.setState({
            errors  : errors,
            counter : counter
        });

        return counter;
    }

    handleInput = (data) => {
        if (data.label != '') {
            this.setState({
                [data.label]: data.value
            });
        }
    }

    handleClick(id) {
        document.getElementById('first_tab').classList.add('disabled');
        this.setState({
            userID: id
        });
    }

    contractorShow(evt) {
        evt.preventDefault();

        document.getElementById('contractor_link').classList.remove('grey');
        document.getElementById('contractor').classList.remove('hide_forms');
        document.getElementById('home_owner').classList.add('hide_forms');
        document.getElementById('home_owner_link').classList.add('grey');

        this.clearContractor();
    }

    clearContractor = () => {
        // Clear Status
        this.setState({
            c_password     : '',
            email          : '',
            name           : '',
            bbb_accredited : '',
            wcb_number     : '',
            password       : '',
            phone          : '',
        });

        // Clear form
        this.nameField.state.value = '';
        $("#name").val('').blur();
        this.phoneField.state.value = '';
        $("#phone").val('').blur();
        this.emailField.state.value = '';
        $("#email").val('').blur();
        this.bbbAccredited.state.value = '';
        $("#bbb_accredit").val('').blur();
        this.wcb_number.state.value = '';
        $("#wcb_number").val('').blur();
        this.passwordField.state.value = '';
        $("#password").val('').blur();
        this.cpasswordField.state.value = '';
        $("#c_password").val('').blur();
    }

    homeOwnerShow(evt) {
        evt.preventDefault();

        document.getElementById('home_owner_link').classList.remove('grey');
        document.getElementById('home_owner').classList.remove('hide_forms');
        document.getElementById('contractor').classList.add('hide_forms');
        document.getElementById('contractor_link').classList.add('grey');

        this.clearHomeOwner();
    }

    clearHomeOwner = () => {
        // Clear Status
        this.setState({
            c_password     : '',
            email          : '',
            name           : '',
            password       : '',
            phone          : '',
        });

        // Clear form
        this.nameFieldH.state.value = '';
        $("#nameHO").val('').blur();
        this.phoneFieldH.state.value = '';
        $("#phoneHO").val('').blur();
        this.emailFieldH.state.value = '';
        $("#emailHO").val('').blur();
        this.passwordFieldH.state.value = '';
        $("#passwordHO").val('').blur();
        this.cpasswordFieldH.state.value = '';
        $("#c_passwordHO").val('').blur();
    }

    closeClick = () => {
        this.setState({
            counter      : 0,
            counterH     : 0,
            errorMessage : 0,
            respMessage  : '',
        });
    }

    closeClickSuccess = () => {
        this.setState({
            successMessage : false
        });
    }

    closeClickE = () => {
        this.setState({
            errorMessage : 0,
            respMessage  : '',
        });
    }

    render() {
        let register, contentTrue, flagDisplay;
        let errorTemplate = (this.state.counter != 0) ? <Errors headerError="Error Creating Account"
                                                                bodyError="All the fields with '*' must be filled in."
                                                                variables={this.state.errors}
                                                                onClick={this.closeClick} /> : "";
        let errorTemplateH = (this.state.counterH != 0) ? <Errors headerError="Error Creating Account"
                                                                  bodyError="All the fields with '*' must be filled in."
                                                                  variables={this.state.errors}
                                                                  onClick={this.closeClick} /> : "";
        let errorResp = (this.state.errorMessage != 0) ? <Errors headerError="Error!"
                                                                 bodyError={this.state.respMessage}
                                                                 onClick={this.closeClick} />: "";
        let optionAccredited = [
            { 'id': 1, 'value': 'Yes' },
            { 'id': 0, 'value': 'No' }
        ];

        // Check if should be displayed the raiting field or not
        if (this.state.bbb_accredited == 1) {
            flagDisplay = {
                display: ''
            };
        } else {
            flagDisplay = {
                display: 'none'
            };
        }

        register =  <div>
                        <div className="modal"></div>
                        {errorResp}

                        <div className="ui info message">
                            <div className="header">
                                <i className="exclamation circle icon"></i>
                                Create New Account
                            </div>
                            <p>
                                Please select a type of account would you like to create.
                            </p>
                        </div>

                        <div className="ui fluid stackable steps">
                            <div className="step">
                                <i className="address card icon"></i>
                                <div className="content">
                                    <div className="title">New Account</div>
                                    <div className="description">
                                        Create New Account
                                    </div>
                                </div>
                            </div>
                            <div className="step">
                                <i className="check circle icon"></i>
                                <div className="content">
                                    <div className="title">Confirmation</div>
                                    <div className="description">
                                        Confirm Your Account
                                    </div>
                                </div>
                            </div>
                            <div className="step">
                                <i className="pencil alternate icon"></i>
                                <div className="content">
                                    <div className="title">Publish Projects</div>
                                    <div className="description">
                                        Start to Publish Projects
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="ui top attached tabular menu">
                            <a className="item" data-tab="contractor" onClick={(evt) => this.clearErrorsMessages(evt, 'contractor')}>
                                Contractor
                            </a>
                            <a className="item" data-tab="home_owner" onClick={(evt) => this.clearErrorsMessages(evt, 'home')}>
                                Home Owner
                            </a>
                        </div>

                        <div className="ui bottom attached tab segment" data-tab="contractor">
                            <div className="twelve wide column" id="contractor">
                                <form className="ui large form thin_font">
                                    <div className="ui stacked segment">

                                        {errorTemplate}

                                        <div className="field">
                                            <Input name="nameContractor" disable="false" labelName="Contractor Name" iconTags="user icon"
                                                   handleFormFields={this.handleInput} defaultText="Contractor Name" require="true"
                                                   id="nameField" classData="ui left icon input" inputClass=""
                                                   isEmpty={this.state.clearValue}
                                                   ref={(nameField) => { this.nameField = nameField }} />
                                        </div>
                                        <div className="field">
                                            <InputTel name="phone" labelName="Phone #" iconTags="phone icon" disable="false"
                                                   handleFormFields={this.handleInput} defaultText="Phone #" require="true"
                                                   classData="ui left icon input" isEmpty={this.state.clearValue} inputClass=""
                                                   ref={(phoneField) => { this.phoneField = phoneField }} />
                                        </div>
                                        <div className="field">
                                            <InputEmail name="email" disable="false" labelEmail="Email" iconTags="envelope icon"
                                                   handleFormFields={this.handleInput} defaultText="Email (john.doe@example.com)" require="true"
                                                   classData="ui left icon input" isEmpty={this.state.clearValue}
                                                   ref={(emailField) => { this.emailField = emailField }} />
                                        </div>
                                        <div className="fields">
                                            <RadioButton name="bbb_accredited" disable="false" labelRadio="Are you BBB Accredited?" iconTags=""
                                                   handleFormFields={this.handleInput} defaultText="false" require="true"
                                                   classData="" optionsValues={optionAccredited} setVal={this.state.bbb_accredited}
                                                   ref={(bbbAccredited) => { this.bbbAccredited = bbbAccredited }} />
                                        </div>
                                        <div className="field" style={flagDisplay}>
                                            <Input name="raiting" disable="false" labelName="Raiting" iconTags="star icon"
                                                   classData="ui left icon input" hide={flagDisplay} isEmpty={this.state.clearValue}
                                                   handleFormFields={this.handleInput} defaultText="Raiting" require="false"
                                                   ref={(raiting) => { this.raiting = raiting }} />
                                        </div>
                                        <div className="field">
                                            <Input name="wcb_number" disable="false" labelName="WCB # (Optional)" iconTags="archive icon"
                                                   classData="ui left icon input" isEmpty={this.state.clearValue}
                                                   handleFormFields={this.handleInput} defaultText="WCB # (Optional)" require="false"
                                                   ref={(wcb_number) => { this.wcb_number = wcb_number }} />
                                        </div>
                                        <div className="field">
                                            <Password name="password" disable="false" labelName="Password" iconTags="lock icon"
                                                   handleFormFields={this.handleInput} defaultText="Password" require="true"
                                                   classData="ui left icon input" isEmpty={this.state.clearValue} inputClass=""
                                                   ref={(passwordField) => { this.passwordField = passwordField }} />
                                        </div>
                                        <div className="field">
                                            <Password name="c_password" disable="false" labelName="Confirm Password" iconTags="lock icon"
                                                   handleFormFields={this.handleInput} defaultText="Confirm Password" require="true"
                                                   classData="ui left icon input" isEmpty={this.state.clearValue} inputClass=""
                                                   ref={(cpasswordField) => { this.cpasswordField = cpasswordField }} />
                                        </div>
                                        <div className="field required">
                                            <Checkbox name="termsConditions" labelName="Accept Terms and Conditions"
                                                   handleFormFields={this.handleInput} require="true" disable="false"
                                                   classData="" dataSent={this.state.dataMessage}
                                                   ref={(conditions) => { this.conditions = conditions }} />
                                        </div>
                                        <div className="field">
                                            <button className="ui fluid teal button right" type="submit" onClick={this.submitFormContractor}>
                                                Create Account
                                            </button>
                                        </div>
                                    </div>
                               </form>
                            </div>
                        </div>

                        <div className="ui bottom attached tab segment" data-tab="home_owner">
                            <div className="twelve wide column" id="home_owner">
                                <form className="ui large form thin_font">
                                    <div className="ui stacked segment">

                                        {errorTemplateH}

                                        <div className="field">
                                           <Input name="nameHO" disable="false" labelName="Home Owner Name" iconTags="user icon"
                                                  handleFormFields={this.handleInput} defaultText="Home Owner Name" require="true"
                                                  classData="ui left icon input" isEmpty={this.state.clearValue} inputClass=""
                                                  ref={(nameFieldH) => { this.nameFieldH = nameFieldH }} />
                                        </div>
                                        <div className="field">
                                            <InputTel name="phoneHO" disable="false" labelName="Phone #" iconTags="phone icon"
                                                   handleFormFields={this.handleInput} defaultText="Phone #" require="true"
                                                   classData="ui left icon input" isEmpty={this.state.clearValue} inputClass=""
                                                   ref={(phoneFieldH) => { this.phoneFieldH = phoneFieldH }} />
                                        </div>
                                        <div className="field">
                                            <InputEmail name="emailHO" disable="false" labelEmail="Email" iconTags="envelope icon"
                                                   handleFormFields={this.handleInput} defaultText="Email (john.doe@example.com)" require="true"
                                                   classData="ui left icon input" isEmpty={this.state.clearValue} inputClass=""
                                                   ref={(emailFieldH) => { this.emailFieldH = emailFieldH }} />
                                        </div>
                                        <div className="field">
                                            <Password name="passwordHO" disable="false" labelName="Password" iconTags="lock icon"
                                                   handleFormFields={this.handleInput} defaultText="Password" require="true"
                                                   classData="ui left icon input" isEmpty={this.state.clearValue} inputClass=""
                                                   ref={(passwordFieldH) => { this.passwordFieldH = passwordFieldH }} />
                                        </div>
                                        <div className="field">
                                            <Password name="c_passwordHO" disable="false" labelName="Confirm Password" iconTags="lock icon"
                                                   handleFormFields={this.handleInput} defaultText="Confirm Password" require="true"
                                                   classData="ui left icon input" isEmpty={this.state.clearValue} inputClass=""
                                                   ref={(cpasswordFieldH) => { this.cpasswordFieldH = cpasswordFieldH }} />
                                        </div>
                                        <div className="field required">
                                            <Checkbox name="termsConditionsHO" labelName="Accept Terms and Conditions"
                                                   handleFormFields={this.handleInput} require="true" disable="false"
                                                   classData="" dataSent={this.state.dataMessage}
                                                   ref={(conditions) => { this.conditions = conditions }} />
                                        </div>
                                        <div className="field">
                                            <button className="ui fluid teal button right" type="submit" onClick={this.submitFormHome}>
                                                Create Account
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div className="ui warning message center_text_aling">
                            Have an Account? <a href="/login" className="thin_font"> Sign In here! </a>
                        </div>
                    </div>;

        contentTrue =   <div>
                            <SuccessTemplate headerSuccess="Account Created Successfully"
                                                           bodySuccess={"Your Account has been created successfully." +
                                                                        "Please, check the email to activate your account and start publishing projects."}
                                                           onClick={this.closeClickSuccess} />

                            <div className="ui fluid stackable steps">
                                <div className="step">
                                    <i className="envelope icon"></i>
                                    <div className="content">
                                        <div className="title">Email</div>
                                        <div className="description">
                                            Receive an Email
                                        </div>
                                    </div>
                                </div>
                                <div className="step">
                                    <i className="check circle icon"></i>
                                    <div className="content">
                                        <div className="title">Confirmation</div>
                                        <div className="description">
                                            Confirm Your Account
                                        </div>
                                    </div>
                                </div>
                                <div className="step">
                                    <i className="pencil alternate icon"></i>
                                    <div className="content">
                                        <div className="title">Publish Projects</div>
                                        <div className="description">
                                            Start to Publish Projects
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>;

        return (this.state.successMessage != false) ? contentTrue : register;
    }
}
