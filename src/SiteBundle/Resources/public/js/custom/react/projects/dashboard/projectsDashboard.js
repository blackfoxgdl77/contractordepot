class PostsDashboard extends React.Component {

	constructor(props) {
        super(props);

        this.createSearchForm  = this.createSearchForm.bind(this);
        this.showSearchForm    = this.showSearchForm.bind(this);
        this.hideForm          = this.hideForm.bind(this);
        this.searchRecords     = this.searchRecords.bind(this);
        this.handleInput       = this.handleInput.bind(this);
        this.displayProjects   = this.displayProjects.bind(this);
		this.changeFlagState   = this.changeFlagState.bind(this);

        this.state = {
            validatingPosts         : 1,
            validatingCategories    : 1,
            validatingCities        : 1,
        	showForm                : false,
            buttonFormShow          : true,
			token		            : props.token,
			source		            : props.source,
			nameToken	            : props.nameT,
			clearValue	            : 0,
			flagSlider				: 0,
            windowPath              : (window.$PATH !== undefined) ? window.$PATH : ""
        };
    }

	changeFlagState = (flag) => {
		if (this.state.clearValue == 1) {
			this.setState({
				clearValue : 0,
			});
		}

		if (flag == 2 && this.state.validatingPosts == 0 
            && this.state.validatingCities == 0 && this.state.validatingCategories == 0) {
			this.setState({
				flagSlider : 0,
			});

			setTimeout(function () {
                $("#overlay_loader").css("display", "none");
            }, 3000);
		}
	}

	componentDidUpdate = () => {
		this.changeFlagState(0);

		if (this.state.flagSlider == 1) {
			this.changeFlagState(2);
		}
	}

    handleInput(data) {
        if (data.label != '') {
            this.setState({
                [data.label]: data.value
            });
        }
    }

    showSearchForm = (evt) => {
        evt.preventDefault();

        this.setState({
            showForm       : true,
            buttonFormShow : false
        });
    }

    hideForm = (evt) => {
        evt.preventDefault();

        this.setState({
            buttonFormShow : true,
            showForm       : false
        });
    }

    createSearchForm = () => {
        if (this.state.cities == undefined || this.state.cities == null ||
            this.state.jobCategories == undefined || this.state.jobCategories == null ) {
            return (
                <h5>Loading Projects Search component...</h5>
            );
        } else if (this.state.cities.length == 0) {
            return (
                <h5>There are not Cities!</h5>
            );
        } else if (this.state.jobCategories.length == 0) {
            return (
                <h5>There are not Categories!</h5>
            );
        } else {

            return (
                <form className="ui column form thin_font">
                    
                    <div className="column row field">
                        <Input name="postname" disable="false" labelName="Postname" iconTags="create"
                            handleFormFields={this.handleInput} defaultText="false" require="true" id="postname"
                            classData="ui left input" isEmpty={this.state.clearValue}
                            ref={(postname) => { this.postname = postname }} />
                    </div>

                    <div className="column row field">
                        <Select name="city" disable="false" labelSelectName="City" iconTags="format_list_numbered"
                            require="true" handleFormFields={this.handleInput} defaultText="false"
                            classData="" displayDataValues={this.state.cities} id="cityID"
                            ref={(city) => { this.city = city }} />
                    </div>

                    <div className="column row field">
                        <Select name="category" disable="false" labelSelectName="Category"
                            iconTags="format_list_numbered" require="true"
                            handleFormFields={this.handleInput} defaultText="false"
                            classData="" displayDataValues={this.state.jobCategories} id="categoryID"
                            ref={(category) => { this.category = category }} />
                    </div>

                    <div className="four column row">
                        <div className="column"></div>
                        <div className="column"></div>
                        <div className="column"></div>
                        <div className="column">
                            <div className="field">
                                <button className="ui fluid teal button right" type="submit" onClick={this.searchRecords}>
                                    Search
                                </button>
                            </div>
                        </div>
                    </div>

                </form>
            );
        }
    }

    searchRecords = (evt) => {

        evt.preventDefault();

        let url = this.state.windowPath + '/api/posts/user/' + this.props.id;
        let queryParams = "";

        if (this.state.postname != undefined && this.state.postname != null && this.state.postname != "") {
            queryParams = queryParams + "postname=" + this.state.postname;
        }
        if (this.state.category != undefined && this.state.category != null && this.state.category != "") {
            if (queryParams != "") {
                queryParams = queryParams + "&";
            }
            queryParams = queryParams + "category=" + this.state.category;
        }
        if (this.state.city != undefined && this.state.city != null && this.state.city != "") {
            if (queryParams != "") {
                queryParams = queryParams + "&";
            }
            queryParams = queryParams + "city=" + this.state.city;
        }

        if (queryParams != "") {
            queryParams = "?" + queryParams;
            url = url + queryParams;
        }

        // Retrieving posts information
        axios.get(url)
            .then((response) => {
                this.setState({
                    posts 	   : response.data.data,
					clearValue : 1,
                });

            }).catch((error) => {
                console.log("Error: " + error);
            });

    }

	componentDidMount = () => {
		let postUserRetrieveUrl = this.state.windowPath + '/api/posts/user/' + this.props.id;

		// Retrieving posts information
		axios.get(postUserRetrieveUrl)
			.then((response) => {
				if (response.status == 200 && response.data.status == 'OK') {
					this.setState({
						posts : response.data.data,
                        validatingPosts : 0,
						flagSlider : 1,
					});
				}

				if (response.status == 200 && response.data.status == 'ERROR') {
                    this.setState({
                        validatingPosts : 0
                    });
				}
			}).catch((error) => {
				console.log("Error: " + error);
                this.setState({
                    validatingPosts : 0
                });
			});
	}

    componentWillMount = () => {

    	let citiesRetrieveAllUrl     = this.state.windowPath + '/api/cities';
    	let categoriesRetrieveAllUrl = this.state.windowPath + '/api/jobCategories';

    	// Retrieving cities
        axios.get(citiesRetrieveAllUrl)
            .then((response) => {
				if (response.status == '200' && response.data.status == 'OK') {
	                this.setState({
	                    cities : response.data.data,
                        validatingCities : 0
	                });
				}

				if (response.status == 200 && response.data.status == 'ERROR') {
                    this.setState({
                        validatingCities : 0
                    });
				}
            }).catch((error) => {
                this.setState({
                    validatingCities : 0
                });
            });

        // Retrieving job categories
        axios.get(categoriesRetrieveAllUrl)
            .then((response) => {
				if (response.status == 200 && response.data.status == 'OK') {
	                this.setState({
	                    jobCategories  : response.data.data,
                        validatingCategories : 0
	                });
				}

				if (response.status == 200 && response.data.status == 'ERROR') {
                    this.setState({
                        validatingCategories : 0
                    });
				}

            }).catch((error) => {
                this.setState({
                    validatingCategories : 0
                });
            });
    }

    displayProjects = () => {
        let postsData = this.state.posts.map((project) => {

            return <div className="item" key={project.id}>
                        <div className="image">
                            <img className="activator" src={this.state.windowPath + project.image} />
                        </div>
                        <div className="content">
                            <a className="header">
                                {project.postname}
                            </a>
                            <div className="meta">
                                <span className="cinema">
                                    {project.category}
                                </span>
                            </div>
                            <div className="description">
                                <span>
                                    { project.description }
                                </span>
                            </div>
                            <div className="extra">
                                <a href={this.state.windowPath + "/projects/show/" + project.id}
                                   className="ui right floated primary button links_hover_blue thin_font">
                                    Project Details
                                    <i className="right chevron icon"></i>
                                </a>
                                <a href={this.state.windowPath + "/projects/edit/" + project.id}
                                   className="ui right floated primary button links_hover_blue thin_font">
                                    Edit Project
                                    <i className="right chevron icon"></i>
                                </a>
                                <div className="ui label">
                                    <i className="globe icon"></i>
                                    { project.city }
                                </div>
                            </div>
                        </div>
                    </div>
            });

        return postsData;
    }

	render() {
		if (this.state.posts == undefined || this.state.posts == null) {
            return (
                <h5></h5>
            );
        } else if (this.state.posts.length == 0) {
            return (
                <h5>There are not posts loaded!</h5>
            );
        } else {

            let buttonForm = "";
            let showForm = "";

            if (this.state.showForm == true) {
                showForm = this.createSearchForm();
            }

            // Verify if the button that will be displayed is for show or hide the form
            if (this.state.buttonFormShow == true) {
                buttonForm = <div className="column">
                                <div className="field">
                                    <button className="ui fluid teal button right" onClick={this.showSearchForm}>
                                        Open Search
                                    </button>
                                </div>                                
                            </div>;  
            } else {
                buttonForm = <div className="column">
                                <div className="field">
                                    <button className="ui fluid orange button right" onClick={this.hideForm}>
                                        Hide Search
                                    </button>
                                </div>                                
                            </div>;
            }

            // data to display
            let templateDatas = this.displayProjects();

			return (
				<div className="ui grid">
                    <div className="row">&nbsp;</div>

                    <div className="four column row">
                        <div className="column"></div>
                        <div className="column"></div>
                        <div className="column"></div>
                        {buttonForm}
                    </div>

                    <div className="one column row">
                        {showForm}
                    </div>

                    <div className="row">
                        <div className="sixteen wide column">
                            <div className="ui divided items">
                                {templateDatas}
                            </div>
                        </div>
                    </div>
				</div>

			);
		}
	}
}
