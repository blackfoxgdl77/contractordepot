/**
 * Class used to create new posts
 * for publish in the platform and giveº
 * the user to create jobs
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */
class PostNew extends React.Component {
    constructor(props) {
        super(props);

        this.save              = this.save.bind(this);
        this.handleInput       = this.handleInput.bind(this);
        this.validateData      = this.validateData.bind(this);
        this.closeClick        = this.closeClick.bind(this);
        this.changeFlagState   = this.changeFlagState.bind(this);
        this.closeClickSuccess = this.closeClickSuccess.bind(this);

        this.state = {
            username   : props.user,
            id         : props.id,
            token      : props.token,
            source     : props.source,
            nameT      : props.nameT,
            clearValue : '',
            messageErr : '',
            messageSuc : '',
            windowPath : (window.$PATH !== undefined) ? window.$PATH : "",
            errors     : [],
            counter    : 0,
            counterE   : 0,
            loader     : 0,
            counterS   : 0,
            counterH   : 0,
        };
    }

    changeFlagState = (flag) => {
        if (flag == 1) {
            setTimeout(function () {
                $("#overlay_loader").css("display", "none");
            }, 3000);

            this.setState({
                loader : 0,
            });
        }
    }

    componentDidUpdate = () => {
        if (this.state.loader == 1) {
            this.changeFlagState(1);
        }
    }

    componentDidMount = () => {
        //let postRetrieveUrl        = this.state.windowPath + '/api/posts/' + this.props.post;
        let cityRetrieveAllUrl     = this.state.windowPath + '/api/cities';
        let categoryRetrieveAllUrl = this.state.windowPath + '/api/jobCategories';

        // Retrieving cities
        axios.get(cityRetrieveAllUrl)
             .then((response) => {
                if (response.status == 200 && response.data.status == 'OK') {
                    this.setState({
                        cities : response.data.data
                    });
                }

                if (response.status == 200 && response.data.status == 'ERROR') {
                    this.setState({
                        respMessage  : response.data.data.message,
                        errorMessage : 1,
                        validating   : 0,
                    });
                }
            }).catch((error) => {
                this.setState({
                    respMessage  : error.data.data.message,
                    errorMessage : 1,
                    validating   : 0,
                });
            });


        // Retrieving job categories
        axios.get(categoryRetrieveAllUrl)
            .then((response) => {

                if (response.status == 200 && response.data.status == 'OK') {
                    this.setState({
                        jobCategories : response.data.data
                    });
                }

                if (response.status == 200 && response.data.status == 'ERROR') {
                    this.setState({
                        respMessage  : response.data.data.message,
                        errorMessage : 1,
                        validating   : 0,
                    });
                }
            }).catch((error) => {
                this.setState({
                    respMessage  : error.data.data.message,
                    errorMessage : 1,
                    validating   : 0,
                });
            });

        // hide loader
        $("#overlay_loader").css("display", "none");
    }

    handleInput = (data) => {
        if (data.label != '') {
            this.setState({
                [data.label]: data.value,
            });
        }
    }

    closeClick = () => {
        this.setState({
            counter    : 0,
            counterE   : 0,
            messageErr : '',
        });
    }

    closeClickSuccess = () => {
        this.setState({
            counterS   : 0,
            messageSuc : '',
        });
    }

    validateData = () => {
        let counter = 0, errors = {};

        if (this.state.postname === '' || this.state.postname === undefined) {
            errors['postname'] = 'Please, enter the post name';
            $("#postname").parent().parent().addClass('error');
            counter++;
        } else {
            errors['postname'] = '';
            $("#postname").parent().parent().removeClass('error');
        }

        if (this.state.description === '' || this.state.description === undefined) {
            errors['description'] = 'Please, enter a description';
            $("#description").parent().parent().addClass('error');
            counter++;
        } else {
            errors['description'] = '';
            $("#description").parent().parent().removeClass('error');
        }

        if (this.state.city === 0 || this.state.city === -1 || this.state.city === undefined) {
            errors['city'] = 'Please, select the city';
            $("#cityId").parent().parent().addClass('error');
            counter++;
        } else {
            errors['city'] = '';
            $("#cityId").parent().parent().removeClass('error');
        }

        if (this.state.category === 0 || this.state.category === -1 || this.state.category === undefined) {
            errors['category'] = 'Please, select the category';
            $("#categoryId").parent().parent().addClass('error');
            counter++;
        } else {
            errors['category'] = '';
            $("#categoryId").parent().parent().removeClass('error');
        }

        if (document.getElementById('postImage') === undefined || document.getElementById('postImage') == null ||
            document.getElementById('postImage').files === undefined || document.getElementById('postImage').value === '') {
            errors['image'] = 'Please, select at least one image / video';
            $("#postImage").parent().parent().addClass('error');
            counter++;
        } else {
            errors['image'] = '';
            $("#postImage").parent().parent().removeClass('error');
        }

        this.setState({
            errors  : errors,
            counter : counter,
        });

        return counter;
    }

    save = (evt) => {
        evt.preventDefault();

        $("#overlay_loader").css("display", "inline");
        let url      = this.state.windowPath + "/api/post/new/project";
        let count    = this.validateData();

        if (count == 0) {
            let dataForm = new FormData();
            let fileList = (document.getElementById('postImage') === undefined ||
                            document.getElementById('postImage') == null ||
                            document.getElementById('postImage').files === undefined) ? null : document.getElementById('postImage').files;

            dataForm.append('postname', this.state.postname);
            dataForm.append('description', this.state.description);
            dataForm.append('payments', (this.state.payments != '') ? this.state.payments : 0);
            dataForm.append('city', this.state.city);
            dataForm.append('category', this.state.category);
            dataForm.append('userId', this.state.id);
            dataForm.append('token', this.state.token);
            dataForm.append('source', this.state.source);
            dataForm.append('name', this.state.nameT);

            if (fileList != undefined && fileList != null && fileList.length > 0) {
                for(var x=0;x<fileList.length;x++) {
                    dataForm.append('image'+x, fileList[x]);
                }
            }

            axios.post(url, dataForm, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }).then((response) => {
                if (response.data.status == 'OK' && response.status == 200) {
                    this.setState({
                        counterS    : 1,
                        messageSuc  : response.data.data.message,
                        loader      : 1,
                        description : '',
                        postname    : '',
                        payments    : '',
                        city        : 0,
                        category    : 0,
                    });

                    $("#postname").val('');
                    $("#description").val('');
                    $("#payments").val('');
                    $("#postImage").val('');
                    $("#categoryId").prop('selectedIndex', 0);
                    $("#cityId").prop('selectedIndex', 0);
                }

                if (response.data.status == 'ERROR' && response.status == 200) {
                    this.setState({
                        counterH   : 1,
                        loader     : 1,
                        messageErr : response.data.data.message,
                    });
                }
            }).catch((error) => {
                this.setState({
                    counterE   : 1,
                    loader     : 1,
                    messageErr : 'Please, contact support team <support@contractorsbids.com',
                });
            });
        }
    }

    render = () => {

        if (this.state.counter != 0 || this.state.counterH != 0 || this.state.counterS != 0) {
            // hide loader
            $("#overlay_loader").css("display", "none");
        }

        let templateError = (this.state.counter != 0) ? <Errors headerError="Error Creating New Project"
                                                                bodyError="All the fields with '*' must be filled in."
                                                                variables={this.state.errors}
                                                                onClick={this.closeClick} /> : "";
        let templateError2 = (this.state.counterH != 0) ? <Errors headerError="Application Error 1!"
                                                                  bodyError={this.state.messageErr}
                                                                  onClick={this.closeClick} /> : "";
        let templateSuccess = (this.state.counterS != 0) ? <SuccessTemplate headerSuccess="Project Created Successfully!"
                                                                            bodySuccess={this.state.messageSuc}
                                                                            onClick={this.closeClickSuccess} /> : "";

        return (
                <div>

                    <div className="ui fluid stackable steps">
                        <div className="step">
                            <i className="newspaper outline icon"></i>
                            <div className="content">
                                <div className="title">New Project</div>
                                <div className="description">
                                    Create a Project would like to Publish
                                </div>
                            </div>
                        </div>
                        <div className="step">
                            <i className="dollar sign icon"></i>
                            <div className="content">
                                <div className="title">Receive Bids</div>
                                <div className="description">
                                    Receive Bids of different Users
                                </div>
                            </div>
                        </div>
                        <div className="step">
                            <i className="check circle icon"></i>
                            <div className="content">
                                <div className="title">Choose a Winner Bid</div>
                                <div className="description">
                                    Review and Choose the User of the Winner Bid
                                </div>
                            </div>
                        </div>
                    </div>

                    <form className="ui large form thin_font">
                        <div className="ui stacked segment">

                            {templateError}
                            {templateError2}
                            {templateSuccess}

                            <div className="field">
                                <Input name="publishedBy" disable="true" labelName="Published By" iconTags="user circle icon"
                                       handleFormFields={this.handleInput} defaultText="Published By" require="false" id="publishedBy"
                                       classData="ui left icon input" isEmpty={this.state.clearValue}
                                       ref={(publishedBy) => { this.publishedBy = publishedBy }}
                                       items={ this.props.user } />
                            </div>

                            <div className="field">
                                <Input name="postname" disable="false" labelName="Post Name" iconTags="pencil alternate icon"
                                       handleFormFields={this.handleInput} defaultText="Post Name" require="true"
                                       classData="ui left icon input" isEmpty={this.state.clearValue} inputClass=""
                                       ref={(postname) => { this.postname = postname }} />
                            </div>

                            <div className="field">
                                <TextArea name="description" disable="false" require="true" defaultText="Description"
                                          iconTags="description" handleFormFields={this.handleInput} labelName="Description"
                                          classData="" classCustom="" rowCustom=""
                                          ref={(description) => { this.description = description }} />
                            </div>

                            <div className="field">
                                <Input name="payments" disable="false" labelName="Payments" iconTags="money bill alternate icon"
                                       handleFormFields={this.handleInput} defaultText="Payments" require="false"
                                       classData="ui left icon input" isEmpty={this.state.clearValue} inputClass=""
                                       ref={(payments) => { this.payments = payments }} />
                            </div>

                            <div className="field">
                                <Select name="city" labelSelectName="City" iconTags="format_list_numbered" require="true"
                                        disable="false" id="cityId" handleFormFields={this.handleInput} defaultText="false"
                                        displayDataValues={this.state.cities} classData=""
                                        ref={(city) => { this.city = city }} />
                            </div>

                            <div className="field">
                                <Select name="category" labelSelectName="Category" iconTags="format_list_numbered" require="true"
                                        disable="false" id="categoryId" handleFormFields={this.handleInput} defaultText="false"
                                        displayDataValues={this.state.jobCategories} classData=""
                                        ref={(category) => { this.category = category }} />
                            </div>

                            <div className="field">
                                <InputFile name="postImage" labelButton="File" handleFormFields={this.handleInput}
                                           iconTags="insert_photo" classData="" disable="false" require="true"
                                           tooltip="true"
                                           ref={(postImage) => { this.postImage = postImage }} />
                            </div>

                            <div className="field">
                                <button className="ui fluid teal button right" type="submit" onClick={this.save}>
                                    Create New Project
                                </button>
                            </div>
                    </div>
                </form>

                <div className="ui warning message center_text_aling">
                    Would you like to see more projects created by you?
                    <a href={"/post/search/user/" + this.state.id} className="thin_font"> Click Here! </a>
                </div>
            </div>
        );
    }
}
