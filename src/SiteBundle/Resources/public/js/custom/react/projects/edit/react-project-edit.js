
ReactDOM.render(
  <PostEdit user={ document.getElementById('user.username').value }
            post={ document.getElementById('post.id').value }
  	        id={ document.getElementById('user.id').value }
            token={ document.getElementById('_csrf_token').value }
            source={ document.getElementById('source').value }
            nameT={ document.getElementById('name').value } />,
  document.getElementById('app')
);
