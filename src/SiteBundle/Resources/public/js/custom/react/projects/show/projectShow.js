// Const for modal
const display = {
    display: 'block'
};
const hide = {
    display: 'none'
};

class PostShow extends React.Component {
    constructor(props) {
        super(props);

        this.handleInput        = this.handleInput.bind(this);
        this.modalReport        = this.modalReport.bind(this);
        this.modalBids          = this.modalBids.bind(this);
        this.modalReviews       = this.modalReviews.bind(this);
        this.modalComments      = this.modalComments.bind(this);
        this.reportAction       = this.reportAction.bind(this);
        this.bidsAction         = this.bidsAction.bind(this);
        this.reviewBidsAction   = this.reviewBidsAction.bind(this);
        this.reportProject      = this.reportProject.bind(this);
        this.reportOffer        = this.reportOffer.bind(this);
        this.closeClickSuccess  = this.closeClickSuccess.bind(this);
        this.closeClick         = this.closeClick.bind(this);
        this.closeModal         = this.closeModal.bind(this);
        this.sendComment        = this.sendComment.bind(this);
        this.createPublish      = this.createPublish.bind(this);
        this.validateOffer      = this.validateOffer.bind(this);
        this.templatePostsBids  = this.templatePostsBids.bind(this);
        this.acceptBidsAction   = this.acceptBidsAction.bind(this);
        this.declineBidsAction  = this.declineBidsAction.bind(this);
        this.changeStatusModal  = this.changeStatusModal.bind(this);
        this.displayAllComments = this.displayAllComments.bind(this);
        this.loginNoLogin       = this.loginNoLogin.bind(this);
        this.loadAllComments    = this.loadAllComments.bind(this);
        this.loadWinnerComments = this.loadWinnerComments.bind(this);
        this.innerComments      = this.innerComments.bind(this);
        this.saveInnerComments  = this.saveInnerComments.bind(this);
        this.displaySliders     = this.displaySliders.bind(this);
        this.initSliders        = this.initSliders.bind(this);
        this.displayWinComments = this.displayWinComments.bind(this);
        this.createWinPublish   = this.createWinPublish.bind(this);
        this.sendWinnerComment  = this.sendWinnerComment.bind(this);
        this.changeFlagState    = this.changeFlagState.bind(this);
        this.redirectLoader     = this.redirectLoader.bind(this);

        this.state = {
            validatingPosts    : 1,
            validatingBids     : 1,
            validatingWinners  : 1,
            validatingComments : 1,
            usersIdData        : [],
            postsBids          : [],
            bidWinner          : [],
            allComments        : [],
            winComments        : [],
            flagAmount         : 0,
            reloadForce        : 0,
            reportFlag         : 0,
            reportBids         : 0,
            reportReview       : 0,
            reportInner        : 0,
            successEnable      : 0,
            successMsj         : '',
            errorEnable        : 0,
            errorMsj           : '',
            publishIdState     : 0,
            clearValue         : 0,
            reportRedirectFlag : 0,
            flagDisplayLoader  : 0,
            source             : props.source,
            token              : props.token,
            nameToken          : props.nameT,
            linkUrl            : '',
            flagSlider         : 0,
            loader             : 0,
            recheck            : 1,
            windowPath         : (window.$PATH !== undefined) ? window.$PATH : ""
        };
    }

    initSliders = () => {
    }

    changeFlagState = (flag) => {
        if (this.state.clearValue == 1) {
            this.setState({
                clearValue : 0,
            });
        }

        if (flag == 2) {
            this.setState({
                flagSlider : 0,
            });
        }

        if (flag == 1) {
            setTimeout(function () {
                $("#overlay_loader").css("display", "none");
            }, 3000);

            this.setState({
                loader : 0,
            });
        }
    }

    handleInput(data) {
        if (data.label != '') {
            this.setState({
                [data.label]: data.value
            });
        }
    }

    modalReport = () => {
        this.setState({
            reportFlag   : 1,
            reportBids   : 0,
            reportReview : 0,
            reportInner  : 0,
        });
    }

    modalBids = () => {
        this.setState({
            reportBids   : 1,
            reportFlag   : 0,
            reportReview : 0,
            reportInner  : 0,
        });
    }

    modalReviews = () => {
        this.setState({
            reportReview : 1,
            reportBids   : 0,
            reportFlag   : 0,
            reportInner  : 0,
        });
    }

    modalComments = (id, evt) => {
        evt.preventDefault();

        this.setState({
            reportReview   : 0,
            reportBids     : 0,
            reportFlag     : 0,
            reportInner    : 1,
            publishIdState : id,
        });
    }

    reportAction = () => {
        let styleData       = (this.state.reportFlag != 0) ? display : hide;
        let errorTemplate   = (this.state.errorEnable == 1) ? <Errors headerError="Application Error!"
                                                                      bodyError={this.state.errorMsj}
                                                                      onClick={this.closeClickE} /> : "";
        let successTemplate = (this.state.successEnable == 1) ? <SuccessTemplate headerSuccess="Report Done Successfully!"
                                                                                 bodySuccess={this.state.successMsj}
                                                                                 onClick={this.closeClickSuccess} /> : "";

        let report =    <div className="ui modal" key={this.props.post} style={styleData}>
                            <div className="header">
                                Show Project  - Report Project
                            </div>
                            <div className="content">
                                {successTemplate}
                                {errorTemplate}

                                <h3 className="ui header">
                                    Are you sure you want to report the projects <b>'{this.state.post.postname}'</b> as fake?
                                </h3>
                            </div>
                            <div className="actions">
                                <a className="ui positive button" onClick={(e) => this.reportProject(e)}>
                                    <i className="bullhorn icon"></i>
                                    Report Project
                                </a>
                                <a className="ui negative button" onClick={(e) => this.closeModal(1, e)}>
                                    <i className="window close icon"></i>
                                    Close
                                </a>
                            </div>
                        </div>;

        return report;
    }

    bidsAction = () => {
        let styleData       = (this.state.reportBids != 0) ? display : hide;
        let errorTemplate   = (this.state.errorEnable == 1 || this.state.flagAmount == 1) ? <Errors headerError="Error Making a Bid"
                                                                                                    bodyError={this.state.errorMsj}
                                                                                                    onClick={this.closeClick} /> : "";
        let successTemplate = (this.state.successEnable == 1) ? <SuccessTemplate headerSuccess="Bid Done Successfully"
                                                                                 bodySuccess={this.state.successMsj}
                                                                                 onClick={this.closeClickSuccess} /> : "";

        let bids =  <div className="ui modal" key={this.props.post} style={styleData}>
                        <div className="header">
                            Projects - Make a Bid
                        </div>
                        <div className="scrolling content">
                            <div className="ui large header">
                                Would you like to make a bid?
                            </div>
                            <div className="">
                                <form className="ui large form thin_font">
                                    <div className="ui stacked segment">
                                        <Input name="offer" disable="false" labelName="Amount $" iconTags="" id="offerField"
                                               handleFormFields={this.handleInput} defaultText="Amount $" require="true"
                                               classData="" isEmpty={this.state.clearValue} specialInput="1"
                                               ref={(offerField) => { this.offerField = offerField }} />
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div className="actions">
                            <div className="ui positive button" onClick={(e) => this.reportOffer(e)}>
                                <i className="check square icon"></i>
                                Yes, Make a Bid!
                            </div>
                            <div className="ui negative button" onClick={(e) => this.closeModal(4, e)}>
                                <i className="window close icon"></i>
                                Close
                            </div>
                        </div>
                    </div>;

        return bids;
    }

    redirectLoader = () => {
        let styleData = (this.state.reportRedirectFlag != 0) ? display : hide;

        let modalLoader =   <div className="ui modal" key="1" style={styleData}>
                                <div className="content">
                                    <div className="center-spiner">
                                        <div className="loaderSpin"></div>
                                    </div>
                                    <div className="font-size-redirect center-aligned colors_links gross_font">
                                        Redirecting to Payment platform ....
                                    </div>
                                </div>
                            </div>;

        return modalLoader;
    }

    reviewBidsAction = () => {
        let styleData       = (this.state.reportReview != 0) ? display : hide;
        let errorTemplate   = (this.state.errorEnable == 1) ? <Errors headerError="Application Error!"
                                                                      bodyError={this.state.errorMsj}
                                                                      onClick={this.closeClick} /> : "";
        let successTemplate = (this.state.successEnable == 1) ? <SuccessTemplate headerSuccess="Data Loaded Successfully"
                                                                                 bodySuccess={this.state.successMsj}
                                                                                 onClick={this.closeClickSuccess} /> : "";
        let bidsBody = this.templatePostsBids();

        let review =    <div className="ui modal" key={this.props.post} style={styleData}>
                            <div className="header">
                                Show Project - Review Bids
                            </div>
                            <div className="scrolling content">
                                {errorTemplate}
                                {successTemplate}

                                <div className="ui medium header">
                                    Bids Done for the Project!
                                </div>
                                <div>
                                    {bidsBody}
                                </div>
                            </div>
                            <div className="action">
                                <div className="negative ui button" onClick={(e) => this.closeModal(2, e)}>
                                    <i className="window close icon"></i>
                                    Close
                                </div>
                            </div>
                        </div>;

        return review;
    }

    changeStatusModal = () => {
        let url = this.state.windowPath + '/api/get/bids/done/' + this.props.post;

        axios.get(url)
             .then((response) => {
                 if (response.status == 200 && response.data.status == 'OK') {
                     this.setState({
                         postsBids : response.data.data.posts,
                     });
                 }

                 if (response.status == 200 && response.data.status == 'ERROR') {
                     this.setState({
                         errorEnable : 1,
                         errorMsj    : response.data.data.message,
                     });
                 }
             }).catch((error) => {
                 this.setState({
                     errorEnable : 1,
                     errorMsj    : error.data.data.message,
                 });
             });
    }

    acceptBidsAction = (id, evt) => {
        evt.preventDefault();

        $("#overlay_loader").css("display", "inline");
        let urlAccept = this.state.windowPath + "/api/post/accept/bid";
        let postData  = {
            postId  : this.props.post,
            offerId : id,
            source  : this.state.source,
            token   : this.state.token,
            nameT   : this.state.nameToken,
        };

        axios.post(urlAccept, postData)
             .then((response) => {
                 if (response.data.status == 'OK' && response.status == 200) {
                     this.setState({
                         successEnable     : 1,
                         successMsj        : response.data.data.message,
                         linkUrl           : response.data.data.linkButton,
                         reloadForce       : 1,
                         flagDisplayLoader : 1,
                         loader            : 1,
                     });

                     this.forceUpdate();
                     this.changeStatusModal();
                 }

                 if (response.data.status == 'ERROR' && response.status == 200) {
                     this.setState({
                         errorEnable : 1,
                         loader      : 1,
                         errorMsj    : response.data.data.message,
                     });

                     this.forceUpdate();
                     this.changeStatusModal();
                 }
             }).catch((error) => {
                 this.setState({
                     errorEnable : 1,
                     errorMsj    : error.data.data.message,
                     loader      : 1,
                 });
             });
    }

    declineBidsAction = (id, evt) => {
        evt.preventDefault();

        $("#overlay_loader").css("display", "inline");
        let urlDecline = this.state.windowPath + "/api/post/decline/bid";
        let postDatas  = {
            postId  : this.props.post,
            offerId : id,
            source  : this.state.source,
            token   : this.state.token,
            nameT   : this.state.nameToken,
        };

        axios.post(urlDecline, postDatas)
             .then((response) => {
                 if (response.status == 200 && response.data.status == 'OK') {
                     this.setState({
                         successEnable : 1,
                         successMsj    : response.data.data.message,
                         loader        : 1,
                     });

                     this.forceUpdate();
                     this.changeStatus();
                 }

                 if (response.status == 200 && response.data.status == 'ERROR') {
                     this.setState({
                         errorEnable : 1,
                         errorMsj    : response.data.data.message,
                         loader      : 1,
                     });

                     this.forceUpdate();
                     this.changeStatusModal();
                 }
             })
             .catch((error) => {
                 this.setState({
                     errorEnable : 1,
                     errorMsj    : error.data.data.message,
                     loader      : 1,
                 });
             })
    }

    templatePostsBids = () => {
        let template = this.state.postsBids.map((bidsDone) => {
            if (bidsDone.isActive == 1) {
                return  <tr key={bidsDone.id_post} className="thin_font blue_color">
                            <td>
                                {bidsDone.username}
                            </td>
                            <td>
                                $ {bidsDone.moneyBid}
                            </td>
                            <td>
                                <a className="ui positive button" onClick={(e) => this.acceptBidsAction(bidsDone.id_post, e)}>
                                    <i className="check circle icon"></i>
                                    Accept Bid
                                </a>
                                <a className="ui negative button" onClick={(e) => this.declineBidsAction(bidsDone.id_post, e)}>
                                    <i className="window close icon"></i>
                                    Decline Bid
                                </a>
                            </td>
                        </tr>
            } else {
                if (bidsDone.isActive == 2 || bidsDone.isActive == 3) {
                    let stringOption = '', extraTitle = '';

                    if (bidsDone.isActive == 3) {
                        extraTitle = '( Pending to do Payment )';
                        stringOption =  <a href={bidsDone.link} className="ui right floated button">
                                            <i className="credit card icon"></i>
                                            Make Payment
                                        </a>;
                    }

                    return  <tr key={bidsDone.id_post} className="thin_font blue_color">
                                <td>
                                    {bidsDone.username}
                                </td>
                                <td>
                                    $ {bidsDone.moneyBid}
                                </td>
                                <td>
                                    <strong>
                                        Bid Aceppted {extraTitle}
                                    </strong>
                                    {stringOption}
                                </td>
                            </tr>
                } else {
                    return  <tr key={bidsDone.id_post} className="thin_font blue_color">
                                <td>
                                    {bidsDone.username}
                                </td>
                                <td>
                                    $ {bidsDone.moneyBid}
                                </td>
                                <td>
                                    <strong>
                                        Bid Declined
                                    </strong>
                                </td>
                            </tr>
                }
            }
        });

        let finalTemplate = <table className="ui selectable celled table">
                                <thead>
                                    <tr>
                                        <th className="center-align">Username</th>
                                        <th className="center-align">Bids</th>
                                        <th className="center-align">Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {template}
                                </tbody>
                            </table>;

        return finalTemplate;
    }

    reportProject = (evt) => {
        evt.preventDefault();

        $("#overlay_loader").css("display", "inline");
        let urlProject = this.state.windowPath + "/api/post/fake/project";
        let postData   = {
            projectId : this.props.post,
            userId    : this.props.userId,
            source    : this.state.source,
            token     : this.state.token,
            nameT     : this.state.nameToken,
        };

        axios.post(urlProject, postData)
             .then((response) => {
                if (response.data.status == 'OK' && response.status == 200) {
                    this.setState({
                        successEnable : 1,
                        successMsj    : response.data.data.message,
                        loader        : 1,
                    });
                }

                if (response.data.status == 'ERROR' && response.status == 200) {
                    this.setState({
                        errorEnable : 1,
                        loader      : 1,
                        errorMsj    : response.data.data.message,
                    });
                }
             }).catch((error) => {
                 this.setState({
                     errorEnable : 1,
                     loader      : 1,
                     errorMsj    : error.data.data.message,
                 });
             });
    }

    validateOffer = () => {
        let amount = 0;

        if (this.state.offer === '' || this.state.offer === undefined) {
            amount++;
        }

        this.setState({
            flagAmount : amount,
            errorMsj   : 'Please, to make a bid enter an amount.'
        });

        return amount;
    }

    reportOffer = (evt) => {
        evt.preventDefault();

        $("#overlay_loader").css("display", "inline");
        let urlOffers = this.state.windowPath + "/api/post/make/bid";
        let postDatas = {
            userId : this.props.userId,
            postId : this.props.post,
            amount : this.state.offer,
            source : this.state.source,
            token  : this.state.token,
            nameT  : this.state.nameToken,
        };

        let total = this.validateOffer();

        // Validate the amount offer has been typed on the field
        if (total == 0) {
            axios.post(urlOffers, postDatas)
                 .then((response) => {
                     if (response.data.status == 'OK' && response.status == 200) {
                         this.setState({
                             clearValue    : 1,
                             successEnable : 1,
                             successMsj    : response.data.data.message,
                             loader        : 1,
                         });
                     }

                     if (response.data.status == 'ERROR' && response.status == 200) {
                         this.setState({
                             errorEnable : 1,
                             loader      : 1,
                             errorMsj    : response.data.data.message,
                         });
                     }
                 }).catch((error) => {
                     this.setState({
                         errorEnable : 1,
                         loader      : 1,
                         errorMsj    : error.data.data.message,
                     });
                 });
         }
    }

    componentDidMount = () => {
        let url1 = this.state.windowPath + '/api/get/bids/done/' + this.props.post;
        let postRetrieveUrl = this.state.windowPath + '/api/posts/' + this.props.post;

        // Retrieving post inserted information
        axios.get(postRetrieveUrl)
            .then((response) => {
                if (response.status == 200 && response.data.status == 'OK') {
                    this.setState({
                        post            : response.data.data,
                        validatingPosts : 0,
                        flagSlider      : 1,
                    });
                }

                if (response.status == 200 && response.data.status == 'ERROR') {
                    this.setState({
                        validatingPosts : 0,
                        errorEnable     : 1,
                        errorMsj        : response.data.data.message,
                    });
                }
            }).catch((error) => {
                this.setState({
                    validatingPosts : 0,
                    errorEnable     : 1,
                    errorMsj        : error.data.data.message,
                });
            });

        axios.get(url1)
             .then((response) => {
                 if (response.status == 200 && response.data.status == 'OK') {

                     this.setState({
                         postsBids      : response.data.data.posts,
                         validatingBids : 0,
                     });
                 }

                 if (response.status == 200 && response.data.status == 'ERROR') {
                    this.setState({
                        validatingBids : 0,
                        errorEnable    : 1,
                        errorMsj       : response.data.data.message,
                    });
                 }
             }).catch((error) => {
                this.setState({
                    validatingBids : 0,
                    errorEnable    : 1,
                    errorMsj       : error.data.data.message,
                });
             });

        this.loadAllComments();
        this.loadWinnerComments();
    }

    loadAllComments = () => {
        let url = this.state.windowPath + '/api/get/comments/subcomments/' + this.props.post;

        axios.get(url)
             .then((response) => {
                 if (response.status == 200 && response.data.status == 'OK') {
                     this.setState({
                         allComments        : response.data.data.comments,
                         validatingComments : 0
                     });
                 }

                 if (response.status == 200 && response.data.status == 'ERROR') {
                    this.setState({
                        validatingComments : 0,
                        errorEnable        : 1,
                        errorMsj           : response.data.data.message,
                    });
                 }

                 // hide loader
                 $("#overlay_loader").css("display", "none");
             })
             .catch((error) => {
                this.setState({
                    validatingComments : 0,
                    errorEnable        : 1,
                    errorMsj           : error.data.data.message,
                });

                // hide loader
                $("#overlay_loader").css("display", "none");
             });
    }

    loadWinnerComments = () => {
        let url = this.state.windowPath + "/api/get/winner/comments/" + this.props.post;

        axios.get(url)
             .then((response) => {
                 if (response.status == 200 && response.data.status == 'OK') {
                     this.setState({
                         winComments       : response.data.data.data,
                         usersIdData       : response.data.data.users,
                         validatingWinners : 0
                     });
                 }

                 if (response.status == 200 && response.data.status == 'ERROR') {
                    this.setState({
                        validatingWinners : 0,
                        errorEnable       : 1,
                        errorMsj          : response.data.data.message,
                    });
                 }

                 // hide loader
                 $("#overlay_loader").css("display", "none");
             })
             .catch((error) => {
                this.setState({
                    validatingWinners : 0,
                    errorEnable       : 1,
                    errorMsj          : error.data.data.message,
                });

                // hide loader
                $("#overlay_loader").css("display", "none");
             });
    }

    componentWillMount() {
    }

    closeModal = (flag, evt) => {
        evt.preventDefault();

        if (flag == 1) {
            this.setState({
                reportFlag : 0,
            });
        } else if (flag == 2) {
            this.setState({
                reportReview : 0,
            });

            if (this.state.flagDisplayLoader == 1) {
                this.setState({
                    reportRedirectFlag : 1,
                });
            }
        } else if (flag == 3) {
            this.setState({
                reportInner : 0,
            });
        } else {
            this.setState({
                reportBids : 0,
            });
        }
    }

    closeClick = () => {
        this.setState({
            flagAmount  : 0,
            errorEnable : 0,
            errorMsj    : '',
        });
    }

    closeClickSuccess = () => {
        this.setState({
            successMsj    : '',
            successEnable : 0,
        });
    }

    sendComment = (evt) => {
        evt.preventDefault();

        $("#overlay_loader").css("display", "inline");
        let url  = this.state.windowPath + '/api/save/publishes'
        let post = {
            usersId  : this.props.userId,
            postsId  : this.props.post,
            title    : this.state.title,
            comments : this.state.comments,
            token    : this.state.token,
            source   : this.state.source,
            nameT    : this.state.nameToken,
        };

        axios.post(url, post)
             .then((response) => {
                 if (response.data.status == 'OK' && response.status == 200) {
                     this.setState({
                         clearValue    : 1,
                         successEnable : 1,
                         successMsj    : response.data.data.message,
                         loader        : 1,
                     });

                     this.loadAllComments();
                 }

                 if (response.data.status == 'ERROR' && response.status == 200) {
                     this.setState({
                         errorEnable : 1,
                         errorMsj    : response.data.data.message,
                         loader      : 1,
                     });
                 }
             }).catch((error) => {
                 this.setState({
                     errorEnable : 1,
                     errorMsj    : error.data.data.message,
                     loader      : 1,
                 });
             });
    }

    sendWinnerComment = (evt) => {
        evt.preventDefault();

        $("#overlay_loader").css("display", "inline");
        let url = this.state.windowPath + '/api/post/save/winner/comments';
        let comments = {
            comment : this.state.commentsW,
            postsId : this.props.post,
            usersId : this.props.userId,
            source  : this.state.source,
            token   : this.state.token,
            nameT   : this.state.nameToken,
        };

        axios.post(url, comments)
             .then((response) => {
                 if (response.status == 200 && response.data.status == 'OK') {
                     this.setState({
                         clearValue    : 1,
                         successEnable : 1,
                         successMsj    : response.data.data.message,
                         loader        : 1,
                     });

                     // Reload informations
                     this.loadWinnerComments();
                 }

                 if (response.status == 200 && response.data.status == 'ERROR') {
                     this.setState({
                         errorEnable : 1,
                         errorMsj    : response.data.data.message,
                         loader      : 1,
                     });
                 }
             })
             .catch((error) => {
                 this.setState({
                     errorEnable : 1,
                     errorMsj    : error.data.data.message,
                     loader      : 1,
                 });
             });
    }

    innerComments = () => {
        let styleData       = (this.state.reportInner != 0) ? display : hide;
        let errorTemplate   = (this.state.errorEnable == 1) ? <Errors headerError="Error Publish Comments"
                                                                      bodyError={this.state.errorMsj}
                                                                      onClick={closeClick} /> : "";
        let successTemplate = (this.state.successEnable == 1) ? <SuccessTemplate headerSuccess="Comment Publish Successfully"
                                                                                 bodySuccess={this.state.successMsj}
                                                                                 onClick={this.closeClickSuccess} /> : "";

        let innerComments = <div className="ui modal" key={this.props.post} style={styleData}>
                                <div className="header">
                                    Show Project  - Reply Comment
                                </div>
                                <div className="scrolling content">
                                    {errorTemplate}
                                    {successTemplate}

                                    <form className="ui large form thin_font">
                                        <div className="ui stacked segment">
                                            <div className="field">
                                                <TextArea name="replyComment" disable="false" require="true" defaultText="false"
                                                        iconTags="description" handleFormFields={this.handleInput} labelName="Reply a Comment"
                                                        classData="input-field col s9 offset-s1" classCustom="" rowCustom="left_move_textarea"
                                                        isEmpty={this.state.clearValue}
                                                        ref={(replyComments) => { this.replyComments = replyComments }} />
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div className="actions">
                                    <div className="positive ui button" onClick={(e) => this.saveInnerComments(e)}>
                                        <i className="edit icon"></i>
                                        Reply a Comment
                                    </div>
                                    <div className="negative ui button" onClick={(e) => this.closeModal(3, e)}>
                                        <i className="window close icon"></i>
                                        Close
                                    </div>
                                </div>
                            </div>;


        return innerComments;
    }

    saveInnerComments = (evt) => {
        evt.preventDefault();

        $("#overlay_loader").css("display", "inline");
        let url   = this.state.windowPath + "/api/post/save/comments";
        let posts = {
            usersId   : this.props.userId,
            postsId   : this.props.post,
            comments  : this.state.replyComment,
            publishId : this.state.publishIdState,
            source    : this.state.source,
            token     : this.state.token,
            nameT     : this.state.nameToken,
        };

        axios.post(url, posts)
             .then((response) => {
                 if (response.status == 200 && response.data.status == 'OK') {
                     this.setState({
                         clearValue    : 1,
                         successEnable : 1,
                         successMsj    : response.data.data.message,
                         loader        : 1,
                     });

                     this.loadAllComments();
                 }

                 if (response.status == 200 && response.data.status == 'ERROR') {
                     this.setState({
                         errorEnable : 1,
                         errorMsj    : response.data.data.message,
                         loader      : 1,
                     });
                 }
             })
             .catch((error) => {
                 this.setState({
                     errorEnable : 1,
                     errorMsj    : error.data.data.message,
                     loader      : 1,
                 });
             });
    }

    displayWinComments = () => {
        let winComments = '';
        winComments = this.state.winComments.map((winnerC) => {
            return (
                <div className="ui comments" key={winnerC.id_winner}>
                    <div className="comment">
                        <a className="avatar">
                            <i classname=""></i>
                        </a>
                        <div className="content">
                            <a className="author blue_color">
                                { winnerC.projectWinner }
                            </a>
                            <div className="metadata">
                                <span className="date">
                                    { winnerC.dateCreates }
                                </span>
                            </div>
                            <div className="text thin_font">
                                { winnerC.comments }
                            </div>
                        </div>
                    </div>
                </div>
            );
        });

        return winComments;
    }

    displayAllComments = () => {
        let comments = this.state.allComments.map((comments) => {
            return (<div className="ui comments" key={comments.id_publish}>
                        <div className="comment">
                            <a className="avatar">
                                <img src="" />
                            </a>
                            <div className="content">
                                <a className="author">
                                    { comments.publish_username }
                                </a>
                                <div className="metadata">
                                    <span className="date">
                                        { comments.date_created }
                                    </span>
                                </div>
                                <div className="text">
                                    <div>
                                        { comments.title }
                                    </div>
                                    <div>
                                        { comments.comments }
                                    </div>
                                </div>
                                <div className="actions">
                                    <a className="reply" onClick={(e) => this.modalComments(comments.id_publish, e)}>
                                        Reply
                                    </a>
                                </div>
                            </div>
                            <div className="comments">
                                { comments.innerComments.map((innerData) => {
                                    return (<div className="comment" key={innerData.id_comment}>
                                                <a className="avatar">
                                                    <img src="" />
                                                </a>
                                                <div className="content">
                                                    <a className="author">
                                                        {innerData.username_comment}
                                                    </a>
                                                    <div className="metadata">
                                                        <span className="date">
                                                            {innerData.date_created}
                                                        </span>
                                                    </div>
                                                    <div className="text">
                                                        {innerData.comments}
                                                    </div>
                                                </div>
                                            </div>
                                        );
                                })}
                            </div>
                        </div>
                    </div>)
        });

        return comments = '';
    }

    createWinPublish = () => {
        let formWinner = <div>
                            <div className="twelve wide column">
                                <div className="ui large header">
                                    COMMENTS
                                </div>
                                <form className="ui large form thin_font">
                                    <div className="ui stacked segment">
                                        <div className="field">
                                            <TextArea name="commentsW" require="true" defaultText="Comment" labelName="Comment"
                                                   handleFormFields={this.handleInput} iconTags=""
                                                   isEmpty={this.state.clearValue}
                                                   classData="" classCustom="" rowCustom=""
                                                   ref={(commentsW) => { this.commentsW = commentsW }} />
                                        </div>
                                        <div className="field">
                                            <button className="ui fluid teal button right" type="submit" onClick={this.sendWinnerComment}>
                                                Publish Comment
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                         </div>;

        return formWinner;
    }

    createPublish = (evt) => {
        let forms = <div>
                        <div className="twelve wide column">
                            <div className="ui large header">
                                COMMENTS
                            </div>
                            <form className="ui large form thin_font">
                                <div className="ui stacked segment">
                                    <div className="field">
                                        <Input name="title" disable="false" labelName="Title" defaultText="Title"
                                               handleFormFields={this.handleInput} require="true" id="titleField"
                                               classData="ui left icon input" inputClass="" iconTags="pencil alternate icon"
                                               isEmpty={this.state.clearValue}
                                               ref={(titleField) => { this.titleField = titleField }} />
                                    </div>
                                    <div className="field">
                                        <TextArea name="comments" require="true" defaultText="Comment" labelName="Comment"
                                               handleFormFields={this.handleInput} iconTags=""
                                               isEmpty={this.state.clearValue}
                                               classData="" classCustom="" rowCustom=""
                                               ref={(comments) => { this.comments = comments }} />
                                    </div>
                                    <div className="field">
                                        <button className="ui fluid teal button right" type="submit" onClick={this.sendComment}>
                                            Publish Comment
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>;

        return forms;
    }

    loginNoLogin = () => {
        let columns         = (this.props.userId == this.state.post.owner) ? "twelve wide columns" : "four wide columns";
        let buttonsValidate = '', completeButtons = '', formComment = '', showComments = '';

        //must have valudate buttons
        if (this.state.post.status == 1 || this.state.post.bids.statusBid == 3) {
            completeButtons =   <div className="ui positive button" onClick={this.modalReviews}>
                                    <i className="dollar sign icon"></i>
                                    Review Bids
                                </div>;
        }

        if (this.state.post.status == 1) {
            if (this.props.userId !== this.state.post.owner) {
                buttonsValidate =   <div className="">
                                        <div className="ui negative button" onClick={this.modalReport}>
                                            <i className="bell icon"></i>
                                            Report Project
                                        </div>
                                        <div className="ui positive button" onClick={this.modalBids}>
                                            <i className="dollar sign icon"></i>
                                            Make a Bid
                                        </div>
                                        {completeButtons}
                                    </div>;
            }
        }

        if (this.state.post.status == 1) {
            formComment  = this.createPublish();
            showComments = this.displayAllComments();
        } else {
            // validate if the user can see the information of the comments
            let ownerId  = parseInt(this.state.usersIdData.ownerId);
            let winnerId = parseInt(this.state.usersIdData.winnerId);

            if (ownerId == this.props.userId || winnerId == this.props.userId) {
                formComment  = this.createWinPublish();
                showComments = this.displayWinComments();
            }
        }

        let buttons =   <div>
                            <div className="row">
                                <div className="center_text_aling">
                                    {buttonsValidate}
                                </div>
                                {formComment}
                                {showComments}
                            </div>
                            <div>&nbsp;</div>
                        </div>;

        let templateNoLogin =   <div className="ui warning message center_text_aling">
                                    <h4>
                                        Please, before do a comment or make a bid, <a href={this.state.windowPath + "/login"}>
                                        login here</a> or create an <a href={this.state.windowPath + "/register"}>account here</a>!
                                    </h4>
                                </div>;

        let finalTemplate = (this.props.userId != '0') ? buttons : templateNoLogin;

        return finalTemplate;
    }

    displaySliders = () => {
        let sliderBody =    <div id="gallery" className="gallery loaded">
                                {
                                    this.state.post.image.map((attachment) => {
                                        let kindFormat = '';
                                        if (attachment.isImage == 1) {
                                            kindFormat = <img   alt={attachment.name}
                                                                key={attachment.id}
                                                                src={this.state.windowPath + attachment.path}
                                                                data-image={this.state.windowPath + attachment.path}
                                                                data-description="" />

                                        } else {
                                            if (attachment.videoType == 'youtube') {
                                                kindFormat = <img   alt=""
                                                                    key={attachment.id}
                                                                    data-type="youtube"
                                                                    data-videoid={attachment.idVideo}
                                                                    data-description="" />
                                            } else {
                                                kindFormat = <img alt="Html5 Video"
			                                                      src={this.state.windowPath + attachment.path}
                                                                  key={attachment.id}
			                                                      data-type="html5video"
			                                                      data-image=""
                                                                  data-videoogv={this.state.windowPath + attachment.path}
                                                                  data-videowebm={this.state.windowPath + attachment.path}
                                                                  data-videomp4={this.state.windowPath + attachment.path}
                                                                  data-description="" />

                                            }
                                        }

                                        return (
                                            kindFormat
                                        )
                                    })
                                }
                            </div>;

        return sliderBody;
    }

    componentDidUpdate(prevProps, prevState) {
        // validate the load of slider library
        if ($("#gallery").length && $("#gallery").hasClass('loaded')) {
            jQuery("#gallery").unitegallery({
                gallery_theme:            "grid",
                theme_panel_position:     "bottom",
                gallery_width:            "auto",
                gallery_autoplay:         true,
                gallery_preserve_ratio:   true,
                slider_enable_zoom_panel: true,
                slider_scale_mode:        "fit",
            });

            $("#gallery").removeClass('loaded');
        }

        if (this.state.loader == 1) {
            this.changeFlagState(1);
        }
    }

    render() {
        let modalInfo, modal= [];
        this.flagData = 1;

        if (this.state.post == undefined || this.state.post == null || this.state.validatingPosts == 1
            || this.state.validatingBids == 1 || this.state.validatingWinners == 1 || this.state.validatingComments == 1) {
            return ('');
        } else {

            // display modals
            if (this.state.reportFlag == 1) {
                document.getElementById('overlay').style.display = "block";
                modalInfo = this.reportAction();
            } else if (this.state.reportBids == 1) {
                document.getElementById('overlay').style.display = "block";
                modalInfo = this.bidsAction();
            } else if (this.state.reportReview == 1) {
                document.getElementById('overlay').style.display = "block";
                modalInfo = this.reviewBidsAction();
            } else if (this.state.reportInner == 1) {
                document.getElementById('overlay').style.display = "block";
                modalInfo = this.innerComments();
            } else {
                if (this.state.reportRedirectFlag == 1) {
                    document.getElementById('overlay').style.display = "block";
                    modalInfo = this.redirectLoader();

                    // Reload the information once accept winner bid
                    window.location = this.state.linkUrl;
                } else {
                    document.getElementById('overlay').style.display = "none";
                    modalInfo = '';
                }
            }

            // Once the bid has been accepted or is pending
            let titleMessage = "";
            if (this.state.post.status == 0) {
                if (this.state.post.bids.statusBid == 3) {
                    titleMessage =  <div className="ui warning message">
                                        <div className="header">
                                            <i className="exclamation circle icon"></i>
                                            Payment PENDING
                                        </div>
                                        Project as finished. Payment is PENDING to do.
                                    </div>;
                } else {
                    titleMessage =  <div className="ui positive message">
                                        <div className="header">
                                            <i className="check circle icon"></i>
                                            Bid ACCEPTED
                                        </div>
                                        Project has finished. A Bid has been ACCEPTED.
                                    </div>;
                }
            }

            modal.push(modalInfo);
            let winner = (this.state.post.status == 0) ? <div className="five wide column">
                                                            <h2 className="ui header">
                                                                $ {this.state.post.bids.bid}
                                                            </h2>
                                                         </div> : "";

            let templateWinner = '';
            if (this.state.post.status == 0) {
                templateWinner = <div className="winner_color">
                                    <div>
                                        Winner: {this.state.post.bids.userName}
                                    </div>
                                    <div>
                                        Bid: $ {this.state.post.bids.bid}
                                    </div>
                                 </div>;
            }

            return (
                <div>
                    {modal}
                    {titleMessage}

                    <div className="row">
                        <div className="eleven left aligned wide column">
                            <h2 className="ui header">
                                {this.state.post.postname}
                            </h2>
                        </div>
                        {winner}
                    </div>
                    <div className="row">
                        <div className="sixteen wide column">
                            { this.displaySliders() }
                        </div>
                    </div>
                    <div className="ui large header">
                        Description
                    </div>
                    <div className="ui stacked segment">
                        { this.state.post.description }
                    </div>
                    <div className="ui large header">
                        Project Details
                    </div>
                    <div className="ui stacked segment">
                        <div>
                            Published by: { this.state.post.publishedBy }
                        </div>
                        <div>
                            Category: { this.state.post.category }
                        </div>
                        <div>
                            City: { this.state.post.city }
                        </div>
                        {templateWinner}
                    </div>

                    {this.loginNoLogin()}
                </div>
            );
        }
    }
}
