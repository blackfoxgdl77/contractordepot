// Const for modal
const display = {
    display: 'block'
};
const hide = {
    display: 'none'
};

class PostsSearch extends React.Component {

	constructor(props) {
        super(props);

		this.toggleBind        = this.toggleBind.bind(this);
		this.reportFake        = this.reportFake.bind(this);
		this.reportProject     = this.reportProject.bind(this);
        this.closeClickE       = this.closeClickE.bind(this);
        this.closeClickSuccess = this.closeClickSuccess.bind(this);
        this.createSearchForm  = this.createSearchForm.bind(this);
        this.showSearchForm    = this.showSearchForm.bind(this);
        this.hideForm          = this.hideForm.bind(this);
        this.searchRecords     = this.searchRecords.bind(this);
        this.handleInput       = this.handleInput.bind(this);
        this.displayProjects   = this.displayProjects.bind(this);
        this.displayAllJobs    = this.displayAllJobs.bind(this);
        this.filterByCategory  = this.filterByCategory.bind(this);
        this.noPostsFunction   = this.noPostsFunction.bind(this);
        this.changeFlagState   = this.changeFlagState.bind(this);

        this.state = {
            jobs           : [],
            showForm       : false,
            buttonFormShow : true,
			toggle         : false,
            successMsj     : '',
            successEnable  : 0,
            errorMessage   : '',
            errorEnable    : 0,
			fake           : {},
            totalRows      : 0,
            clearValue     : 0,
            token          : props.token,
            source         : props.source,
            nameToken      : props.nameT,
            windowPath     : (window.$PATH !== undefined) ? window.$PATH : ""
        };
    }

    changeFlagState = (flag) => {
        if (flag == 1) {
            setTimeout(function () {
                $("#overlay_loader").css("display", "none");
            }, 3000);

            this.setState({
                loader : 0,
            });
        }
    }

    componentDidUpdate = () => {
        if (this.state.loader == 1) {
            this.changeFlagState(1);
        }
    }

    handleInput(data) {
        if (data.label != '') {
            this.setState({
                [data.label]: data.value
            });
        }
    }

    showSearchForm = (evt) => {
        evt.preventDefault();

        this.setState({
            showForm       : true,
            buttonFormShow : false
        });
    }

    hideForm = (evt) => {
        evt.preventDefault();

        this.setState({
            buttonFormShow : true,
            showForm       : false
        });
    }

    toggleBind = (data, evt) => {
        evt.preventDefault();

        this.setState(prevState => ({
            toggle        : !prevState.toggle,
            fake          : data,
            errorEnable   : 0,
            errorMessage  : '',
            successEnable : 0,
            successMsj    : '',
        }));
    }

    closeClickE = () => {
        this.setState({
            errorEnable  : 0,
            errorMessage : '',
        });
    }

    closeClickSuccess = () => {
        this.setState({
            successMsj    : '',
            successEnable : 0,
        });
    }

    displayAllJobs = () => {
        let jobsLists = this.state.jobs.map((job) => {
            return <div key={job.id_job} className="two column row ">
                        <div className="left thin_font blue_color">
                            <a href="#" onClick={(e) => this.filterByCategory(job, e)}>
                                {job.name_job} ({job.total_jobs})
                            </a>
                        </div>
                   </div>
        });

        return jobsLists;
    }

    filterByCategory = (job, e) => {
        e.preventDefault();
        let url = this.state.windowPath + '/api/job/category/' + ( job == null ? 0 : job.id_job);

        axios.get(url)
             .then((response) => {
                 if (response.status == 200 && response.data.status == 'OK') {
                     this.setState({
                         posts     : response.data.data.data,
                         totalRows : response.data.data.total,
                     });
                 }

                 if (response.status == 200 && response.data.status == 'ERROR') {
                     this.setState({
                         errorEnable  : 1,
                         errorMessage : response.data.data.message,
                     });
                 }
             })
             .catch((error) => {
                 this.setState({
                     errorEnable  : 1,
                     errorMessage : response.data.data.message,
                 });
             });
    }

    reportFake = () => {
		let styleData       = (this.state.toggle == true) ? display : hide;
        let errorTemplate   = (this.state.errorEnable == 1) ? <Errors errors="" respError={this.state.errorMessage}
                                                                      onClick={this.closeClickE} /> : "";
        let successTemplate = (this.state.successEnable == 1) ? <SuccessTemplate successTitle={this.state.successMsj}
                                                                  successMessage="" extraFlag="0" onClick={this.closeClickSuccess} /> : "";

        let modalData = <div key={this.state.fake.id}>
                            <div className="modal margin_modal_react" key={this.state.fake.id} style={styleData}>
                                <div className="modal-content">
                                    <div>
                                        {errorTemplate}
                                        {successTemplate}
                                    </div>
                                    <div className="col s5 left thin_font blue_color">
                                        Post > Search > Report Project
                                    </div>
                                    <div className="col s7">
                                        <h3 className="right-align thin_font blue_color">
                                            Report Project
                                        </h3>
                                    </div>
                                    <div className="justify_align thin_font">
                                        Are you sure you want to report the projects <b>'{this.state.fake.postname}'</b> as fake?
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <a href="#!" className="btn modal-action modal-close waves-effect waves-green btn-flat" type="button"
                                       onClick={(e) => this.reportProject(e)}>
                                        Report the Project!
                                    </a>
                                    <a href="#!" className="btn modal-action modal-close waves-effect waves-green btn-flat" type="button"
                                       onClick={(e) => this.toggleBind(0, e)}>
                                        Close
                                    </a>
                                </div>
                            </div>
                        </div>;

		return modalData;
	}

	reportProject = (evt) => {
		evt.preventDefault();

        let url = this.state.windowPath + "/api/post/fake/project";
        let postData = {
            projectId : this.state.fake.id,
            userId    : this.props.userId,
            source    : this.state.source,
            token     : this.state.token,
            nameT     : this.state.nameToken,
        };

        axios.post(url, postData)
             .then((response) => {
                 if (response.data.status == 'OK' && response.status == 200) {
                     this.setState({
                         successEnable : 1,
                         successMsj    : response.data.data.message,
                     });
                 }

                 if (response.data.status == 'ERROR' && response.status == 200) {
                     this.setState({
                         errorEnable  : 1,
                         errorMessage : response.data.data.message,
                     });
                 }
             })
             .catch((error) => {
                 this.setState({
                     errorEnable  : 1,
                     errorMessage : error.data.data.message,
                 });
             });
	}

    createSearchForm = () => {
        if (this.state.cities == undefined || this.state.cities == null ||
            this.state.jobCategories == undefined || this.state.jobCategories == null ) {
            return (
                <h5></h5>
            );
        } else if (this.state.cities.length == 0) {
            return (
                <h5>There are not Cities!</h5>
            );
        } else if (this.state.jobCategories.length == 0) {
            return (
                <h5>There are not Categories!</h5>
            );
        } else {

            return (
                <form className="ui column form thin_font">
                    
                    <div className="column row field">
                        <Input name="postname" disable="false" labelName="Postname" iconTags="create"
                            handleFormFields={this.handleInput} defaultText="false" require="true" id="postname"
                            classData="ui left input" isEmpty={this.state.clearValue}
                            ref={(postname) => { this.postname = postname }} />
                    </div>

                    <div className="column row field">
                        <Select name="city" disable="false" labelSelectName="City" iconTags="format_list_numbered"
                            require="true" handleFormFields={this.handleInput} defaultText="false"
                            classData="" displayDataValues={this.state.cities} id="cityID"
                            ref={(city) => { this.city = city }} />
                    </div>

                    <div className="column row field">
                        <Select name="category" disable="false" labelSelectName="Category"
                            iconTags="format_list_numbered" require="true"
                            handleFormFields={this.handleInput} defaultText="false"
                            classData="" displayDataValues={this.state.jobCategories} id="categoryID"
                            ref={(category) => { this.category = category }} />
                    </div>

                    <div className="four column row">
                        <div className="column"></div>
                        <div className="column"></div>
                        <div className="column"></div>
                        <div className="column">
                            <div className="field">
                                <button className="ui fluid teal button right" type="submit" onClick={this.searchRecords}>
                                    Search
                                </button>
                            </div>
                        </div>
                    </div>

                </form>
            );
        }
    }

    searchRecords = (evt) => {

        evt.preventDefault();

        let url = this.state.windowPath + '/api/posts';
        let queryParams = "";

        if (this.state.postname != undefined && this.state.postname != null && this.state.postname != "") {
            queryParams = queryParams + "postname=" + this.state.postname;
        }
        if (this.state.category != undefined && this.state.category != null && this.state.category != "") {
            if (queryParams != "") {
                queryParams = queryParams + "&";
            }
            queryParams = queryParams + "category=" + this.state.category;
        }
        if (this.state.city != undefined && this.state.city != null && this.state.city != "") {
            if (queryParams != "") {
                queryParams = queryParams + "&";
            }
            queryParams = queryParams + "city=" + this.state.city;
        }

        if (queryParams != "") {
            queryParams = "?" + queryParams;
            url = url + queryParams;
        }

        // Retrieving posts information
        axios.get(url)
            .then((response) => {
                this.setState({
                    posts      : response.data.data,
                    clearValue : 1,
                });

            }).catch((error) => {
                console.log("Error: " + error);
            });

    }

    componentWillMount() {

    	let citiesRetrieveAllUrl     = this.state.windowPath + '/api/cities';
    	let categoriesRetrieveAllUrl = this.state.windowPath + '/api/jobCategories';
    	let postRetrieveAllUrl       = this.state.windowPath + '/api/posts';
        let jobsCategories           = this.state.windowPath + '/api/job/categories/per/post';

    	// Retrieving cities
        axios.get(citiesRetrieveAllUrl)
            .then((response) => {
                if (response.status == 200 && response.data.status == 'OK') {
                    this.setState({
                        cities : response.data.data
                    });
                }

                if (response.status == 200 && response.data.status == 'ERROR') {
                    this.setState({
                        errorEnable  : 1,
                        errorMessage : response.data.data.message,
                    });
                }
            }).catch((error) => {
                this.setState({
                    errorEnable  : 1,
                    errorMessage : error.data.data.message,
                });
            });

        // Retrieving job categories
        axios.get(categoriesRetrieveAllUrl)
            .then((response) => {
                if (response.data.status == 'OK' && response.status == 200) {
                    this.setState({
                        jobCategories : response.data.data
                    });
                }

                if (response.status == 200 && response.data.status == 'ERROR') {
                    this.setState({
                        errorEnable  : 1,
                        errorMessage : response.data.data.message,
                    });
                }
            }).catch((error) => {
                this.setState({
                    errorEnable  : 1,
                    errorMessage : error.data.data.message,
                });
            });

        // Retrieving posts information
        axios.get(postRetrieveAllUrl)
            .then((response) => {
                if (response.status == 200 && response.data.status == 'OK') {
                    this.setState({
                        posts : response.data.data
                    });
                }

                if (response.status == 200 && response.data.status == 'ERROR') {
                    this.setState({
                        errorEnable  : 1,
                        errorMessage : response.data.data.message,
                    });
                }

            }).catch((error) => {
                this.setState({
                    errorEnable  : 1,
                    errorMessage : error.data.data.message,
                });
            });

        // Retrieving 
        axios.get(jobsCategories)
             .then((response) => {
                 if (response.status == 200 && response.data.status == 'OK') {
                     this.setState({
                         jobs : response.data.data.jobs,
                     });
                 }

                 if (response.status == 200 && response.data.status == 'ERROR') {
                     this.setState({
                         errorEnable  : 1,
                         errorMessage : response.data.data.message,
                     });
                 }
             })
             .catch((error) => {
                 this.setState({
                     errorEnable  : 1,
                     errorMessage : error.data.data.message,
                 });
             });

        // hide loader
        $("#overlay_loader").css("display", "none");
    }

    displayProjects = () => {
        let postsData = this.state.posts.map((project) => {
            let reportButton = '';
            if (this.props.userId != 0 || this.props.userId != '0') {
                reportButton = <a href="#" onClick={(e) => this.toggleBind(project, e) }>
                                  Report Project
                               </a>;
            }

            return <div className="item" key={project.id}>
                        <div className="image">
                            <img className="activator" src={this.state.windowPath + project.image} />
                        </div>
                        <div className="content">
                            <a className="header">
                                {project.postname}
                            </a>
                            <div className="meta">
                                <span className="cinema">
                                    {project.category}
                                </span>
                            </div>
                            <div className="description">
                                <span>
                                    { project.description }
                                </span>
                            </div>
                            <div className="extra">
                                <a href={this.state.windowPath + "/projects/show/" + project.id}
                                   className="ui right floated primary button links_hover_blue thin_font">
                                    Project Details
                                    <i className="right chevron icon"></i>
                                </a>
                                <div className="ui label">
                                    <i className="globe icon"></i>
                                    { project.city }
                                </div>
                            </div>
                        </div>
                    </div>
                });

        return postsData;
    }

    noPostsFunction = () => {
        let templateNoPosts = <div className="color_text_no_data center gross_font">
                                <h3> There are no projects for this category!</h3>
                                <br />
                                <h5>Please search projects in another category</h5>
                              </div>;

        return templateNoPosts;
    }

    // TO-DO: Mejorar esta function hacerla o intentar hacerla mas mantenible
	render() {
		let modalInfo, modal = [];

		if (this.state.posts == undefined || this.state.posts == null) {
            return (
                <h5></h5>
            );
        } else if (this.state.posts.length == 0) {
            return (
                <h5>There are not posts loaded!</h5>
            );
        } else {
			if (this.state.toggle == true) {
				document.getElementById('overlay').style.display = "block";
				modalInfo = this.reportFake();
			} else {
				document.getElementById('overlay').style.display = "none";
				modalInfo = '';
			}
			modal.push(modalInfo);

            let buttonForm = "";
            let showForm = "";

            if (this.state.showForm == true) {
                showForm = this.createSearchForm();
            }

            // Verify if the button that will be displayed is for show or hide the form
            if (this.state.buttonFormShow == true) {
                buttonForm = <div className="column">
                                <div className="field">
                                    <button className="ui fluid teal button right" onClick={this.showSearchForm}>
                                        Open Search
                                    </button>
                                </div>                                
                            </div>; 
            } else {
                buttonForm = <div className="column">
                                <div className="field">
                                    <button className="ui fluid orange button right" onClick={this.hideForm}>
                                        Hide Search
                                    </button>
                                </div>                                
                            </div>;
            }

            // data to display
            let templateDatas = (this.state.posts.length == 0) ? this.noPostsFunction() : this.displayProjects();

			return (
				<div className="ui grid">
					{modal}

                    <div className="row">&nbsp;</div>

                    <div className="four column row">
                        <div className="column"></div>
                        <div className="column"></div>
                        <div className="column"></div>
                        {buttonForm}
                    </div>

                    <div className="one column row">
                        {showForm}
                    </div>

                    <div className="sixteen column row">
                        
                        <div className="three column">
                            <div className="row left gross_font blue_color">
                                Filter by Category
                            </div>
                            <div key="0" className="two column row ">
                                <div className="left thin_font blue_color">
                                    <a href="#" onClick={(e) => this.filterByCategory(null, e)}>
                                        View All
                                    </a>
                                </div>
                           </div>
                            
                            {this.displayAllJobs()}
                            
                        </div>

                        <div className="column">&nbsp;</div>

                        <div className="twelve wide column">
                            <div className="ui divided items">
                                {templateDatas}
                            </div>
                        </div>
                    </div>
				</div>

			);
		}
	}
}
