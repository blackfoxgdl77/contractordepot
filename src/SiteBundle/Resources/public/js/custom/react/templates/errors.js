/**
 * Template will be used to display the messages
 * once the user has done some things wrong or
 * inclusive the systems returns an error in every
 * action done by it
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */
class Errors extends React.Component {
    constructor(props) {
        super(props);

        this.closeClick = this.closeClick.bind(this);
        this.getObject  = this.getObject.bind(this);
        this.listMaps   = this.listMaps.bind(this);

        this.state = {
            windowPath : (window.$PATH !== undefined) ? window.$PATH : ""
        };
    }

    componentDidMount = () => {
        $('.message .close').on('click', function() {
            $(this).closest('.message').transition('fade');
        });
    }

    componentDidUpdate = () => {

    }

    closeClick = () => {
        this.props.onClick();
    }

    getObject = () => {
        let options = [];

        for (let key in this.props.variables) {
            if (this.props.variables.hasOwnProperty(key)) {
                if (this.props.variables[key] != '') {
                    options.push({
                        label: key,
                        value: this.props.variables[key]
                    });
                }
            }
        }

        return options;
    }

    listMaps = (options) => {
        let listMaps = options.map((errors, index) =>
            <li key={errors.label}>
                {errors.value}
            </li>
        );

        return listMaps;
    }

    render() {

        let bodyTemplateData = '';

        if (this.props.variables) {
            let objectData = this.getObject();
            let listMaps   = this.listMaps(objectData);

            bodyTemplateData =  <ul className="list">
                                    <li>{this.props.bodyError}</li>
                                    {listMaps}
                                </ul>;
        } else {
            bodyTemplateData = this.props.bodyError;
        }

        return (
            <div className="ui negative message">
                <i className="close icon" onClick={this.closeClick}></i>
                <div className="header">
                    <i className="exclamation circle icon"></i>
                    {this.props.headerError}
                </div>
                {bodyTemplateData}
            </div>
        );
    }
}
