/**
 * Template will be used to create the
 * success message with all the data is going
 * to be displayed to the user once the action
 * was executed successfully
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */
class SuccessTemplate extends React.Component {
    constructor(props) {
        super(props);

        this.closeClickSuccess = this.closeClickSuccess.bind(this);

        this.state = {
            windowPath : (window.$PATH !== undefined) ? window.$PATH : ""
        };
    }

    componentDidMount = () => {
        $('.message .close').on('click', function() {
            $(this).closest('.message').transition('fade');
        });
    }

    componentDidUpdate = () => {

    }

    closeClickSuccess = () => {
        this.props.onClick();
    }

    render() {
        return (
            <div className="ui positive message">
                <i className="close icon"></i>
                <div className="header">
                    <i className="check circle icon"></i>
                    {this.props.headerSuccess}
                </div>
                <p>{this.props.bodySuccess}</p>
            </div>
        );
    }
}
