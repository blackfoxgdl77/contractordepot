/**
 * Class will contain the information for display all the data
 * related to the users and the offers done in the projects
 * that has finished
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */

// Const for modal
const display = {
    display: 'block'
};
const hide = {
    display: 'none'
};

class EndBids extends React.Component {
    constructor(props) {
        super(props);

        this.templateBidsEnded = this.templateBidsEnded.bind(this);
        this.setModalUsers     = this.setModalUsers.bind(this);
        this.setTemplateModal  = this.setTemplateModal.bind(this);
        this.closeModal        = this.closeModal.bind(this);
        this.closeClick        = this.closeClick.bind(this);
        this.showModal         = this.showModal.bind(this);
        this.hideModal         = this.hideModal.bind(this);

        this.state = {
            endOffers      : [],
            flagNoBids     : false,
            noBidsMessage  : '',
            dataUsers      : [],
            modalUsers     : false,
            loading        : true,
            messageError   : '',
            flagError      : 0,
            messageSuccess : '',
            flagSuccess    : 0,
            show           : false,
            windowPath     : (window.$PATH !== undefined) ? window.$PATH : "",
        };
    }

    componentDidMount() {
        let url = this.state.windowPath + "/api/get/ended/bids/" + this.props.userId;

        axios.get(url)
             .then((response) => {
                 if (response.data.data.counter != 0) {
                     if (response.status == 200 && response.data.status == 'OK') {
                         this.setState({
                             endOffers : response.data.data.data,
                             loading   : false,
                         });
                     }

                     if (response.status == 200 && response.data.status == 'ERROR') {
                         this.setState({
                             messageError : response.data.data.message,
                             flagError    : 1,
                             loading      : false,
                         });
                     }
                 } else {
                     this.setState({
                         noBidsMessage : "Currently, You don't have bids finished",
                         flagNoBids    : true,
                         loading       : false,
                     });
                 }

                 // hide loader
                 $("#overlay_loader").css("display", "none");
             })
             .catch((error) => {
                 this.setState({
                     flagError    : 1,
                     messageError : error.data.data.message
                 });

                 // hide loader
                 $("#overlay_loader").css("display", "none");
             });
    }

    showModal = (data) => {
        this.setState({
            show      : true,
            dataUsers : data,
        });
    }

    hideModal = () => {
        this.setState({
            show      : false,
            dataUsers : {},
        });
    }

    closeClick = () => {
        this.setState({
            flagError    : 0,
            messageError : "",
        });
    }

    setTemplateModal = () => {
        //let styleDisplay = (this.state.modalUsers == true) ? display : hide;
        const showHideClassName = this.state.show ? 'modal display-block' : 'modal display-none';
        let collectionUsers = this.state.dataUsers.users.map((users) =>
            <div className="ui relaxed divided list" key={users.id_user}>
                <div className="item">
                    <i className="large dollar sign middle aligned icon"></i>
                    <div className="content">
                        <div className="header">Username: {users.username}</div>
                        <div className="description">
                            <div>
                                Bidder: {users.moneyBidder}
                            </div>
                            <div>
                                Status: {users.status}
                            </div>
                            <div>
                                Date of Bid: {users.date}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );

        // (e) => this.setModalUsers('', e)
        let template =  <div className={showHideClassName} key={this.state.dataUsers.id_offer}>
                            <div className="modal-main">
                                <div className="ui segment">
                                    <div className="ui block header gross_font blue_color">
                                        End Bids - List of Bids
                                    </div>

                                    <div className="sixteen wide column">
                                        <div className="scrolling content">
                                            {collectionUsers}
                                        </div>
                                    </div>

                                    <div>
                                        <div className="negative ui right floated button" onClick={this.hideModal}>
                                            <i className="window close icon"></i>
                                            Close
                                        </div>
                                        <div className="clear_floating"></div>
                                    </div>
                                </div>
                            </div>
                        </div>;

        return template;
    }

    // deprecated
    setModalUsers = (data, e) => {
        e.preventDefault();

        this.setState(prevState => ({
            modalUsers : !prevState.modalUsers,
            dataUsers  : data,
        }));
    }

    // deprecated
    closeModal = () => {
    }

    templateBidsEnded = () => {
        let mainTemplate = '';
        // (e) => this.setModalUsers(offers, e)
        if (this.state.flagNoBids == false) {
            let template = this.state.endOffers.map((offers) => {
                let buttonProject = '';
                if (offers.is_winner == 1) {
                    buttonProject = <a className="ui teal button" href={this.state.windowPath + "/post/" + offers.id_post}>
                                        <i className="eye icon"></i> Go To Project
                                    </a>;
                }

                return (
                    <div className="item" key={offers.id_offer}>
                        <div className="image">
                            <img src="" />
                        </div>
                        <div className="content">
                            <a className="header">
                                {offers.postName}
                            </a>
                            <div className="meta">
                                <span className="cinema">
                                    {offers.category}
                                </span>
                            </div>
                            <div className="description">
                                <p>
                                    {offers.description}
                                </p>
                            </div>
                        </div>
                        <div className="extra">
                            {buttonProject}
                            <a className="ui teal button" onClick={() => this.showModal(offers)}>
                                <i className="list icon"></i> Bids Users
                            </a>
                            <div className="ui label">{offers.status}</div>
                            <div className="ui label">
                                <i className="globe icon"></i> {offers.city}
                            </div>
                        </div>
                    </div>
                )
            });

            mainTemplate =  <div className="ui divided items">
                                {template}
                            </div>;
        } else {
            mainTemplate =  <div className="ui warning message">
                                <div className="header">
                                    <i className="exclamation triangle icon"></i>
                                    End Bids
                                </div>
                                {this.state.noBidsMessage}
                            </div>;
        }

        return mainTemplate;
    }

    render() {
        let modal = [], modalInfo, collectionData;
        let errorMsg = (this.state.flagError == 1) ? <Errors headerError="Problems with Load Data"
                                                             bodyError={this.state.messageError}
                                                             onClick={this.closeClick} /> : "";

        if (this.state.loading == false) {
            collectionData = this.templateBidsEnded();
        }

        if (this.state.modalUsers == true) {
            //document.getElementById('overlay').style.display = 'block';
            modalInfo = this.setTemplateModal();
        } else {
            //document.getElementById('overlay').style.display = 'none';
            modalInfo = '';
        }

        modal.push(modalInfo);

        return (
            <div>
                {errorMsg}
                {modal}
                <div className="row">
                    {collectionData}
                </div>
            </div>
        );
    }
}
