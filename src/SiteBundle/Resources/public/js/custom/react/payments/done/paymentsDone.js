/**
 * Class that contains all the information
 * about the projects has been paid by the owner
 * and the money is in the payment platform
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */
// Const for modal
const display = {
 display: 'block'
};
const hide = {
 display: 'none'
};

class ProjectsDone extends React.Component {
    constructor (props) {
        super(props);

        this.displayRecords     = this.displayRecords.bind(this);
        this.displayInformation = this.displayInformation.bind(this);
        this.toggle             = this.toggle.bind(this);
        this.closeClickE        = this.closeClickE.bind(this);
        this.showModal          = this.showModal.bind(this);
        this.hideModal          = this.hideModal.bind(this);

        this.state = {
            token        : props.token,
            source       : props.source,
            nameToken    : props.name,
            errorMessage : 0,
            toggle       : false,
            respMessage  : '',
            loading      : true,
            show         : false,
            windowPath   : (window.$PATH !== undefined) ? window.$PATH : "",
        };
    }

    componentDidMount = () => {
        let url = this.state.windowPath + "/api/get/projects/payment/done";

        axios.get(url)
             .then((response) => {
                 if (response.status == 200 && response.data.status == 'OK') {
                     this.setState({
                         records : response.data.data.data,
                         loading : false,
                     });
                 }

                 if (response.data.status == 'ERROR' && response.status == 200) {
                     this.setState({
                         errorMessage : 1,
                         respMessage  : response.data.data.message,
                     });
                 }

                 // hide loader
                 $("#overlay_loader").css("display", "none");
             })
             .catch((error) => {
                 this.setState({
                     errorMessage : 1,
                     respMessage  : error.data.data.message,
                 });

                 // hide loader
                 $("#overlay_loader").css("display", "none");
             });
    }

    closeClickE = () => {
        this.setState({
            errorMessage : 0,
            respMessage  : '',
        });
    }

    // deprecated
    toggle = (data, evt) => {
        evt.preventDefault();

        this.setState(prevState => ({
            toggle    : !prevState.toggle,
            modalData : data,
        }));
    }

    displayInformation = () => {
        //let styleData    = (this.state.toggle == true) ? display : hide;
        // (e) => this.toggle('', e)
        const showHideClassName = this.state.show ? 'modal display-block' : 'modal display-none';
        let contentModal =  <div className={showHideClassName} key={this.state.modalData.idPost}>
                                <div className="modal-main">
                                    <div className="ui segment">

                                        <div className="ui block header gross_font blue_color">
                                            Payments Done - List of Payments Done
                                        </div>

                                        <div className="sixteen wide column">
                                            <div className="scrolling content">
                                                <div className="description">
                                                    <div>
                                                        Id Payment: {this.state.modalData.idPayment}
                                                    </div>
                                                    <div>
                                                        Post Name: {this.state.modalData.namePost}
                                                    </div>
                                                    <div>
                                                        Description: {this.state.modalData.postDesc}
                                                    </div>
                                                    <div>
                                                        Username: {this.state.modalData.username}
                                                    </div>
                                                    <div>
                                                        Owner: {this.state.modalData.owner}
                                                    </div>
                                                    <div>
                                                        Email: {this.state.modalData.emailO}
                                                    </div>
                                                    <div>
                                                        Category: {this.state.modalData.category}
                                                    </div>
                                                    <div>
                                                        City: {this.state.modalData.cityName}
                                                    </div>
                                                    <div>
                                                        Capital: {this.state.modalData.capital}
                                                    </div>
                                                    <div>
                                                        Date Created: {this.state.modalData.dateCrt}
                                                    </div>
                                                    <div>
                                                        Date Updated: {this.state.modalData.dateUpt}
                                                    </div>
                                                    <div>
                                                        Username Winner: {this.state.modalData.userWin}
                                                    </div>
                                                    <div>
                                                        Amount: {this.state.modalData.amount}
                                                    </div>
                                                    <div>
                                                        Winner Name: {this.state.modalData.userNameW}
                                                    </div>
                                                    <div>
                                                        Winner Email: {this.state.modalData.userEmail}
                                                    </div>
                                                    <div>
                                                        Transaction Status: {this.state.modalData.statusT}
                                                    </div>
                                                    <div>
                                                        ID Transaction: {this.state.modalData.idTrans}
                                                    </div>
                                                    <div>
                                                        Date Paid Done: {this.state.modalData.datePaid}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="ui divider"></div>
                                        </div>

                                        <div>
                                            <div className="negative ui right floated button" onClick={this.hideModal}>
                                                <i className="window close icon"></i>
                                                Close
                                            </div>
                                            <div className="clear_floating"></div>
                                        </div>

                                    </div>
                                </div>
                            </div>;

        return contentModal;
    }

    displayRecords = () => {
        let bodyContent, tableContent;
        // (evt) => this.toggle(info, evt)

        bodyContent = this.state.records.map((info) =>
            <div className="item" key={info.idPost}>
                <div className="ui small image">
                    <img src="" />
                </div>
                <div className="middle aligned content">
                    <div className="header">
                        {info.namePost}
                    </div>
                    <div className="description">
                        <div>
                            Amount: {info.amount}
                        </div>
                        <div>
                            Status: COMPLETE
                        </div>
                        <div>
                            Date Created: {info.dateCrt}
                        </div>
                        <div>
                            Date Updated: {info.dateUpt}
                        </div>
                    </div>
                    <div className="extra">
                        <a className="ui right floated button" onClick={() => this.showModal(info)}>
                            <i className="file alternate icon"></i>
                            Details
                        </a>
                    </div>
                </div>
            </div>
        );

        tableContent =  <div className="ui divided items">
                            {bodyContent}
                        </div>;

        return tableContent;
    }

    showModal = (data) => {
        this.setState({
            show      : true,
            modalData : data,
        });
    }

    hideModal = () => {
        this.setState({
            show      : false,
            modalData : {},
        });
    }

    render = () => {
        let modalInfo, modal = [], content = '', messageEmpty = '';

        let error = (this.state.errorMessage == 1) ? <Errors headerError="Problems With Load Data"
                                                             bodyError={this.state.respMessage}
                                                             onClick={this.closeClick} /> : "";

        if (this.state.loading == false) {
            if (this.state.records.length == 0) {
                messageEmpty =  <div className="ui warning message">
                                    <div className="header">
                                        <i className="exclamation triangle icon"></i>
                                        No Paid Projects
                                    </div>
                                    Currently you do not have projects paid.
                                </div>;
            }

            content = this.displayRecords();
        }

        if (this.state.show == true) {
            //document.getElementById('overlay').style.display = "block";
            modalInfo = this.displayInformation();
        } else {
            //document.getElementById('overlay').style.display = "none";
            modalInfo = '';
        }

        modal.push(modalInfo);

        return <div>
                    {error}
                    {modal}
                    <div>
                        { messageEmpty }
                        { content }
                    </div>
               </div>;
    }
}
