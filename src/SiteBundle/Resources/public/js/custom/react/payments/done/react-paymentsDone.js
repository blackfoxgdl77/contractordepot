ReactDOM.render(
    <ProjectsDone token={ document.getElementById('_csrf_token').value }
                  source={ document.getElementById('source').value }
                  name={ document.getElementById('name').value } />,
    document.getElementById('app')
);
