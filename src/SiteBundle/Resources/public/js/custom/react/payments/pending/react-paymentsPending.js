ReactDOM.render(
    <PendingProject name={ document.getElementById('name').value }
                    source={ document.getElementById('source').value }
                    token={ document.getElementById('_csrf_token').value } />,
    document.getElementById('app')
);
