/**
 * Class is going to contain all the information related to the
 * contact form and will be used to create the interface and all
 * the communication between front and back for contacting form
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */
class ContactForm extends React.Component {
    constructor(props) {
        super(props);

        this.submitContactForm = this.submitContactForm.bind(this);
        this.handleInput       = this.handleInput.bind(this);
        this.validateData      = this.validateData.bind(this);
        this.closeClick        = this.closeClick.bind(this);
        this.closeClickE       = this.closeClickE.bind(this);
        this.closeClickSuccess = this.closeClickSuccess.bind(this);
        this.changeFlagState   = this.changeFlagState.bind(this);

        this.state = {
            validating   : 1,
            'token'      : props.token,
            'source'     : props.source,
            errors       : [],
            'counter'    : 0,
            'success'    : 0,
            nameToken    : props.nameT,
            errorMessage : 0,
            respMessage  : '',
            clearValue   : 0,
            loader       : 0,
            windowPath   : (window.$PATH !== undefined) ? window.$PATH : "",
        };
    }

    changeFlagState = (flag) => {
        if (flag == 0) {
            if (this.state.clearValue == 1) {
                this.setState({
                    clearValue : 0,
                });
            }
        }

        if (flag == 1) {
            this.setState({
                loader : 0,
            });

            setTimeout(function () {
                $("#overlay_loader").css('display', 'none');
            }, 3000);
        }
    }

    componentDidUpdate = () => {
        this.changeFlagState(0);

        if (this.state.loader == 1) {
            this.changeFlagState(1);
        }
    }

    componentDidMount = () => {
        // hide loader
        $("#overlay_loader").css("display", "none");
    }

    submitContactForm(evt) {
        evt.preventDefault();

        // Validate information
        let counter = 0;
        counter     = this.validateData();
        $("#overlay_loader").css("display", "inline");

        if (counter === 0) {
            // send data to the users for send email
            let posts = {
                'subject'  : this.state.subject,
                'email'    : this.state.email,
                'comments' : this.state.comments,
                'phone'    : (this.state.phone) ? this.state.phone : '',
                'token'    : this.state.token,
                'nameT'    : this.state.nameToken,
            };
            let url = this.state.windowPath + '/api/post/user/contact/form';

            axios.post(url, JSON.stringify(posts))
                 .then((response) => {

                     if (response.data.status == 'OK' && response.status == 200) {
                         this.setState({
                             clearValue   : 1,
                             respMessage  : '',
                             errorMessage : 0,
                             success      : 1,
                             successM     : response.data.data.message,
                             subject      : '',
                             email        : '',
                             comments     : '',
                             phone        : '',
                             validating   : 0,
                             loader       : 1,
                         });

                         document.getElementById('subject').value  = '';
                         document.getElementById('email').value    = '';
                         document.getElementById('phone').value    = '';
                         document.getElementById('comments').value = '';
                     }

                     if (response.data.status == 'ERROR' && response.status == 200) {
                         this.setState({
                             respMessage  : response.data.data.message,
                             errorMessage : 1,
                             validating   : 0,
                             loader       : 1,
                         });
                     }
                 }).catch((error) => {
                    this.setState({
                        validating   : 0,
                        errorMessage : 1,
                        loader       : 1,
                        respMessage  : error.data.data.message,
                    });
                 });
        }
    }

    validateData = () => {
        let counter = 0;
        let errors  = {};

        this.setState({
          validating   : 1
        });

        if (this.subject.state.value === "" || this.subject.state.value === undefined) {
            errors['subject'] = 'Please, type a subject.';
            $("#subject").parent().parent().addClass('error');
            counter++;
        } else {
            errors['subject'] = "";
            $("#subject").parent().parent().removeClass('error');
        }

        if (this.email.state.value === "" || this.email.state.value === undefined) {
            errors['email'] = 'Please, type an email.';
            $("#email").parent().parent().addClass('error');
            counter++;
        } else {
            errors['email'] = "";
            $("#subject").parent().parent().removeClass('error');
        }

        if (this.comments.state.value === "" || this.comments.state.value === undefined) {
            errors['comments'] = "Please, type a comment.";
            $("#comments").parent().parent().addClass('error');
            counter++;
        } else {
            $("#comments").parent().parent().removeClass('error');
            errors['comments'] = "";
        }

        this.setState({
            errors  : errors,
            counter : counter
        });

        return counter;
    }

    handleInput(data) {
        this.setState({
            [data.label]: data.value
        });
    }

    closeClick = () => {
        this.setState({
            counter      : 0,
            respMessage  : '',
            errorMessage : 0,
        });
    }

    closeClickE = () => {
        this.setState({
            respMessage  : '',
            errorMessage : 0
        });
    }

    closeClickSuccess = () => {
        this.setState({
            success  : 0,
            successM : '',
            subject  : '',
            email    : '',
            comments : '',
            phone    : '',
        });
    }

    render() {
        let contact;
        let errorTemplate   = (this.state.counter != 0) ? <Errors headerError="Error Sending Contact Data"
                                                                  bodyError="All the fields with '*' must be filled in."
                                                                  variables={this.state.errors}
                                                                  onClick={this.closeClick} /> : "";
        let errorMessage    = (this.state.errorMessage != 0) ? <Errors headerError="Error!"
                                                                       bodyError={this.state.respMessage}
                                                                       onClick={this.closeClick} /> : "";
        let successTemplate = (this.state.success != 0) ? <SuccessTemplate headerSuccess="Comment Sent Successfully"
                                                                           bodySuccess={this.state.successM}
                                                                           onClick={this.closeClickSuccess} /> : "";

        contact = <div>
                    {errorTemplate}
                    {successTemplate}


                    <form className="ui large form thin_font">
                        <div className="field">
                            <Input name="subject" disable="false" labelName="Subject" iconTags="user icon" require="true"
                                   handleFormFields={this.handleInput} defaultText="Subject" isEmpty={this.state.clearValue}
                                   classData="ui left icon input" inputClass=""
                                   ref={(subject) => { this.subject = subject; }} />
                        </div>
                        <div className="field">
                            <InputEmail name="email" disable="false" labelEmail="Email" iconTags="envelope icon" require="true"
                                   handleFormFields={this.handleInput} defaultText="Email (john.doe@example.com)"
                                   isEmpty={this.state.clearValue} classData="ui left icon input" inputClass=""
                                   ref={(email) => { this.email = email; }}/>
                        </div>
                        <div className="field">
                            <InputTel name="phone" disable="false" labelName="Phone # (Optional)" iconTags="phone icon"
                                   handleFormFields={this.handleInput} require="false" defaultText="Phone # (Optional)"
                                   isEmpty={this.state.clearValue} classData="ui left icon input" inputClass=""
                                   ref={(phone) => { this.phone = phone; }} />
                        </div>
                        <div className="field">
                            <TextArea name="comments" labelName="Text" iconTags="comment" require="true"
                                   handleFormFields={this.handleInput} defaultText="Text" isEmpty={this.state.clearValue}
                                   classData="" classCustom="" rowCustom=""
                                   ref={(comments) => { this.comments = comments; }} />
                        </div>
                        <div className="field">
                            <button className="ui fluid teal button right" type="submit" onClick={this.submitContactForm}>
                                Send Comments
                            </button>
                        </div>
                    </form>
                  </div>;

        //if (this.state.validating == 1) {
        //    return (
        //        <div className="loader"></div>
        //    );
        //}

        return contact;
    }
}
