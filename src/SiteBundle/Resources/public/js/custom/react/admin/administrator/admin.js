/**
 * Class that contains all the methods and views
 * will be used at the momento to display the section
 * for display all the admin data in the view and giving
 * to admin user the option to update information for users
 * in the site
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */

// Const for modal
const display = {
    display: 'block'
};
const hide = {
    display: 'none'
};

class Admin extends React.Component {
    constructor(props) {
        super(props);

        this.componentDidMount = this.componentDidMount.bind(this);
        this.loadDataBanners   = this.loadDataBanners.bind(this);
        this.loadDataWorks     = this.loadDataWorks.bind(this);
        this.loadDataAboutUs   = this.loadDataAboutUs.bind(this);
        this.loadDataTerms     = this.loadDataTerms.bind(this);
        this.showAddForm       = this.showAddForm.bind(this);
        this.saveNewRecord     = this.saveNewRecord.bind(this);
        this.createAddForm     = this.createAddForm.bind(this);
        this.handleInput       = this.handleInput.bind(this);
        this.validateData      = this.validateData.bind(this);
        this.validateDataEdit  = this.validateDataEdit.bind(this);
        this.changeStatus      = this.changeStatus.bind(this);
        this.deleteRecord      = this.deleteRecord.bind(this);
        this.updateRecord      = this.updateRecord.bind(this);
        this.templateDelete    = this.templateDelete.bind(this);
        this.closeClick        = this.closeClick.bind(this);
        this.closeClickSuccess = this.closeClickSuccess.bind(this);
        this.hideForm          = this.hideForm.bind(this);
        this.toggle            = this.toggle.bind(this);
        this.viewData          = this.viewData.bind(this);
        this.deletedData       = this.deletedData.bind(this);
        this.updateData        = this.updateData.bind(this);
        this.getTitle          = this.getTitle.bind(this);
        this.closeClickE       = this.closeClickE.bind(this);
        this.changeFlagState   = this.changeFlagState.bind(this);
        this.templateModal     = this.templateModal.bind(this);
        this.showModal         = this.showModal.bind(this);
        this.hideModal         = this.hideModal.bind(this);

        this.state = {
            validating        : 0,
            'information'     : [],
            'loading'         : true,
            'successMessage'  : 0,
            successUptMessage : 0,
            errorMessage      : 0,
            errorMessageUpt   : 0,
            'responseMessage' : '',
            'showForm'        : false,
            'messageLoad'     : true,
            errors            : [],
            'counter'         : 0,
            'idDeleted'       : 0,
            buttonFormShow    : true,
            toggle            : false,
            data              : {},
            flagSection       : '',
            loader            : 0,
            token             : props.token,
            nameToken         : props.nameT,
            show              : false,
            windowPath        : (window.$PATH !== undefined) ? window.$PATH : "",
        };
    }

    changeFlagState = () => {
        setTimeout(function () {
            $("#overlay_loader").css("display", "none");
        }, 3000);

        this.setState({
            loader : 0,
        });
    }

    handleInput = (data) => {
        if (data.label != '') {
            this.setState({
                [data.label]: data.value
            });
        }
    }

    loadDataBanners() {
        let bannerContent;

        if (this.state.information[0].Banners.count === 0) {
            bannerContent = <div>
                                There are no information!
                            </div>;
        } else {
            // (e) => this.toggle(banners, 'view', e)
            // (e) => this.toggle(banners, 'update', e)
            // (e) => this.toggle(banners, 'delete', e)
            let dynamicbanner = this.state.information[0].Banners.inner.map((banners) =>
                                <tr key={banners.id}>
                                    <td>
                                        {banners.description}
                                    </td>
                                    <td>
                                        {this.getTitle(banners.section)}
                                    </td>
                                    <td>
                                        {banners.status}
                                    </td>
                                    <td>
                                        <a className="ui teal button" onClick={() => this.showModal('view', banners)}>
                                            <i className="eye icon"></i>
                                            View
                                        </a>
                                        <a className="ui teal button" onClick={() => this.showModal('update', banners)}>
                                            <i className="edit icon"></i>
                                            Edit
                                        </a>
                                        <a className="ui teal button" onClick={() => this.showModal('delete', banners)}>
                                            <i className="trash alternate icon"></i>
                                            Delete
                                        </a>
                                    </td>
                                </tr>
                            );

            bannerContent = <table className="ui selectable celled table">
                                    <thead>
                                        <tr>
                                            <th align="center">Image</th>
                                            <th align="center">Section</th>
                                            <th align="center">Status</th>
                                            <th align="center">Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {dynamicbanner}
                                    </tbody>
                                 </table>;
        }

        return bannerContent;
    }

    loadDataAboutUs() {
            let aboutusContent;

            if (this.state.information[2].AboutUs.count === 0) {
                aboutusContent = <div>
                                    There are no information!
                                 </div>;
            } else {
                // (e) => this.toggle(about, 'view', e)
                // (e) => this.toggle(about, 'update', e)
                // (e) => this.toggle(about, 'delete', e)
                let dynamicbanner = this.state.information[2].AboutUs.inner.map((about) =>
                                    <tr key={about.id}>
                                        <td>
                                            {about.description}
                                        </td>
                                        <td>
                                            {this.getTitle(about.section)}
                                        </td>
                                        <td>
                                            {about.status}
                                        </td>
                                        <td>
                                            <a className="ui teal button" onClick={() => this.showModal('view', about)}>
                                                <i className="eye icon"></i>
                                                View
                                            </a>
                                            <a className="ui teal button" onClick={() => this.showModal('update', about)}>
                                                <i className="edit icon"></i>
                                                Edit
                                            </a>
                                            <a className="ui teal button" onClick={() => this.showModal('delete', about)}>
                                                <i className="trash alternate icon"></i>
                                                Delete
                                            </a>
                                        </td>
                                    </tr>
                                );

                aboutusContent = <table className="ui fixed table">
                                        <thead>
                                            <tr>
                                                <th>Image</th>
                                                <th>Section</th>
                                                <th>Status</th>
                                                <th>Options</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {dynamicbanner}
                                        </tbody>
                                     </table>;
            }

            return aboutusContent;
        }

    loadDataWorks() {
        let worksContent;
        //(e) => this.toggle(works, 'view', e)
        //(e) => this.toggle(works, 'update', e)
        //(e) => this.toggle(works, 'delete', e)
        if (this.state.information[1].HowItWorks.count === 0) {
            worksContent = <div>
                            There are no information!
                           </div>;
        } else {
            let dynamicWorks = this.state.information[1].HowItWorks.inner.map((works) =>
                                <tr key={works.id}>
                                    <td>
                                        {works.description}
                                    </td>
                                    <td>
                                        {this.getTitle(works.section)}
                                    </td>
                                    <td>
                                        {works.status}
                                    </td>
                                    <td>
                                        <a className="ui teal button" onClick={() => this.showModal('view', works)}>
                                            <i className="eye icon"></i>
                                            View
                                        </a>
                                        <a className="ui teal button" onClick={() => this.showModal('update', works)}>
                                            <i className="edit icon"></i>
                                            Edit
                                        </a>
                                        <a className="ui teal button" onClick={() => this.showModal('delete', works)}>
                                            <i className="trash alternate icon"></i>
                                            Delete
                                        </a>
                                    </td>
                                </tr>
                           );

           worksContent = <table className="ui selectable celled table">
                                   <thead>
                                       <tr>
                                           <th>Image</th>
                                           <th>Section</th>
                                           <th>Status</th>
                                           <th>Options</th>
                                       </tr>
                                   </thead>
                                   <tbody>
                                       {dynamicWorks}
                                   </tbody>
                              </table>;
        }

        return worksContent;
    }

    loadDataTerms() {
        let termsContent;

        //(e) => this.toggle(terms, 'view', e)
        //(e) => this.toggle(terms, 'update', e)
        //(e) => this.toggle(terms, 'delete', e)
        if (this.state.information[3].TermsConditions.count === 0) {
            termsContent = <div>
                                There are no information!
                           </div>;
        } else {
            let dynamicterms = this.state.information[3].TermsConditions.inner.map((terms) =>
                                <tr key={terms.id}>
                                    <td>
                                        {terms.description.substring(0, 30) + '...'}
                                    </td>
                                    <td>
                                        {this.getTitle(terms.section)}
                                    </td>
                                    <td>
                                        {terms.status}
                                    </td>
                                    <td>
                                        <a className="ui teal button" onClick={() => this.showModal('view', terms)}>
                                            <i className="eye icon"></i>
                                            View
                                        </a>
                                        <a className="ui teal button" onClick={() => this.showModal('update', terms)}>
                                            <i className="edit icon"></i>
                                            Edit
                                        </a>
                                        <a className="ui teal button" onClick={() => this.showModal('delete', terms)}>
                                            <i className="trash alternate icon"></i>
                                            Delete
                                        </a>
                                    </td>
                                </tr>
                            );

            termsContent = <table className="ui selectable celled table">
                                    <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Section</th>
                                            <th>Status</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {dynamicterms}
                                    </tbody>
                                 </table>;
        }

        return termsContent;
    }

    deleteRecord(evt) {
        evt.preventDefault();

        $("#overlay_loader").css("display", "inline");
        let url = this.state.windowPath + "/api/delete/admin/data/section/" + this.state.data.id;

        axios.get(url)
             .then((response) => {
                 if (response.data.status == 'OK' && response.status == 200) {
                     this.setState({
                         messageLoad        : false,
                         loader             : 1,
                         successUptMessage  : 1,
                         responseMessageUpt : response.data.data.message,
                     });

                     this.forceUpdate();
                     this.changeStatus();
                 }

                 if (response.data.status == 'ERROR' && response.status == 200) {
                     this.setState({
                         errorMessage    : 1,
                         responseMessage : response.data.data.message,
                         loader          : 1,
                     });

                     this.forceUpdate();
                     this.changeStatus();
                 }
             })
             .catch((error) => {
                 this.setState({
                     errorMessage    : 1,
                     responseMessage : response.data.data.message,
                     loader          : 1,
                 });
             });
    }

    updateRecord = (evt) => {
        evt.preventDefault();

        $("#overlay_loader").css("display", "inline");
        let validate = 0;
        let      url = this.state.windowPath + "/api/put/admin/update/" + this.state.data.id;
        validate = this.validateDataEdit();

        if (validate === 0) {
            let dataForm = new FormData();
            let nameFile = (document.getElementById('fileUpdate').files[0] === undefined) ? "" : document.getElementById('fileUpdate').files[0];

            dataForm.append("file", nameFile);
            dataForm.append('section', (this.state.sectionUpdate) ? this.state.sectionUpdate : this.state.data.sectionId);
            dataForm.append('description', (this.state.descriptionUpdate) ? this.state.descriptionUpdate : this.state.data.description);
            dataForm.append('url_video', (this.state.url_videoUpdate) ? this.state.url_videoUpdate : this.state.data.url);
            dataForm.append('status', (this.state.statusUpdate) ? this.state.statusUpdate : this.state.data.statusId);
            dataForm.append('token', this.state.token);
            dataForm.append('nameT', this.state.nameToken);

            axios.post(url, dataForm, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }).then((response) => {
                 if (response.data.status == 'OK' && response.data.status != 'ERROR' && response.status == 200) {
                     this.setState({
                         messageLoad        : false,
                         successUptMessage  : 1,
                         responseMessageUpt : response.data.data.message,
                         loader             : 1,
                     });

                     this.forceUpdate();
                     this.changeStatus("updateE");
                 }

                 if (response.data.status == 'ERROR' && response.status == 200) {
                     this.setState({
                         errorMessageUpt : 1,
                         responseMessage : response.data.data.message,
                         showForm        : true,
                         loader          : 1,
                     });

                     this.forceUpdate();
                     this.changeStatus("updateE");
                 }
             }).catch((error) => {
                 this.setState({
                     errorMessageUpt : 1,
                     responseMessage : error.data.data.message,
                     loader          : 1,
                 });
             })
        }
    }

    componentDidUpdate = (prevProps, prevState) => {

        if ($(".ui.modal.viewAdmin" + this.state.data.id)) {
            $(".ui.modal.viewAdmin" + this.state.data.id).modal('setting', 'closable', false).modal('show');
        }

        if ($(".ui.modal.updateAdmin" + this.state.data.id)) {
            $(".ui.modal.updateAdmin" + this.state.data.id).modal('setting', 'closable', false).modal('show');
        }

        if ($(".ui.modal.deleteAdmin" + this.state.data.id)) {
            $(".ui.modal.deleteAdmin" + this.state.data.id).modal('show');
        }

        if (this.state.loader == 1) {
            this.changeFlagState();
        }
    }

    componentDidMount = () => {
        let url = this.state.windowPath + "/api/get/admin/lists";

        axios.get(url)
             .then((response) => {
                 if (response.data.status == 'OK' && response.status == 200) {
                     this.setState({
                         information : response.data.data.data,
                     });

                     this.setState({
                         loading : false
                     });
                 }

                 if (response.data.status == 'ERROR' && response.status == 200) {
                     thos.setState({
                         errorMessage : 1,
                         responseMessage : response.data.data.message
                     });
                 }

                 // hide loader
                 $("#overlay_loader").css("display", "none");
             })
             .catch((error) => {
                 this.setState({
                     errorMessage    : 1,
                     responseMessage : error.data.data.message,
                 });

                 // hide loader
                 $("#overlay_loader").css("display", "none");
             });

             $(".ui.accordion").accordion();
    }

    showAddForm = (evt) => {
        evt.preventDefault();

        this.setState({
            showForm       : true,
            buttonFormShow : false
        });
    }

    // deprecated
    closeClickE = () => {
        this.setState({
            responseMessage    : '',
            errorMessage       : 0,
            errorMessageUpt    : 0,
            options            : undefined,
        });
    }

    saveNewRecord = (evt) => {
        evt.preventDefault();

        $("#overlay_loader").css("display", "inline");
        let validate = 0,
            url      = "";
        validate     = this.validateData();

        if (validate === 0) {
            let dataForm = new FormData();
            let nameFile = (document.getElementById('file').files[0] === undefined) ? "" : document.getElementById('file').files[0];
            url = this.state.windowPath + "/api/post/admin/save";

            dataForm.append('section', this.state.section);
            dataForm.append('description', this.state.description);
            dataForm.append('file', nameFile);
            dataForm.append('url_video', this.state.url_video);
            dataForm.append('status', this.state.status);
            dataForm.append('token', this.state.token);
            dataForm.append('nameT', this.state.nameToken);

            axios.post(url, dataForm, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }).then((response) => {
                if (response.data.status == 'OK' && response.status == 200) {
                    this.setState({
                        successMessage  : 1,
                        responseMessage : response.data.data.message,
                        showForm        : true,
                        loader          : 1,
                        description     : '',
                        url_video       : '',
                        status          : 0,
                        section         : 0,
                    });

                    $("#url_video").val('');
                    $("#file").val('');
                    $("#statusId").prop('selectedIndex', 0);
                    $("#optionsId").prop('selectedIndex', 0);
                    $("#sectionID").prop('selectedIndex', 0);
                    $("#description").val("");

                    this.forceUpdate();
                    this.changeStatus('save');
                }

                if (response.data.status == 'ERROR' && response.status == 200) {
                    this.setState({
                        errorMessage    : 1,
                        responseMessage : response.data.data.message,
                        showForm        : true,
                        loader          : 1,
                    });

                    this.forceUpdate();
                    this.changeStatus('save');
                }
            }).catch((error) => {
                this.setState({
                    errorMessage    : 1,
                    responseMessage : error.data.data.message,
                    loader          : 1,
                });
            });
        }
    }

    validateData = () => {
        let counter = 0;
        let errors  = {};

        if (parseInt(this.state.section) === 0 || this.state.section === undefined) {
            errors['section'] = "Please, select a section";
            $("#sectionID").parent().parent().addClass('error');
            counter++;
        } else {
            errors['section'] = "";
            $("#sectionID").parent().parent().removeClass('error');
        }

        if (this.state.description === "" || this.state.description === undefined) {
            errors['description'] = "Please, enter description";
            $("#description").parent().parent().addClass('error');
            counter++;
        } else {
            errors['description'] = "";
            $("#description").parent().parent().removeClass('error');
        }

        if (parseInt(this.state.options) === 0 || this.state.options === undefined) {
            errors['options'] = 'Please, select an option';
            $("#optionsId").parent().parent().addClass('error');
            counter++;
         } else {
             errors['options'] = '';
             $("#optionsId").parent().parent().removeClass('error');
        }

        if (parseInt(this.state.status) === 0 || this.state.status === undefined) {
            errors['status'] = "Please, select a status";
            $("#statusId").parent().parent().addClass('error');
            counter++;
        } else {
            errors['status'] = "";
            $("#statusId").parent().parent().removeClass('error');
        }

        this.setState({
            errors  : errors,
            counter : counter,
            loader  : 1,
        });

        return counter;
    }

    validateDataEdit = () => {
        let counter = 0;
        let errors  = {};

        if (this.state.sectionUpdate != undefined) {
            if (parseInt(this.state.sectionUpdate) === 0 || this.state.sectionUpdate === undefined) {
                errors['section'] = "Please, select a section";
                $("#sectionIDUpdate").parent().parent().addClass('error');
                counter++;
            } else {
                errors['section'] = "";
                $("#sectionIDUpdate").parent().parent().removeClass('error');
            }
        } else {
            if (parseInt(this.state.data.sectionId) === 0 || this.state.data.sectionId === undefined) {
                errors['section'] = "Please, select a section";
                $("#sectionIDUpdate").parent().parent().addClass('error');
                counter++;
            } else {
                errors['section'] = "";
                $("#sectionIDUpdate").parent().parent().removeClass('error');
            }
        }

        if (this.state.descriptionUpdate != undefined) {
            if (this.state.descriptionUpdate === "" || this.state.descriptionUpdate === undefined) {
                errors['description'] = "Please, type a description";
                $("#descriptionUpdate").parent().parent().addClass('error');
                counter++;
            } else {
                errors['description'] = "";
                $("#descriptionUpdate").parent().parent().removeClass('error');
            }
        } else {
            if (this.state.data.description === "" || this.state.data.description === undefined) {
                errors['description'] = "Please, type a description";
                $("#descriptionUpdate").parent().parent().addClass('error');
                counter++;
            } else {
                errors['description'] = "";
                $("#descriptionUpdate").parent().parent().removeClass('error');
            }
        }

        /*if (this.state.optionsUpdate != undefined) {
            if (parseInt(this.state.optionsUpdate) === 0 || this.state.optionsUpdate === undefined) {
                errors['options'] = 'Please, select an option';
                $("#optionsIdUpdates").parent().parent().addClass('error');
                counter++;
             } else {
                 errors['options'] = '';
                 $("#optionsIdUpdates").parent().parent().removeClass('error');
            }
        } else {
            if (parseInt(this.state.data.optionsUpdate) === 0 || this.state.data.optionsUpdate === undefined) {
                errors['options'] = 'Please, select an option';
                $("#optionsIdUpdates").parent().parent().addClass('error');
                counter++;
             } else {
                 errors['options'] = '';
                 $("#optionsIdUpdates").parent().parent().removeClass('error');
            }
        }*/

        if (this.state.statusUpdate != undefined) {
            if (parseInt(this.state.statusUpdate) === 0 || this.state.statusUpdate === undefined) {
                errors['status'] = "Please, select a status";
                $("#statusIdUpdate").parent().parent().addClass('error');
                counter++;
            } else {
                errors['status'] = "";
                $("#statusIdUpdate").parent().parent().removeClass('error');
            }
        } else {
            if (parseInt(this.state.data.statusId) === 0 || this.state.data.statusId === undefined) {
                errors['status'] = "Please, select a status";
                $("#statusIdUpdate").parent().parent().addClass('error');
                counter++;
            } else {
                errors['status'] = "";
                $("#statusIdUpdate").parent().parent().removeClass('error');
            }
        }

        this.setState({
            errors  : errors,
            counter : counter,
            loader  : 1,
        });

        return counter;
    }

    createAddForm = () => {
        let values = [
            { 'id': '0', value: 'Select the Status' },
            { 'id': '1',  value: 'Active' },
            { 'id': '2',  value: 'Inactive' }
        ];

        let categories = [
            { 'id' : '0', value: 'Select a section' },
            { 'id' : '1', value: 'Banners' },
            { 'id' : '2', value: 'How It Works' },
            { 'id' : '3', value: 'About Us!' },
            { 'id' : '4', value: 'Terms and Conditions' }
        ];

        let options = [
            { 'id': '0', value: 'Select an option' },
            { 'id': '1', value: 'Images' },
            { 'id': '2', value: 'Videos' }
        ];


        let errorTemplate   = (this.state.counter != 0) ? <Errors headerError="Error Adding Admin Data"
                                                                  bodyError="All the fields with '*' must be filled in."
                                                                  variables={this.state.errors}
                                                                  onClick={this.closeClick} /> : "";
        /*let errorTemplate2  = (this.state.errorMessage != 0) ? <Errors errors="" respError={this.state.responseMessage}
                                                                       onClick={this.closeClickE} /> : "";*/

        // Get the value deoending on the options selected
        let stateFile = "true", stateVideo = "true";
        if (this.state.options !== undefined) {
            if (this.state.options == 1) {
                stateFile  = "false";
            } else if (this.state.options == 2) {
                stateVideo = "false";
            }
        }

        return (
            <div id="forms">
                <div className="row">
                    <div className="ten wide column">
                        <form className="ui large form">
                            <div className="ui stacked segment">

                                {errorTemplate}

                                <div className="field">
                                    <Select name="section" disable="false" labelSelectName="Section" iconTags="dashboard" require="true"
                                            handleFormFields={this.handleInput} defaultText="false"
                                            classData=""
                                            displayDataValues={categories} id="sectionID"
                                            ref={(section) => { this.section = section }} />
                                </div>
                                <div className="field">
                                    <TextArea name="description" disable="false" require="true" defaultText="Description"
                                            iconTags="description" handleFormFields={this.handleInput} labelName="Description"
                                            classData="" classCustom="" rowCustom=""
                                            ref={(description) => { this.description = description }} />
                                </div>
                                <div className="field">
                                    <Select name="options" labelSelectName="Select an Option" iconTags="format_list_numbered" require="false"
                                            disable="false" id="optionsId" handleFormFields={this.handleInput} defaultText="false"
                                            displayDataValues={options} classData="input-field col s8 offset-s2"
                                            ref={(options) => { this.options = options }} />
                                </div>
                                <div className="field">
                                    <InputFile name="file" labelButton="File" handleFormFields={this.handleInput}
                                            iconTags="insert_photo" classData="" disable={stateFile} require="false"
                                            ref={(file) => { this.file = file }} />
                                </div>
                                <div className="field">
                                    <Input name="url_video" require="false" defaultText="Video URL"
                                            iconTags="youtube icon" handleFormFields={this.handleInput} labelName="Video URL"
                                            disable={stateVideo} classData="ui left icon input" inputClass=""
                                            ref={(url_video) => { this.url_video = url_video }} />
                                </div>
                                <div className="field">
                                    <Select name="status" labelSelectName="Status" iconTags="iso" require="true" disable="false"
                                            id="statusId" handleFormFields={this.handleInput} defaultText="Status"
                                            displayDataValues={values} classData=""
                                            ref={(status) => { this.status = status }} />
                                </div>
                                <div className="field">
                                    <button className="ui fluid teal button right" type="submit" onClick={this.saveNewRecord}>
                                        Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

    changeStatus(methodName) {
        let url = this.state.windowPath + "/api/get/admin/lists";

        axios.get(url)
             .then((response) => {
                 if (response.data.status == 'OK' && response.status == 200) {
                     this.setState({
                         information : response.data.data.data,
                     });

                     if (methodName == "update") {
                         this.setState({
                             loading : false,
                             successUptMessage : 1,
                             responseMessageUpt : "The record has been updated successfully!",
                         });
                     }
                 }

                 if (response.data.status == 'ERROR' && response.status == 200) {
                     this.setState({
                         errorMessageUpt : 1,
                         responseMessage : response.data.data.message,
                     });
                 }
             })
             .catch((error) => {
                 this.setState({
                     errorMessageUpt : 1,
                     responseMessage : error.data.data.message,
                 });
             });
    }

    templateDelete() {
        let template = <div className="row">
                         <div className="">
                            <div>
                                Confirm Delete Record
                            </div>
                            <div>
                                Are you sure you want to delete the record?
                            </div>
                            <div>
                                <a href="#">Cancel</a>
                                <a href="#" onClick={this.deleteRecord}>Confirm Delete</a>
                            </div>
                         </div>
                       </div>;

        return template;
    }

    closeClick = () => {
        this.setState({
            counter            : 0,
            successUptMessage  : 0,
            errorMessageUpt    : 0,
            errorMessage       : 0,
            responseMessageUpt : "",
            responseMessage    : "",
            options            : undefined,
        });
    }

    closeClickSuccess = () => {
        this.setState({
            successMessage    : 0,
            successUptMessage : 0,
            options           : undefined,
        });
    }

    hideForm = (evt) => {
        evt.preventDefault();

        this.setState({
            buttonFormShow     : true,
            showForm           : false,
            options            : undefined,
            counter            : 0,
            successUptMessage  : 0,
            responseMessageUpt : "",
            responseMessage    : '',
            errorMessage       : 0,
        });
    }

    // Currently deprecated method toggle
    toggle(data, section, evt) {

        if (section == '') {
        //    console.log("holas");
            //document.getElementById('modalesAdmin').removeChild(document.getElementById('4'));
        //    $("#modalesAdmin").remove();
            //$(".ui.dimmer.modals.page.transition.hidden .ui.modal.viewAdmin.transition.hidden").remove();

            this.setState({
                toggle : false,
                data   : '',
                flagSection : '',
            });
            /*this.setState({
                data               : {},
                flagSection        : '',
                options            : undefined,
                counter            : 0,
                successUptMessage  : 0,
                responseMessageUpt : "",
                responseMessage    : '',
                errorMessage       : 0,
                successMessage     : 0,
            });*/
        } else {
            this.setState({
                toggle             : true,
                data               : data,
                flagSection        : section,
                options            : undefined,
                counter            : 0,
                successUptMessage  : 0,
                responseMessageUpt : "",
                responseMessage    : '',
                errorMessage       : 0,
                successMessage     : 0,
            });
        }
    }

    getTitle = (section) => {
        let title = '';

        if (section == 'TermsConditions') {
            title = 'Terms and Conditions';
        } else if (section == 'AboutUs') {
            title = 'About Us!';
        } else if (section == 'HowItWorks') {
            title = 'How it works!';
        } else {
            title = 'Banners';
        }

        return title;
    }

    deletedData = () => {
        //let styleData = (this.state.toggle == true) ? display : hide;
        const showHideClassName = this.state.show ? 'modal display-block' : 'modal display-none';

        let errorTem = (this.state.errorMessage != 0) ? <Errors headerError="Error Deleting Data"
                                                                bodyError={this.state.responseMessage}
                                                                onClick={this.closeClick} /> : "";
        let successM  = (this.state.successUptMessage != 0) ? <SuccessTemplate headerSuccess="Record Deleted Successfully."
                                                                               bodySuccess={this.state.responseMessageUpt}
                                                                               onClick={this.closeClickSuccess} /> : "";
        //(e) => this.toggle(null, '', e)
        let modalData = <div className={showHideClassName} key={this.state.data.id}>
                            <div className="modal-main">
                                <div className="ui segment">

                                    {errorTem}
                                    {successM}

                                    <div className="ui block header gross_font blue_color">
                                        Delete Information - {this.getTitle(this.state.data.section)}
                                    </div>

                                    <div className="sixteen wide column">
                                        <div className="scrolling content">
                                            <div className="center aligned">
                                                <h3 className="right-align thin_font blue_color">
                                                    {this.getTitle(this.state.data.section)}
                                                </h3>
                                            </div>
                                            <div>&nbsp;</div>
                                            <div className="justify_align thin_font">
                                                Are you sure you want to delete the record selected?
                                            </div>
                                        </div>
                                        <div className="ui divider"></div>
                                    </div>
                                    <div>
                                        <div className="positive ui right floated button" onClick={(e) => this.deleteRecord(e)}>
                                            <i className="trash alternate icon"></i>
                                            Delete
                                        </div>
                                        <div className="negative ui right floated button" onClick={this.hideModal}>
                                            <i className="window close icon"></i>
                                            Close
                                        </div>
                                        <div className="clear_floating"></div>
                                    </div>
                                </div>
                            </div>
                        </div>;

        return modalData;
    }

    updateData = () => {
        let values = [
            { 'id': '0', value: 'Select the Status' },
            { 'id': '1',  value: 'Active' },
            { 'id': '2',  value: 'Inactive' }
        ];

        let categories = [
            { 'id' : '0', value: 'Select a section' },
            { 'id' : '1', value: 'Banners' },
            { 'id' : '2', value: 'How It Works' },
            { 'id' : '3', value: 'About Us!' },
            { 'id' : '4', value: 'Terms and Conditions' }
        ];

        let options = [
            { 'id': '0', value: 'Select an option' },
            { 'id': '1', value: 'Images' },
            { 'id': '2', value: 'Videos' }
        ];

        let updateFlagFile = "true", updateFlagVideo = "true";
        // Validate exists type_file
        if (!this.state.optionsUpdate) {
            if (this.state.data.flagOption != "") {
                if (this.state.data.flagOption == 1) {
                    updateFlagFile = "false";
                } else {
                    updateFlagVideo = "false";
                }
            }
        }

        if (this.state.optionsUpdate == 1) {
            updateFlagFile  = "false";
        } else if(this.state.optionsUpdate == 2) {
            updateFlagVideo = "false";
        }

        let errorTemp = (this.state.counter != 0) ? <Errors headerError="Error Editing Data"
                                                            bodyError="All the fields with '*' must be filled in."
                                                            variables={this.state.errors}
                                                            onClick={this.closeClick} /> : "";
        let errorTem2 = (this.state.errorMessageUpt != 0) ? <Errors headerError="Error Updating Data"
                                                                    bodyError={this.state.responseMessage}
                                                                    onClick={this.closeClick} /> : "";
        let successM  = (this.state.successUptMessage != 0) ? <SuccessTemplate headerSuccess="Record updated successfully."
                                                                               bodySuccess={this.state.responseMessageUpt}
                                                                               onClick={this.closeClickSuccess} /> : "";
        //let styleData = (this.state.toggle == true) ? display : hide;
        const showHideClassName = this.state.show ? 'modal display-block' : 'modal display-none';
        let modalData = <div className={showHideClassName} key={this.state.data.id}>
                            <div className="modal-main">
                                <div className="ui segment">

                                    {errorTem2}
                                    {errorTemp}
                                    {successM}

                                    <div className="ui block header gross_font blue_color">
                                        Update Information - {this.getTitle(this.state.data.section)}
                                    </div>

                                    <div className="sixteen wide column">
                                        <form className="ui large form">
                                            <div className="ui stacked segment">

                                                {errorTemp}

                                                <Hidden value={this.state.data.id} id="editId" name="editId" />
                                                <div className="field">
                                                    <Select name="sectionUpdate" disable="false" labelSelectName="Section" iconTags="dashboard" require="true"
                                                            handleFormFields={this.handleInput} defaultText="false"
                                                            classData="" value={this.state.data.sectionId}
                                                            displayDataValues={categories} id="sectionIDUpdate"
                                                            ref={(sectionUpdate) => { this.sectionUpdate = sectionUpdate }} />
                                                </div>
                                                <div className="field">
                                                    <TextArea name="descriptionUpdate" disable="false" require="true"
                                                            defaultText="true"
                                                            is_update="true"
                                                            items={this.state.data.description}
                                                            iconTags="description"
                                                            handleFormFields={this.handleInput} labelName="Description"
                                                            classData="input-field col s9 offset-s1"
                                                            classCustom=""
                                                            rowCustom="left_move_textarea"
                                                            ref={(descriptionUpdate) => { this.descriptionUpdate = descriptionUpdate }} />
                                                </div>
                                                <div className="field">
                                                    <Select name="optionsUpdate" labelSelectName="Select an Option" iconTags="format_list_numbered"
                                                            require="false" disable="false" id="optionsIdUpdates" handleFormFields={this.handleInput}
                                                            value={this.state.data.flagOption} defaultText="false"
                                                            displayDataValues={options} classData="input-field col s8 offset-s2"
                                                            ref={(optionsUpdate) => { this.optionsUpdate = optionsUpdate }} />
                                                </div>
                                                <div className="field">
                                                    <InputFile name="fileUpdate" labelButton="File" handleFormFields={this.handleInput}
                                                            iconTags="" disable={updateFlagFile}
                                                            classData="file-field input-field col s8 offset-s2"
                                                            ref={(fileUpdate) => { this.fileUpdate = fileUpdate }} />
                                                </div>
                                                <div className="field">
                                                    <Input name="url_videoUpdate" disable={updateFlagVideo} require="false"
                                                            defaultText="true"
                                                            is_update="true"
                                                            iconTags="live_tv"
                                                            items={this.state.data.url}
                                                            handleFormFields={this.handleInput} labelName="Video URL"
                                                            classData="input-field col s8 offset-s2"
                                                            ref={(url_videoUpdate) => { this.url_videoUpdate = url_videoUpdate }} />
                                                </div>
                                                <div className="field">
                                                    <Select name="statusUpdate" labelSelectName="Status" iconTags="iso" require="true"
                                                            disable="false"
                                                            value={this.state.data.statusId}
                                                            id="statusIdUpdate" handleFormFields={this.handleInput} defaultText="false"
                                                            displayDataValues={values}
                                                            classData="input-field col s8 offset-s2"
                                                            ref={(statusUpdate) => { this.statusUpdate = statusUpdate }} />
                                                </div>
                                            </div>
                                        </form>
                                        <div>&nbsp;</div>
                                    </div>

                                    <div>
                                        <div className="positive ui right floated button" onClick={(e) => this.updateRecord(e)}>
                                            <i className="edit icon"></i>
                                            Update
                                        </div>
                                        <div className="negative ui right floated button" onClick={(e) => this.toggle(null, '', e)}>
                                            <i className="window close icon"></i>
                                            Close
                                        </div>
                                        <div className="clear_floating"></div>
                                    </div>

                                </div>
                            </div>
                        </div>;

        return modalData;
    }

    viewData = () => {
        //let styleData = (this.state.toggle == true) ? display : hide;
        const showHideClassName = this.state.show ? 'modal display-block' : 'modal display-none';
        let templateImgVideo = '';
        let arrayExtensions = ['jpg', 'png', 'bmp', 'jpeg', 'gif'];
        let value = arrayExtensions.indexOf(this.state.data.extension);

        // Set the video or image template
        if (this.state.data.type_file != '') {
            if (this.state.data.type_file == 0) {
                templateImgVideo = <img src={this.state.data.file_resize} alt="Banner" />;
            } else if (this.state.data.type_file == 1) {
                if (this.state.data.type == 'youtube') {
                    templateImgVideo = <iframe width="800" height="400" src={this.state.data.videoURL}></iframe>;
                } else {
                    if (this.state.data.file != "" && this.state.data.file_extension != "" && parseInt(value) == -1) {
                        templateImgVideo = <iframe src={this.state.data.file} width="600" height="400"></iframe>;
                    } else {
                        templateImgVideo = <Videos videoSrc={this.state.data.url} autoplay="0"
                                                  rel="0" modest="1" />;
                    }
                }
            }
        }
        // this.toggle(this.state.data.id, '', e) style={styleData}
        let modalData = <div className={showHideClassName} id={this.state.data.id} key={this.state.data.id}>
                            <div className="modal-main">
                                <div className="ui segment">
                                    <div className="ui block header gross_font blue_color">
                                        View Details Information - {this.getTitle(this.state.data.section)}
                                    </div>

                                    <div className="sixteen wide column">
                                        <div className="scrolling content">
                                            <div className="center aligned">
                                                <h3 className="right-align thin_font blue_color">
                                                    {this.getTitle(this.state.data.section)}
                                                </h3>
                                            </div>
                                            <div className="justify_align thin_font">
                                                <span className="gross_font">Description:</span>
                                                <span className="thin_font">{this.state.data.description}</span>
                                            </div>
                                            <div className="center aligned">
                                                {templateImgVideo}
                                            </div>
                                            <div className="justify_align thin_font">
                                                <span className="gross_font">Created At:</span>
                                                <span className="thin_font">{this.state.data.created_at}</span>
                                            </div>
                                            <div className="justify_align thin_font">
                                                <span className="gross_font">Updated At:</span>
                                                <span className="thin_font">{this.state.data.updated_at}</span>
                                            </div>
                                        </div>
                                        <div className="ui divider"></div>
                                    </div>

                                    <div>
                                        <div className="negative ui right floated button" onClick={this.hideModal}>
                                            <i className="window close icon"></i>
                                            Close
                                        </div>
                                        <div className="clear_floating"></div>
                                    </div>

                                </div>
                            </div>
                        </div>;
        // close button : (e) => this.close(null, '', e)

        return modalData;
    }

    showModal = (view, data) => {
        this.setState({ show: true, flagSection: view, data: data });
    }

    hideModal = () => {
        this.setState({ show: false, flagSection: '', data: {} });
    }

    templateModal = () => {
        const showHideClassName = this.state.show ? 'modal display-block' : 'modal display-none';
        /*console.log("*******");
        console.log(this.state.toggle);
        console.log("*******");
        let styleData = (this.state.toggle == true) ? display : hide;
        let classModal = "ui modal viewAdmin" + this.state.data.id;

        let modaltemplates =    <div className="ui modal viewAdmin4" style={styleData} key={this.state.data.id}>
                                    holas
                                    <div className="negative ui button" onClick={(e) => this.toggle(null, '', e)}>
                                        <i className="window close icon"></i>
                                        Close
                                    </div>
                                </div>;

        return modaltemplates;*/
        let data = <div className={showHideClassName}>
                        <div>
                          <section className='modal-main'>
                            holas esto es una prueba
                            <button onClick={this.hideModal}>
                              Close
                            </button>
                          </section>
                        </div>
                </div>;

        return data;
    }

    render() {
        let banners, works, about, terms, formsAdd, modalInfo, modal = [];

        if (this.state.loading == false) {
            banners = this.loadDataBanners();
            works   = this.loadDataWorks();
            about   = this.loadDataAboutUs();
            terms   = this.loadDataTerms();
        }

        if (this.state.showForm == true) {
            formsAdd = this.createAddForm();
        }

        let showDeleteRecord = (this.state.showDelWorks == true) ? this.templateDelete() : "";

        let successTemplate = (this.state.successMessage == 1) ? <SuccessTemplate headerSuccess="Record saved successfully."
                                                                                  bodySuccess={this.state.responseMessage}
                                                                                  onClick={this.closeClickSuccess} /> : "";

        // Verify if the button that will be displayed is for show or hide the form
        let buttonForm = "";
        if (this.state.buttonFormShow == true) {
            buttonForm = <div className="margin-top-two">
                            <a className="ui fluid teal button" onClick={this.showAddForm}>
                                Add New Record
                            </a>
                         </div>;
        } else {
            buttonForm = <div className="margin-top-two">
                            <a className="ui fluid red button" onClick={this.hideForm}>
                                Hide Form
                            </a>
                         </div>;
        }

        // modal functionality
        if (this.state.show == true) { //toggle
            //document.getElementById('overlay').style.display = "block";
            if (this.state.flagSection == 'view') {
                modalInfo = this.viewData();
            } else if (this.state.flagSection == 'delete') {
                modalInfo = this.deletedData();
            } else if (this.state.flagSection == 'update') {
                modalInfo = this.updateData();
            } else {
                modalInfo = '';
            }
        } else {
            modalInfo = '';
        }

        // set the modal to the array
        modal.push(modalInfo);

        return (
            <div>

                <div className="ui info message">
                    <div className="header">
                        <i className="info icon"></i>
                        Please add Information will be displayed on different section of the site such as:
                    </div>
                    <ul className="list">
                        <li> Banners </li>
                        <li> How it works! </li>
                        <li> About Us! </li>
                        <li> Terms and Conditions </li>
                    </ul>
                </div>

                <div id="modalesAdmin">
                    {modal}
                </div>
                {successTemplate}

                <div className="row">&nbsp;</div>
                <div className="row top_admin_accordion">
                    {buttonForm}
                </div>

                <div className="row top_admin_accordion">
                    {formsAdd}

                    {showDeleteRecord}
                </div>

                <div className="row top_admin_accordion">
                    <div className="ui styled fluid accordion">
                        <div className="title">
                            <i className="dropdown icon"></i>
                            Banners
                        </div>
                        <div className="content">
                            {banners}
                        </div>
                        <div className="title">
                            <i className="dropdown icon"></i>
                            How it Works!
                        </div>
                        <div className="content">
                            {works}
                        </div>
                        <div className="title">
                            <i className="dropdown icon"></i>
                            About us!
                        </div>
                        <div className="content">
                            {about}
                        </div>
                        <div className="title">
                            <i className="dropdown icon"></i>
                            Terms and Conditions
                        </div>
                        <div className="content">
                            {terms}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
