/**
 * Class will contain all the information is related to
 * the fake projects, this UI will display all the information
 * of the users have reported the projects as fake
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */

// Const for modal
const display = {
 display: 'block'
};
const hide = {
 display: 'none'
};

class FakeProjects extends React.Component {
    constructor(props) {
        super(props);

        this.displayCollections = this.displayCollections.bind(this);
        this.displayModal       = this.displayModal.bind(this);
        this.toggle             = this.toggle.bind(this);
        this.closeClick         = this.closeClick.bind(this);
        this.showModal          = this.showModal.bind(this);
        this.hideModal          = this.hideModal.bind(this);

        this.state = {
            validating  : 1,
            loading     : true,
            modalLoad   : false,
            dataInfo    : {},
            information : [],
            posts       : [],
            errorFlag   : 0,
            msgError    : '',
            show        : false,
            source      : props.source,
            token       : props.token,
            nameToken   : props.nameT,
            windowPath  : (window.$PATH !== undefined) ? window.$PATH : "",
        };
    }

    componentDidMount() {
        let url = this.state.windowPath + "/api/get/fake/projects";

        axios.get(url)
             .then((response) => {
                 if (response.status == 200 && response.data.status == 'OK') {
                     this.setState({
                         information : response.data.data.data.projects,
                         loading     : false,
                         validating  : 0
                     });
                 }

                 if (response.status == 200 && response.data.status == 'ERROR') {
                     this.setState({
                         errorFlag  : 1,
                         msgError   : response.data.data.message,
                         validating : 0,
                     });
                 }

                 // hide loader
                 $("#overlay_loader").css("display", "none");
             })
             .catch((error) => {
                 this.setState({
                     errorFlag  : 1,
                     msgError   : error.data.data.message,
                     validating : 0
                 });

                 // hide loader
                 $("#overlay_loader").css("display", "none");
             });

    }

    componentDidUpdate = (prevProps, prevState) => {
        if ($(".ui.modal.fakeViews"))  {
            $(".ui.modal.fakeViews").modal('setting', 'closable', false).modal('modal');
        }
    }

    showModal = (view, data) => {
        this.setState({
            show        : true,
            flagSection : view,
            dataInfo    : data,
        });
    }

    hideModal = () => {
        this.setState({
            show        : false,
            flagSection : '',
            dataInfo    : {},
        });
    }

    closeClick = () => {
        this.setState({
            errorFlag : 0,
            msgError  : '',
        });
    }

    // deprecated
    toggle = (data, e) => {
        e.preventDefault();

        if (data == '') {
            $(".ui.modal.fakeViews").modal('hide');
        } else {
            this.setState(prevState => ({
                modalLoad : true,
                dataInfo  : data,
            }));
        }
    }

    displayModal = () => {
        //let styleDisplay = (this.state.modalLoad == true) ? display : hide;
        let collectionUsers;

        if (this.state.dataInfo != '') {
            collectionUsers = this.state.dataInfo.users.map((user) =>
                                <div className="ui relaxed divided list" key={user.userId}>
                                    <div className="item">
                                        <i className="large user middle aligned icon"></i>
                                        <div className="content">
                                            <div className="header">Username: {user.username}</div>
                                            <div className="description">
                                                <div>
                                                    Name: {user.name}
                                                </div>
                                                <div>
                                                    Email: {user.email}
                                                </div>
                                                <div>
                                                    User Type: {user.typeUser}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              );
        } else {
            collectionUsers = '';
        }


        // (e) => this.toggle('', e)
        const showHideClassName = this.state.show ? 'modal display-block' : 'modal display-none';
        let modalData = <div className={showHideClassName} key={this.state.dataInfo.fakeProject}>
                            <div className="modal-main">
                                <div className="ui segment">

                                    <div className="ui block header gross_font blue_color">
                                        List of Users Report - { this.state.dataInfo.name }
                                    </div>

                                    <div className="sixteen wide column">
                                        <div className="scrolling content">
                                            { collectionUsers }
                                        </div>
                                    </div>

                                    <div>
                                        <div className="negative ui right floated button" onClick={this.hideModal}>
                                            <i className="window close icon"></i>
                                            Close
                                        </div>
                                        <div className="clear_floating"></div>
                                    </div>

                                </div>
                            </div>
                        </div>;

        return modalData;
    }

    displayCollections = () => {
        let collection, collections;

        // (e) => this.toggle(post, e)
        collection = this.state.information.posts.map((post) =>
                        <tr key={post.id}>
                            <td> { post.name } </td>
                            <td> { post.desc } </td>
                            <td> { post.category_name } </td>
                            <td> { post.city_name } </td>
                            <td>
                                <a className="ui teal icon button" data-content="List of Users" onClick={() => this.showModal('view', post)}>
                                    <i className="address book icon"></i>
                                    List of Users
                                </a>
                            </td>
                        </tr>
                     );

        collections = <table className="ui fixed single line celled table">
                            <thead>
                                <tr>
                                    <th>Project Name</th>
                                    <th>Description</th>
                                    <th>Category</th>
                                    <th>City</th>
                                    <th>Options</th>
                                </tr>
                            </thead>
                            <tbody>
                                {collection}
                            </tbody>
                      </table>;

        return collections;
    }

    render () {
        let collectionData, modalInfo, modal = [];

        let errorMessage = (this.state.errorFlag != 0) ? <Errors headerError="Problems with Load Data"
                                                                 bodyError={this.state.msgError}
                                                                 onClick={this.closeClick} /> : "";

        if (this.state.loading == false) {
            collectionData = this.displayCollections();
        }

        if (this.state.show == true) {
            //document.getElementById('overlay').style.display = "block";
            modalInfo = this.displayModal();
        } else {
            //document.getElementById('overlay').style.display = "none";
            modalInfo = '';
        }

        modal.push(modalInfo);

        return (
            <div>
                {errorMessage}
                {modal}
                <div className="row">
                    {collectionData}
                </div>
            </div>
        );
    }
}
