ReactDOM.render(
    <FakeProjects token={ document.getElementById('_csrf_token').value }
                  source={ document.getElementById('_source').value }
                  nameT={ document.getElementById('name').value }
                  id={ document.getElementById('_id').value } />,
    document.getElementById('app')
);
