/**
 * Class that contains all the actions
 * and events will be used for this
 * component that is used for upload images
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */
class InputFile extends React.Component {
    constructor(props) {
        super(props);

        this.onBlur    = this.onBlur.bind(this);
        this.inputFile = this.inputFile.bind(this);

        this.state = {
            value: '',
            label: ''
        };
    }

    onBlur(evt) {
        this.props.handleFormFields(this.state);
    }

    inputFile(event) {
        this.setState({
            value: event.target.value,
            label: event.target.name
        });
    }

    componentDidUpdate = () => {
        if (this.props.isEmpty == 1) {
            this.setState({
                value : '',
            });
        }

        $(".inline.icon").popup({
            inline: true
        });
    }

    render() {
        //let propsInput = {};
        //let dynamicClass = (this.props.classData != "") ? this.props.classData : "";
        //let hideProp = (this.props.hide) ?  "hide" : "";
        //let parameters = "row " + hideProp;
        //if (this.props.disable == "true") propsInput.disabled = true;
        let classDiv = '', finalClass = '';
        let disabled = (this.props.disable == 'true' || this.props.disable == true) ? 'disabled' : '';
        if (this.props.require == 'false' || this.props.require == false) {
            classDiv = 'field';
        } else {
            classDiv = 'field required';
        }
        finalClass = classDiv + ' ' + disabled;

        let labelPopUp = "Please, select all the images/videos you want upload in the project.";
        let iconPopUp = (this.props.tooltip == "") ? '' : <i className="question inline circle icon link"
                                                             data-content={labelPopUp}
                                                             data-variation="mini"></i>

        return (
            <div className={finalClass}>
                <label>{this.props.labelButton} {iconPopUp}</label>
                <div className="ui fluid input">
                    <input placeholder="Select a File" multiple id={this.props.name} type="file" />
                </div>
            </div>
        );
    }
}
