/**
 * Class contains all the events and actions
 * will be executed once the checkbox has been
 * executed by the user
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */

// Const for modal
const display = {
    display: 'block'
};
const hide = {
    display: 'none'
};

class Checkbox extends React.Component {
    constructor(props) {
        super(props);

        this.toggleChange = this.toggleChange.bind(this);
        this.openModal    = this.openModal.bind(this);
        this.getTitle     = this.getTitle.bind(this);

        this.state = {
            label      : this.props.name,
            value      : false,
            flag       : 0,
            modal      : 0,
            messages   : '',
            statusFlag : false,
        };
    }

    getTitle = (section) => {
        let title = '';

        if (section == 'TermsConditions') {
            title = 'Terms and Conditions';
        } else if (section == 'AboutUs') {
            title = 'About Us!';
        } else if (section == 'HowItWorks') {
            title = 'How it works!';
        } else {
            title = 'Banners';
        }

        return title;
    }

    openModal = (evt, messages) => {
        evt.preventDefault();

        $("#title").html(this.getTitle(messages.section));
        $("#dataValue").html(messages.description);

        $('.ui.modal').modal('show');
    }

    toggleChange() {
        const currentValue = this.state.value;
        this.setState({
            value : !currentValue,
            flag  : 1
        });
    }

    componentDidUpdate() {
        if (this.state.value == true && this.state.flag == 1) {
            this.setState({flag: 0});
            this.props.handleFormFields(this.state);
        }
    }

    render() {
        //let propsInput = {}, dynamicClass, parameters;
        let message      = (this.props.dataSent != "") ? this.props.dataSent : "";
        let dynamicClass = (this.props.classData != "") ? this.props.classData :  "";
        //if (this.props.disable === "true") propsInput.disabled = true;
        //if (this.props.require === "true") propsInput.required = true;
        //let hideProp = (this.props.hide) ?  "hide" : "";
        //parameters = "row " + hideProp;

        return (
            <div className="ui checkbox">
                <input type="checkbox"
                       name={this.props.name}
                       id={this.props.name}
                       className={dynamicClass}
                       checked={this.state.value}
                       onChange={this.toggleChange} />
                <label>
                    <a href="#" className="thin_font" onClick={(evt) => this.openModal(evt, message)}>
                        {this.props.labelName}
                    </a>
                </label>
            </div>
        );
    }
}
