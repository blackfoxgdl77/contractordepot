/**
 * Class contains all the functionality for the
 * single component called tel. Calling this
 * component in the form will be created the
 * field for the phone
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */
class InputTel extends React.Component {
    constructor(props) {
        super(props);

        this.handleTel = this.handleTel.bind(this);
        this.onBlur = this.onBlur.bind(this);

        this.state = {
            value  : '',
            label  : '',
            active : 0
        };
    }

    onBlur(evt) {
        this.props.handleFormFields(this.state);
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.isEmpty == 1) {
            this.setState({
                value : '',
            });
        }

        if (this.state.active == 0 && typeof prevProps.items !== "undefined") {
            this.setState({
                active : 1,
                value  : prevProps.items
            });
        }
    }

    handleTel(event) {
        const validatePhone = /^[+() 0-9\b]+$/;
        if (event.target.value != '' && validatePhone.test(event.target.value)) {
            this.setState({
                value : event.target.value,
                label : event.target.name
            });
        }
    }

    render() {
        // Add parameters dynamically for the field will be used in the forms
        //var propsInput = {}, dynamicClass, parameters;
        //dynamicClass = (this.props.classData != "") ? this.props.classData : "";
        //if (this.props.disable == "true")     propsInput.disabled = true;
        //if (this.props.require == "true")     propsInput.required = true;
        //if (this.props.defaultText == "true") propsInput.placeholder = "";
        //let hideProp = (this.props.hide) ?  "hide" : "";
        //parameters = "row " + hideProp;
        let classDiv = '';
        if (this.props.require == "false" || this.props.require == false) {
            classDiv = 'field';
        } else {
            classDiv = 'field required';
        }

        return (
            <div className={classDiv}>
                <label>{this.props.labelName}</label>
                <div className={this.props.classData}>
                    <i className={this.props.iconTags}></i>
                    <input  type="tel"
                            name={this.props.name}
                            id={this.props.name}
                            placeholder={this.props.defaultText}
                            value={this.state.value}
                            onChange={this.handleTel}
                            onBlur={this.onBlur} />
                </div>
            </div>
        );
    }
}
