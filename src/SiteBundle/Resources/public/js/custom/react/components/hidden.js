class Hidden extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value  : '',
            label  : '',
            active : 0
        };
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.active == 0 && typeof prevProps.items !== "undefined") {
            this.setState({
                active: 1,
                value: prevProps.items
            });
        }
    }

    render() {
        return (
            <input type="hidden"
                   value={this.state.value}
                   id={this.props.name}
                   name={this.props.name} />
        );
    }
}
