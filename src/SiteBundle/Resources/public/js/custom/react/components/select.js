/**
 * Class contains all the actions y events will be
 * executed for the select component in the
 * use of all the platform in the forms
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */
class Select extends React.Component {
    constructor(props) {
        super(props);
        this.handleInput = this.handleInput.bind(this);
        this.getValues   = this.getValues.bind(this);
        this.onBlur      = this.onBlur.bind(this);

        this.state = {
            value: "-1",
            label: this.props.name
        };
    }

    onBlur = () => {
        this.props.handleFormFields(this.state);
    }

    handleInput = (event) => {
        this.setState({
            value : event.target.value
        });
    }

    getValues = () => {
        let options = [];

        for (let key in this.props.displayDataValues) {
            if (this.props.displayDataValues.hasOwnProperty(key)) {

                let option = this.props.displayDataValues[key];

                // Check if comes from JSON response
                if(option.id == undefined && option.value == undefined) {
                    option = JSON.parse(option);
                }

                options.push({
                    key     : key,
                    value   : option.id,
                    label   : option.value
                });
            }
        }

        return options;
    }

    render() {
        //let propsInput = {}, arrayDataValues, optionsValues, dynamicClass, parameters;
        //dynamicClass = (this.props.classData != "") ? this.props.classData  : "";
        //if (this.props.disable == "true")     propsInput.disabled    = true;
        //if (this.props.require == "true")     propsInput.required    = true;
        //if (this.props.defaultText == "true") propsInput.placeholder = "";
        //let hideProp = (this.props.hide) ?  "hide" : "";
        //parameters = "row " + hideProp;

        let finalClass = '', classDiv = '';
        let disable = (this.props.disable == 'true' || this.props.disable == true) ? 'disabled' : '';
        if (this.props.require == 'false' || this.props.require == false) {
            classDiv = 'field';
        } else {
            classDiv = 'field required';
        }
        finalClass = classDiv + disable;

        let arrayDataValues = this.getValues();
        let optionsValues   = arrayDataValues.map((values) =>
                <option value={values.value} key={values.key}>
                    {values.label}
                </option>
            );

        return (
            <div className={finalClass}>
                <label>{this.props.labelSelectName}</label>
                <select className="ui fluid dropdown selection"
                        name={this.props.name}
                        id={this.props.id}
                        value={this.props.value}
                        className=""
                        onBlur={this.onBlur}
                        onChange={this.handleInput}>
                    {optionsValues}
                </select>
            </div>
        );
    }
}
