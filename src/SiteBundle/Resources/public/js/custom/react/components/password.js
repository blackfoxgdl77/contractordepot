/**
 * Class contains all the information about
 * the password element will be used on the
 * platform just calling this class
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */
class Password extends React.Component {
    constructor(props) {
        super(props);

        this.handleInput = this.handleInput.bind(this);
        this.onBlur = this.onBlur.bind(this);

        this.state = {
            value: '',
            label: ''
        };
    }
    componentDidUpdate = () => {
        if (this.props.isEmpty == 1) {
            this.setState({
                value : '',
            });
        }
    }

    onBlur(evt) {
        this.props.handleFormFields(this.state);
    }

    handleInput(event) {
        this.setState({
            value: event.target.value,
            label: event.target.name
        });
    }

    render() {
        //var propsInput = {}, dynamicClass, parameters;
        //dynamicClass = (this.props.classData != "") ? this.props.classData : "";
        //if (this.props.disable == "true")     propsInput.disabled = true;
        //if (this.props.require == "true")     propsInput.required = true;
        //if (this.props.defaultText == "true") propsInput.placeholder = "";
        //let hideProp = (this.props.hide) ?  "hide" : "";
        //parameters = "row " + hideProp;

        return (
            <div className="field required">
                <label>{this.props.labelName}</label>
                <div className={this.props.classData}>
                    <i className={this.props.iconTags}></i>
                    <input type="password"
                           name={this.props.name}
                           id={this.props.name}
                           value={this.state.value}
                           placeholder={this.props.defaultText}
                           className={this.props.inputClass}
                           onChange={this.handleInput}
                           onBlur={this.onBlur} />
                </div>
            </div>
        );
    }
}
