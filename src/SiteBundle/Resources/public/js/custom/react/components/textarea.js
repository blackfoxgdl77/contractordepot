/**
 * Component will be used to create a
 * textarea in the forms just calling this
 * class with the parameters set in every
 * single component
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */
class TextArea extends React.Component {
    constructor(props) {
        super(props);

        this.handleInput = this.handleInput.bind(this);
        this.onBlur      = this.onBlur.bind(this);

        this.state = {
            value  : (this.props.items != undefined) ? this.props.items : "",
            label  : '',
            active : 0,
        };
    }

    onBlur(evt) {
        this.props.handleFormFields(this.state);
    }

    handleInput(event) {
        this.setState({
            value : event.target.value,
            label : event.target.name
        });
    }

    componentDidUpdate = () => {
        if (this.props.isEmpty == 1) {
            this.setState({
                value : '',
            });
        }
    }

    componentWillMount() {
        if (this.props.is_update == "true") {
            if (this.state.active == 0) {
                this.setState({
                    active : 1,
                    value  : this.props.items
                });
            }
        }
    }

    render() {
        // Add parameters dynamically for the field will be used in the forms
        //var propsInput = {}, dynamicClass, parameters, textareaClass, labelClassUpdated, iconTags = '';
        //dynamicClass = (this.props.classData != "") ? this.props.classData  : "";
        //labelClassUpdated = (this.props.is_update == "true" && this.props.items != undefined) ? "thin_font active" : "thin_font";
        //if (this.props.disable == "true")     propsInput.disabled = true;
        //if (this.props.require == "true")     propsInput.required = true;
        //if (this.props.defaultText == "true") propsInput.placeholder = "";
        //if (this.props.iconTags) iconTags = <i className="material-icons prefix">{this.props.iconTags}</i>;

        //let customTextClass = (this.props.classCustom) ? this.props.classCustom : "";
        //let rowClassCustom  = (this.props.rowCustom)   ? this.props.rowCustom : "";
        //let hideProp = (this.props.hide) ?  " hide " : "";
        //parameters = "row " + hideProp + " " + rowClassCustom;
        //textareaClass = "materialize-textarea validate thin_font " + customTextClass;
        let classDiv = '', finalClass = '';
        let disable = (this.props.disable == 'true' || this.props.disable == true) ? 'disabled' : '';
        if (this.props.require == 'false' || this.props.require == false) {
            classDiv = 'field';
        } else {
            classDiv = 'field required';
        }
        finalClass = classDiv + disable;

        return (
            <div className={finalClass}>
                <label>{this.props.labelName}</label>
                <textarea value={this.state.value}
                          id={this.props.name}
                          name={this.props.name}
                          onChange={this.handleInput}
                          onBlur={this.onBlur}></textarea>
            </div>
        );
    }
}
