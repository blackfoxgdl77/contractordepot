/**
 * Component will be used to load all the information
 * related to the youtube videos that the user could
 * upload to the platform for displatig information
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */
class Videos extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let videoSrc = this.props.videoSrc +
                       "?autoplay=" + this.props.autoplay +
                       "&rel=" + this.props.rel +
                       "&modestbranding=" + this.props.modest;

        return(
            <div>
                <iframe className="player"
                        type="text/html"
                        width="100%"
                        height="450"
                        src={videoSrc}
                        frameBorder="0" />
            </div>
        );
    }
}
