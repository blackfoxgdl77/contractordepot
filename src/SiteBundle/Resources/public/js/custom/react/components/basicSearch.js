
class BasicSearch extends React.Component {
    constructor(props) {
        super(props);

        this.handleInput    = this.handleInput.bind(this);
        this.searchRecords  = this.searchRecords.bind(this);
        
        this.state = {
            'user'      : props.user,
            'inputError': [],
            'counter'   : 0
        };

    }

    handleInput(data) {
        if (data.label != '') {
            this.setState({
                [data.label]: data.value
            });
        }
    }

    searchRecords = (evt) => {
        
        evt.preventDefault();

        let url = '/api/posts'
        let queryParams = "";

        if (this.state.postname != undefined && this.state.postname != null && this.state.postname != "") {
            queryParams = queryParams + "postname=" + this.state.postname;
        }        
        if (this.state.category != undefined && this.state.category != null && this.state.category != "") {
            if (queryParams != "") {
                queryParams = queryParams + "&";
            }
            queryParams = queryParams + "category=" + this.state.category;
        }
        if (this.state.city != undefined && this.state.city != null && this.state.city != "") {
            if (queryParams != "") {
                queryParams = queryParams + "&";
            }
            queryParams = queryParams + "city=" + this.state.city;
        }

        if (queryParams != "") {
            queryParams = "?" + queryParams;
            url = url + queryParams;
        }

        // Retrieving posts information
        axios.get(url)
            .then((response) => {                
                this.setState({
                    posts          : response.data.data
                });

            }).catch((error) => {
                console.log("Error: " + error);
            });

    }


    componentDidMount() {
             
    }

    componentWillMount() {

        // Retrieving cities
        axios.get('/api/cities')
            .then((response) => {                
                this.setState({
                    cities          : response.data.data
                });

            }).catch((error) => {
                
            });


        // Retrieving job categories
        axios.get('/api/jobCategories')
            .then((response) => {
                
                this.setState({
                    jobCategories  : response.data.data
                });
                
            }).catch((error) => {
                
            });

    }

    render() {
        
        if (this.state.cities == undefined || this.state.cities == null || this.state.cities.length == 0 || 
            this.state.jobCategories == undefined || this.state.jobCategories == null || this.state.jobCategories.length == 0) {
            return (
                <h5>Loading Search component...</h5>
            );
        } else { 

            return (

                <div className="row">
                    <div className="col s12">
                        <form>
                            <Input name="postname" disable="false" labelName="Postname" iconTags=""
                                handleFormFields={this.handleInput} defaultText="false" require="true" id="postname"
                                classData="input-field col s9" ref={(postname) => { this.postname = postname }} />

                            <Select name="city" disable="false" labelSelectName="City" iconTags="" require="true"
                                handleFormFields={this.handleInput} defaultText="false" classData="input-field col s9"
                                displayDataValues={this.state.cities} id="cityID" ref={(city) => { this.city = city }} />

                            <Select name="category" disable="false" labelSelectName="Category" iconTags="" require="true"
                                handleFormFields={this.handleInput} defaultText="false" classData="input-field col s9"
                                displayDataValues={this.state.jobCategories} id="categoryID" ref={(category) => { this.category = category }} />

                            <div className="row center">
                                <div className="col s12">
                                    <button className="btn waves-effect waves-light" type="submit" onClick={this.searchRecords}>
                                        Search
                                        <i className="large material-icons">find_in_page</i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            );
        }


        
    }
}
