/**
 * Class contains all the functionality for the
 * component will be used as simple input in the
 * forms and can be used when is needed, just needs to
 * set the parameters required
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */
class Input extends React.Component {
    constructor(props) {
        super(props);

        this.handleInput = this.handleInput.bind(this);
        this.onBlur      = this.onBlur.bind(this);

        this.state = {
            value  : (this.props.items) ? this.props.items : "",
            label  : this.props.name,
            active : 0,
        };
    }

    onBlur(event) {
        this.props.handleFormFields(this.state);
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.isEmpty == 1) {
            this.setState({
                value : '',
            });
        }

        if (this.state.active == 0 && typeof prevProps.items !== "undefined") {
            this.setState({
                active : 1,
                value  : prevProps.items
            });
        }
    }

    componentWillMount() {
        if (this.props.is_update == "true") {
            if (this.state.active == 0) {
                this.setState({
                    active : 1,
                    value  : this.props.items
                });
            }
        }
    }

    handleInput(event) {
        this.setState({
            value: event.target.value
        });
    }

    render() {
        // Add parameters dynamically for the field will be used in the forms
        let classData = (this.props.disable == "true") ? this.props.classData + " disabled" : this.props.classData;
        //let propsInput = {}, dynamicClass, parameters, classUpdated, defaultValueText, iconTags = '';
        //defaultValueText = (this.props.items != 'undefined') ? this.props.items : "";
        //dynamicClass = (this.props.classData != "") ? this.props.classData  : "";
        //classUpdated = (this.props.is_update == "true" && this.state.value != "") ? "thin_font active" : "thin_font";
        //if (this.props.disable     == "true") propsInput.disabled    = true;
        //if (this.props.require     == "true") propsInput.required    = true;
        //if (this.props.defaultText == "true") propsInput.placeholder = " ";
        //if (this.props.iconTags != undefined) iconTags = <i className="material-icons prefix">{this.props.iconTags}</i>;
        //let hideProp = (this.props.hide) ?  "hide" : "";
        //parameters = "row " + hideProp;

        let buttons  = '', classDiv;
        let flag     = (this.props.specialInput == undefined) ? 0 : this.props.specialInput;
        let disabled = (this.props.disable == 'true' || this.props.disable == true) ? 'disabled' : '';

        if (this.props.require == "false" || this.props.require == false) {
            classDiv = 'field';
        } else {
            classDiv = 'field required';
        }
        let finalClass = classDiv + disabled;

        if (flag == 0) {
            buttons =   <div className={classDiv}>
                            <label>{this.props.labelName}</label>
                            <div className={classData}>
                                <i className={this.props.iconTags}></i>
                                <input name={this.props.name}
                                       id={this.props.name}
                                       value={this.state.value}
                                       placeholder={this.props.defaultText}
                                       className={this.props.inputClass}
                                       onChange={this.handleInput}
                                       onBlur={this.onBlur}
                                       type="text" />
                            </div>
                        </div>;
        } else {
            buttons =   <div className={finalClass}>
                            <label>{this.props.labelName}</label>
                            <div className="ui right labeled input">
                                <label htmlFor="amount" className="ui label">$</label>
                                <input name={this.props.name}
                                       id={this.props.name}
                                       value={this.state.value}
                                       placeholder={this.props.defaultText}
                                       className={this.props.inputClass}
                                       onChange={this.handleInput}
                                       onBlur={this.onBlur}
                                       type="text" />
                            </div>
                        </div>;
        }

        return (
            buttons
        );
    }
}
