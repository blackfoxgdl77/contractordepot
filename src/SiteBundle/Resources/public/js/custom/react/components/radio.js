/**
 * Class contains all the functionality, actions
 * and events that will be executed in this component
 * that will be used on the platform
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */
class RadioButton extends React.Component {
    constructor(props) {
        super(props);

        this.getValues   = this.getValues.bind(this);
        this.handleInput = this.handleInput.bind(this);

        this.state = {
            flag  : 0,
            value : "",
            label : this.props.name
        };
    }

    componentDidMount = () => {
        $('.ui.radio.checkbox').checkbox();
    }

    componentDidUpdate () {
        if (this.state.value != "" && this.state.flag == 1) {
            this.setState({ flag: 0});
            this.props.handleFormFields(this.state);
        }
    }

    handleInput = (event) => {
        this.setState({
            value : event.target.value,
            flag  : 1
        });
    }

    getValues() {
        let arrayOptions = [], radioValues, checkedValue = [], flagVal = 0;

        if (this.props.setVal) {
            for (let key in this.props.optionsValues) {
                if (this.props.optionsValues.hasOwnProperty(key)) {

                    if (this.props.setVal && flagVal == 0) {
                        if (this.props.setVal == 1 || this.props.setVal == '1') {
                            checkedValue[0] = 'checked';
                            checkedValue[1] = 'none';
                            flagVal = 1;
                        } else {
                            checkedValue[0] = 'none';
                            checkedValue[1] = 'checked';
                            flagVal = 1;
                        }
                    }

                    arrayOptions.push({
                        key         : key,
                        value       : this.props.optionsValues[key].id,
                        label       : this.props.optionsValues[key].value,
                        checkedData : checkedValue[key]
                    });
                }
            }

            radioValues = arrayOptions.map((values) =>
                <div className="field" key={values.key}>
                    <input type="radio"
                           name={this.props.name}
                           id={values.key}
                           value={values.value}
                           onChange={this.handleInput}
                           checked={(values.checkedData == 'none') ? "" : "checked" } />
                    <label htmlFor={values.key} className="thin_font">{values.label}</label>
                </div>
            );
            flagVal = 0;
        } else {
            for (let key in this.props.optionsValues) {
                if (this.props.optionsValues.hasOwnProperty(key)) {

                    arrayOptions.push({
                        key         : key,
                        value       : this.props.optionsValues[key].id,
                        label       : this.props.optionsValues[key].value
                    });
                }
            }

            radioValues = arrayOptions.map((values) =>
                <div className="field" key={values.key}>
                    <input type="radio"
                           name={this.props.name}
                           id={values.key}
                           value={values.value}
                           onChange={this.handleInput} />
                    <label htmlFor={values.key} className="thin_font">{values.label}</label>
                </div>
            );
        }

        return radioValues;
    }

    render() {
        //var propsInput = {}, dynamicClass, optionsValues, parameters;
        //dynamicClass = (this.props.classData != "") ? this.props.classData  : "";
        //if (this.props.disable == "true")     propsInput.disabled    = true;
        ////if (this.props.require == "true")     propsInput.required    = true;
        //if (this.props.defaultText == "true") propsInput.placeholder = "";
        //let hideProp = (this.props.hide) ?  "hide" : "";
        //parameters = "row " + hideProp;

        let optionsValues = this.getValues();

        return (
            <div className="inline fields">
                <label>
                    {this.props.labelRadio}
                </label>
                {optionsValues}
            </div>
        );
    }
}
