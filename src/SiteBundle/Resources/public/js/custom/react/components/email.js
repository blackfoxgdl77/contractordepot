/**
 * Class contais email component that can be used
 * as single element in the forms and can be used when
 * is required in the forms
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */
class InputEmail extends React.Component {
    constructor(props) {
        super(props);

        this.handleEmail = this.handleEmail.bind(this);
        this.onBlur      = this.onBlur.bind(this);

        this.state = {
            value  : '',
            label  : '',
            active : 0
        };
    }

    onBlur(evt) {
        this.props.handleFormFields(this.state);
    }

    handleEmail(event) {
        this.setState({
            value : event.target.value,
            label : event.target.name
        });
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.isEmpty == 1) {
            this.setState({
                value : '',
            });
        }

        if (this.state.active == 0 && typeof prevProps.items !== "undefined") {
            this.setState({
                active : 1,
                value  : prevProps.items
            });
        }
    }

    render() {
        // Add parameters dynamically for the field will be used in the forms
        //var propsInput = {}, dynamicClass, parameters;
        //dynamicClass = (this.props.classData != "") ? this.props.classData : "";
        //if (this.props.disable == "true")     propsInput.disabled = true;
        //if (this.props.require == "true")     propsInput.required = true;
        //if (this.props.defaultText == "true") propsInput.placeholder = "";
        //let hideProp = (this.props.hide) ?  "hide" : "";
        //parameters = "row " + hideProp;
        let classDiv = '';

        if (this.props.require == 'false' || this.props.require == false) {
            classDiv = 'field';
        } else {
            classDiv = 'field required';
        }

        return (
            <div className={classDiv}>
                <label>{this.props.labelEmail}</label>
                <div className={this.props.classData}>
                    <i className={this.props.iconTags}></i>
                    <input type="email"
                           name={this.props.name}
                           id={this.props.name}
                           value={this.state.value}
                           placeholder={this.props.defaultText}
                           className={this.props.inputClass}
                           onChange={this.handleEmail}
                           onBlur={this.onBlur} />
                </div>
            </div>
        );
    }
}
