class PostEdit extends React.Component {
    constructor(props) {
        super(props);

        this.handleInput       = this.handleInput.bind(this);
        this.saveNewRecord     = this.saveNewRecord.bind(this);
        this.validateData      = this.validateData.bind(this);
        this.closeClick        = this.closeClick.bind(this);
        this.closeClickSuccess = this.closeClickSuccess.bind(this);
        this.closeClickE       = this.closeClickE.bind(this);
        this.changeFlagState   = this.changeFlagState.bind(this);

        this.state = {
            validating       : 0,
            'successMessage' : false,
            'inputError'     : [],
            'counter'        : 0,
            errorMessage     : 0,
            respMessage      : '',
            clearValue       : 0,
            token            : props.token,
            source           : props.source,
            nameToken        : props.nameT,
            windowPath       : (window.$PATH !== undefined) ? window.$PATH : ""
        };

    }

    changeFlagState = () => {
        if (this.state.clearValue == 1) {
            this.setState({
                clearValue : 0,
            });
        }
    }

    componentDidUpdate = () => {
        this.changeFlagState();
    }

    handleInput(data) {
        if (data.label != '') {
            this.setState({
                [data.label]: data.value
            });
        }
    }

    validateData = () => {

        let counter = 0;
        let errors  = {};

        if (this.state.postname === "" || this.state.postname === undefined) {
            errors['postname'] = "Please, type a postname";
            counter++;
        } else {
            errors['postname'] = "";
        }

        if (this.state.description === "" || this.state.description === undefined) {
            errors['description'] = "Please, type a description";
            counter++;
        } else {
            errors['description'] = "";
        }

        if (parseInt(this.state.city) === 0 || this.state.city === undefined) {
            errors['city'] = "Please, select a city";
            counter++;
        } else {
            errors['city'] = "";
        }

        if (parseInt(this.state.category) === 0 || this.state.category === undefined) {
            errors['category'] = "Please, select a category";
            counter++;
        } else {
            errors['category'] = "";
        }

        this.setState({
            inputError: errors,
            counter: counter
        });

        return counter;
    }

    saveNewRecord = (evt) => {

        evt.preventDefault();

        this.setState({
            validating : 1
        });

        let validate    = 0;
        let postSaveUrl = this.state.windowPath + "/api/post/data"; // "/api/save/posts/0";
        console.log(postSaveUrl);
        //validate        = this.validateData();

        //if (validate === 0) {
            let dataForm = new FormData();
            let fileList = (document.getElementById('postImage') === undefined ||
                document.getElementById('postImage') == null ||
                documentdocument.getElementById('postImage').files === undefined) ? null : document.getElementById('postImage').files;

            dataForm.append("postName", this.state.postname);
            dataForm.append("description", this.state.description);
            dataForm.append("city", this.state.city);
            dataForm.append("jobCategory", this.state.category);
            dataForm.append("user", this.props.id);
            dataForm.append('token', this.state.token);
            dataForm.append('source', this.state.source);
            dataForm.append('nameT', this.state.nameToken);

            //if (this.state.postPayment != '') {
                dataForm.append('post', this.state.postPayment);
            //}
            console.log(this);
            // Add files to request
            if (fileList != undefined && fileList != null && fileList.length > 0) {
                for(var x=0;x<fileList.length;x++) {
                    dataForm.append('image'+x, fileList[x]);
                }
            }

            axios.post(postSaveUrl, JSON.stringify({
                    posts: this.state.postPayment,
                    jobCategory : this.state.category,
                    description: this.state.description,
                })).then((response) => {
                    console.log(response);
                });

        /*    axios.post(postSaveUrl, JSON.stringify({
                post: this.state.postPayment,
                jobCategory : this.state.category,
                description: this.state.description,
            }), {
                headers: {
                  'Content-Type': 'multipart/form-data'
                }}).then((response) => {
                    if (response.data.status == 'OK' && response.status == 200) {
                        this.setState({
                            clearValue       : 1,
                            respMessage      : response.data.data.message,
                            'successMessage' : true,
                            validating       : 0
                        });
                    }

                    if (response.data.status == 'ERROR' && response.status == 200) {
                        this.setState({
                            respMessage  : response.data.data.message,
                            errorMessage : 1,
                            validating   : 0
                        });
                    }
            }).catch((error) => {
                this.setState({
                    respMessage  : error.data.data.message,
                    errorMessage : 1,
                    validating   : 0,
                });
            });*/
        //}

    }

    componentDidMount() {
        // Nothing defined
    }

    componentWillMount() {
        let postRetrieveUrl        = this.state.windowPath + '/api/posts/' + this.props.post;
        let cityRetrieveAllUrl     = this.state.windowPath + '/api/cities';
        let categoryRetrieveAllUrl = this.state.windowPath + '/api/jobCategories';

        // Validate if post id is setup
        if (this.props.post != undefined && this.props.post != null && this.props.post > 0) {

            // Retrieving post inserted information
            axios.get(postRetrieveUrl)
                .then((response) => {

                    if (response.status == 200 && response.data.status == 'OK') {
                        this.setState({
                            post : response.data.data
                        });
                    }

                    if (response.status == 200 && response.data.status == 'ERROR') {
                        this.setState({
                            respMessage  : response.data.data.message,
                            errorMessage : 1,
                            validating   : 0,
                        });
                    }
                }).catch((error) => {
                    this.setState({
                        respMessage  : response.data.data.message,
                        errorMessage : 1,
                        validating   : 0,
                    });
                });

        }

        // Retrieving cities
        axios.get(cityRetrieveAllUrl)
            .then((response) => {
                if (response.status == 200 && response.data.status == 'OK') {
                    this.setState({
                        cities : response.data.data
                    });
                }

                if (response.status == 200 && response.data.status == 'ERROR') {
                    this.setState({
                        respMessage  : response.data.data.message,
                        errorMessage : 1,
                        validating   : 0
                    });
                }
            }).catch((error) => {
                this.setState({
                    respMessage  : error.data.data.message,
                    errorMessage : 1,
                    validating   : 0,
                });
            });


        // Retrieving job categories
        axios.get(categoryRetrieveAllUrl)
            .then((response) => {

                if (response.status == 200 && response.data.status == 'OK') {
                    this.setState({
                        jobCategories : response.data.data
                    });
                }

                if (response.status == 200 && response.data.status == 'ERROR') {
                    this.setState({
                        respMessage  : response.data.data.message,
                        errorMessage : 1,
                        validating   : 0,
                    });
                }
            }).catch((error) => {
                this.setState({
                    respMessage  : error.data.data.message,
                    errorMessage : 1,
                    validating   : 0,
                });
            });

    }

    closeClick = () => {
        this.setState({
            counter : 0
        });
    }

    closeClickSuccess = () => {
        this.setState({
            successMessage : false
        });
    }

    closeClickE = () => {
        this.setState({
            errorMessage : 0,
            respMessage  : '',
        });
    }

    render() {
        if (this.state.cities == undefined || this.state.cities == null || this.state.cities.length == 0 ||
            this.state.jobCategories == undefined || this.state.jobCategories == null || this.state.jobCategories.length == 0 ||
            this.state.validating == 1) {
            return (
                <div className="loader"></div>
            );
        } else {
            let errorTemplate = (this.state.counter != 0) ? <Errors errors={this.state.inputError} onClick={this.closeClick} /> : "";
            let errorResp = (this.state.errorMessage != 0) ? <Errors errors="" respError={this.state.respMessage}
                                                                    onClick={this.closeClickE} /> : "";

            // TO-DO: Check this part of code to improve the code, maintability and functionality
            let postForm = '';
            if (this.props.post != 0) {
                postForm = <div>
                        {errorTemplate}

                        <div className="modal"></div>
                        <div className="row">
                            <div className="col s12">
                                <form>

                                    <Input name="publishedBy" disable="true" labelName="Published By" iconTags="assignment_id"
                                           handleFormFields={this.handleInput} defaultText="false" require="false" id="publishedBy"
                                           classData="input-field col s8 offset-s2" isEmpty={this.state.clearValue}
                                           ref={(publishedBy) => { this.publishedBy = publishedBy }}
                                           is_update="true" items={ this.props.user } />

                                    <Input name="postname" disable="false" labelName="Postname" iconTags="create" is_update="true"
                                           handleFormFields={this.handleInput} defaultText="true" require="true" id="postname"
                                           classData="input-field col s8 offset-s2"
                                           items={this.state.post.postname} isEmpty={this.state.clearValue}
                                           ref={(postname) => { this.postname = postname }} />

                                    <TextArea name="description" disable="false" require="true" defaultText="true" is_update="true"
                                            iconTags="description" handleFormFields={this.handleInput} labelName="Description"
                                            classData="input-field col s9 offset-s1" classCustom="" rowCustom="left_move_textarea"
                                            items={this.state.post.description} isEmpty={this.state.clearValue}
                                            ref={(description) => { this.description = description }} />

                                    <Input name="payments" disable="false" labelName="Initial Bid (Optional)" require="false"
                                            defaultText="true" is_update="true" iconTags="attach_money" id="payments"
                                            handleFormFields={this.handleInput} isEmpty={this.state.clearValue}
                                            classData="input-field col s8 offset-s2" items={this.state.post.postPayment}
                                            ref={(postPayment) => { this.postPayment = postPayment }} />

                                    <Select name="city" disable="false" labelSelectName="City" iconTags="format_list_numbered"
                                            require="true" handleFormFields={this.handleInput} defaultText="false"
                                            classData="input-field col s8 offset-s2"
                                            displayDataValues={this.state.cities} id="cityID"
                                            value={this.state.post.city_id}
                                            ref={(city) => { this.city = city }} />

                                    <Select name="category" disable="false" labelSelectName="Category" iconTags="format_list_numbered"
                                            require="true" handleFormFields={this.handleInput} defaultText="false"
                                            classData="input-field col s8 offset-s2"
                                            value={this.state.post.category_id}
                                            displayDataValues={this.state.jobCategories} id="categoryID"
                                            ref={(category) => { this.category = category }} />

                                    <InputFile name="postImage" labelButton="File" handleFormFields={this.handleInput}
                                            iconTags="insert_photo" classData="file-field input-field col s8 offset-s2"
                                            isEmpty={this.state.clearValue}
                                            ref={(postImage) => { this.postImage = postImage }} />

                                    <div className="row center">
                                        <div className="col s12">
                                            <button className="btn waves-effect waves-light" type="submit" onClick={this.saveNewRecord}>
                                                Save
                                                <i className="material-icons right">send</i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>;
            } else {
                postForm = <div>
                        {errorTemplate}

                        <div className="modal"></div>
                        <div className="row">
                            <div className="col s12">
                                <form>

                                    <Input name="publishedBy" disable="true" labelName="Published By" iconTags="assignment_id"
                                           handleFormFields={this.handleInput} defaultText="false" require="false" id="publishedBy"
                                           classData="input-field col s8 offset-s2"
                                           ref={(publishedBy) => { this.publishedBy = publishedBy }}
                                           is_update="true" items={ this.props.user } />

                                    <Input name="postname" disable="false" labelName="Postname" iconTags="create" is_update="false"
                                           handleFormFields={this.handleInput} defaultText="true" require="true" id="postname"
                                           classData="input-field col s8 offset-s2"
                                           ref={(postname) => { this.postname = postname }} />

                                    <TextArea name="description" disable="false" require="true" defaultText="true" is_update="false"
                                            iconTags="description" handleFormFields={this.handleInput} labelName="Description"
                                            classData="input-field col s9 offset-s1" classCustom="" rowCustom="left_move_textarea"
                                            ref={(description) => { this.description = description }} />

                                    <Input name="payments" disable="false" labelName="Initial Bid (Optional)" iconTags="attach_money"
                                            handleFormFields={this.handleInput} defaultText="false" require="false" id="payments"
                                            classData="input-field col s8 offset-s2"
                                            ref={(postPayment) => { this.postPayment = postPayment }} />

                                    <Select name="city" disable="false" labelSelectName="City" iconTags="format_list_numbered"
                                            require="true" handleFormFields={this.handleInput} defaultText="false"
                                            classData="input-field col s8 offset-s2"
                                            displayDataValues={this.state.cities} id="cityID"
                                            ref={(city) => { this.city = city }} />

                                    <Select name="category" disable="false" labelSelectName="Category" iconTags="format_list_numbered"
                                            require="true" handleFormFields={this.handleInput} defaultText="false"
                                            classData="input-field col s8 offset-s2"
                                            displayDataValues={this.state.jobCategories} id="categoryID"
                                            ref={(category) => { this.category = category }} />

                                    <InputFile name="postImage" labelButton="File" handleFormFields={this.handleInput}
                                            iconTags="insert_photo" classData="file-field input-field col s8 offset-s2"
                                            ref={(postImage) => { this.postImage = postImage }} />

                                    <div className="row center">
                                        <div className="col s12">
                                            <button className="btn waves-effect waves-light" type="submit" onClick={this.saveNewRecord}>
                                                Save
                                                <i className="material-icons right">send</i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>;
            }

            let contentTrue = <SuccessTemplate successTitle="The post has been created successfully"
                                       successMessage="To continue do click on 'X'"
                                       extraFlag="1" onClick={this.closeClickSuccess} />;

            return (this.state.successMessage == true) ? contentTrue : postForm;
        }

    }
}
