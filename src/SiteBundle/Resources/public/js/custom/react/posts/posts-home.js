

class PostsHome extends React.Component {
	render() {
		return (

			<div className="container">
				<div className="row">
					<div className="col s4 m5">
					    <h5 className="header">Post a New Project</h5>
			    		<div className="card horizontal">
			      			<div className="card-image">
			      				<i className="large material-icons">add_circle</i>
			      			</div>
			      			<div className="card-stacked">
			        			<div className="card-content">
			          				<p>Create new offers based on what you need.</p>
			        			</div>
			        			<div className="card-action">
			          				<a href="/post/new">Create</a>
			        			</div>
			      			</div>
			    		</div>
			  		</div>

			  		<div className="col s4 m5">
					    <h5 className="header">My Projects</h5>
			    		<div className="card horizontal">
			      			<div className="card-image">
			      				<i className="large material-icons">assignment_ind</i>
			      			</div>
			      			<div className="card-stacked">
			        			<div className="card-content">
			          				<p>Create new offers based on what you need.</p>
			        			</div>
			        			<div className="card-action">
			          				<a href="/posts/dashboard">Access</a>
			        			</div>
			      			</div>
			    		</div>
			  		</div>
			  	</div>
			  	<div className="row">
			  		<div className="col s4 m5">
					    <h5 className="header">Search for a Project</h5>
			    		<div className="card horizontal">
			      			<div className="card-image">
			        			<i className="large material-icons">find_in_page</i>
			      			</div>
			      			<div className="card-stacked">
			        			<div className="card-content">
			          				<p>Create new offers based on what you need.</p>
			        			</div>
			        			<div className="card-action">
			          				<a href="/posts/search">Search</a>
			        			</div>
			      			</div>
			    		</div>
			  		</div>

			  		<div className="col s4 m5">
					    <h5 className="header">Watching Projects</h5>
			    		<div className="card horizontal">
			      			<div className="card-image">
			        			<i className="large material-icons">assignment_turned_in</i>
			      			</div>
			      			<div className="card-stacked">
			        			<div className="card-content">
			          				<p>Create new offers based on what you need.</p>
			        			</div>
			        			<div className="card-action">
			          				<a href="/posts/search/user/1">Watch</a>
			        			</div>
			      			</div>
			    		</div>
			  		</div>
				</div>
			</div>
		);
	}
}