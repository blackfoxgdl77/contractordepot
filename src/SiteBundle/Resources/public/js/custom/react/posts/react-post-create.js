ReactDOM.render(
    <PostNew user={ document.getElementById('user_username').value }
  	         id={ document.getElementById('user_id').value }
             token={ document.getElementById('_csrf_token').value }
             source={ document.getElementById('source').value }
             nameT={ document.getElementById('name').value } />,
    document.getElementById('app')
);
