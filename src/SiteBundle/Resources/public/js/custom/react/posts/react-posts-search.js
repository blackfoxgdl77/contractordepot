ReactDOM.render(
  <PostsSearch userId={ document.getElementById('user_id').value }
               token={ document.getElementById('_csrf_token').value }
               source={ document.getElementById('source').value }
               nameT={ document.getElementById('name').value } />,
  document.getElementById('app')
);
