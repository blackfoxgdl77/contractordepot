// Const for modal
const display = {
    display: 'block'
};
const hide = {
    display: 'none'
};

class PostsSearch extends React.Component {

	constructor(props) {
        super(props);

		this.toggleBind        = this.toggleBind.bind(this);
		this.reportFake        = this.reportFake.bind(this);
		this.reportProject     = this.reportProject.bind(this);
        this.closeClickE       = this.closeClickE.bind(this);
        this.closeClickSuccess = this.closeClickSuccess.bind(this);
        this.createSearchForm  = this.createSearchForm.bind(this);
        this.showSearchForm    = this.showSearchForm.bind(this);
        this.hideForm          = this.hideForm.bind(this);
        this.searchRecords     = this.searchRecords.bind(this);
        this.handleInput       = this.handleInput.bind(this);
        this.displayProjects   = this.displayProjects.bind(this);
        this.displayAllJobs    = this.displayAllJobs.bind(this);
        this.filterByCategory  = this.filterByCategory.bind(this);
        this.noPostsFunction   = this.noPostsFunction.bind(this);
        this.changeFlagState   = this.changeFlagState.bind(this);

        this.state = {
            jobs           : [],
            showForm       : false,
            buttonFormShow : true,
			toggle         : false,
            successMsj     : '',
            successEnable  : 0,
            errorMessage   : '',
            errorEnable    : 0,
			fake           : {},
            totalRows      : 0,
            clearValue     : 0,
            token          : props.token,
            source         : props.source,
            nameToken      : props.nameT,
            windowPath     : (window.$PATH !== undefined) ? window.$PATH : ""
        };
    }

    changeFlagState = () => {
        if (this.state.clearValue == 1) {
            this.setState({
                clearValue : 0,
            });
        }
    }

    componentDidUpdate = () => {
        this.changeFlagState();
    }

    handleInput(data) {
        if (data.label != '') {
            this.setState({
                [data.label]: data.value
            });
        }
    }

    showSearchForm = (evt) => {
        evt.preventDefault();

        this.setState({
            showForm       : true,
            buttonFormShow : false
        });
    }

    hideForm = (evt) => {
        evt.preventDefault();

        this.setState({
            buttonFormShow : true,
            showForm       : false
        });
    }

    toggleBind = (data, evt) => {
        evt.preventDefault();

        this.setState(prevState => ({
            toggle        : !prevState.toggle,
            fake          : data,
            errorEnable   : 0,
            errorMessage  : '',
            successEnable : 0,
            successMsj    : '',
        }));
    }

    closeClickE = () => {
        this.setState({
            errorEnable  : 0,
            errorMessage : '',
        });
    }

    closeClickSuccess = () => {
        this.setState({
            successMsj    : '',
            successEnable : 0,
        });
    }

    displayAllJobs = () => {
        let jobsLists = this.state.jobs.map((job) => {
            return <div key={job.id_job}>
                        <div className="col s12 left thin_font blue_color">
                            <a href="#" onClick={(e) => this.filterByCategory(job, e)}>
                                {job.name_job} ({job.total_jobs})
                            </a>
                        </div>
                   </div>
        });

        return jobsLists;
    }

    filterByCategory = (job, e) => {
        e.preventDefault();
        let url = this.state.windowPath + '/api/job/category/' + job.id_job;

        axios.get(url)
             .then((response) => {
                 if (response.status == 200 && response.data.status == 'OK') {
                     this.setState({
                         posts     : response.data.data.data,
                         totalRows : response.data.data.total,
                     });
                 }

                 if (response.status == 200 && response.data.status == 'ERROR') {
                     this.setState({
                         errorEnable  : 1,
                         errorMessage : response.data.data.message,
                     });
                 }
             })
             .catch((error) => {
                 this.setState({
                     errorEnable  : 1,
                     errorMessage : response.data.data.message,
                 });
             });
    }

    reportFake = () => {
		let styleData       = (this.state.toggle == true) ? display : hide;
        let errorTemplate   = (this.state.errorEnable == 1) ? <Errors errors="" respError={this.state.errorMessage}
                                                                      onClick={this.closeClickE} /> : "";
        let successTemplate = (this.state.successEnable == 1) ? <SuccessTemplate successTitle={this.state.successMsj}
                                                                  successMessage="" extraFlag="0" onClick={this.closeClickSuccess} /> : "";

        let modalData = <div key={this.state.fake.id}>
                            <div className="modal margin_modal_react" key={this.state.fake.id} style={styleData}>
                                <div className="modal-content">
                                    <div>
                                        {errorTemplate}
                                        {successTemplate}
                                    </div>
                                    <div className="col s5 left thin_font blue_color">
                                        Post > Search > Report Project
                                    </div>
                                    <div className="col s7">
                                        <h3 className="right-align thin_font blue_color">
                                            Report Project
                                        </h3>
                                    </div>
                                    <div className="justify_align thin_font">
                                        Are you sure you want to report the projects <b>'{this.state.fake.postname}'</b> as fake?
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <a href="#!" className="btn modal-action modal-close waves-effect waves-green btn-flat" type="button"
                                       onClick={(e) => this.reportProject(e)}>
                                        Report the Project!
                                    </a>
                                    <a href="#!" className="btn modal-action modal-close waves-effect waves-green btn-flat" type="button"
                                       onClick={(e) => this.toggleBind(0, e)}>
                                        Close
                                    </a>
                                </div>
                            </div>
                        </div>;

		return modalData;
	}

	reportProject = (evt) => {
		evt.preventDefault();

        let url = this.state.windowPath + "/api/post/fake/project";
        let postData = {
            projectId : this.state.fake.id,
            userId    : this.props.userId,
            source    : this.state.source,
            token     : this.state.token,
            nameT     : this.state.nameToken,
        };

        axios.post(url, postData)
             .then((response) => {
                 if (response.data.status == 'OK' && response.status == 200) {
                     this.setState({
                         successEnable : 1,
                         successMsj    : response.data.data.message,
                     });
                 }

                 if (response.data.status == 'ERROR' && response.status == 200) {
                     this.setState({
                         errorEnable  : 1,
                         errorMessage : response.data.data.message,
                     });
                 }
             })
             .catch((error) => {
                 this.setState({
                     errorEnable  : 1,
                     errorMessage : error.data.data.message,
                 });
             });
	}

    createSearchForm = () => {
        if (this.state.cities == undefined || this.state.cities == null ||
            this.state.jobCategories == undefined || this.state.jobCategories == null ) {
            return (
                <div className="loader"></div>
            );
        } else if (this.state.cities.length == 0) {
            return (
                <h5>There are not Cities!</h5>
            );
        } else if (this.state.jobCategories.length == 0) {
            return (
                <h5>There are not Categories!</h5>
            );
        } else {

            return (
                <form>
                    <div className="row">
                        <div className="col s12">

                                <Input name="postname" disable="false" labelName="Postname" iconTags="create"
                                       handleFormFields={this.handleInput} defaultText="false" require="true" id="postname"
                                       classData="input-field col s8 offset-s2" isEmpty={this.state.clearValue}
                                       ref={(postname) => { this.postname = postname }} />

                                <Select name="city" disable="false" labelSelectName="City" iconTags="format_list_numbered"
                                        require="true" handleFormFields={this.handleInput} defaultText="false"
                                        classData="input-field col s8 offset-s2"
                                        displayDataValues={this.state.cities} id="cityID"
                                        ref={(city) => { this.city = city }} />

                                <Select name="category" disable="false" labelSelectName="Category"
                                        iconTags="format_list_numbered" require="true"
                                        handleFormFields={this.handleInput} defaultText="false"
                                        classData="input-field col s8 offset-s2"
                                        displayDataValues={this.state.jobCategories} id="categoryID"
                                        ref={(category) => { this.category = category }} />

                                <div className="row center">
                                    <div className="col s12">
                                        <button className="btn waves-effect waves-light" type="submit" onClick={this.searchRecords}>
                                            Search
                                            <i className="large material-icons">find_in_page</i>
                                        </button>
                                    </div>
                                </div>

                        </div>
                    </div>
                </form>
            );
        }
    }

    searchRecords = (evt) => {

        evt.preventDefault();

        let url = this.state.windowPath + '/api/posts';
        let queryParams = "";

        if (this.state.postname != undefined && this.state.postname != null && this.state.postname != "") {
            queryParams = queryParams + "postname=" + this.state.postname;
        }
        if (this.state.category != undefined && this.state.category != null && this.state.category != "") {
            if (queryParams != "") {
                queryParams = queryParams + "&";
            }
            queryParams = queryParams + "category=" + this.state.category;
        }
        if (this.state.city != undefined && this.state.city != null && this.state.city != "") {
            if (queryParams != "") {
                queryParams = queryParams + "&";
            }
            queryParams = queryParams + "city=" + this.state.city;
        }

        if (queryParams != "") {
            queryParams = "?" + queryParams;
            url = url + queryParams;
        }

        // Retrieving posts information
        axios.get(url)
            .then((response) => {
                this.setState({
                    posts      : response.data.data,
                    clearValue : 1,
                });

            }).catch((error) => {
                console.log("Error: " + error);
            });

    }

    componentWillMount() {

    	let citiesRetrieveAllUrl     = this.state.windowPath + '/api/cities';
    	let categoriesRetrieveAllUrl = this.state.windowPath + '/api/jobCategories';
    	let postRetrieveAllUrl       = this.state.windowPath + '/api/posts';
        let jobsCategories           = this.state.windowPath + '/api/job/categories/per/post';

    	// Retrieving cities
        axios.get(citiesRetrieveAllUrl)
            .then((response) => {
                if (response.status == 200 && response.data.status == 'OK') {
                    this.setState({
                        cities : response.data.data
                    });
                }

                if (response.status == 200 && response.data.status == 'ERROR') {
                    this.setState({
                        errorEnable  : 1,
                        errorMessage : response.data.data.message,
                    });
                }
            }).catch((error) => {
                this.setState({
                    errorEnable  : 1,
                    errorMessage : error.data.data.message,
                });
            });

        // Retrieving job categories
        axios.get(categoriesRetrieveAllUrl)
            .then((response) => {
                if (response.data.status == 'OK' && response.status == 200) {
                    this.setState({
                        jobCategories : response.data.data
                    });
                }

                if (response.status == 200 && response.data.status == 'ERROR') {
                    this.setState({
                        errorEnable  : 1,
                        errorMessage : response.data.data.message,
                    });
                }
            }).catch((error) => {
                this.setState({
                    errorEnable  : 1,
                    errorMessage : error.data.data.message,
                });
            });

        // Retrieving posts information
        axios.get(postRetrieveAllUrl)
            .then((response) => {
                if (response.status == 200 && response.data.status == 'OK') {
                    this.setState({
                        posts : response.data.data
                    });
                }

                if (response.status == 200 && response.data.status == 'ERROR') {
                    this.setState({
                        errorEnable  : 1,
                        errorMessage : response.data.data.message,
                    });
                }

            }).catch((error) => {
                this.setState({
                    errorEnable  : 1,
                    errorMessage : error.data.data.message,
                });
            });

        axios.get(jobsCategories)
             .then((response) => {
                 if (response.status == 200 && response.data.status == 'OK') {
                     this.setState({
                         jobs : response.data.data.jobs,
                     });
                 }

                 if (response.status == 200 && response.data.status == 'ERROR') {
                     this.setState({
                         errorEnable  : 1,
                         errorMessage : response.data.data.message,
                     });
                 }
             })
             .catch((error) => {
                 this.setState({
                     errorEnable  : 1,
                     errorMessage : error.data.data.message,
                 });
             });
    }

    displayProjects = () => {
        let postsData = this.state.posts.map((project) => {
            let reportButton = '';
            if (this.props.userId != 0 || this.props.userId != '0') {
                reportButton = <a href="#" onClick={(e) => this.toggleBind(project, e) }>
                                  Report Project
                               </a>;
            }

            return <div className="col s6" key={project.id}>
                <div className="card small hoverable">
                    <div className="card-image waves-effect waves-block waves-light">
                        <img className="activator" src={this.state.windowPath + project.image} />
                    </div>
                    <div className="card-content">
                        <span className="card-title activator thin_font blue_color">
                            {project.postname} <i className="material-icons right">more_vert</i>
                        </span>
                    </div>
                    <div className="card-action">
                        {reportButton}
                        <a  href={"/post/" + project.id}>
                            Project Details
                        </a>
                    </div>
                    <div className="card-reveal">
                        <span className="card-title thin_font blue_color">
                            <i className="material-icons right">close</i>
                            {project.postname}
                        </span>
                        <p className="thin_font blue_color">
                            Category: {project.category}
                        </p>
                        <p className="thin_font blue_color">
                            City: {project.city}
                        </p>
                        <p className="thin_font blue_color">
                            Published by: {project.publishedBy}
                        </p>
                        <p className="thin_font blue_color">
                            {project.description}
                        </p>
                    </div>
                </div>
            </div>
         });

        return postsData;
    }

    noPostsFunction = () => {
        let templateNoPosts = <div className="color_text_no_data center gross_font">
                                <h3> There are no projects for this category!</h3>
                                <br />
                                <h5>Please search projects in another category</h5>
                              </div>;

        return templateNoPosts;
    }

    // TO-DO: Mejorar esta function hacerla o intentar hacerla mas mantenible
	render() {
		let modalInfo, modal = [];

		if (this.state.posts == undefined || this.state.posts == null) {
            return (
                <div className="loader"></div>
            );
        } else {
			if (this.state.toggle == true) {
				document.getElementById('overlay').style.display = "block";
				modalInfo = this.reportFake();
			} else {
				document.getElementById('overlay').style.display = "none";
				modalInfo = '';
			}
			modal.push(modalInfo);

            let buttonForm = "";
            let showForm = "";

            if (this.state.showForm == true) {
                showForm = this.createSearchForm();
            }

            // Verify if the button that will be displayed is for show or hide the form
            if (this.state.buttonFormShow == true) {
                buttonForm = <div className="col s3 offset-s9 margin-top-two">
                                <a className="waves-effect waves-light btn-large green" onClick={this.showSearchForm}>
                                    <i className="material-icons left">add</i>
                                    Open Search
                                </a>
                            </div>;
            } else {
                buttonForm = <div className="col s3 offset-s9 margin-top-two">
                                <a className="waves-effect waves-light btn-large red" onClick={this.hideForm}>
                                    <i className="material-icons left">remove</i>
                                    Hide Search
                                </a>
                            </div>;
            }

            // data to display
            let templateDatas = (this.state.posts.length == 0) ? this.noPostsFunction() : this.displayProjects();

			return (
				<div>
					{modal}

                    <div className="row">&nbsp;</div>

                    <div className="row">
                        {buttonForm}
                    </div>

                    <div>
                        {showForm}

                        <div className="row">
                            <div className="col s2">
                                <div className="col s12 left gross_font blue_color">
                                    Filter by Category
                                </div>
                                <div className="col s12">&nbsp;</div>
                                {this.displayAllJobs()}
                            </div>

                            <div className="col s10">
                                {templateDatas}
                            </div>
                        </div>
                    </div>
				</div>

			);
		}
	}
}
