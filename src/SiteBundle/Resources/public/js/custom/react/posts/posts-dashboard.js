class PostsDashboard extends React.Component {

	constructor(props) {
        super(props);

        this.createSearchForm  = this.createSearchForm.bind(this);
        this.showSearchForm    = this.showSearchForm.bind(this);
        this.hideForm          = this.hideForm.bind(this);
        this.searchRecords     = this.searchRecords.bind(this);
        this.handleInput       = this.handleInput.bind(this);
        this.displayProjects   = this.displayProjects.bind(this);
		this.changeFlagState   = this.changeFlagState.bind(this);

        this.state = {
        	showForm       : false,
            buttonFormShow : true,
			token		   : props.token,
			source		   : props.source,
			nameToken	   : props.nameT,
			clearValue	   : 0,
            windowPath     : (window.$PATH !== undefined) ? window.$PATH : ""
        };
    }

	changeFlagState = () => {
		if (this.state.clearValue == 1) {
			this.setState({
				clearValue : 0,
			});
		}
	}

	componentDidUpdate = () => {
		this.changeFlagState();
	}

    handleInput(data) {
        if (data.label != '') {
            this.setState({
                [data.label]: data.value
            });
        }
    }

    showSearchForm = (evt) => {
        evt.preventDefault();

        this.setState({
            showForm       : true,
            buttonFormShow : false
        });
    }

    hideForm = (evt) => {
        evt.preventDefault();

        this.setState({
            buttonFormShow : true,
            showForm       : false
        });
    }

    createSearchForm = () => {
        if (this.state.cities == undefined || this.state.cities == null ||
            this.state.jobCategories == undefined || this.state.jobCategories == null ) {
            return (
                <h5>Loading Posts Search component...</h5>
            );
        } else if (this.state.cities.length == 0) {
            return (
                <h5>There are not Cities!</h5>
            );
        } else if (this.state.jobCategories.length == 0) {
            return (
                <h5>There are not Categories!</h5>
            );
        } else {

            return (
                <form>
                    <div className="row">
                        <div className="col s12">

                                <Input name="postname" disable="false" labelName="Postname" iconTags="create"
                                       handleFormFields={this.handleInput} defaultText="false" require="true" id="postname"
                                       classData="input-field col s8 offset-s2" isEmpty={this.state.clearValue}
                                       ref={(postname) => { this.postname = postname }} />

                                <Select name="city" disable="false" labelSelectName="City" iconTags="format_list_numbered"
                                        require="true" handleFormFields={this.handleInput} defaultText="false"
                                        classData="input-field col s8 offset-s2"
                                        displayDataValues={this.state.cities} id="cityID"
                                        ref={(city) => { this.city = city }} />

                                <Select name="category" disable="false" labelSelectName="Category"
                                        iconTags="format_list_numbered" require="true"
                                        handleFormFields={this.handleInput} defaultText="false"
                                        classData="input-field col s8 offset-s2"
                                        displayDataValues={this.state.jobCategories} id="categoryID"
                                        ref={(category) => { this.category = category }} />

                                <div className="row center">
                                    <div className="col s12">
                                        <button className="btn waves-effect waves-light" type="submit" onClick={this.searchRecords}>
                                            Search
                                            <i className="large material-icons">find_in_page</i>
                                        </button>
                                    </div>
                                </div>

                        </div>
                    </div>
                </form>
            );
        }
    }

    searchRecords = (evt) => {

        evt.preventDefault();

        let url = this.state.windowPath + '/api/posts/user/' + this.props.id;
        let queryParams = "";

        if (this.state.postname != undefined && this.state.postname != null && this.state.postname != "") {
            queryParams = queryParams + "postname=" + this.state.postname;
        }
        if (this.state.category != undefined && this.state.category != null && this.state.category != "") {
            if (queryParams != "") {
                queryParams = queryParams + "&";
            }
            queryParams = queryParams + "category=" + this.state.category;
        }
        if (this.state.city != undefined && this.state.city != null && this.state.city != "") {
            if (queryParams != "") {
                queryParams = queryParams + "&";
            }
            queryParams = queryParams + "city=" + this.state.city;
        }

        if (queryParams != "") {
            queryParams = "?" + queryParams;
            url = url + queryParams;
        }

        // Retrieving posts information
        axios.get(url)
            .then((response) => {
                this.setState({
                    posts 	   : response.data.data,
					clearValue : 1,
                });

            }).catch((error) => {
                console.log("Error: " + error);
            });

    }

	componentDidMount = () => {
		let postUserRetrieveUrl = this.state.windowPath + '/api/posts/user/' + this.props.id;

		// Retrieving posts information
		axios.get(postUserRetrieveUrl)
			.then((response) => {
				if (response.status == 200 && response.data.status == 'OK') {
					this.setState({
						posts : response.data.data
					});
				}

				if (response.status == 200 && response.data.status == 'ERROR') {

				}
			}).catch((error) => {
				console.log("Error: " + error);
			});
	}

    componentWillMount = () => {

    	let citiesRetrieveAllUrl     = this.state.windowPath + '/api/cities';
    	let categoriesRetrieveAllUrl = this.state.windowPath + '/api/jobCategories';

    	// Retrieving cities
        axios.get(citiesRetrieveAllUrl)
            .then((response) => {
				if (response.status == '200' && response.data.status == 'OK') {
	                this.setState({
	                    cities : response.data.data
	                });
				}

				if (response.status == 200 && response.data.status == 'ERROR') {

				}
            }).catch((error) => {
            });

        // Retrieving job categories
        axios.get(categoriesRetrieveAllUrl)
            .then((response) => {
				if (response.status == 200 && response.data.status == 'OK') {
	                this.setState({
	                    jobCategories  : response.data.data
	                });
				}

				if (response.status == 200 && response.data.status == 'ERROR') {

				}

            }).catch((error) => {

            });
    }

    displayProjects = () => {
        let postsData = this.state.posts.map((project) => {

            return <div className="col s6" key={project.id}>
                <div className="card small hoverable">
                    <div className="card-image waves-effect waves-block waves-light">
                        <img className="activator" src={this.state.windowPath + project.image} />
                    </div>
                    <div className="card-content">
                        <span className="card-title activator thin_font blue_color">
                            {project.postname} <i className="material-icons right">more_vert</i>
                        </span>
                    </div>
                    <div className="card-action">
                        <a  href={this.state.windowPath + "/post/" + project.id} className="left-align">
                            Project Details
                        </a>
						<a href={this.state.windowPath + "/post/edit/" + project.id} className="right-align">
							Edit Project
						</a>
                    </div>
                    <div className="card-reveal">
                        <span className="card-title thin_font blue_color">
                            <i className="material-icons right">close</i>
                            {project.postname}
                        </span>
                        <p className="thin_font blue_color">
                            Category: {project.category}
                        </p>
                        <p className="thin_font blue_color">
                            City: {project.city}
                        </p>
                        <p className="thin_font blue_color">
                            Published by: {project.publishedBy}
                        </p>
                        <p className="thin_font blue_color">
                            {project.description}
                        </p>
                    </div>
                </div>
            </div>
         });

        return postsData;
    }

	render() {
		if (this.state.posts == undefined || this.state.posts == null) {
            return (
                <div className="loader"></div>
            );
        } else if (this.state.posts.length == 0) {
            return (
                <h5>There are not posts loaded!</h5>
            );
        } else {

            let buttonForm = "";
            let showForm = "";

            if (this.state.showForm == true) {
                showForm = this.createSearchForm();
            }

            // Verify if the button that will be displayed is for show or hide the form
            if (this.state.buttonFormShow == true) {
                buttonForm = <div className="col s3 offset-s9 margin-top-two">
                                <a className="waves-effect waves-light btn-large green" onClick={this.showSearchForm}>
                                    <i className="material-icons left">add</i>
                                    Open Search
                                </a>
                            </div>;
            } else {
                buttonForm = <div className="col s3 offset-s9 margin-top-two">
                                <a className="waves-effect waves-light btn-large red" onClick={this.hideForm}>
                                    <i className="material-icons left">remove</i>
                                    Hide Search
                                </a>
                            </div>;
            }

            // data to display
            let templateDatas = this.displayProjects();

			return (
				<div>
                    <div className="row">&nbsp;</div>

                    <div className="row">
                        {buttonForm}
                    </div>

                    <div>
                        {showForm}

                        <div className="row">

                            <div className="col s10">
                                {templateDatas}
                            </div>
                        </div>
                    </div>
				</div>

			);
		}
	}
}
