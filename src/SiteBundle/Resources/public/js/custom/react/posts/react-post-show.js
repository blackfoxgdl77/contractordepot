ReactDOM.render(
  <PostShow user={ document.getElementById('user.username').value }
            userId={ document.getElementById('_id').value }
  		    post={ document.getElementById('post.id').value }
            token={ document.getElementById('_csrf_token').value }
            source={ document.getElementById('source').value }
            nameT={ document.getElementById('name').value } />,
  document.getElementById('app')
);
