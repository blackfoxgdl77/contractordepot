/**
 * Event fill with some validations that will be
 * used in the platform. This file will be used just for
 * the validations when the user is not logged in
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */
$(document).ready(function () {
    $('#globalModal').modal({
        dismissable: false,
        opacity: .5,
        inDuration: 300,
        outDuration: 200
    });
    $("#confirmModal").modal({
        dismissable: false,
        opacity: .5,
        inDuration: 300,
        outDuration: 200
    });

    $('.indicator-item').on('click', function () {
        $('.slider').slider('pause');
    });

    $("#password").on("focusout", function (evt) {
        if ($(this).val() != $("#c_password").val()) {
            $("#c_password").removeClass("valid").addClass("invalid")
        } else {
            $("#c_password").removeClass("invalid").addClass("valid");
        }
    });

    $("#c_password").on('keyup', function (evt) {
        if ($("#password").val() != $(this).val()) {
            $(this).removeClass("valid").addClass("invalid");
        } else {
            $(this).removeClass("invalid").addClass("valid");
        }
    });

    $(".dropdown-button").dropdown({
        inDuration  : 300,
        outDuration : 225,
        hover       : true,
        belowOrigin : true,
        alignment   : 'right'
    });

    $("#closeError").click(function (evt) {
        evt.preventDefault();

        $("#error_login_alert").hide();
    });

    /**
     * Actions for components enabled in the platform
     */
    $(".ui.dropdown").dropdown();

    /**
     * Actions to embed videos on the site
     */
    $(".url.videos ui.embed").embed();
});
