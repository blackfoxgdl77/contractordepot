/**
 * File will be used to get the validations of the fields
 * once the user has saved the credentials in the browsers
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package customLibs\loginValidations.js
 */
$(document).ready(function () {

    // set the field in upper case
    $("#email").keyup(function () {
        $(this).val($(this).val().toUpperCase());
    });

    $(".message .close").on('click', function () {
        $(this).closest('.message').transition('fade');
    });
});
