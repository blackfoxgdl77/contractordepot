<?php

namespace SiteBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use SiteBundle\Libs\PrincipalController;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class will contain all the information related to
 * the job's post will be created by the users and published
 * on the site
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */
class PostsController extends PrincipalController {

    /**
     * Method will list all the jobs by user
     *
     * @Template()
     */
    public function indexAction() {
    }

    /**
     * Method is going to have add functionality
     *
     * @Template()
     */
    public function searchAction() {
        $this->addBreadcrumb('Home', 'indexpage');
        $this->addBreadcrumb('Search Project');

        $nameT = $this->__generateDynamicName('contactFormData');
        $token = $this->__generateTokenCsrf($nameT);

        return array('source'      => 'web',
                     'token'       => $token,
                     'name'        => $nameT,
                     'breadcrumbs' => $this->breadcrumbs);
    }

    /**
     * Method is going to be used for edit jobs
     *
     * @Template()
     */
    public function dashboardAction($id) {
        if (false == $this->__usersStatus()) {
            return new RedirectResponse($this->generateUrl('indexpage'));
        }

        $this->addBreadcrumb('Home', 'indexpage');
        $this->addBreadcrumb('My Projects');

        $nameT = $this->__generateDynamicName('contactFormData');
        $token = $this->__generateTokenCsrf($nameT);

        return array('source'      => 'web',
                     'token'       => $token,
                     'name'        => $nameT,
                     'breadcrumbs' => $this->breadcrumbs);
    }

    /**
     * Method is going to be used for edit jobs
     *
     * @Template()
     */
    public function editAction($id) {

        if (false == $this->__usersStatus()) {
            return new RedirectResponse($this->generateUrl('indexpage'));
        }

        $this->addBreadcrumb('Home', 'indexpage');
        if ($id == 0) {
            $this->addBreadcrumb('Create Project');
        } else {
            $this->addBreadcrumb('Edit Project');
        }

        $nameT = $this->__generateDynamicName('editPostAction');
        $token = $this->__generateTokenCsrf($nameT);

        return array('id'          => $id,
                     'token'       => $token,
                     'name'        => $nameT,
                     'source'      => 'web',
                     'breadcrumbs' => $this->breadcrumbs);
    }

    /**
     * Method is going to be used for create projects
     *
     * @Template()
     */
    public function createAction() {
        if (false === $this->__usersStatus()) {
            return new RedirectResponse($this->generateUrl('indexpage'));
        }

        $this->addBreadcrumb('Home', 'indexpage');
        $this->addBreadcrumb('Create Project');

        $nameT = $this->__generateDynamicName('createProjectAction');
        $token = $this->__generateTokenCsrf($nameT);

        return array('token'       => $token,
                     'name'        => $nameT,
                     'source'      => 'web',
                     'breadcrumbs' => $this->breadcrumbs);
    }

    /**
     * Method is going to be used for edit jobs
     *
     * @Template()
     */
    public function showAction($id) {
        if ($id == 0 || is_null($id)) {
            return new RedirectResponse($this->generateUrl('indexpage'));
        }

        // Add breadcrumbs
        $this->addBreadcrumb('Home', 'indexpage');
        $this->addBreadcrumb('Search Project', 'web_posts_search');
        $this->addBreadcrumb('Show Projects');

        $nameT = $this->__generateDynamicName('editPostAction');
        $token = $this->__generateTokenCsrf($nameT);

        return array('id'          => $id,
                     'token'       => $token,
                     'name'        => $nameT,
                     'source'      => 'web',
                     'breadcrumbs' => $this->breadcrumbs);
    }

}
