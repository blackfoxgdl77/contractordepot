<?php

namespace SiteBundle\Controller;

use SiteBundle\Libs\PrincipalController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class contains all the information about the login
 * and we will use in the system to check the user's
 * data, where the information is going to be recovery
 * to start session in the platform
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */
class SecurityController extends PrincipalController {

    /**
     * Method will display the information of the login
     * form that will be used for start session of the users
     *
     * @Template()
     */
    public function loginAction() {
        $this->addBreadcrumb('Home', 'indexpage');
        $this->addBreadcrumb('Sign In!');

        $authenticationUtils = $this->get('security.authentication_utils');
        $error               = $authenticationUtils->getLastAuthenticationError();
        $lastUsername        = $authenticationUtils->getLastUsername();

        return array(
            'last_username' => $lastUsername,
            'error'         => $error,
            'breadcrumbs'   => $this->breadcrumbs
        );
    }
}
