<?php

namespace SiteBundle\Controller;

use SiteBundle\Libs\PrincipalController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Config\Definition\Exception\Exception;
use APIBundle\Controller\APIUsersController;
use APIBundle\Entity\DynamicData;
use APIBundle\Entity\Posts;

/**
 * Class will contain all the methods
 * are going to be used to display
 * information in the site but before
 * to login to the platform
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package SiteBundle\Controller\WebsiteController
 */
class WebsiteController extends PrincipalController {

    /**
     * @Template()
     */
    public function indexAction() {
        if (false == $this->__usersStatus()) {
            $this->__clearSessionVariables();
        }

        // get all the information about banners
        $banners = $this->loadRepo($this->bundleName, "DynamicData")->imagesForBanners();
        $post = $this->loadRepo($this->bundleName, "Posts")->getPostRecently(6);

        $array = array('png', 'jpg', 'jpeg', 'bmp', 'gif');
        return array('banners'   => $banners,
                     'imagesA'   => $array,
                     'totalPost' => count($post),
                     'posts'     => $post);
    }

    /**
     * Method will display the information of the contact form
     *
     * @Template()
     */
    public function contactAction() {
        $nameT = $this->__generateDynamicName('contactFormData');
        $token = $this->__generateTokenCsrf($nameT);

        $this->addBreadcrumb('Home', 'indexpage');
        $this->addBreadcrumb('Contact Us!');

        return array('source'      => 'web',
                     'token'       => $token,
                     'name'        => $nameT,
                     'breadcrumbs' => $this->breadcrumbs);
    }

    /**
     * Method will be used to send the information provided
     * by the users in the contact forms
     *
     * @return response
     */
    public function sendContactAction() {
        try {
            $contactData = json_decode(file_get_contents('php://input'), true);


            $this->emailData->indexAction($contactData['subject'],
                                          $contactData['comments'],
                                          $contactData['email'],
                                          ($contactData['phone'] != '') ? $contactData['phone'] : null);


            return new Response();
        } catch (\Exception $e) {

        }
    }

    /**
     * Method will load all the information related to about us,
     * all the data of the information about us, who am i and so.
     *
     * @Template()
     */
    public function aboutAction() {
        $this->addBreadcrumb('Home', 'indexpage');
        $this->addBreadcrumb('About Us!');

        $about = $this->loadRepo($this->bundleName, "DynamicData")->findBy(array('section' => 3,
                                                                                 'status'  => 1));

        return array('description' => (!empty($about)) ? $about[0]->getDescription() : null,
                     'url'         => (!empty($about)) ? $about[0]->getUrl() : null,
                     'typeFile'    => (!empty($about)) ? $about[0]->getTypeFile() : null,
                     'file'        => (!empty($about)) ? $about[0]->getFile() : null,
                     'breadcrumbs' => $this->breadcrumbs);
    }

    /**
     * Method will load the view with all the information about
     * how it works this platform and all the process he has
     *
     * @Template()
     */
    public function worksAction() {
        $this->addBreadcrumb('Home', 'indexpage');
        $this->addBreadcrumb('How it works!');

        $works = $this->loadRepo($this->bundleName, "DynamicData")->findBy(array('section' => 2,
                                                                                 'status'  => 1));

        return array('description' => (!empty($works)) ? $works[0]->getDescription() : null,
                     'url'         => (!empty($works)) ? $works[0]->getUrl() : null,
                     'typeFile'    => (!empty($works)) ? $works[0]->getTypeFile() : null,
                     'file'        => (!empty($works)) ? $works[0]->getFile() : null,
                     'breadcrumbs' => $this->breadcrumbs);
    }

    /**
     * Method will be used to get the information of the new user
     * wants to create an account. Will contain all the information
     * that should be filled by the user
     *
     * @Template()
     */
    public function newAccountAction() {
        $nameT = $this->__generateDynamicName('newAccountFormData');
        $token = $this->__generateTokenCsrf($nameT);

        $this->addBreadcrumb('Home', 'indexpage');
        $this->addBreadcrumb('Sign Up');

        return array('source'      => 'web',
                     'token'       => $token,
                     'name'        => $nameT,
                     'breadcrumbs' => $this->breadcrumbs);
    }

    /**
     * Method will be used to display the terms
     * and conditions will be loader once the user
     * wants to see the terms and conditions of the
     * platform
     *
     * @Template()
     */
    public function termsAndConditionsAction() {
        $this->addBreadcrumb('Home', 'indexpage');
        $this->addBreadcrumb('Terms And Conditions');

        $terms = $this->loadRepo($this->bundleName, "DynamicData")->findBy(array('section' => 4,
                                                                                 'status'  => 1));

        return array('description' => (!empty($terms)) ? $terms[0]->getDescription() : null,
                     'url'         => (!empty($terms)) ? $terms[0]->getUrl() : null,
                     'typeFile'    => (!empty($terms)) ? $terms[0]->getTypeFile() : null,
                     'file'        => (!empty($terms)) ? $terms[0]->getFile() : null,
                     'breadcrumbs' => $this->breadcrumbs);
    }

    /**
     * Method is going to be used to send the information to the
     * API and then we can check if the data will be saved or not.
     * This avoid combine information of the users when is a PHP Validations
     * for the form login and register, this will be used with
     * security to check information data
     *
     * @return response
     */
    public function saveAction() {
        try {
            $response      = $this->forward('APIBundle:APIUsers:checkEmail', array('request' => $this->request));
            $value         = $response->getContent();
            $finalResponse = json_decode($value);

            if ($finalResponse->data->flag == 0) {
                return $this->render('@Site/Website/new_account.html.twig', array('error' => $finalResponse->data->message,
                                                                                  'email' => $this->request->request->get('email'),
                                                                                  'phone' => $this->request->request->get('phone'),
                                                                                  'first_name' => $this->request->request->get('first_name'),
                                                                                  'last_name' => $this->request->request->get('last_name')));
            } else {
                $responseSave = $this->forward('APIBundle:APIUsers:save', array('request' => $this->request));
                $value        = $responseSave->getContent();
                $message      = json_decode($value);

                return $this->render('@Site/Website/successMessage.html.twig', array('message' => $message->data->message));
            }
        } catch(\Exception $e) {

        }
    }
}
