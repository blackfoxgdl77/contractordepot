<?php

namespace SiteBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use SiteBundle\Libs\PrincipalController;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Controller will contain all the information that is going
 * to display all the information will be disabled if the
 * projects has been reported as fake
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package SiteBundle\Controller\FakeController
 */
class FakeController extends PrincipalController {

    /**
     * Constructor
     */
    public function __construct() {
    }

    /**
     * Method will be used to display all the projects
     * has been reported as fake by at least one user
     * by any reason
     *
     * @Template()
     */
    public function indexAction() {

        if (false == $this->__usersStatus()) {
            return new RedirectResponse($this->generateUrl('indexpage'));
        }

        $this->addBreadcrumb('Home', 'indexpage');
        $this->addBreadcrumb('Admin', 'admin_fake_projects');
        $this->addBreadcrumb('Report Fake Projects');

        $nameT = $this->__generateDynamicName('fakePrj');
        $token = $this->__generateTokenCsrf($nameT);

        return array('source'      => 'web',
                     'name'        => $nameT,
                     'token'       => $token,
                     'breadcrumbs' => $this->breadcrumbs);
    }
}
