<?php

namespace SiteBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use SiteBundle\Libs\PrincipalController;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class will contain all the information of
 * the users will have access as Administrator on the
 * platform to check different actions
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package SiteBundle\Controller\AdminController
 */
class AdminController extends PrincipalController {

    /**
     * Constructor
     */
    public function __construct() {
    }

    /**
     * Index page will display all the information about the
     * user will be created as admin on the platform
     *
     * @Template()
     */
    public function indexAction() {
        if (false == $this->__usersStatus()) {
            return new RedirectResponse($this->generateUrl('indexpage'));
        }

        $this->addBreadcrumb('Home', 'indexpage');
        $this->addBreadcrumb('Admin', 'admin_list_section');
        $this->addBreadcrumb('List Admin');

        $nameT = $this->__generateDynamicName('adminLst');
        $token = $this->__generateTokenCsrf($nameT);

        return array('source'      => 'web',
                     'name'        => $nameT,
                     'token'       => $token,
                     'breadcrumbs' => $this->breadcrumbs);
    }

    /**
     * Method will contain fake projects list and all
     * the actions enabled in that view. With this view
     * the admin will see the list of the projects and what users
     * has reported the job as fake
     *
     * @Template()
     */
    public function fakeProjectsAction() {
        if (false == $this->__usersStatus()) {
            return new RedirectResponse($this->generateUrl('indexpage'));
        }

        $nameT = $this->__generateDynamicName('resetPwd');
        $token = $this->__generateTokenCsrf($nameT);

        $this->addBreadcrumb('Home', 'indexpage');
        $this->addBreadcrumb('Admin', 'admin_list_section');
        $this->addBreadcrumb('Fake Projects');

        return array('source'      => 'web',
                     'name'        => $nameT,
                     'token'       => $token,
                     'breadcrumbs' => $this->addBreadcrumb);
    }

    /**
     * Method will be used to list all the banners
     * will display the slider in the index page
     *
     * @Template()
     */
    public function listBannersAction() {
        if (false == $this->__usersStatus()) {
            return new RedirectResponse($this->generateUrl('indexpage'));
        }

        return array('source' => 'web');
    }

    /**
     * Method will be used to shwo the information that
     * is going to be showed on the how it works. This information
     * can be changed from the platform
     *
     * @Template()
     */
    public function adminWorksAction() {
        if (false == $this->__usersStatus()) {
            return new RedirectResponse($this->generateUrl('indexpage'));
        }

        return array('source' => 'web');
    }

    /**
     * Method is going to be used for displaying
     * the information of about us in the website
     *
     * @Template()
     */
    public function aboutUsAction() {
        if (false == $this->__usersStatus()) {
            return new RedirectResponse($this->generateUrl('indexpage'));
        }

        return array('source' => 'web');
    }
}
