<?php

namespace SiteBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use SiteBundle\Libs\PrincipalController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use APIBundle\Entity\Offers;
use APIBundle\Entity\Posts;

/**
 * Class will contain all the methods used to call the
 * frontend data and for has access to the admin site
 * once the user has accepted the offer
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package SiteBundle\Controller\PaymentsController
 */
class PaymentsController extends PrincipalController {

    /**
     * Constructor
     */
    public function __construct() {
    }

    /**
     * Method is going to display the message will be
     * showed to the user once has accepted the
     * bid and has done the payment
     *
     * @param integer $idProject
     * @Template()
     */
    public function successPaymentAction($idProject) {
        if (false == $this->__usersStatus()) {
            return new RedirectResponse($this->generateUrl('indexpage'));
        }

        $this->addBreadcrumb('Projects', 'web_posts_dashboard');
        $this->addBreadcrumb('Payment Success');

        $nameT = $this->__generateDynamicName('successPaymentToken');
        $token = $this->__generateTokenCsrf($nameT);

        // Get information
        $id   = $this->loadRepo($this->bundleName, "Offers")->find($idProject);
        $post = $this->loadRepo($this->bundleName, "Posts")->findBy(array('id' => $id->getPost()->getId()));

        return array('source'      => 'web',
                     'token'       => $token,
                     'name'        => $nameT,
                     'breadcrumbs' => $this->breadcrumbs,
                     'moneybidder' => $id->getMoneyBidder(),
                     'postName'    => $post[0]->getPostName(),
                     'description' => $post[0]->getDescription(),
                     'username'    => $post[0]->getUser()->getUsername(),
                     'email'       => $post[0]->getUser()->getEmail(),
                     'name'        => $post[0]->getUser()->getName());
    }

    /**
     * Method will be used once the user has cancelled the payment
     * by any reason  and will be displayed a message for check with
     * the user that the payment has been cancelled
     *
     * @param integer $idProject
     * @Template()
     */
    public function cancelPaymentAction($idProject) {
        if (false == $this->__usersStatus()) {
            return new RedirectResponse($this->generateUrl('indexpage'));
        }

        $this->addBreadcrumb('Projects', 'web_posts_dashboard');
        $this->addBreadcrumb('Payment Cancelled');

        $nameT = $this->__generateDynamicName('cancelPaymentAction');
        $token = $this->__generateTokenCsrf($nameT);

        // Get information
        $id   = $this->loadRepo($this->bundleName, "Offers")->find($idProject);
        $post = $this->loadRepo($this->bundleName, "Posts")->findBy(array('id' => $id->getPost()->getId()));

        return array('source'      => 'web',
                     'token'       => $token,
                     'name'        => $nameT,
                     'breadcrumbs' => $this->breadcrumbs,
                     'moneybidder' => $id->getMoneyBidder(),
                     'postName'    => $post[0]->getPostName(),
                     'description' => $post[0]->getDescription(),
                     'username'    => $post[0]->getUser()->getUsername(),
                     'email'       => $post[0]->getUser()->getEmail(),
                     'name'        => $post[0]->getUser()->getName());
    }

    /**
     * Method will be used to get the list of projects has been
     * paid by the user and the money is in the payment platform
     *
     * @Template()
     */
    public function donePaymentsListAction() {
        if (false == $this->__usersStatus()) {
            return new RedirectResponse($this->generateUrl('indexpage'));
        }

        $this->addBreadcrumb('Projects', 'web_posts_dashboard');
        $this->addBreadcrumb('Paid Projects');

        $nameT = $this->__generateDynamicName('donePayments');
        $token = $this->__generateTokenCsrf($nameT);

        return array('source'      => 'web',
                     'token'       => $token,
                     'name'        => $nameT,
                     'breadcrumbs' => $this->breadcrumbs);
    }

    /**
     * Method will be used to get the list of projects has been
     * cancelled and the user needs to pay for being able to
     * reach the user with the winner bid
     *
     * @Template()
     */
    public function pendingPaymentsListAction() {
        if (false == $this->__usersStatus()) {
            return new RedirectResponse($this->generateUrl('indexpage'));
        }

        $this->addBreadcrumb('Projects', 'web_posts_dashboard');
        $this->addBreadcrumb('Pending Projects');

        $nameT = $this->__generateDynamicName('pendingPayments');
        $token = $this->__generateTokenCsrf($nameT);

        return array('source'      => 'web',
                     'token'       => $token,
                     'name'        => $nameT,
                     'breadcrumbs' => $this->breadcrumbs);
    }

    /**
     * Method will be used to redirect to the error
     * page once somenthing wrong happenden in the
     * process of payments section
     *
     * @Template()
     */
    public function errorPaymentsAction() {
        if (false == $this->__usersStatus()) {
            return new RedirectResponse($this->generateUrl('indexpage'));
        }

        $this->addBreadcrumb('Projects', web_posts_dashboard);
        $this->addBreadcrumb('Error Page');

        return array('source'      => 'web',
                     'breadcrumbs' => $this->breadcrumbs);
    }
}
