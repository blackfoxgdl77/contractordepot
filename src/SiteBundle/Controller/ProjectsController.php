<?php

namespace SiteBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use SiteBundle\Libs\PrincipalController;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ProjectsController extends PrincipalController
{
	/**
     * Search for projects
     *
     * @Template()
     */
    public function searchAction() {
        $this->addBreadcrumb('Home', 'indexpage');
        $this->addBreadcrumb('Search Project');

        $nameT = $this->__generateDynamicName('searchProjectsAction');
        $token = $this->__generateTokenCsrf($nameT);

        return array('source'      => 'web',
                     'token'       => $token,
                     'name'        => $nameT,
                     'breadcrumbs' => $this->breadcrumbs);
    }

    /**
     * Access to project dashboard
     *
     * @Template()
     */
    public function dashboardAction($id) {
        if (false == $this->__usersStatus()) {
            return new RedirectResponse($this->generateUrl('indexpage'));
        }

        $this->addBreadcrumb('Home', 'indexpage');
        $this->addBreadcrumb('My Projects');

        $nameT = $this->__generateDynamicName('dashboardProjectsAction');
        $token = $this->__generateTokenCsrf($nameT);

        return array('source'      => 'web',
                     'token'       => $token,
                     'name'        => $nameT,
                     'breadcrumbs' => $this->breadcrumbs);
    }

    /**
     * Create a project
     *
     * @Template()
     */
    public function createAction() {
        if (false === $this->__usersStatus()) {
            return new RedirectResponse($this->generateUrl('indexpage'));
        }

        $this->addBreadcrumb('Home', 'indexpage');
        $this->addBreadcrumb('Create Project');

        $nameT = $this->__generateDynamicName('createProjectAction');
        $token = $this->__generateTokenCsrf($nameT);

        return array('token'       => $token,
                     'name'        => $nameT,
                     'source'      => 'web',
                     'breadcrumbs' => $this->breadcrumbs);
    }

    /**
     * Edit a project
     *
     * @Template()
     */
    public function editAction($id) {

        if (false == $this->__usersStatus() || $id == 0) {
            return new RedirectResponse($this->generateUrl('indexpage'));
        }

        $this->addBreadcrumb('Home', 'indexpage');
        $this->addBreadcrumb('Edit Project');
        
        $nameT = $this->__generateDynamicName('editProjectAction');
        $token = $this->__generateTokenCsrf($nameT);

        return array('id'          => $id,
                     'token'       => $token,
                     'name'        => $nameT,
                     'source'      => 'web',
                     'breadcrumbs' => $this->breadcrumbs);
    }

    /**
     * Show project details
     *
     * @Template()
     */
    public function showAction($id) {
        if ($id == 0 || is_null($id)) {
            return new RedirectResponse($this->generateUrl('indexpage'));
        }

        // Add breadcrumbs
        $this->addBreadcrumb('Home', 'indexpage');
        $this->addBreadcrumb('Search Project', 'web_projects_search');
        $this->addBreadcrumb('Show Projects');

        $nameT = $this->__generateDynamicName('showProjectAction');
        $token = $this->__generateTokenCsrf($nameT);

        return array('id'          => $id,
                     'token'       => $token,
                     'name'        => $nameT,
                     'source'      => 'web',
                     'breadcrumbs' => $this->breadcrumbs);
    }
}
