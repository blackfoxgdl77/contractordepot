<?php

namespace SiteBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use SiteBundle\Libs\PrincipalController;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class will contain all the information about the
 * bids of the users when he wants to see the data
 * that will be used in the views of the bids
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @package SiteBundle\Controller\BidsController
 */
class BidsController extends PrincipalController {

    /**
     * Constructor
     */
    public function __construct() {
    }

    /**
     * Method will be used to display all the
     * information related to open bids of every
     * user
     *
     * @Template()
     */
    public function myBidsAction() {
        if (false == $this->__usersStatus()) {
            return new RedirectResponse($this->generateUrl('indexpage'));
        }

        $this->addBreadcrumb('Home', 'indexpage');
        $this->addBreadcrumb('My Bids');

        return array('source'      => 'web',
                     'breadcrumbs' => $this->breadcrumbs);
    }

    /**
     * Method is going to display all the information
     * of the bids done by the user but already
     *
     * @Template()
     */
    public function endBidsAction() {
        if (false == $this->__usersStatus()) {
            return new RedirectResponse($this->generateUrl('indexpage'));
        }

        $this->addBreadcrumb('Home', 'indexpage');
        $this->addBreadcrumb('End Bids');

        return array('source'      => 'web',
                     'breadcrumbs' => $this->breadcrumbs);
    }
}
