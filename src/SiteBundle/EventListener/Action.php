<?php

namespace SiteBundle\EventListener;

use Symfony\Component\HttpKernel\Event\FilterControllerEvent as FilterEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Action Listener for beforeAction method will executed
 *
 * @package SiteBundle\EventListener
 */
class Action {
    public function onKernelController(FilterEvent $event) {
        if (HttpKernelInterface::MASTER_REQUEST === $event->getRequestType()) {
            $controller = $event->getController();
            if (is_array($controller)) {
                $controllers = $controller[0];

                if (is_object($controllers) && method_exists($controllers, 'beforeAction')) {
                    $controllers->beforeAction();
                }
            }
        }
    }
}
