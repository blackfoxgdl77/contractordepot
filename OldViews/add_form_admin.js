// add form
<Select name="section" disable="false" labelSelectName="Section" iconTags="dashboard" require="true"
        handleFormFields={this.handleInput} defaultText="false"
        classData="input-field col s8 offset-s2"
        displayDataValues={categories} id="sectionID"
        ref={(section) => { this.section = section }} />
<TextArea name="description" disable="false" require="true" defaultText="false" iconTags="description"
        handleFormFields={this.handleInput} labelName="Description"
        classData="input-field col s9 offset-s1" classCustom="" rowCustom="left_move_textarea"
        ref={(description) => { this.description = description }} />
<Select name="options" labelSelectName="Select an Option" iconTags="format_list_numbered" require="false"
        disable="false" id="optionsId" handleFormFields={this.handleInput} defaultText="false"
        displayDataValues={options} classData="input-field col s8 offset-s2"
        ref={(options) => { this.options = options }} />
<InputFile name="file" labelButton="File" handleFormFields={this.handleInput} iconTags="insert_photo"
        classData="file-field input-field col s8 offset-s2" disable={stateFile}
        ref={(file) => { this.file = file }} />
<Input name="url_video" disable="false" require="false" defaultText="false" iconTags="live_tv"
        handleFormFields={this.handleInput} labelName="Video URL" disable={stateVideo}
        classData="input-field col s8 offset-s2"
        ref={(url_video) => { this.url_video = url_video }} />
<Select name="status" labelSelectName="Status" iconTags="iso" require="true" disable="false"
        id="statusId" handleFormFields={this.handleInput} defaultText="false" displayDataValues={values}
        classData="input-field col s8 offset-s2"
        ref={(status) => { this.status = status }} />

<div className="row center">
    <div className="col s12">
        <button className="btn waves-effect waves-light" type="submit" onClick={this.saveNewRecord}>
            Save
            <i className="material-icons right">send</i>
        </button>
    </div>
</div>


// add button
<div className="col s3 offset-s9 margin-top-two">
                <a className="waves-effect waves-light btn-large green" onClick={this.showAddForm}>
                    <i className="material-icons left">add</i>
                    Add New Record
                </a>
            </div>;

// hide button
<div className="col s3 offset-s9 margin-top-two">
                <a className="waves-effect waves-light btn-large red" onClick={this.hideForm}>
                    <i className="material-icons left">remove</i>
                    Hide Form
                </a>
            </div>;

// body render
<ul className="collapsible popout" data-collapsible="accordion">
    <li>
        <div className="collapsible-header">
            <i className="material-icons"></i>
            Banners
        </div>
        <div className="collapsible-body">
            {banners}
        </div>
    </li>
    <li>
        <div className="collapsible-header">
            <i className="material-icons"></i>
            How it Works?
        </div>
        <div className="collapsible-body">
            {works}
        </div>
    </li>
    <li>
        <div className="collapsible-header">
            <i className="material-icons"></i>
            About us!
        </div>
        <div className="collapsible-body">
            <ul>
                {about}
            </ul>
        </div>
    </li>
    <li>
        <div className="collapsible-header">
            <i className="material-icons"></i>
            Terms and Conditions!
        </div>
        <div className="collapsible-body">
            {terms}
        </div>
    </li>
</ul>
