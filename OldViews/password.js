<div className={parameters}>
    <div className={dynamicClass}>
        <i className="material-icons prefix">{this.props.iconTags}</i>
        <input type="password"
               {...propsInput}
               id={this.props.name}
               name={this.props.name}
               className="validate"
               onChange={this.handleInput}
               onBlur={this.onBlur} />
        <label htmlFor={this.props.name} className="thin_font">{this.props.labelName}</label>
    </div>
</div>
