<div className="header gross_font blue_color">
    View Details Information - {this.getTitle(this.state.data.section)}
</div>
<div className="scrolling content">
    <div className="center aligned">
        <h3 className="right-align thin_font blue_color">
            {this.getTitle(this.state.data.section)}
        </h3>
    </div>
    <div className="justify_align thin_font">
        <span className="gross_font">Description:</span>
        <span className="thin_font">{this.state.data.description}</span>
    </div>
    <div className="center">
        {templateImgVideo}
    </div>
    <div className="justify_align thin_font">
        <span className="gross_font">Created At:</span>
        <span className="thin_font">{this.state.data.created_at}</span>
    </div>
    <div className="justify_align thin_font">
        <span className="gross_font">Updated At:</span>
        <span className="thin_font">{this.state.data.updated_at}</span>
    </div>
</div>
<div className="actions">
    <div className="negative ui button" onClick={this.hideModal}>
        <i className="window close icon"></i>
        Close
    </div>
</div>

=======

<div className="header gross_font blue_color">
    Update Information - {this.getTitle(this.state.data.section)}
</div>
<div className="scrolling content">
    <div className="center aligned">
        <h3 className="right-align thin_font blue_color">
            {this.getTitle(this.state.data.section)}
        </h3>
    </div>
    <form className="ui large form">
        <div className="ui stacked segment">

            {errorTemp}

            <Hidden value={this.state.data.id} id="editId" name="editId" />
            <div className="field">
                <Select name="sectionUpdate" disable="false" labelSelectName="Section" iconTags="dashboard" require="true"
                        handleFormFields={this.handleInput} defaultText="false"
                        classData="" value={this.state.data.sectionId}
                        displayDataValues={categories} id="sectionIDUpdate"
                        ref={(sectionUpdate) => { this.sectionUpdate = sectionUpdate }} />
            </div>
            <div className="field">
                <TextArea name="descriptionUpdate" disable="false" require="true"
                        defaultText="true"
                        is_update="true"
                        items={this.state.data.description}
                        iconTags="description"
                        handleFormFields={this.handleInput} labelName="Description"
                        classData="input-field col s9 offset-s1"
                        classCustom=""
                        rowCustom="left_move_textarea"
                        ref={(descriptionUpdate) => { this.descriptionUpdate = descriptionUpdate }} />
            </div>
            <div className="field">
                <Select name="optionsUpdate" labelSelectName="Select an Option" iconTags="format_list_numbered"
                        require="false" disable="false" id="optionsIdUpdates" handleFormFields={this.handleInput}
                        value={this.state.data.flagOption} defaultText="false"
                        displayDataValues={options} classData="input-field col s8 offset-s2"
                        ref={(optionsUpdate) => { this.optionsUpdate = optionsUpdate }} />
            </div>
            <div className="field">
                <InputFile name="fileUpdate" labelButton="File" handleFormFields={this.handleInput}
                        iconTags="" disable={updateFlagFile}
                        classData="file-field input-field col s8 offset-s2"
                        ref={(fileUpdate) => { this.fileUpdate = fileUpdate }} />
            </div>
            <div className="field">
                <Input name="url_videoUpdate" disable={updateFlagVideo} require="false"
                        defaultText="true"
                        is_update="true"
                        iconTags="live_tv"
                        items={this.state.data.url}
                        handleFormFields={this.handleInput} labelName="Video URL"
                        classData="input-field col s8 offset-s2"
                        ref={(url_videoUpdate) => { this.url_videoUpdate = url_videoUpdate }} />
            </div>
            <div className="field">
                <Select name="statusUpdate" labelSelectName="Status" iconTags="iso" require="true"
                        disable="false"
                        value={this.state.data.statusId}
                        id="statusIdUpdate" handleFormFields={this.handleInput} defaultText="false"
                        displayDataValues={values}
                        classData="input-field col s8 offset-s2"
                        ref={(statusUpdate) => { this.statusUpdate = statusUpdate }} />
            </div>
        </div>
    </form>
</div>
<div className="actions">
    <div className="positive ui button" onClick={(e) => this.updateRecord(e)}>
        <i className="edit icon"></i>
        Update
    </div>
    <div className="negative ui button" onClick={(e) => this.toggle(null, '', e)}>
        <i className="window close icon"></i>
        Close
    </div>
</div>

============================

<div className="header">
        End Bids - List of Bids
</div>
<div className="scrolling content">
    {collectionUsers}
</div>
<div className="actions">
        <div className="negative ui button" onClick={this.hideModal}>
            <i className="window close icon"></i>
            Close
        </div>
</div>

=======================

<div className="header">
    My Open Bids - List of Open Bids
</div>
<div className="scrolling content">
    {collectionUsers}
</div>
<div className="actions">
    <div className="negative ui button" onClick={this.hideModal}>
        <i className="window close icon"></i>
        Close
    </div>
</div>

==================

<div className="header">
    Payments Done - List of Payments Done
</div>
<div className="scrolling content">
    <div className="ui items">
        <div className="item">
            <div className="content">
                <a className="header">
                    Payments Projects Complete
                </a>
                <div className="description">
                    <div>
                        Id Payment: {this.state.modalData.idPayment}
                    </div>
                    <div>
                        Post Name: {this.state.modalData.namePost}
                    </div>
                    <div>
                        Description: {this.state.modalData.postDesc}
                    </div>
                    <div>
                        Username: {this.state.modalData.username}
                    </div>
                    <div>
                        Owner: {this.state.modalData.owner}
                    </div>
                    <div>
                        Email: {this.state.modalData.emailO}
                    </div>
                    <div>
                        Category: {this.state.modalData.category}
                    </div>
                    <div>
                        City: {this.state.modalData.cityName}
                    </div>
                    <div>
                        Capital: {this.state.modalData.capital}
                    </div>
                    <div>
                        Date Created: {this.state.modalData.dateCrt}
                    </div>
                    <div>
                        Date Updated: {this.state.modalData.dateUpt}
                    </div>
                    <div>
                        Username Winner: {this.state.modalData.userWin}
                    </div>
                    <div>
                        Amount: {this.state.modalData.amount}
                    </div>
                    <div>
                        Winner Name: {this.state.modalData.userNameW}
                    </div>
                    <div>
                        Winner Email: {this.state.modalData.userEmail}
                    </div>
                    <div>
                        Transaction Status: {this.state.modalData.statusT}
                    </div>
                    <div>
                        ID Transaction: {this.state.modalData.idTrans}
                    </div>
                    <div>
                        Date Paid Done: {this.state.modalData.datePaid}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div className="extra">
    <a className="negative ui button" onClick={this.hideModal}>
        <i className="window close icon"></i>
        Close
    </a>
</div>

================

<div className="header">
    Pending Payments - List of Pending Payments
</div>
<div className="scrolling content">
    <div className="ui items">
        <div className="item">
            <div className="content">
                <a classsName="header">
                    Pending Payments Projects
                </a>
                <div className="description">
                    <div>
                        Post Name: {this.state.modalData.namePost}
                    </div>
                    <div>
                        Description: {this.state.modalData.postDesc}
                    </div>
                    <div>
                        Username: {this.state.modalData.username}
                    </div>
                    <div>
                        Owner: {this.state.modalData.owner}
                    </div>
                    <div>
                        Email: {this.state.modalData.emailO}
                    </div>
                    <div>
                        Category: {this.state.modalData.category}
                    </div>
                    <div>
                        City: {this.state.modalData.cityName}
                    </div>
                    <div>
                        Capital: {this.state.modalData.capital}
                    </div>
                    <div>
                        Date Created: {this.state.modalData.dateCrt}
                    </div>
                    <div>
                        Date Updated: {this.state.modalData.dateUpt}
                    </div>
                    <div>
                        Username Winner: {this.state.modalData.userWin}
                    </div>
                    <div>
                        Winner Amount: {this.state.modalData.amount}
                    </div>
                    <div>
                        Winner Name: {this.state.modalData.userNameW}
                    </div>
                    <div>
                        Email Winner: {this.state.modalData.userEmail}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div className="actions">
    <div className="negative ui button" onClick={this.hideModal}>
        <i className="window close icon"></i>
        Close
    </div>
</div>
