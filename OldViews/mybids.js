// modal body
<li className="collection-item blue_color" key={users.id_user}>
    <div className="col s2 gross_font blue_color">
        Username:
    </div>
    <div className="col s10 thin_font blue_color">
        { users.username }
    </div>
    <div className="col s2 gross_font blue_color">
        Bidder:
    </div>
    <div className="col s10 thin_font blue_color">
        { users.moneyBidder }
    </div>
    <div className="col s2 gross_font blue_color">
        Status:
    </div>
    <div className="col s10 thin_font blue_color">
        { users.status }
    </div>
    <div className="col s2 gross_font blue_color">
        Date of Bid:
    </div>
    <div className="col s10 thin_font blue_color">
        { users.date }
    </div>
</li>

// template modal
<div className="modal margin_modal_react" key={this.state.dataUsers.id_offer} style={styleDisplay}>
                    <div className="modal-content">
                        <div className="col s5 left thin_font blue_color">
                            My Bids > My Bids Open > List of Bids Open
                        </div>
                        <div className="col s7">
                            <h3 className="right-align thin_font blue_color">
                                Bids Per Project
                            </h3>
                        </div>
                        <div className="col s12">
                            <ul className="collection">
                                {collectionUsers}
                            </ul>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <a href="#!" className="modal-action modal-close waves-effect waves-green btn-flat"
                           onClick={(e) => this.setModalUsers('', e)}>
                            Close
                        </a>
                    </div>
                </div>

// display information template
<ul className="collection">
                   {template}
                  </ul>


//body content
<li className="collection-item" key={offers.id_offer}>
    <div className="col s2 left-align gross_font blue_color">
        Project Name:
    </div>
    <div className="col s10 left-align thin_font">
        { offers.postName }
    </div>
    <div className="col s2 left-align gross_font blue_color">
        Description:
    </div>
    <div className="col s10 left-align thin_font">
        { offers.description }
    </div>
    <div className="col s2 left-align gross_font blue_color">
        Category:
    </div>
    <div className="col s10 left-align thin_font">
        { offers.category }
    </div>
    <div className="col s2 left-align gross_font blue_color">
        City:
    </div>
    <div className="col s10 left-align thin_font">
        { offers.city }
    </div>
    <div className="col s2 left-align gross_font blue_color">
        Status:
    </div>
    <div className="col s10 left-align thin_font">
        { offers.status }
    </div>

    <div className="right">
        <a className="waves-effect waves-light btn" onClick={(e) => this.setModalUsers(offers, e)}>
            <i className="material-icons right">assignment_ind</i>Bids Users
        </a>
    </div>
    <div className="right right_separate_buttons">
        <a className="waves-effect waves-light btn" href={this.state.windowPath + "/post/" + offers.id_post}>
            <i className="material-icons right">visibility</i>Go To Project
        </a>
    </div>
    <div className="clear: both">&nbsp;</div>
    <div>&nbsp;</div>
</li>
