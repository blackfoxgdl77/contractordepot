<div className={parameters}>
    <div className={dynamicClass}>
        {iconTags}
        <input type="text"
               value={this.state.value}
               {...propsInput}
               className="validate thin_font"
               id={this.props.name}
               name={this.props.name}
               onChange={this.handleInput}
               onBlur={this.onBlur} />
        <label htmlFor={this.props.name} className={classUpdated}>{this.props.labelName}</label>
    </div>
</div>
