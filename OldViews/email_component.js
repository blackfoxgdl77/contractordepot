<div className={parameters}>
    <div className={dynamicClass}>
        <i className="material-icons prefix">{this.props.iconTags}</i>
        <input value={this.state.value}
               type="email"
               className="validate thin_font"
               {...propsInput}
               id={this.props.name}
               name={this.props.name}
               onChange={this.handleEmail}
               onBlur={this.onBlur} />
        <label htmlFor={this.props.email} className="thin_font" data-error="Wrong Email Format">{this.props.labelEmail}</label>
    </div>
</div>
