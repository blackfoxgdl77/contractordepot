<div className={parameters}>
    <div className={dynamicClass}>
        <div className="btn">
            <span>{this.props.labelButton}</span>
            <input type="file"
                   {...propsInput}
                   name={this.props.name}
                   id={this.props.name}
                   className="validate thin_font"
                   required=""
                   onChange={this.inputFile}
                   onBlur={this.onBlur}
                   multiple />
        </div>
        <div className="file-path-wrapper">
            <input type="text" className="file-path validate thin_font" name={this.props.name} id={this.props.name}
                   required="" onChange={this.inputFile} onBlur={this.onBlur} {...propsInput} />
        </div>
    </div>
</div>
