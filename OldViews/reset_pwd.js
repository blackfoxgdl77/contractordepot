<Password name="password" disable="false" labelName="Password" iconTags="lock"
       handleFormFields={this.handleInput} defaultText="false" require="true"
       classData="input-field col s8 offset-s2" isEmpty={this.state.clearValue}
       ref={(password) => { this.password = password; }} />
<Password name="c_password" disable="false" labelName="Confirm Password" iconTags="lock"
       handleFormFields={this.handleInput} defaultText="false" require="true"
       classData="input-field col s8 offset-s2" isEmpty={this.state.clearValue}
       ref={(c_password) => { this.c_password = c_password; }} />

<div className="row center">
    <div className="col s12">
        <button className="btn waves-effect waves-light thin_font" type="submit" onClick={this.submitForm}>
            Reset Password
        <i className="material-icons right">send</i>
    </button>
    </div>
</div>
