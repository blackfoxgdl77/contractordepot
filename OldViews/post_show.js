// create publish
<div>
                <div className="col s12 left color_title_comments">
                    <h4>COMMENT:</h4>
                </div>
                <div className="col s12">
                    <form>
                        <Input name="title" disable="false" labelName="Title"
                               handleFormFields={this.handleInput} defaultText="false" require="true" id="titleField"
                               classData="input-field col s8" isEmpty={this.state.clearValue}
                               ref={(titleField) => { this.titleField = titleField }} />
                        <TextArea name="comments" disable="false" require="true" defaultText="false"
                               handleFormFields={this.handleInput} labelName="Comment" isEmpty={this.state.clearValue}
                               classData="input-field col s8" classCustom="" rowCustom=""
                               ref={(comments) => { this.comments = comments }} />

                        <div className="row left">
                           <div className="col s12">
                               <button className="btn waves-effect waves-light" type="submit" onClick={this.sendComment}>
                                   SUBMIT
                                   <i className="material-icons right">send</i>
                               </button>
                           </div>
                        </div>
                    </form>
                </div>
            </div>

//create winner post
<div>
                    <div className="col s12 left color_title_comments">
                        <h4>
                            COMMENT:
                        </h4>
                    </div>
                    <div className="col s12">
                        <form>
                            <TextArea name="commentsW" disable="false" require="true" defaultText="false"
                                   handleFormFields={this.handleInput} labelName="Comment" isEmpty={this.state.clearValue}
                                   classData="input-field col s8" classCustom="" rowCustom=""
                                   ref={(commentsW) => { this.commentsW = commentsW }} />

                            <div className="row left">
                               <div className="col s12">
                                   <button className="btn waves-effect waves-light" type="submit" onClick={this.sendWinnerComment}>
                                       SUBMIT
                                       <i className="material-icons right">send</i>
                                   </button>
                               </div>
                            </div>
                        </form>
                    </div>
                 </div>

// buttons
<div>
            <div className="col s4 center">
                <a className="waves-effect waves-light btn pulse" onClick={this.modalReport}>
                    <i className="material-icons left">announcement</i>
                        Report Project
                </a>
            </div>
            <div className="col s4 center">
                <a className="waves-effect waves-light btn pulse" onClick={this.modalBids}>
                    <i className="material-icons left">attach_money</i>
                    Make a Bid
                </a>
            </div>
        </div>


        <div className="col s12">
                            {buttonsValidate}
                            <div className={columns}>
                                <a className="waves-effect waves-light btn pulse" onClick={this.modalReviews}>
                                    <i className="material-icons left">announcement</i>
                                    Review Bids
                                </a>
                            </div>
                        </div>

// Message finished project
<div className="row">
                <div className="col s12 center-align text_message_templates">
                    <span className="gross_font blue_color">
                        The Project has finished. Still the bid payment is PENDING.
                    </span>
                </div>
            </div>

<div className="row">
                <div className="col s12 center-align text_message_templates">
                    <span className="gross_font blue_color">
                        The Project has finished. One Bid has been accepted.
                    </span>
                </div>
            </div>

// BIDS ACTION
<div key={this.props.post}>
                <div className="modal margin_modal_react" key={this.props.post} style={styleData}>
                    <div className="modal-content">
                        <div>
                            {errorTemplate}
                            {successTemplate}
                        </div>
                    <div className="col s5 left thin_font blue_color">
                        Post > Show > Make a Bid
                    </div>
                    <div className="col s7">
                        <h3 className="right-align thin_font blue_color">
                            Make a Bid
                        </h3>
                    </div>
                    <div className="justify_align thin_font">
                        <p>
                            Would you like to make a bid?
                        </p>
                        <form>
                            <Input name="offer" disable="false" labelName="Offer Amount $" iconTags="attach_money"
                                   handleFormFields={this.handleInput} defaultText="false" require="true" id="offerField"
                                   classData="input-field col s8 offset-s2" isEmpty={this.state.clearValue}
                                   ref={(offerField) => { this.offerField = offerField }} />
                        </form>
                    </div>
                </div>
                <div className="modal-footer">
                    <a href="#!" className="btn modal-action modal-close waves-effect waves-green btn-flat" type="button"
                       onClick={(e) => this.reportOffer(e)}>
                        Yes, make bid!
                    </a>
                    <a href="#!" className="btn modal-action modal-close waves-effect waves-green btn-flat" type="button"
                       onClick={(e) => this.closeModal(4, e)}>
                        Close
                    </a>
                </div>
            </div>
        </div>

// winner label
<div className="col s4 right-align winner_color">
                                                <h4>
                                                    $ {this.state.post.bids.bid}
                                                </h4>
                                             </div>

// render
<div>
    {modal}
    {titleMessage}
    <div className="row">
        <div className="col s12 left">
            <div className="col s8 left-align">
                <h4>{this.state.post.postname}</h4>
            </div>
            {winner}
        </div>
        <div className="col s12 center">
            {this.displaySliders()}
        </div>
        <div className="col s7 left-align">
            <div className="col s12" className="textSize">
                { this.state.post.description }
            </div>
        </div>
        <div className="col s5 right">
            <div className="col s12">
                <div className="card-panel">
                    <div className="row">
                        <div className="col s12">
                            { this.state.post.postname }
                        </div>
                    </div>
                    <div className="row">
                        <div className="col s4 left-align">
                            Category:
                        </div>
                        <div className="col s8 left-align">
                            { this.state.post.category }
                        </div>
                        <div className="col s4 left-align">
                            City:
                        </div>
                        <div className="col s8 left-align">
                            { this.state.post.city }
                        </div>
                        <div className="col s4 left-align">
                            Published by:
                        </div>
                        <div className="col s8 left-align">
                            { this.state.post.publishedBy }
                        </div>
                        {templateWinner}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {this.loginNoLogin()}
</div>

// inner comments -- reply
<div key={this.props.post}>
                        <div className="modal margin_modal_react" key={this.props.post} style={styleData}>
                            <div className="modal-content">
                                <div>
                                    {errorTemplate}
                                    {successTemplate}
                                </div>
                                <div className="col s5 left thin_font blue_color">
                                    Post > Show > Reply a Comment
                                </div>
                                <div className="col s7">
                                    <h3 className="right-align thin_font blue_color">
                                        Reply a Comment
                                    </h3>
                                </div>
                                <div className="justify_align thin_font">
                                    <form>
                                        <TextArea name="replyComment" disable="false" require="true" defaultText="false"
                                                iconTags="description" handleFormFields={this.handleInput} labelName="Reply a Comment"
                                                classData="input-field col s9 offset-s1" classCustom="" rowCustom="left_move_textarea"
                                                isEmpty={this.state.clearValue}
                                                ref={(replyComments) => { this.replyComments = replyComments }} />

                                        <div className="row center">
                                            <div className="col s12">
                                                <button className="btn waves-effect waves-light" type="submit"
                                                    onClick={(e) => this.saveInnerComments(e)}>
                                                        Reply a Comment
                                                        <i className="material-icons right">send</i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <a href="#!" className="btn modal-action modal-close waves-effect waves-green btn-flat" type="button"
                                    onClick={(e) => this.closeModal(3, e)}>
                                        Close
                                </a>
                            </div>
                        </div>
                    </div>

// Template posts bids
<div>
    <table className="highlight">
        <thead>
            <tr>
                <th className="center-align"> UserName </th>
                <th className="center-align"> Bids </th>
                <th className="center-align"> Options </th>
            </tr>
        </thead>
        <tbody>
            {template}
        </tbody>
    </table>
</div>

<tr key={bidsDone.id_post} className="thin_font blue_color">
<td>
    {bidsDone.username}
</td>
<td>
    $ {bidsDone.moneyBid}
</td>
<td>
    <span>
        <a className="waves-effect waves-light btn thin_font"
           onClick={(e) => this.acceptBidsAction(bidsDone.id_post, e)}>
            <i className="material-icons left">cloud</i>
            Accept Bid
        </a>
    </span>
    <span className="left_separate_button">
        <a className="waves-effect waves-light btn thin_font"
           onClick={(e) => this.declineBidsAction(bidsDone.id_post, e)}>
            <i className="material-icons left">cloud</i>
            Decline Bid
        </a>
    </span>
</td>
</tr>

<tr key={bidsDone.id_post} className="thin_font blue_color">
    <td>
        {bidsDone.username}
    </td>
    <td>
        $ {bidsDone.moneyBid}
    </td>
    <td>
        <span>
            <b>
                Bid Aceppted ({extraTitle})
            </b>
        </span>
        <span className="left_separate_button">
            <b>
                {stringOption}
            </b>
        </span>
    </td>
</tr>

// Review bids Action
<div key={this.props.post}>
        <div className="modal margin_modal_react" key={this.props.post} style={styleData}>
            <div className="modal-content">
                <div>
                    {errorTemplate}
                    {successTemplate}
                </div>
            <div className="col s5 left thin_font blue_color">
                Post > Show > Review Bids
            </div>
            <div className="col s7">
                <h3 className="right-align thin_font blue_color">
                    Bids Done for the Project!
                </h3>
            </div>
            <div className="justify_align thin_font">
                <div>
                    { bidsBody }
                </div>
            </div>
        </div>
        <div className="modal-footer">
            <a href="#!" className="btn modal-action modal-close waves-effect waves-green btn-flat" type="button"
               onClick={(e) => this.closeModal(2, e)}>
                Close
            </a>
        </div>
    </div>
</div>

// redirect
<div className="modal margin_modal_react_loader" key="1" style={styleData}>
    <div className="col s12">
        <div className="center-spiner">
            <div className="loaderSpin"></div>
        </div>
    </div>
    <div className="col s12 font-size-redirect center-align colors_links gross_font top_space">
        Redirecting to Payment platform ....
    </div>
</div>

// reportAction
<div key={this.props.post}>
                <div className="modal margin_modal_react" key={this.props.post} style={styleData}>
                    <div className="modal-content">
                        <div>
                            {errorTemplate}
                            {successTemplate}
                        </div>
                    <div className="col s5 left thin_font blue_color">
                        Post > Show > Report Project
                    </div>
                    <div className="col s7">
                        <h3 className="right-align thin_font blue_color">
                            Report Project
                        </h3>
                    </div>
                    <div className="justify_align thin_font">
                        Are you sure you want to report the projects <b>'{this.state.post.postname}'</b> as fake?
                    </div>
                </div>
                <div className="modal-footer">
                    <a href="#!" className="btn modal-action modal-close waves-effect waves-green btn-flat" type="button"
                       onClick={(e) => this.reportProject(e)}>
                        Report the Project!
                    </a>
                    <a href="#!" className="btn modal-action modal-close waves-effect waves-green btn-flat" type="button"
                       onClick={(e) => this.closeModal(1, e)}>
                        Close
                    </a>
                </div>
            </div>
        </div>

// winner comments
<div className="" key={winnerC.id_winner}>
    <div className="col s12 left">
        <hr className="blue_color" />
        <article>
            <div className="col s12 left gross_font padding-left font-size-title">
                <div className="col s12">
                    <span className="blue_color">
                        "{ winnerC.projectWinner }"
                    </span>
                    <span className="left_date_separation">
                        { winnerC.dateCreates }
                    </span>
                </div>
            </div>
            <div className="col s12 left thin_font font-size-body">
                { winnerC.comments }
            </div>
            <div className="col s12">
                &nbsp;
            </div>
        </article>
    </div>
</div>

// all comments
<div className="" key={comments.id_publish}>
    <div className="col s12 left">
        <hr className="blue_color" />
        <article>
            <div className="col s12 left gross_font padding-left font-size-title">
                <div className="col s12">
                    <span className="blue_color">
                        "{ comments.publish_username }"
                    </span>
                    <span className="left_date_separation">
                        { comments.date_created }
                    </span>
                </div>
            </div>
            <div className="col s12 left gross_font font-size-body">
                { comments.title }
            </div>
            <div className="col s12 left thin_font font-size-body">
                { comments.comments }
            </div>
            <div className="col s12 font-size-body">
                <a href="#" className="links_hover_blue" onClick={(e) => this.modalComments(comments.id_publish, e)}>
                    Reply
                </a>
            </div>
            <div className="col s12">
                &nbsp;
            </div>
            <div className="col s12">
                { comments.innerComments.map((innerData) => {
                    return (
                        <div className="col s12 left" key={innerData.id_comment}>
                            <article>
                                <div className="col s12 gross_font font-size-body">
                                    <div className="col s12">
                                        <span className="blue_color">
                                            "{innerData.username_comment}"
                                        </span>
                                        <span className="left_date_separation">
                                            {innerData.date_created}
                                        </span>
                                    </div>
                                </div>
                                <div className="col s12 thin_font font-size-inner-comment left_margin_subcomments">
                                    {innerData.comments}
                                </div>
                            </article>
                        </div>
                    );
                })}
            </div>
        </article>
    </div>
</div>
