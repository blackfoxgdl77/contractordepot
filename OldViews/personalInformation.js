{errorTemplate}
{successTemplate}

<form>
    <Hidden name="id" items={this.state.id} />
    <Hidden name="source" items={this.props.source} />
    <Hidden name="token" items={this.props.reference} />

    <ul className="collapsible expandable">
        <li>
            <div className="collapsible-header">
                Account Information
            </div>
            <div className="collapsible-body">
                <Input name="username" disable="true" labelName="Username" iconTags="account_box"
                        classData="input-field col s8 offset-s2" hide=""
                        handleFormFields={this.handleInput} items={this.state.username} defaultText="true" />
                <Input name="name" disable="false" labelName="Name" iconTags="account_box"
                       classData="input-field col s8 offset-s2" hide=""
                       handleFormFields={this.handleInput} items={this.state.name} defaultText={nameFlag} />
                <InputEmail name="email" disable="false" labelEmail="Email" iconTags="email"
                        classData="input-field col s8 offset-s2" hide=""
                        handleFormFields={this.handleInput} items={this.state.email} defaultText={emailFlag} />
                <InputTel name="phone" labelName="Phone #" iconTags="phone" disable="false" hide=""
                        classData="input-field col s8 offset-s2" defaultText={phoneFlag}
                        handleFormFields={this.handleInput} items={this.state.phone} />
            </div>
        </li>
        <li>
            <div className="collapsible-header">
                Personal Information
            </div>
            <div className="collapsible-body">
                <Input name="payment_method" labelName="Payment Method" iconTags="account_box" disable="false"
                        classData="input-field col s8 offset-s2" defaultText={paymentFlag} hide=""
                        handleFormFields={this.handleInput} items={this.state.payment_method} />
                <InputEmail name="bbb_account" labelEmail="BBB Account (EX: xxxx@xxx.com)" iconTags="account_box"
                        classData="input-field col s8 offset-s2" defaultText="" hide={hideFields}
                        handleFormFields={this.handleInput} items={this.state.bbb_account} disable="false" />
                <RadioButton name="bbb_accredited" disable="false" labelRadio="Are you Accredited?" iconTags=""
                        handleFormFields={this.handleInput} defaultText="false" require="false"
                        classData="col s8 offset-s2" optionsValues={optionAccredited}
                        setVal={this.state.bbb_accredited} />
                <Input name="raiting" labelName="Raiting" iconTags="grade" disable="false"
                       classData="input-field col s8 offset-s2" defaultText="" hide={hideFields}
                       handleFormFields={this.handleInput} items={this.state.raiting} />
                <Input name="wcb_number" labelName="WCB Number" iconTags="" disable="false" hide={hideFields}
                        classData="input-field col s8 offset-s2" defaultText={wcb_numberFlag}
                        handleFormFields={this.handleInput} items={this.state.wcb_number} />
                <Input name="address" labelName="Address" iconTags="account_box" disable="false"
                        classData="input-field col s8 offset-s2" defaultText={addressFlag} hide=""
                        handleFormFields={this.handleInput} items={this.state.address} />
            </div>
        </li>
    </ul>
           <div className="row right">
               <div className="col s12">
                   <button className="btn waves-effect waves-light" type="submit" onClick={this.submitForm}>
                       Save
                       <i className="material-icons right">send</i>
                   </button>
               </div>
           </div>

         </form>
