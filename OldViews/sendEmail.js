<InputEmail name="email" labelEmail="Email" iconTags="email" defaultText="false"
            handleFormFields={this.handleInput} disable="false" require="true"
            classData="input-field col s8 offset-s2" isEmpty={this.state.clearValue}
            ref={(email) => { this.email = email; }} />
<InputEmail name="c_email" labelEmail="Confirm Email" iconTags="email" defaultText="false"
            handleFormFields={this.handleInput} disable="false" require="true"
            classData="input-field col s8 offset-s2" isEmpty={this.state.clearValue}
            ref={(c_email) => { this.c_email = c_email; }} />

<div className="row center">
    <div className="col s6 offset-s3">
        <button className="btn waves-effect waves-light" type="submit" onClick={this.submitForm}>
            Reset Password
            <i className="material-icons right">send</i>
        </button>
    </div>
</div>
