/**
 * Template will be used to display the messages
 * once the user has done some things wrong or
 * inclusive the systems returns an error in every
 * action done by it
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */
class Errors extends React.Component {
    constructor(props) {
        super(props);

        this.getValues  = this.getValues.bind(this);
        this.closeClick = this.closeClick.bind(this);

        this.state = {
            windowPath : (window.$PATH !== undefined) ? window.$PATH : ""
        };
    }

    getValues() {
        let options = [];

        for (let key in this.props.errors) {
            if (this.props.errors.hasOwnProperty(key)) {
                if (this.props.errors[key] != '') {
                    options.push({
                        label: key,
                        value: this.props.errors[key]
                    });
                }
            }
        }

        return options;
    }

    closeClick = () => {
        this.props.onClick();
    }

    render() {
        //let displayeErrors = this.getValues();
        //let listErrors, listErrors2;
        //let imgUrl = this.state.windowPath + "/bundles/site/images/website/delete.png";

        /*if (this.props.errors != "") {
            listErrors2 = displayeErrors.map((error, index) =>
                            <li key={error.label} className="left-align">
                                <strong>
                                    * {error.value}
                                </strong>
                            </li>
                );

            listErrors = <div>
                            <p className="text_message_templates left-align valign-wrapper">
                                Incorrect Data
                            </p>
                            <ul>
                                { listErrors2 }
                            </ul>
                         </div>;
        } else {
            listErrors = <div>
                            <p className="text_message_templates left-align valign-wrapper">
                                Error!
                            </p>
                            <p className="text_message_inner_message_template left-align valign-wrapper">
                                {this.props.respError}
                            </p>
                        </div>;
        }

        return (
            <div className="row center error_color">
                <div className="col s12 color_text_white">
                    <div className="col s1">
                        <p className="center-align valign-wrapper">
                            <img src={imgUrl} width="70" height="70" />
                        </p>
                    </div>
                    <div className="col s11">
                        <p className="right">
                            <a href="#" className="colors_links" onClick={this.closeClick}>
                                <i className="material-icons small">cancel</i>
                            </a>
                        </p>
                        {listErrors}
                    </div>
                </div>
            </div>
        );
    }
}
