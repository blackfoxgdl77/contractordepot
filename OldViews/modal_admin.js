// modal view
<div className="modal margin_modal_react" key={this.state.data.id} style={styleData}>
                    <div className="modal-content">
                        <div className="col s5 left thin_font blue_color">
                            Admin > {this.getTitle(this.state.data.section)} > View Details Information
                        </div>
                        <div className="col s7">
                            <h3 className="right-align thin_font blue_color">
                                {this.getTitle(this.state.data.section)}
                            </h3>
                        </div>
                        <div className="justify_align thin_font">
                            <span className="gross_font">Description:</span>
                            <span className="thin_font">{this.state.data.description}</span>
                        </div>
                        <div className="center">
                            {templateImgVideo}
                        </div>
                        <div className="justify_align thin_font">
                            <span className="gross_font">Created At:</span>
                            <span className="thin_font">{this.state.data.created_at}</span>
                        </div>
                        <div className="justify_align thin_font">
                            <span className="gross_font">Updated At:</span>
                            <span className="thin_font">{this.state.data.updated_at}</span>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <a href="#!" className="modal-action modal-close waves-effect waves-green btn-flat"
                           onClick={(e) => this.toggle(null, '', e)}>
                            Close
                        </a>
                    </div>
                </div>

// modal edit
<div className="modal margin_modal_react" key={this.state.data.id} style={styleData}>
                    <div className="modal-content">
                        <div className="">
                            {errorTem2}
                            {errorTemp}
                            {successM}
                        </div>
                        <div className="col s5 left thin_font blue_color">
                            Admin > {this.getTitle(this.state.data.section)} > Delete Information
                        </div>
                        <div className="col s7">
                            <h3 className="right-align thin_font blue_color">
                                {this.getTitle(this.state.data.section)}
                            </h3>
                        </div>
                        <div className="justify_align thin_font">
                            <Hidden value={this.state.data.id} id="editId" name="editId" />
                            <Select name="sectionUpdate" disable="false" labelSelectName="Section" iconTags="dashboard"
                                    require="true"
                                    handleFormFields={this.handleInput} defaultText="false"
                                    classData="input-field col s8 offset-s2" value={this.state.data.sectionId}
                                    displayDataValues={categories} id="sectionIDUpdate"
                                    ref={(sectionUpdate) => { this.sectionUpdate = sectionUpdate }} />
                            <TextArea name="descriptionUpdate" disable="false" require="true"
                                    defaultText="true"
                                    is_update="true"
                                    items={this.state.data.description}
                                    iconTags="description"
                                    handleFormFields={this.handleInput} labelName="Description"
                                    classData="input-field col s9 offset-s1"
                                    classCustom=""
                                    rowCustom="left_move_textarea"
                                    ref={(descriptionUpdate) => { this.descriptionUpdate = descriptionUpdate }} />
                            <Select name="optionsUpdate" labelSelectName="Select an Option" iconTags="format_list_numbered"
                                    require="false" disable="false" id="optionsIdUpdates" handleFormFields={this.handleInput}
                                    value={this.state.data.flagOption} defaultText="false"
                                    displayDataValues={options} classData="input-field col s8 offset-s2"
                                    ref={(optionsUpdate) => { this.optionsUpdate = optionsUpdate }} />
                            <InputFile name="fileUpdate" labelButton="File" handleFormFields={this.handleInput}
                                    iconTags="" disable={updateFlagFile}
                                    classData="file-field input-field col s8 offset-s2"
                                    ref={(fileUpdate) => { this.fileUpdate = fileUpdate }} />
                            <Input name="url_videoUpdate" disable={updateFlagVideo} require="false"
                                    defaultText="true"
                                    is_update="true"
                                    iconTags="live_tv"
                                    items={this.state.data.url}
                                    handleFormFields={this.handleInput} labelName="Video URL"
                                    classData="input-field col s8 offset-s2"
                                    ref={(url_videoUpdate) => { this.url_videoUpdate = url_videoUpdate }} />
                            <Select name="statusUpdate" labelSelectName="Status" iconTags="iso" require="true"
                                    disable="false"
                                    value={this.state.data.statusId}
                                    id="statusIdUpdate" handleFormFields={this.handleInput} defaultText="false"
                                    displayDataValues={values}
                                    classData="input-field col s8 offset-s2"
                                    ref={(statusUpdate) => { this.statusUpdate = statusUpdate }} />
                        </div>
                    </div>
                    <div className="modal-footer">
                        <a href="#!" className="btn modal-action modal-close waves-effect waves-green btn-flat" type="button"
                           onClick={(e) => this.updateRecord(e)}>
                            Update
                        </a>
                        <a href="#!" className="btn modal-action modal-close waves-effect waves-green btn-flat" type="button"
                           onClick={(e) => this.toggle(null, '', e)}>
                            Close
                        </a>
                    </div>
                </div>

//delete modal
<div className="modal margin_modal_react" key={this.state.data.id} style={styleData}>
                    <div className="modal-content">
                        <div className="col s5 left thin_font blue_color">
                            Admin > {this.getTitle(this.state.data.section)} > Delete Information
                        </div>
                        <div className="col s7">
                            <h3 className="right-align thin_font blue_color">
                                {this.getTitle(this.state.data.section)}
                            </h3>
                        </div>
                        <div className="justify_align thin_font">
                            Are you sure you want to delete the record selected?
                        </div>
                    </div>
                    <div className="modal-footer">
                        <a href="#!" className="btn modal-action modal-close waves-effect waves-green btn-flat" type="button"
                           onClick={(e) => this.deleteRecord(e)}>
                            Delete
                        </a>
                        <a href="#!" className="btn modal-action modal-close waves-effect waves-green btn-flat" type="button"
                           onClick={(e) => this.toggle(null, '', e)}>
                            Cancel
                        </a>
                    </div>
                </div>
