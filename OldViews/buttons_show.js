if (this.state.post.status == 1) {
    if (this.props.userId != this.state.post.owner) {
        buttonsValidate =   <div><div className="six wide column">
                                    <div className="ui negative fluid button" onClick={this.modalReport}>
                                        <i className="bell icon"></i>
                                        Report Project
                                    </div>
                                </div>
                                <div className="six wide column">
                                    <div className="ui positive fluid button" onClick={this.modalBids}>
                                        <i className="dollar sign icon"></i>
                                        Make a Bid
                                    </div>
                                </div></div>;
    }
}


if (this.state.post.status == 1 || this.state.post.bids.statusBid == 3) {
    completeButtons =   <div className="twelve wide column">
                            {buttonsValidate}
                            <div className={columns}>
                                <div className="ui positive fluid button" onClick={this.modalReviews}>
                                    <i className="dollar sign icon"></i>
                                    Review Bids
                                </div>
                            </div>
                        </div>;
}
