// table display information
<table className="striped responsive-table">
                    <thead>
                        <tr>
                            <th className="center-align">
                                Project Name
                            </th>
                            <th className="center-align">
                                Final Amount
                            </th>
                            <th className="center-align">
                                Date Created
                            </th>
                            <th className="center-align">
                                Status
                            </th>
                            <th className="center-align">
                                Options
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        { bodyContent }
                    </tbody>
               </table>

               <tr key={info.idPost}>
                   <td>
                       { info.namePost }
                   </td>
                   <td>
                       { info.amount }
                   </td>
                   <td>
                       { info.dateCrt }
                   </td>
                   <td>
                       PENDING
                   </td>
                   <td>
                       <a href="#" className="waves-effect waves-light btn" onClick={(evt) => this.toggle(info, evt)}>
                           <i className="material-icons left">assignment</i>
                           Details
                       </a>
                       <a href={this.state.link} className="waves-effect waves-light btn left_separate_button">
                           <i className="material-icons left">attach_money</i>
                           Make Payment
                       </a>
                   </td>
               </tr>

// modal
<div className="modal margin_modal_react" key={this.state.modalData.idPost} style={styleData}>
                        <div className="modal-content">
                            <div className="col s5 left thin_font blue_color">
                                Projects > Pending Projects Paid > Details
                            </div>
                            <div className="col s7">
                                <h3 className="right-align thin_font blue_color">
                                    Pending Projects Details
                                </h3>
                            </div>
                            <div className="col s12">
                                <h4 className="center-align thin_font blue_color">
                                    Details of the project you have not paid!
                                </h4>
                            </div>
                            <div className="center gross_font font-size-title">
                                Post Details
                            </div>
                            <div className="justify_align thin_font">
                                <span className="gross_font">Name Post:</span>
                                <span className="thin_font">{ this.state.modalData.namePost }</span>
                            </div>
                            <div className="justify_align thin_font">
                                <span className="gross_font">Description:</span>
                                <span className="thin_font">{ this.state.modalData.postDesc }</span>
                            </div>
                            <div className="justify_align thin_font">
                                <span className="gross_font">Username Owner:</span>
                                <span className="thin_font">{ this.state.modalData.username }</span>
                            </div>
                            <div className="justify_align thin_font">
                                <span className="gross_font">Owner:</span>
                                <span className="thin_font">{ this.state.modalData.owner }</span>
                            </div>
                            <div className="justify_align thin_font">
                                <span className="gross_font">Email Owner:</span>
                                <span className="thin_font">{ this.state.modalData.emailO }</span>
                            </div>
                            <div className="justify_align thin_font">
                                <span className="gross_font">Category:</span>
                                <span className="thin_font">{ this.state.modalData.category }</span>
                            </div>
                            <div className="justify_align thin_font">
                                <span className="gross_font">City Name:</span>
                                <span className="thin_font">{ this.state.modalData.cityName }</span>
                            </div>
                            <div className="justify_align thin_font">
                                <span className="gross_font">Capital:</span>
                                <span className="thin_font">{ this.state.modalData.capital }</span>
                            </div>
                            <div className="justify_align thin_font">
                                <span className="gross_font">Date Created:</span>
                                <span className="thin_font">{ this.state.modalData.dateCrt }</span>
                            </div>
                            <div className="justify_align thin_font">
                                <span className="gross_font">Date Updated:</span>
                                <span className="thin_font">{ this.state.modalData.dateUpt }</span>
                            </div>
                            <div className="center gross_font font-size-title">Bid Winner</div>
                            <div className="justify_align thin_font">
                                <span className="gross_font">Username Winner:</span>
                                <span className="thin_font">{ this.state.modalData.userWin }</span>
                            </div>
                            <div className="justify_align thin_font">
                                <span className="gross_font">Winner Amount:</span>
                                <span className="thin_font">{ this.state.modalData.amount }</span>
                            </div>
                            <div className="justify_align thin_font">
                                <span className="gross_font">Winner Name:</span>
                                <span className="thin_font">{ this.state.modalData.userNameW }</span>
                            </div>
                            <div className="justify_align thin_font">
                                <span className="gross_font">Email User Winner:</span>
                                <span className="thin_font">{ this.state.modalData.userEmail }</span>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <a href="#!" className="modal-action modal-close waves-effect waves-green btn-flat"
                               onClick={(e) => this.toggle('', e)}>
                                Close
                            </a>
                        </div>
                   </div>
