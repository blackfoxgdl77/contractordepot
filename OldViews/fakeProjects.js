<ul className="collection">
                {collection}
              </ul>;

              <li className="collection-item" key={post.id}>
                  <div className="col s1 left-align gross_font blue_color">
                      Project Name:
                  </div>
                  <div className="col s11 left-align thin_font">
                      { post.name }
                  </div>
                  <div className="col s1 left-align gross_font blue_color">
                      Description:
                  </div>
                  <div className="col s11 left-align thin_font">
                      { post.desc }
                  </div>
                  <div className="col s1 left-align gross_font blue_color">
                      Category:
                  </div>
                  <div className="col s11 left-align thin_font">
                      { post.category_name }
                  </div>

                  <div className="col s1 left-align gross_font blue_color">
                      City:
                  </div>
                  <div className="col s11 left-align thin_font">
                      { post.city_name }
                  </div>

                  <div className="right">
                      <a className="waves-effect waves-light btn" onClick={(e) => this.toggle(post, e)}>
                          <i className="material-icons right">assignment_ind</i>List Users
                      </a>
                  </div>
                  <div className="clear: both">&nbsp;</div>
                  <div>&nbsp;</div>
              </li>


/// modal
<div className="modal margin_modal_react" key={this.state.dataInfo.fakeProject} style={styleDisplay}>
                    <div className="modal-content">
                        <div className="col s5 left thin_font blue_color">
                            Admin > Fake Projects > List of Users Reported
                        </div>
                        <div className="col s7">
                            <h3 className="right-align thin_font blue_color">
                                List of Users Reported
                            </h3>
                        </div>
                        <div className="col s12">
                            <ul className="collection">
                                {collectionUsers}
                            </ul>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <a href="#!" className="modal-action modal-close waves-effect waves-green btn-flat"
                           onClick={(e) => this.toggle('', e)}>
                                Close
                        </a>
                    </div>
                </div>

                <li className="collection-item blue_color" key={user.userId}>
                    <div className="col s1 gross_font blue_color">
                        Username:
                    </div>
                    <div className="col s11 thin_font blue_color">
                        { user.username }
                    </div>
                    <div className="col s1 gross_font blue_color">
                        Name:
                    </div>
                    <div className="col s11 thin_font blue_color">
                        { user.name }
                    </div>
                    <div className="col s1 gross_font blue_color">
                        Email:
                    </div>
                    <div className="col s11 thin_font blue_color">
                        { user.email }
                    </div>
                    <div className="col s1 gross_font blue_color">
                        User Type:
                    </div>
                    <div className="col s11 thin_font blue_color">
                        { user.typeUser }
                    </div>
                </li>
