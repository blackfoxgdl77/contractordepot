// contractors
<Input name="name" disable="false" labelName="Contractor Name" iconTags="account_box"
       handleFormFields={this.handleInput} defaultText="false" require="true" id="nameField"
       classData="input-field col s8 offset-s2" isEmpty={this.state.clearValue}
       ref={(nameField) => { this.nameField = nameField }} />
<InputTel name="phone" disable="false" labelName="Phone #" iconTags="phone"
       handleFormFields={this.handleInput} defaultText="false" require="true"
       classData="input-field col s8 offset-s2" isEmpty={this.state.clearValue}
       ref={(phoneField) => { this.phoneField = phoneField }} />
<InputEmail name="email" disable="false" labelEmail="Email" iconTags="email"
       handleFormFields={this.handleInput} defaultText="false" require="true"
       classData="input-field col s8 offset-s2" isEmpty={this.state.clearValue}
       ref={(emailField) => { this.emailField = emailField }} />
<RadioButton name="bbb_accredited" disable="false" labelRadio="Are you BBB Accredited?" iconTags=""
       handleFormFields={this.handleInput} defaultText="false" require="true"
       classData="col s8 offset-s2" optionsValues={optionAccredited}
       ref={(bbbAccredited) => { this.bbbAccredited = bbbAccredited }} />
<Input name="raiting" disable="false" labelName="Raiting" iconTags="grade"
       classData="input-field col s8 offset-s2" hide={flagDisplay} isEmpty={this.state.clearValue}
       handleFormFields={this.handleInput} defaultText="false" require="false"
       ref={(raiting) => { this.raiting = raiting }} />
<Input name="wcb_number" disable="false" labelName="WCB # (Optional)" iconTags="exposure"
       classData="input-field col s8 offset-s2" isEmpty={this.state.clearValue}
       handleFormFields={this.handleInput} defaultText="false" require="false"
       ref={(wcb_number) => { this.wcb_number = wcb_number }} />
<Password name="password" disable="false" labelName="Password" iconTags="lock"
       handleFormFields={this.handleInput} defaultText="false" require="true"
       classData="input-field col s8 offset-s2" isEmpty={this.state.clearValue}
       ref={(passwordField) => { this.passwordField = passwordField }} />
<Password name="c_password" disable="false" labelName="Confirm Password" iconTags="lock"
       handleFormFields={this.handleInput} defaultText="false" require="true"
       classData="input-field col s8 offset-s2" isEmpty={this.state.clearValue}
       ref={(cpasswordField) => { this.cpasswordField = cpasswordField }} />
<Checkbox name="termsConditions" labelName="Accept Terms and Conditions"
       handleFormFields={this.handleInput} require="true" disable="false"
       classData="col s8 offset-s2" dataSent={this.state.dataMessage}
       ref={(conditions) => { this.conditions = conditions }} />

<div className="row center">
    <div className="col s12">
        <button className="btn waves-effect waves-light thin_font" type="submit" onClick={this.submitFormContractor}>
            Register
            <i className="material-icons right">send</i>
        </button>
    </div>
</div>

//home owner
<Hidden value={this.props.source} name="source" />
<Hidden value={this.props.reference} name="token" />

<Input name="nameHO" disable="false" labelName="Home Owner Name" iconTags="account_box"
       handleFormFields={this.handleInput} defaultText="false" require="true"
       classData="input-field col s8 offset-s2" isEmpty={this.state.clearValue}
       ref={(nameFieldH) => { this.nameFieldH = nameFieldH }} />
<InputTel name="phoneHO" disable="false" labelName="Phone #" iconTags="phone"
       handleFormFields={this.handleInput} defaultText="false" require="true"
       classData="input-field col s8 offset-s2" isEmpty={this.state.clearValue}
       ref={(phoneFieldH) => { this.phoneFieldH = phoneFieldH }} />
<InputEmail name="emailHO" disable="false" labelEmail="Email" iconTags="email"
       handleFormFields={this.handleInput} defaultText="false" require="true"
       classData="input-field col s8 offset-s2" isEmpty={this.state.clearValue}
       ref={(emailFieldH) => { this.emailFieldH = emailFieldH }} />
<Password name="passwordHO" disable="false" labelName="Password" iconTags="lock"
       handleFormFields={this.handleInput} defaultText="false" require="true"
       classData="input-field col s8 offset-s2" isEmpty={this.state.clearValue}
       ref={(passwordFieldH) => { this.passwordFieldH = passwordFieldH }} />
<Password name="c_passwordHO" disable="false" labelName="Confirm Password" iconTags="lock"
       handleFormFields={this.handleInput} defaultText="false" require="true"
       classData="input-field col s8 offset-s2" isEmpty={this.state.clearValue}
       ref={(cpasswordFieldH) => { this.cpasswordFieldH = cpasswordFieldH }} />
<Checkbox name="termsConditions2" labelName="Accept Terms and Conditions"
       handleFormFields={this.handleInput} require="true" disable="false"
       classData="col s8 offset-s2" dataSent={this.state.dataMessage}
       ref={(conditionsH) => { this.conditionsH = conditionsH }} />

<div className="row center">
    <div className="col s12">
        <button className="btn waves-effect waves-light thin_font" type="submit" onClick={this.submitFormHome}>
            Register
            <i className="material-icons right">send</i>
        </button>
    </div>
</div>
