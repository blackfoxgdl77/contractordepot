<div className={parameters}>
    <div className={dynamicClass}>
        <i className="material-icons prefix">{this.props.iconTags}</i>
        <input type="tel"
               className="validate thin_font"
               id={this.props.name}
               name={this.props.name}
               value={this.state.value}
               {...propsInput}
               onChange={this.handleTel}
               onBlur={this.onBlur} />
        <label htmlFor={this.props.telname} className="thin_font">{this.props.labelName}</label>
    </div>
</div>
