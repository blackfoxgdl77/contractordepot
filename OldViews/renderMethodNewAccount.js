// render method new account
<div className="row top_space_tabs">
    <div className="five wide column">
        <a className="waves-effect waves-light grey lighten-1 btn-large right thin_font" id="contractor_link"
           onClick={this.contractorShow}>
            Contractor
        </a>
    </div>
    <div className="five wide column">
        <a className="waves-effect waves-light grey lighten-1 btn-large left thin_font" id="home_owner_link"
           onClick={this.homeOwnerShow}>
            Home Owner
        </a>
    </div>
</div>

// form contractor
<div className="row">
    <div className="twelve wide column hide_forms1" id="contractor">
        <form className="ui large form thin_font">
            <div className="ui tall stacked segment">
                <div className="field">
                    <Input name="nameContractor" disable="false" labelName="Contractor Name" iconTags="user icon"
                           handleFormFields={this.handleInput} defaultText="Contractor Name" require="true"
                           id="nameField" classData="ui left icon input" inputClass=""
                           isEmpty={this.state.clearValue}
                           ref={(nameField) => { this.nameField = nameField }} />
                </div>
                <div className="field">
                    <InputTel name="phone" disable="false" labelName="Phone #" iconTags="phone icon"
                           handleFormFields={this.handleInput} defaultText="Phone #" require="true"
                           classData="ui left icon input" isEmpty={this.state.clearValue} inputClass=""
                           ref={(phoneField) => { this.phoneField = phoneField }} />
                </div>
                <div className="field">
                    <InputEmail name="email" disable="false" labelEmail="Email" iconTags="envelope icon"
                           handleFormFields={this.handleInput} defaultText="Email (john.doe@example.com)" require="true"
                           classData="ui left icon input" isEmpty={this.state.clearValue}
                           ref={(emailField) => { this.emailField = emailField }} />
                </div>
                <div className="field">
                    <RadioButton name="bbb_accredited" disable="false" labelRadio="Are you BBB Accredited?" iconTags=""
                           handleFormFields={this.handleInput} defaultText="false" require="true"
                           classData="col s8 offset-s2" optionsValues={optionAccredited}
                           ref={(bbbAccredited) => { this.bbbAccredited = bbbAccredited }} />
                </div>
                <div className="field">
                    <Input name="raiting" disable="false" labelName="Raiting" iconTags="star icon"
                           classData="ui left icon input" hide={flagDisplay} isEmpty={this.state.clearValue}
                           handleFormFields={this.handleInput} defaultText="Raiting" require="false"
                           ref={(raiting) => { this.raiting = raiting }} />
                </div>
                <div className="field">
                    <Input name="wcb_number" disable="false" labelName="WCB # (Optional)" iconTags="archive icon"
                           classData="ui left icon input" isEmpty={this.state.clearValue}
                           handleFormFields={this.handleInput} defaultText="WCB # (Optional)" require="false"
                           ref={(wcb_number) => { this.wcb_number = wcb_number }} />
                </div>
                <div className="field">
                    <Password name="password" disable="false" labelName="Password" iconTags="lock icon"
                           handleFormFields={this.handleInput} defaultText="Password" require="true"
                           classData="ui left icon input" isEmpty={this.state.clearValue} inputClass=""
                           ref={(passwordField) => { this.passwordField = passwordField }} />
                </div>
                <div className="field">
                    <Password name="c_password" disable="false" labelName="Confirm Password" iconTags="lock icon"
                           handleFormFields={this.handleInput} defaultText="Confirm Password" require="true"
                           classData="ui left icon input" isEmpty={this.state.clearValue} inputClass=""
                           ref={(cpasswordField) => { this.cpasswordField = cpasswordField }} />
                </div>
                <div className="field">
                    <Checkbox name="termsConditions" labelName="Accept Terms and Conditions"
                           handleFormFields={this.handleInput} require="true" disable="false"
                           classData="" dataSent={this.state.dataMessage}
                           ref={(conditions) => { this.conditions = conditions }} />
                </div>
                <div className="field">
                    <button className="ui fluid teal button right" type="submit" onClick={this.submitFormContractor}>
                        Create Account
                    </button>
                </div>
            </div>
       </form>
    </div>

// form home owner
<div className="twelve wide column hide_forms" id="home_owner">
   <form className="ui large form thin_font">
        <div className="field">
           <Input name="nameHO" disable="false" labelName="Home Owner Name" iconTags="user icon"
                  handleFormFields={this.handleInput} defaultText="Home Owner Name" require="true"
                  classData="ui left icon input" isEmpty={this.state.clearValue} inputClass=""
                  ref={(nameFieldH) => { this.nameFieldH = nameFieldH }} />
        </div>
        <div className="field">
            <InputTel name="phoneHO" disable="false" labelName="Phone #" iconTags="phone icon"
                   handleFormFields={this.handleInput} defaultText="Phone #" require="true"
                   classData="ui left icon input" isEmpty={this.state.clearValue} inputClass=""
                   ref={(phoneFieldH) => { this.phoneFieldH = phoneFieldH }} />
        </div>
        <div className="field">
        <InputEmail name="emailHO" disable="false" labelEmail="Email" iconTags="envelope icon"
               handleFormFields={this.handleInput} defaultText="Email (john.doe@example.com)" require="true"
               classData="ui left icon input" isEmpty={this.state.clearValue} inputClass=""
               ref={(emailFieldH) => { this.emailFieldH = emailFieldH }} />
        </div>
        <div className="field">
            <Password name="passwordHO" disable="false" labelName="Password" iconTags="lock icon"
                   handleFormFields={this.handleInput} defaultText="Password" require="true"
                   classData="ui left icon input" isEmpty={this.state.clearValue} inputClass=""
                   ref={(passwordFieldH) => { this.passwordFieldH = passwordFieldH }} />
        </div>
        <div className="field">
            <Password name="c_passwordHO" disable="false" labelName="Confirm Password" iconTags="lock icon"
                   handleFormFields={this.handleInput} defaultText="Confirm Password" require="true"
                   classData="ui left icon input" isEmpty={this.state.clearValue} inputClass=""
                   ref={(cpasswordFieldH) => { this.cpasswordFieldH = cpasswordFieldH }} />
        </div>
        <div className="field">
            <Checkbox name="termsConditions" labelName="Accept Terms and Conditions"
                   handleFormFields={this.handleInput} require="true" disable="false"
                   classData="" dataSent={this.state.dataMessage}
                   ref={(conditions) => { this.conditions = conditions }} />
        </div>
        <div className="field">
            <button className="ui fluid teal button right" type="submit" onClick={this.submitFormContractor}>
                Create Account
            </button>
        </div>
  </form>
</div>
