/**
 * Template will be used to create the
 * success message with all the data is going
 * to be displayed to the user once the action
 * was executed successfully
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 */
class SuccessTemplate extends React.Component {
    constructor(props) {
        super(props);

        this.closeClickSuccess = this.closeClickSuccess.bind(this);

        this.state = {
            windowPath : (window.$PATH !== undefined) ? window.$PATH : ""
        };
    }

    closeClickSuccess = () => {
        this.props.onClick();
    }

    render() {
        let templates;
        let imgUrl = this.state.windowPath + "/bundles/site/images/website/success.png";

        if (this.props.extraFlag == 1 || this.props.extraFlag == "1") {
            templates = <div className="blue_color font_body_template center-align">
                            <strong>
                                { this.props.successMessage }
                            </strong>
                        </div>;
        }

        return (
            <div>
                <div className="row center success_color">
                    <div className="col s12 color_text_white">
                        <div className="col s1">
                            <p className="center-align">
                                <img src={imgUrl} width="60" height="75" />
                            </p>
                        </div>
                        <div className="col s11">
                            <p className="right">
                                <a href="#" className="colors_links" onClick={this.closeClickSuccess}>
                                    <i className="material-icons small">cancel</i>
                                </a>
                            </p>
                            <p className="text_message_templates left-align valign-wrapper">
                                {this.props.successTitle}
                            </p>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col s12">
                        { templates }
                    </div>
                </div>
            </div>
        );
    }
}
