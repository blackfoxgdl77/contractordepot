<div className={parameters}>
    <div className={dynamicClass}>
        {iconTags}
        <textarea
            {...propsInput}
            value={this.state.value}
            id={this.props.name}
            name={this.props.name}
            className={textareaClass}
            onChange={this.handleInput}
            onBlur={this.onBlur}></textarea>
        <label htmlFor={this.props.name} className={labelClassUpdated}>{this.props.labelName}</label>
    </div>
</div>
