{% extends '@Site/layout.html.twig' %}

{% block title %} - Login {% endblock %}

{% block content %}
    <div class="row">
        {% include '@Site/breadcrumbs.html.twig' with breadcrumbs %}
        <div class="col s6">
            <h2 class="right-align thin_font">
                Sign In
            </h2>
        </div>
    </div>
    <div class="row">
        <div class="row">
            <div class="col s12">
                {% if error %}
                    <div class="row center error_color" id="error_login_alert">
                        <div class="col s12 color_text_white">
                            <div class="col s1">
                                <p class="center-align valign-wrapper">
                                    <img src="{{ asset('/bundles/site/images/website/delete.png') }}" width="70" height="70" />
                                </p>
                            </div>
                            <div class="col s11">
                                <p class="right">
                                    <a href="#" class="colors_links" id="closeError">
                                        <i class="material-icons small">cancel</i>
                                    </a>
                                </p>
                                <p class="text_message_templates left-align valign-wrapper">
                                    {% set customMessage = '' %}
                                    {% if error.messageKey == 'Invalid credentials.' %}
                                        {% set customMessage = 'Invalid Username / Password' %}
                                    {% endif %}

                                    Error! {{ customMessage }}
                                </p>
                            </div>
                        </div>
                    </div>
                {% endif %}
            </div>
        </div>
        <form action="{{ path('login_check') }}" method="post" class="col s12">
            <div class="row thin_font">
                <div class="input-field col s6 offset-s3">
                    <i class="material-icons prefix">account_box</i>
                    <input type="text" class="validate" placeholder="Username" required="" id="email" name="_username">
                    <label for="email">Username</label>
                </div>
            </div>
            <div class="row thin_font">
                <div class="input-field col s6 offset-s3">
                    <i class="material-icons prefix">lock</i>
                    <input type="password" class="validate" required="" placeholder="Password" id="password" name="_password">
                    <label for="password">Password</label>
                </div>
            </div>
            <div class="row center">
                <div class="col s4 offset-s2">
                    <a href="{{ path('sendEmail') }}" class="thin_font">
                        Forgot your password?
                    </a>
                </div>
            </div>
            <div class="row center">
                <div class="col s12">
                    <button class="btn waves-effect waves-light thin_font" type="submit">
                        Login
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </div>
            <div class="clear: both">&nbsp;</div>
        </form>
    </div>
{% endblock %}

{% block javascript %}
<script src="{{ asset('bundles/site/js/custom/customLibs/loginValidations.js') }}"></script>
{% endblock %}
