<div>
    <div className={parameters}>
        <div className={dynamicClass}>
            <input type="checkbox"
                   id={this.props.name}
                   {...propsInput}
                   name={this.props.name}
                   className="validate"
                   checked={this.state.value}
                   onChange={this.toggleChange} />
            <label htmlFor={this.props.name}>
                <a href="#globalModal" className="modal-trigger thin_font" onClick={(evt) => this.openModal(evt, message)}>
                    {this.props.labelName}
                </a>
            </label>
        </div>
    </div>
</div>
