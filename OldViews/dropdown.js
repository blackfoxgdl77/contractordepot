<div className={parameters}>
    <div className={dynamicClass}>
        <i className="material-icons prefix">{this.props.iconTags}</i>
        <select name={this.props.name}
                value={this.props.value}
                {...this.propsInput}
                className="validate thin_font"
                id={this.props.id}
                onChange={this.handleInput} >
            {optionsValues}
        </select>
        <label htmlFor={this.props.name} className="thin_font">{this.props.labelSelectName}</label>
    </div>
</div>

<select className="ui fluid dropdown selection"
        name={this.props.name}
        id={this.props.id}
        value={this.props.value}
        className=""
        onChange={this.handleInput}>
    {optionsValues}
</select>

<Select name="section" disable="false" labelSelectName="Section" iconTags="dashboard" require="true"
        handleFormFields={this.handleInput} defaultText="false"
        classData="input-field col s8 offset-s2"
        displayDataValues={categories} id="sectionID"
        ref={(section) => { this.section = section }} />


        <Select name="options" labelSelectName="Select an Option" iconTags="format_list_numbered" require="false"
                disable="false" id="optionsId" handleFormFields={this.handleInput} defaultText="false"
                displayDataValues={options} classData="input-field col s8 offset-s2"
                ref={(options) => { this.options = options }} />



                <Select name="status" labelSelectName="Status" iconTags="iso" require="true" disable="false"
                        id="statusId" handleFormFields={this.handleInput} defaultText="Status"
                        displayDataValues={values} classData=""
                        ref={(status) => { this.status = status }} />
