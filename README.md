contractordepot
===============

A Symfony project created on January 11, 2018, 4:26 am.

====================
FORMS BEFORE USER'S LOGIN

 * Should be done with forms native in Symfony3 to validate data
 * To be sure, always be done with request directly to the server
 * Validations should be done on the server and if there are any error, return the error and displaying on it
 * Verify with Memo what should the order of the fields and colors

FORMS AFTER USER'S LOGIN

 * Adding ReactJS libraries
 * Adding Axio library to do ajax request from react modules
 * Define de forms will be displayed using reactjs
 * Validate the fields from javascript and also from PHP
 * Verify with Memo the orders
 * Make a library using AXIO in another javascript file to just import one file and one function
 * Make just for login and register controllers to catch values from website instead of call to API, from
   these methods will be called with $this->forward() method and once to start session we can call
   directly the API

--------===========================--------
1.- Crear edit Profile del usuario
2.- Crear mene y mostrarlo si esta logueado el usuario
3.- Agregar Form para la parte de publicar post de trabajo
4.- En el form de posts, agregar la opcion de videos / imagenes en los forms
5.- Agregar form de contacto
6.- Agregar form de recuperar contraseña
7.- Crear las funciones de API's
8.- Generar formularios de login y registro de Apps (IOS y Android)
9.- Agregar los diseños al layout para vestir la pagina
10.- Agregar el reloj de retroceso
11.- Verificar si hay un flag para activar la publicacion hecha
12.- Generar la lista de publicaciones
13.- Filtro de publicaciones dependiendo la ciudad
14.- Verificar las relaciones en las entidades (get y set)
15.- terminar funciones bases para cuando el usuario esta logueado
16.- agregar los tokens checking para _csrf
17.- Agregar la libreria axios para los ajax request de reactJS
18.- Agregar los componentes para reutilizarlos en los forms
19.- Agregar los archivos para los setState y los request al submit
20.- Crear configuraciones del server mail
21.- Agregar templates de los mails
22.- Agregar la seccion de comentarios entre contratista y el trabajador
23.- Hacer el guardado de ofertas e ir generando la de ultima hecha y quesea mejor
24.- Agregar la API para BBB.org
25.- Agregar la API para los metodos de pago
